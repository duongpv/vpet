package com.lthien.card;

public abstract interface CardVerifier {
  public abstract CardResponse verify(String paramString1, String paramString2, CardType paramCardType)
    throws CardVerifierException;
  
  public abstract String getCardType(CardType paramCardType)
    throws CardVerifierException;
}
