package com.lthien.card;

public enum CardType {
  VINA_PHONE,  MOBI_FONE,  VTC_VCOIN,  FPT_GATE,  VIETTEL,  MEGA_CARD,  ONCASH;
  private CardType() {}
}
