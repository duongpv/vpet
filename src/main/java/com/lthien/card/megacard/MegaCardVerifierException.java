package com.lthien.card.megacard;

import com.lthien.card.CardVerifierException;


public class MegaCardVerifierException extends CardVerifierException {
    public MegaCardVerifierException(String message) {
        super(message);
    }

    public MegaCardVerifierException() {
    }

    public MegaCardVerifierException(String message, Throwable cause) {
        super(message, cause);
    }

    public MegaCardVerifierException(Throwable cause) {
        super(cause);
    }

    public MegaCardVerifierException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
