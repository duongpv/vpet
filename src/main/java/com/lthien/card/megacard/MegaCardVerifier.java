package com.lthien.card.megacard;

import com.lthien.card.CardResponse;
import com.lthien.card.CardType;
import com.lthien.card.CardVerifier;
import com.lthien.card.CardVerifierException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class MegaCardVerifier implements CardVerifier {

    Logger logger = Logger.getLogger(MegaCardVerifier.class);

    private static final String URL = "http://gachthe.megapay.net.vn/chargingApi?method=verifyCard&partnerId=%partnerId%&cardSerial=%cardSerial%&cardPin=%cardPin%&transId=%transId%&telcoCode=%telcoCode%&targetAcc=%targetAcc%&password=%password%&signature=%signature%";
    private final MessageDigest md;
    private String partnerId;
    private String targetAcc;
    private String password;
    private OkHttpClient client = new OkHttpClient();

    public MegaCardVerifier() {
        try {
            this.md = MessageDigest.getInstance("MD5");
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    public MegaCardVerifier(String partnerId, String targetAcc, String password) {
        this();

        this.partnerId = partnerId;
        this.targetAcc = targetAcc;
        this.password = password;
    }

    public CardResponse verify(String cardSerial, String cardPin, CardType cardType) throws CardVerifierException {
        try {
            String telcoCode = getCardType(cardType);
            String requestUrl = createRequestUrl(cardSerial, cardPin, telcoCode);

            String response = sendRequest(requestUrl);

            MegaCardResponse megaCardResponse = parseResponse(response);

            if (megaCardResponse.getStatus() != 0) {
                String error = "TransId: " + megaCardResponse.getTransId();
                error = error + " - Status: " + megaCardResponse.getStatus();
                error = error + " - Message: " + megaCardResponse.getMessage();

                throw new CardVerifierException(error, megaCardResponse);
            }

            return megaCardResponse;
        } catch (IOException ex) {
            throw new CardVerifierException(ex);
        } catch (URISyntaxException ex) {
            throw new CardVerifierException(ex);
        }
    }

    private MegaCardResponse parseResponse(String response) throws URISyntaxException {
        Map<String, String> map = parseParams(response);

        MegaCardResponse megaCardResponse = new MegaCardResponse((String) map.get("status"), (String) map.get("message"), (String) map.get("realAmount"), (String) map.get("transId"));

        return megaCardResponse;
    }

    private Map<String, String> parseParams(String text) {
        Map<String, String> map = new HashMap();

        String[] params = text.split("&");

        for (String param : params) {
            String[] temp = param.split("=");

            map.put(temp[0], temp[1]);
        }

        return map;
    }

    private String sendRequest(String requestUrl) throws IOException {
        Request request = new Request.Builder().url(requestUrl).build();


        Response response = this.client.newCall(request).execute();

        String body = response.body().string();
        logger.warn("response: " + body);
        return body;
    }

    private String createRequestUrl(String cardSerial, String cardPin, String telcoCode) {
        return createRequestUrl(this.partnerId, cardSerial, cardPin, telcoCode, this.targetAcc, this.password);
    }

    private String createRequestUrl(String partnerId, String cardSerial, String cardPin, String telcoCode, String targetAcc, String password) {
        String transId = createTransId(partnerId);
        String signature = createSignature(partnerId, cardSerial, cardPin, transId, telcoCode, password);

        String url = "http://gachthe.megapay.net.vn/chargingApi?method=verifyCard&partnerId=%partnerId%&cardSerial=%cardSerial%&cardPin=%cardPin%&transId=%transId%&telcoCode=%telcoCode%&targetAcc=%targetAcc%&password=%password%&signature=%signature%".replace("%partnerId%", partnerId).replace("%cardSerial%", cardSerial).replace("%cardPin%", cardPin).replace("%transId%", transId).replace("%telcoCode%", telcoCode).replace("%targetAcc%", targetAcc).replace("%password%", DigestUtils.md5Hex(password)).replace("%signature%", signature);

        return url;
    }

    private String createTransId(String partnerID) {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyMMddHHmmss");

        return partnerID + "_" + df.format(date) + "_" + randomNumber();
    }

    private static int randInt(int min, int max) {
        Random rand = new Random();

        int randomNum = rand.nextInt(max - min + 1) + min;

        return randomNum;
    }

    private String randomNumber() {
        return "" + randInt(0, 999);
    }

    private String createSignature(String partnerId, String cardSerial, String cardPin, String transId, String telcoCode, String password) {
        String signatureString = partnerId;
        signatureString = signatureString + "&" + cardSerial;
        signatureString = signatureString + "&" + cardPin;
        signatureString = signatureString + "&" + transId;
        signatureString = signatureString + "&" + telcoCode;
        signatureString = signatureString + "&" + DigestUtils.md5Hex(password);

        return DigestUtils.md5Hex(signatureString);
    }

    public String getCardType(CardType cardType) throws CardVerifierException {
        switch (cardType) {
            case VINA_PHONE:
                return "VNP";
            case MOBI_FONE:
                return "VMS";
            case VTC_VCOIN:
                return "VTC";
            case FPT_GATE:
                return "FPT";
            case VIETTEL:
                return "VTT";
            case MEGA_CARD:
                return "MGC";
            case ONCASH:
                return "ONC";
        }

        throw new CardVerifierException("Card type invalid");
    }
}
