package com.lthien.card.megacard;

import com.lthien.card.CardResponse;


public class MegaCardResponse extends CardResponse {
    int status;
    String message;

    public MegaCardResponse() {
    }

    public MegaCardResponse(String status, String message, String realAmount, String transId) {
        this.status = Integer.parseInt(status);
        this.message = message;
        this.realAmount = Integer.parseInt(realAmount);
        this.transId = transId;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
