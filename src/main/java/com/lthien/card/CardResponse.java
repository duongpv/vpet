package com.lthien.card;


public class CardResponse {
    protected String transId;

    protected int realAmount;

    public String getTransId() {
        return this.transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public int getRealAmount() {
        return this.realAmount;
    }

    public void setRealAmount(int realAmount) {
        this.realAmount = realAmount;
    }
}
