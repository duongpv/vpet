package com.lthien.card;


public class CardVerifierException extends Exception {
    protected CardResponse cardResponse;

    public CardVerifierException(String message, CardResponse cardResponse) {
        super(message);
        this.cardResponse = cardResponse;
    }

    public CardVerifierException(String message) {
        super(message);
    }

    public CardVerifierException() {
    }

    public CardVerifierException(String message, Throwable cause) {
        super(message, cause);
    }

    public CardVerifierException(Throwable cause) {
        super(cause);
    }

    public CardVerifierException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public CardResponse getCardResponse() {
        return this.cardResponse;
    }

    public void setCardResponse(CardResponse cardResponse) {
        this.cardResponse = cardResponse;
    }
}