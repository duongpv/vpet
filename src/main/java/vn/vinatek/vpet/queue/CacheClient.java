package vn.vinatek.vpet.queue;

/**
 * Created by duongpv on 5/17/16.
 */
public interface CacheClient {

    void add(String key, Object data);

    void add(String key, int exp, Object data);

    Object get(String key);

    void push(String hash, String key, Object data);

    <T> Object get(String hash, String key, Class clazz);

    void delete(String hash, String key);
}
