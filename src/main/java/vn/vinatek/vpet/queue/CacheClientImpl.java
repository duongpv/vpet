package vn.vinatek.vpet.queue;

import com.google.gson.Gson;
import net.spy.memcached.AddrUtil;
import net.spy.memcached.ConnectionFactoryBuilder;
import net.spy.memcached.MemcachedClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by duongpv on 5/17/16.
 */
public class CacheClientImpl implements CacheClient {

    private Logger logger = Logger.getLogger(CacheClientImpl.class);

    private MemcachedClient client;

    @Autowired
    private Gson gson;

    public CacheClientImpl(String host) {
        try {
            client = new MemcachedClient(new ConnectionFactoryBuilder().setDaemon(true).build(), AddrUtil.getAddresses(host));
            logger.info("[Memcached] init memcached");
        }
        catch (Exception e) {
            logger.error("[Memcached] error " + e.getMessage());
        }
    }

    @Override
    public void add(String key, Object data) {
        add(key, 3600, data);
    }

    @Override
    public void add(String key, int exp, Object data) {
        try {
            client.set(key.replace(" ", "_"), exp, data);
        } catch (Exception e) {
            logger.error("[Memcached] add error" + e.getMessage());
        }
    }

    @Override
    public Object get(String key) {
        try {
            return client.get(key.replace(" ", "_"));
        }
        catch (Exception e) {
            logger.error("[Memcached] get error " + e.getMessage());
        }

        return null;
    }

    @Override
    public void push(String hash, String key, Object data) {
        try {
            client.set(String.format("%s_%s", hash, key).replace(" ", "_"), 3600, gson.toJson(data));
        } catch (Exception e) {
            logger.error("[CacheClient] push " + e.getMessage());
        }
    }

    @Override
    public <T> Object get(String hash, String key, Class clazz) {
        String val = (String) client.get(String.format("%s_%s", hash, key).replace(" ", "_"));
        if (val != null) {
            try {
                return gson.fromJson(val, clazz);
            } catch (Exception e) {
                logger.error("[CacheClient] get " + e.getMessage());
            }
        }

        return null;
    }

    @Override
    public void delete(String hash, String key) {
        try {
            client.delete(String.format("%s_%s", hash, key).replace(" ", "_"));
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
