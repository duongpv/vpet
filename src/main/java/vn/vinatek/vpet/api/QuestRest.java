package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestErrorFactory;
import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.*;
import vn.vinatek.vpet.database.pojo.*;
import vn.vinatek.vpet.util.RandomHelper;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("/quest")
public class QuestRest extends BaseRest {

    @GET
    @Path("/current")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response getCurrentQuest() {
        QuestDAO questDAO = (QuestDAO) getBean(QuestDAO.class);
        int id = getCharacterId();

        Quest quest = questDAO.get(id);
        if (quest == null) {
            return Response.status(200).entity(new RestSuccess(null)).build();
        }

        return Response.status(200).entity(new RestSuccess(quest.getQuestInfo())).build();
    }

    @POST
    @Path("/hero/{questMode}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postHeroQuestAccept(@PathParam("questMode") int questMode) {
        if ((questMode < 1) && (questMode > 3)) {
            return Response.status(400).entity(RestErrorFactory.QUEST_TYPE_NOT_EXIST).build();
        }

        QuestDAO questDAO = (QuestDAO) getBean(QuestDAO.class);
        if (questDAO.exist(getCharacterId())) {
            return Response.status(400).entity(RestErrorFactory.QUEST_EXIST).build();
        }

        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.getHeroQuestItem(getCharacterId(), questMode);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.QUEST_ITEM_NOT_EXIST).build();
        }

        characterItemDAO.delete(characterItem);

        QuestInfo questInfo = createHeroQuestInfo(questMode);
        Quest quest = new Quest(getCharacterId(), questInfo);

        questDAO.save(quest);
        return Response.status(200).entity(new RestSuccess(questInfo)).build();
    }

    @POST
    @Path("/summon/{questMode}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postSummonQuestAccept(@PathParam("questMode") int questMode) {
        if ((questMode < 1) && (questMode > 3)) {
            return Response.status(400).entity(RestErrorFactory.QUEST_TYPE_NOT_EXIST).build();
        }

        QuestDAO questDAO = (QuestDAO) getBean(QuestDAO.class);
        if (questDAO.exist(getCharacterId())) {
            return Response.status(400).entity(RestErrorFactory.QUEST_EXIST).build();
        }

        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.getSummonQuestItem(getCharacterId(), questMode);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.QUEST_ITEM_NOT_EXIST).build();
        }

        QuestInfo questInfo = createSummonQuestInfo(questMode);
        if (questInfo == null) {
            return Response.status(400).entity(RestErrorFactory.QUEST_TYPE_NOT_EXIST).build();
        }

        Quest quest = new Quest(getCharacterId(), questInfo);
        questDAO.save(quest);

        characterItemDAO.delete(characterItem);

        return Response.status(200).entity(new RestSuccess(questInfo)).build();
    }

    @POST
    @Path("/item/{questMode}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postItemQuestAccept(@PathParam("questMode") int questMode) {
        if ((questMode < 1) && (questMode > 3)) {
            return Response.status(400).entity(RestErrorFactory.QUEST_TYPE_NOT_EXIST).build();
        }

        QuestDAO questDAO = (QuestDAO) getBean(QuestDAO.class);
        if (questDAO.exist(getCharacterId())) {
            return Response.status(400).entity(RestErrorFactory.QUEST_EXIST).build();
        }

        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.getItemQuestItem(getCharacterId(), questMode);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.QUEST_ITEM_NOT_EXIST).build();
        }

        QuestInfo questInfo = createItemQuestInfo(questMode);
        if (questInfo == null) {
            return Response.status(400).entity(RestErrorFactory.QUEST_TYPE_NOT_EXIST).build();
        }

        Quest quest = new Quest(getCharacterId(), questInfo);
        questDAO.save(quest);

        characterItemDAO.delete(characterItem);

        return Response.status(200).entity(new RestSuccess(questInfo)).build();
    }

    @POST
    @Path("/remove")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postRemoveQuest() {
        QuestDAO questDAO = (QuestDAO) getBean(QuestDAO.class);
        questDAO.delete(getCharacterId());

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/hero/complete")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postHeroQuestComplete() {
        QuestDAO questDAO = (QuestDAO) getBean(QuestDAO.class);
        Quest quest = questDAO.get(getCharacterId());
        if (quest == null) {
            return Response.status(400).entity(RestErrorFactory.QUEST_NOT_EXIST).build();
        }

        QuestInfo questInfo = quest.getQuestInfo();
        if (questInfo.getQuestType() != 3) {
            return Response.status(400).entity(RestErrorFactory.QUEST_NOT_EXIST).build();
        }

        QuestRequire questRequire = questInfo.getQuestRequires().get(0);
        if (questRequire.getHave() < questRequire.getRequire()) {
            return Response.status(400).entity(RestErrorFactory.QUEST_NOT_COMPLETE).build();
        }

        List<Item> rewards;
        switch (questInfo.getQuestMode()) {
            case 1:
                rewards = heroEasyReward();
                break;
            case 2:
                rewards = heroNormalReward();
                break;
            default:
                rewards = heroHardReward();
        }

        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        characters.addExperiences(questInfo.getQuestMode());
        characterDAO().update(characters);

        characterDAO().addCharacterItems(getCharacterId(), rewards);
        for (Item item : rewards) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        questDAO.delete(quest);

        return Response.status(200).entity(new RestSuccess(rewards)).build();
    }

    @POST
    @Path("/summon/complete")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postSummonQuestComplete() {
        QuestDAO questDAO = (QuestDAO) getBean(QuestDAO.class);
        Quest quest = questDAO.get(getCharacterId());
        if (quest == null) {
            return Response.status(400).entity(RestErrorFactory.QUEST_NOT_EXIST).build();
        }

        QuestInfo questInfo = quest.getQuestInfo();
        if (questInfo.getQuestType() != 1) {
            return Response.status(400).entity(RestErrorFactory.QUEST_NOT_EXIST).build();
        }

        if (questInfo.getTimeEnd().getTime() < new Date().getTime()) {
            return Response.status(400).entity(RestErrorFactory.QUEST_TIME_OUT).build();
        }

        long goldCost = 0L;
        switch (questInfo.getQuestMode()) {
            case 1:
                goldCost = 10000L;
                break;
            case 2:
                goldCost = 50000L;
                break;
            default:
                goldCost = 100000L;
        }

        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        if (characters.getGold() < goldCost) {
            return Response.status(400).entity(RestErrorFactory.BANK_GOLD_NOT_ENOUGH).build();
        }

        CharacterSummonDAO characterSummonDAO = (CharacterSummonDAO) getBean(CharacterSummonDAO.class);

        List<QuestRequire> questRequires = questInfo.getQuestRequires();
        boolean isHaveAll = true;

        List<CharacterSummon> characterSummons = new ArrayList();
        for (QuestRequire questRequire : questRequires) {
            CharacterSummon characterSummon = characterSummonDAO.getSummonQuest(getCharacterId(), questRequire.getId());
            if (characterSummon == null) {
                isHaveAll = false;
                break;
            }

            characterSummons.add(characterSummon);
        }

        if (!isHaveAll) {
            return Response.status(400).entity(RestErrorFactory.QUEST_NOT_COMPLETE).build();
        }

        characters.subGold(goldCost);
        characters.addExperiences(questInfo.getQuestMode());
        characterDAO().update(characters);

        for (CharacterSummon characterSummon : characterSummons) {
            characterSummonDAO.delete(characterSummon);
        }

        List<Summon> rewards;
        switch (questInfo.getQuestMode()) {
            case 1:
                rewards = summonEasyReward(questRequires);
                break;
            case 2:
                rewards = summonNormalReward(questRequires);
                break;
            default:
                rewards = summonHardReward(questRequires);
        }

        for (Summon reward : rewards) {
            characterDAO().addSummon(getUserName(), reward.getId(), "Quest");
            reward.setAreas(new ArrayList<Area>());
        }

        questDAO.delete(quest);

        return Response.status(200).entity(new RestSuccess(rewards)).build();
    }

    @POST
    @Path("/item/complete")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postItemQuestComplete() {
        QuestDAO questDAO = (QuestDAO) getBean(QuestDAO.class);
        Quest quest = questDAO.get(getCharacterId());
        if (quest == null) {
            return Response.status(400).entity(RestErrorFactory.QUEST_NOT_EXIST).build();
        }

        QuestInfo questInfo = quest.getQuestInfo();
        if (questInfo.getQuestType() != 2) {
            return Response.status(400).entity(RestErrorFactory.QUEST_NOT_EXIST).build();
        }

        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);

        List<QuestRequire> questRequires = questInfo.getQuestRequires();
        boolean isHaveAll = true;

        List<CharacterItem> characterItems = new ArrayList();
        for (QuestRequire questRequire : questRequires) {
            CharacterItem characterItem = characterItemDAO.getItemQuest(getCharacterId(), questRequire.getId());
            if (characterItem == null) {
                isHaveAll = false;
                break;
            }

            characterItems.add(characterItem);
        }

        if (!isHaveAll) {
            return Response.status(400).entity(RestErrorFactory.QUEST_NOT_COMPLETE).build();
        }

        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        characters.addExperiences(questInfo.getQuestMode());
        characterDAO().update(characters);

        for (CharacterItem characterItem : characterItems) {
            characterItemDAO.delete(characterItem);
        }

        List<Item> rewards = null;
        switch (questInfo.getQuestMode()) {
            case 1:
                rewards = itemEasyReward();
                break;
            case 2:
                rewards = itemNormalReward();
                break;
            default:
                rewards = itemHardReward();
        }

        characterDAO().addCharacterItems(getCharacterId(), rewards);
        for (Item item : rewards) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        questDAO.delete(quest);

        return Response.status(200).entity(new RestSuccess(rewards)).build();
    }

    private QuestInfo createItemQuestInfo(int questMode) {
        int begin = 1;
        int end = 4;
        switch (questMode) {
            case 1:
                begin = 1;
                end = 4;
                break;
            case 2:
                begin = 4;
                end = 7;
                break;
            default:
                begin = 8;
                end = 10;
        }

        int number = 2 + questMode;

        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getItemByMagic(begin, end);

        if (items.size() < number) {
            return null;
        }

        List<QuestRequire> questRequires = createItemQuestRequire(items, number);

        Date endDate = new Date(100000000000L + System.currentTimeMillis());
        QuestInfo questInfo = createQuestInfo(2, questMode, endDate);
        questInfo.setQuestRequires(questRequires);

        return questInfo;
    }

    private List<QuestRequire> createItemQuestRequire(List<Item> items, int number) {
        List<QuestRequire> questRequires = new ArrayList();
        for (int i = 0; i < number; i++) {
            int idx = RandomHelper.randInt(0, items.size() - 1);

            Item item = items.get(idx);
            items.remove(item);

            QuestRequire questRequire = new QuestRequire();
            questRequire.setId(item.getId());
            questRequire.setRequire(1);

            questRequires.add(questRequire);
        }

        return questRequires;
    }

    private List<Item> itemEasyReward() {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getItemByMagic(4, 6);
        List<Item> rewards = new ArrayList();

        for (int i = 0; i < 2; i++) {
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        int rate = RandomHelper.randInt(0, 99);
        if (rate <= 10) {
            items = itemDAO.getIncreaseItemByMagic(2, 4);
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        } else if (rate <= 30) {
            items = itemDAO.getPetItemByMagic(1, 4);
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        for (Item reward : rewards) {
            reward.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return rewards;
    }

    private List<Item> itemNormalReward() {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getItemByMagic(7, 10);
        List<Item> rewards = new ArrayList();

        for (int i = 0; i < 2; i++) {
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        int rate = RandomHelper.randInt(0, 99);
        if (rate <= 10) {
            items = itemDAO.getIncreaseItemByMagic(4, 6);
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        } else if (rate <= 30) {
            items = itemDAO.getPetItemByMagic(3, 6);
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        for (Item reward : rewards) {
            reward.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return rewards;
    }

    private List<Item> itemHardReward() {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getAllPetItem();
        items.addAll(itemDAO.getIncreaseItemByMagic(6, 7));
        List<Item> rewards = new ArrayList();

        for (int i = 0; i < 3; i++) {
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        int rate = RandomHelper.randInt(0, 99);
        if (rate <= 2) {
            items = itemDAO.getAllEvolutionItem();
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        for (Item reward : rewards) {
            reward.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return rewards;
    }

    private List<Item> heroEasyReward() {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getPetItemByMagic(3, 5);
        List<Item> rewards = new ArrayList();

        for (int i = 0; i < 2; i++) {
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        int rate = RandomHelper.randInt(0, 99);
        if (rate <= 20) {
            items = itemDAO.getIncreaseItemByMagic(1, 2);
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        for (Item reward : rewards) {
            reward.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return rewards;
    }

    private List<Item> heroNormalReward() {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getPetItemByMagic(5, 7);
        List<Item> rewards = new ArrayList();

        for (int i = 0; i < 2; i++) {
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        int rate = RandomHelper.randInt(0, 99);
        if (rate <= 15) {
            items = itemDAO.getIncreaseItemByMagic(3, 5);
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        for (Item reward : rewards) {
            reward.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return rewards;
    }

    private List<Item> heroHardReward() {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getAllPetItem();
        List<Item> rewards = new ArrayList();

        for (int i = 0; i < 4; i++) {
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        int rate = RandomHelper.randInt(0, 99);
        if (rate <= 10) {
            items = itemDAO.getIncreaseItemByMagic(7, 10);
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }
        if (rate <= 1) {
            items = itemDAO.getAllEvolutionItem();
            int idx = RandomHelper.randInt(0, items.size() - 1);
            rewards.add(items.get(idx));
        }

        for (Item reward : rewards) {
            reward.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return rewards;
    }

    private List<Summon> summonEasyReward(List<QuestRequire> questRequires) {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        List<Summon> rewards = new ArrayList();

        List<Summon> summons = summonDAO.getPointSummonByMagic(15, 20);
        int idx = RandomHelper.randInt(0, summons.size() - 1);
        rewards.add(summons.get(idx));

        int rate = RandomHelper.randInt(0, 99);
        if (rate <= 20) {
            idx = RandomHelper.randInt(0, questRequires.size() - 1);
            rewards.add(summonDAO.get((questRequires.get(idx)).getId()));
        }

        return rewards;
    }

    private List<Summon> summonNormalReward(List<QuestRequire> questRequires) {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        List<Summon> rewards = new ArrayList();

        List<Summon> summons = summonDAO.getPointSummonByMagic(20, 25);
        int idx = RandomHelper.randInt(0, summons.size() - 1);
        rewards.add(summons.get(idx));

        int rate = RandomHelper.randInt(0, 99);
        if (rate <= 20) {
            idx = RandomHelper.randInt(0, questRequires.size() - 1);
            rewards.add(summonDAO.get((questRequires.get(idx)).getId()));
        }

        return rewards;
    }

    private List<Summon> summonHardReward(List<QuestRequire> questRequires) {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        List<Summon> rewards = new ArrayList();

        List<Summon> summons = summonDAO.getPointSummonByMagic(30, 35);
        int idx = RandomHelper.randInt(0, summons.size() - 1);
        rewards.add(summons.get(idx));

        int rate = RandomHelper.randInt(0, 99);
        if (rate <= 1) {
            rewards.add(summonDAO.getPointSummonByMagic(40, 99).get(0));
        } else if (rate <= 10) {
            rewards.add(summonDAO.getPointSummonByMagic(30, 30).get(0));
        }

        return rewards;
    }

    private QuestInfo createHeroQuestInfo(int questMode) {
        MonsterDAO monsterDAO = (MonsterDAO) getBean(MonsterDAO.class);
        List<Monster> monsters = monsterDAO.getQuestMonster();
        Monster monster = monsters.get(RandomHelper.randInt(0, monsters.size() - 1));

        QuestRequire questRequire = new QuestRequire();
        questRequire.setRequire(questMode * 100);
        questRequire.setId(monster.getId());

        Date end = new Date(100000000000L + System.currentTimeMillis());

        QuestInfo questInfo = createQuestInfo(3, questMode, end);
        questInfo.getQuestRequires().add(questRequire);

        return questInfo;
    }

    private QuestInfo createQuestInfo(int questType, int questMode, Date end) {
        QuestInfo questInfo = new QuestInfo();
        questInfo.setQuestMode(questMode);
        questInfo.setQuestType(questType);
        questInfo.setTimeStart(new Date());
        questInfo.setTimeEnd(end);

        return questInfo;
    }

    private QuestInfo createSummonQuestInfo(int questMode) {
        int begin = 10;
        int end = 15;
        long endTime = 0L;
        switch (questMode) {
            case 1:
                begin = 10;
                end = 15;
                endTime = 1800000L;
                break;
            case 2:
                begin = 15;
                end = 20;
                endTime = 5400000L;
                break;
            default:
                begin = 25;
                end = 25;
                endTime = 100000000000L;
        }

        int number = 2 + questMode;

        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        List<Summon> summons = summonDAO.getPointSummonByMagic(begin, end);

        if (summons.size() < number) {
            return null;
        }

        List<QuestRequire> questRequires = createSummonQuestRequire(summons, number);

        Date endDate = new Date(endTime + System.currentTimeMillis());

        QuestInfo questInfo = createQuestInfo(1, questMode, endDate);
        questInfo.setQuestRequires(questRequires);

        return questInfo;
    }

    private List<QuestRequire> createSummonQuestRequire(List<Summon> summons, int number) {
        List<QuestRequire> questRequires = new ArrayList();
        for (int i = 0; i < number; i++) {
            int idx = RandomHelper.randInt(0, summons.size() - 1);

            Summon summon = summons.get(idx);
            summons.remove(summon);

            QuestRequire questRequire = new QuestRequire();
            questRequire.setId(summon.getId());
            questRequire.setRequire(1);

            questRequires.add(questRequire);
        }

        return questRequires;
    }
}