package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestErrorFactory;
import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.CharacterDAO;
import vn.vinatek.vpet.database.dao.ItemDAO;
import vn.vinatek.vpet.database.dao.SummonDAO;
import vn.vinatek.vpet.database.pojo.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Path("/shop")
public class ShopRest extends BaseRest {

    @GET
    @Path("/weapon")
    @Produces({"application/json;charset=utf-8"})
    public Response getWeapon() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getShopWeapon();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/medical")
    @Produces({"application/json;charset=utf-8"})
    public Response getMedical() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getShopMedical();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/premium")
    @Produces({"application/json;charset=utf-8"})
    public Response getPremiumShop() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getPremiumShop();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }
        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/grocery")
    @Produces({"application/json;charset=utf-8"})
    public Response getGroceryShop() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getGroceryShop();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }
        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/golden")
    @Produces({"application/json;charset=utf-8"})
    public Response getGoldenShop() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getGoldenShop();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }
        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/special/summon")
    @Produces({"application/json;charset=utf-8"})
    public Response getSpecialSummon() throws IOException {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        List<Summon> summonShop = summonDAO.getSummonShop();
        for (Summon summon : summonShop) {
            summon.setAreas(new ArrayList<Area>());
        }

        return Response.status(200).entity(new RestSuccess(summonShop)).build();
    }

    @GET
    @Path("/special/weapon")
    @Produces({"application/json;charset=utf-8"})
    public Response getSpecialWeapon() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getSpecialShop();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }
        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @POST
    @Path("/buy/item/{itemId}")
    @Produces({"application/json;charset=utf-8"})
    public Response postBuyItem(@PathParam("itemId") int itemId) throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        Item item = itemDAO.get(itemId);
        if (item == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }
        if (!item.isSell()) {
            return Response.status(400).entity(RestErrorFactory.ITEM_CANT_BUY).build();
        }

        CharacterDAO characterDAO = (CharacterDAO) getBean(CharacterDAO.class);
        Characters characters = characterDAO().getCharacterByUserName(getUserName());

        long goldNeed = item.getGoldCostBuy();
        if (characters.getGold() < goldNeed) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_GOLD_NOT_ENOUGH).build();
        }

        long crystalNeed = item.getCrystalCostBuy();
        if (characters.getCrystal() < crystalNeed) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_CRYSTAL_NOT_ENOUGH).build();
        }

        long bcoinNeed = item.getBcoinCostBuy();
        if (characters.getBcoin() < bcoinNeed) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_BCOIN_NOT_ENOUGH).build();
        }

        characters.setGold(characters.getGold() - goldNeed);
        characters.setCrystal(characters.getCrystal() - crystalNeed);
        characters.setBcoin(characters.getBcoin() - bcoinNeed);

        characterDAO.addCharacterItem(getUserName(), item);
        characterDAO.update(characters);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/buy/summon/{summonId}")
    @Produces({"application/json;charset=utf-8"})
    public Response postBuySummon(@PathParam("summonId") int summonId) throws IOException {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        Summon summon = summonDAO.get(summonId);
        if (summon == null) {
            return Response.status(400).entity(RestErrorFactory.SUMMON_NOT_EXIST).build();
        }

        if (!summon.isSell()) {
            return Response.status(400).entity(RestErrorFactory.ITEM_CANT_BUY).build();
        }

        CharacterDAO characterDAO = (CharacterDAO) getBean(CharacterDAO.class);
        Characters characters = characterDAO().getCharacterByUserName(getUserName());

        long goldNeed = summon.getGoldCostBuy();
        if (characters.getGold() < goldNeed) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_GOLD_NOT_ENOUGH).build();
        }

        long crystalNeed = summon.getCrystalCostBuy();
        if (characters.getCrystal() < crystalNeed) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_CRYSTAL_NOT_ENOUGH).build();
        }

        long bcoinNeed = summon.getBcoinCostBuy();
        if (characters.getBcoin() < bcoinNeed) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_BCOIN_NOT_ENOUGH).build();
        }

        characters.setGold(characters.getGold() - goldNeed);
        characters.setCrystal(characters.getCrystal() - crystalNeed);
        characters.setBcoin(characters.getBcoin() - bcoinNeed);

        characterDAO.addSummon(getUserName(), summon.getId(), "");
        characterDAO.update(characters);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }
}
