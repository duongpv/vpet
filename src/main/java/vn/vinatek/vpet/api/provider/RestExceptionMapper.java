package vn.vinatek.vpet.api.provider;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;


public class RestExceptionMapper implements ExceptionMapper<WebApplicationException> {
    public Response toResponse(WebApplicationException exception) {
        Response response;
        if ((exception instanceof WebApplicationException)) {
            WebApplicationException webEx = exception;
            response = webEx.getResponse();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Internal error").type("text/plain").build();
        }

        return response;
    }
}
