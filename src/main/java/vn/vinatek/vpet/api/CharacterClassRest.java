package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.CharacterClassDAO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/characterclass")
public class CharacterClassRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response infoCharacter() throws IOException {
        CharacterClassDAO characterClassDAO = (CharacterClassDAO) getBean(CharacterClassDAO.class);
        return Response.status(200).entity(new RestSuccess(characterClassDAO.getAll())).build();
    }
}
