package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.PetDAO;
import vn.vinatek.vpet.database.pojo.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Path("/pet")
public class PetRest extends BaseRest {

    @GET
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response get(@PathParam("id") int id) throws IOException {
        PetDAO petDAO = (PetDAO) getBean(PetDAO.class);
        Pet pet = petDAO.get(id);
        if (pet != null) {
            List<Area> areas = petDAO.getAreas(id);
            for (Area area : areas) {
                area.setAreaRewardItems(new ArrayList<AreaRewardItem>());
            }
            pet.setAreas(areas);
            pet.setEvolutions(petDAO.getEvolutions(id));
            pet.setPetSummons(new ArrayList<PetSummon>());
        }

        return Response.status(200).entity(new RestSuccess(pet)).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response delete(@PathParam("id") int id) throws IOException {
        PetDAO petDAO = (PetDAO) getBean(PetDAO.class);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(petDAO.delete(id)))).build();
    }

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response getAll() throws IOException {
        PetDAO petDAO = (PetDAO) getBean(PetDAO.class);
        List<Pet> pets = petDAO.getAll();
        for (Pet pet : pets) {
            pet.setAreas(new ArrayList<Area>());
            pet.setEvolutions(new ArrayList<Evolution>());
            pet.setPetSummons(new ArrayList<PetSummon>());
        }
        return Response.status(200).entity(new RestSuccess(pets)).build();
    }

    @POST
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response add(Pet samplePet) throws IOException {
        PetDAO petDAO = (PetDAO) getBean(PetDAO.class);
        petDAO.save(samplePet);

        return Response.status(200).entity(new RestSuccess(samplePet)).build();
    }

    @PUT
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response update(Pet samplePet) throws IOException {
        PetDAO petDAO = (PetDAO) getBean(PetDAO.class);
        Pet pet = petDAO.get(samplePet.getId());

//        copy(samplePet, samplePetExist);
        pet.setAppearRate(samplePet.getAppearRate());
        pet.setAreas(samplePet.getAreas());
        pet.setCatchedRate(samplePet.getCatchedRate());
        pet.setCatched(samplePet.isCatched());
        pet.setDefaultLevel(samplePet.getDefaultLevel());
        pet.setDefense(samplePet.getDefense());
        pet.setDescription(samplePet.getDescription());
        pet.setEvolutions(samplePet.getEvolutions());
        pet.setEvolved(samplePet.isEvolved());
        pet.setHatchTime(samplePet.getHatchTime());
        pet.setHp(samplePet.getHp());
        pet.setImageAvatarUrl(samplePet.getImageAvatarUrl());
        pet.setImageEggUrl(samplePet.getImageEggUrl());
        pet.setIntelligent(samplePet.getIntelligent());
        pet.setPetClass(samplePet.getPetClass());
        pet.setRequireLevel(samplePet.getRequireLevel());
        pet.setMp(samplePet.getMp());
        pet.setName(samplePet.getName());
        pet.setSpeed(samplePet.getSpeed());
        pet.setStrength(samplePet.getStrength());
        pet.setMutantAttribute(samplePet.getMutantAttribute());

        petDAO.update(pet);

        pet.setEvolutions(new ArrayList<Evolution>());
        pet.setPetSummons(new ArrayList<PetSummon>());

        return Response.status(200).entity(new RestSuccess(pet)).build();
    }

//    protected void copy(Pet src, Pet pet) {
//
//    }
}
