package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.pojo.ItemUpdateGoldCost;
import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.ConfigDAO;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/config")
public class ConfigRest extends BaseRest {

    @GET
    @Path("/itemupdategoldcost")
    @Produces({"application/json;charset=utf-8"})
    public Response getItemUpdateGoldCost() throws IOException {
        ConfigDAO configDAO = (ConfigDAO) getBean(ConfigDAO.class);
        ItemUpdateGoldCost itemUpdateGoldCost = configDAO.getItemUpdateGoldCost();

        return Response.status(200).entity(new RestSuccess(itemUpdateGoldCost)).build();
    }

    @POST
    @Path("/itemupdategoldcost")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postItemUpdateGoldCost(ItemUpdateGoldCost itemUpdateGoldCost) throws IOException {
        ConfigDAO configDAO = (ConfigDAO) getBean(ConfigDAO.class);
        configDAO.saveItemUpdateGoldCost(itemUpdateGoldCost);

        return Response.status(200).entity(new RestSuccess(itemUpdateGoldCost)).build();
    }
}
