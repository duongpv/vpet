package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.SkillDAO;
import vn.vinatek.vpet.database.pojo.Skill;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;


@Path("/skill")
public class SkillRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response getAll() throws IOException {
        SkillDAO skillDAO = (SkillDAO) getBean(SkillDAO.class);
        List skills = skillDAO.getAll();

        return Response.status(200).entity(new RestSuccess(skills)).build();
    }

    @GET
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response getAll(@PathParam("id") int id) throws IOException {
        SkillDAO skillDAO = (SkillDAO) getBean(SkillDAO.class);
        return Response.status(200).entity(new RestSuccess(skillDAO.get(id))).build();
    }

    @POST
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response postSkill(Skill skill) throws IOException {
        SkillDAO skillDAO = (SkillDAO) getBean(SkillDAO.class);
        skillDAO.save(skill);

        return Response.status(200).entity(new RestSuccess(skill)).build();
    }

    @PUT
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response putSkill(Skill skill) throws IOException {
        SkillDAO skillDAO = (SkillDAO) getBean(SkillDAO.class);
        skillDAO.update(skill);

        return Response.status(200).entity(new RestSuccess(skill)).build();
    }
}
