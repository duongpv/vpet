package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.AreaDAO;
import vn.vinatek.vpet.database.pojo.Area;
import vn.vinatek.vpet.database.pojo.AreaRewardItem;
import vn.vinatek.vpet.database.pojo.ItemRecipe;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Path("/area")
public class AreaRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response area() throws IOException {
        AreaDAO areaDAO = (AreaDAO) getBean(AreaDAO.class);
        List<Area> areas = areaDAO.getAll();
        for (Area area : areas) {
            area.setAreaRewardItems(new ArrayList<AreaRewardItem>());
        }
        return Response.status(200).entity(new RestSuccess(areas)).build();
    }

    @GET
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response area(@PathParam("id") int id) throws IOException {
        AreaDAO areaDAO = (AreaDAO) getBean(AreaDAO.class);
        Area area = areaDAO.get(id, Area.class);
        if (area != null) {
            List<AreaRewardItem> areaRewardItems = areaDAO.getAreaRewardItems(area.getId());
            for (AreaRewardItem areaRewardItem : areaRewardItems) {
                areaRewardItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
            }

            area.setAreaRewardItems(areaRewardItems);
        }

        return Response.status(200).entity(new RestSuccess(area)).build();
    }

    @PUT
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response putArea(Area area) throws IOException {
        AreaDAO areaDAO = (AreaDAO) getBean(AreaDAO.class);
        Area existArea = areaDAO.get(area.getId());

        existArea.setDescription(area.getDescription());
        existArea.setGoldCost(area.getGoldCost());
        existArea.setLevelRequire(area.getLevelRequire());
        existArea.setAreaRewardItems(area.getAreaRewardItems());
        existArea.setMapElements(area.getMapElements());
        existArea.setMapLimit(area.isMapLimit());

        areaDAO.update(area);

        return Response.status(200).entity(new RestSuccess(existArea)).build();
    }
}
