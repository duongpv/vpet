package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.SummonDAO;
import vn.vinatek.vpet.database.pojo.Area;
import vn.vinatek.vpet.database.pojo.Summon;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Path("/summon")
public class SummonRest extends BaseRest {

    @GET
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response get(@PathParam("id") int id) throws IOException {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        Summon summon = summonDAO.get(id);
        if (summon != null) {
            summon.setAreas(new ArrayList<Area>());
        }
        return Response.status(200).entity(new RestSuccess(summon)).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response delete(@PathParam("id") int id) throws IOException {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(summonDAO.delete(id)))).build();
    }

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response getAll() throws IOException {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        List<Summon> summons = summonDAO.getAll();
        for (Summon summon : summons) {
            summon.setAreas(new ArrayList<Area>());
        }

        return Response.status(200).entity(new RestSuccess(summons)).build();
    }

    @POST
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response add(Summon summon) throws IOException {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);
        summonDAO.save(summon);

        return Response.status(200).entity(new RestSuccess(summon)).build();
    }

    @PUT
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response update(Summon summon) throws IOException {
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);

        Summon summonExist = summonDAO.get(summon.getId());
        copy(summon, summonExist);

        summonDAO.update(summonExist);

        return Response.status(200).entity(new RestSuccess(summonExist)).build();
    }

    protected void copy(Summon src, Summon des) {
        des.setAreas(src.getAreas());
        des.setDescription(src.getDescription());
        des.setHpRegen(src.getHpRegen());
        des.setHunting(src.isHunting());
        des.setMagic(src.getMagic());
        des.setImageUrl(src.getImageUrl());
        des.setIncreasingDefense(src.isIncreasingDefense());
        des.setIncreasingHp(src.isIncreasingHp());
        des.setIncreasingIntelligent(src.isIncreasingIntelligent());
        des.setIncreasingSpeed(src.isIncreasingSpeed());
        des.setIncreasingStrength(src.isIncreasingStrength());
        des.setName(src.getName());
        des.setSummonClass(src.getSummonClass());
        des.setSell(src.isSell());
        des.setCrystalCostBuy(src.getCrystalCostBuy());
        des.setGoldCostBuy(src.getGoldCostBuy());
        des.setBcoinCostBuy(src.getBcoinCostBuy());
        des.setQuest(src.isQuest());
    }
}
