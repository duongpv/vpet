package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.PetClassDAO;
import vn.vinatek.vpet.database.pojo.PetClass;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/petclass")
public class PetClassRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response getAll() throws IOException {
        PetClassDAO petClassDAO = (PetClassDAO) getBean(PetClassDAO.class);
        return Response.status(200).entity(new RestSuccess(petClassDAO.getAll())).build();
    }

    @POST
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response add(PetClass petClass) throws IOException {
        PetClassDAO petClassDAO = (PetClassDAO) getBean(PetClassDAO.class);
        petClassDAO.save(petClass);

        return Response.status(200).entity(new RestSuccess(petClass)).build();
    }

    @PUT
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response update(PetClass petClass) throws IOException {
        PetClassDAO petClassDAO = (PetClassDAO) getBean(PetClassDAO.class);

        PetClass petClassExist = petClassDAO.get(petClass.getId());
        copy(petClass, petClassExist);

        petClassDAO.update(petClassExist);

        return Response.status(200).entity(new RestSuccess(petClassExist)).build();
    }

    protected void copy(PetClass src, PetClass des) {
        des.setDefense(src.getDefense());
        des.setDescription(src.getDescription());
        des.setHp(src.getHp());
        des.setIntelligent(src.getIntelligent());
        des.setMp(src.getMp());
        des.setName(src.getName());
        des.setSpeed(src.getSpeed());
        des.setStrength(src.getStrength());
    }
}
