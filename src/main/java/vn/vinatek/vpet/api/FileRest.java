package vn.vinatek.vpet.api;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


@Path("/file")
public class FileRest extends BaseRest {
    private static String UPLOAD_PATH = "";

    static {
        Properties prop = new Properties();
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream input = classLoader.getResourceAsStream("/upload.properties");

            prop.load(input);

            UPLOAD_PATH = prop.getProperty("uploadPath");

            input.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileRest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @POST
    @Consumes({"multipart/form-data"})
    public Response upload(@FormDataParam("file") InputStream uploadedInputStream, @FormDataParam("file") FormDataContentDisposition fileDetail, @FormDataParam("file") FormDataBodyPart body)
            throws IOException {
        byte[] bytes = IOUtils.toByteArray(uploadedInputStream);

        if (bytes.length > 524288) {
            return Response.status(400).entity("File vượt quá 512 kb").build();
        }

        if (!body.getMediaType().getType().equals("image")) {
            return Response.status(400).entity("File không phải hình ảnh").build();
        }

        String fileName = createFileName(fileDetail.getFileName());
        String filePath = UPLOAD_PATH + "/" + fileName;

        writeToFile(bytes, filePath);

        return Response.ok().entity(fileName).build();
    }

    private void writeToFile(byte[] bytes, String uploadedFileLocation) throws IOException {
        OutputStream out = new FileOutputStream(new File(uploadedFileLocation));

        out.write(bytes);
        out.flush();
        out.close();
    }

    public String createFileName(String fileName) {
        String filePath = UPLOAD_PATH + "/" + fileName;
        File file = new File(filePath);

        if (!file.exists()) {
            return fileName;
        }

        int count = 0;
        for (; ; ) {
            String tempName = FilenameUtils.getBaseName(fileName) + "." + count + "." + FilenameUtils.getExtension(fileName);
            filePath = UPLOAD_PATH + "/" + tempName;

            file = new File(filePath);

            if (!file.exists()) {
                return tempName;
            }

            count++;
        }
    }

    public static boolean deleteImage(String fileName) {
        String filePath = UPLOAD_PATH + "/" + fileName;
        File file = new File(filePath);

        if (!file.exists()) {
            return false;
        }

        return file.delete();
    }
}
