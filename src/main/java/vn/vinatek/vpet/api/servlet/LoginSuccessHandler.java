package vn.vinatek.vpet.api.servlet;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.context.support.WebApplicationContextUtils;
import vn.vinatek.vpet.database.dao.CharacterDAO;
import vn.vinatek.vpet.database.pojo.Characters;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;


public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    private Logger logger = Logger.getLogger(LoginSuccessHandler.class);

    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        long t = System.currentTimeMillis();
        ApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        CharacterDAO characterDAO = appContext.getBean(CharacterDAO.class);

        String userName = authentication.getName();
        Characters characters = characterDAO.getCharacterByUserName(userName);
        if (characters != null) {
            Map<Integer, HttpSession> sessionMap = SessionCounter.sessions;
            sessionMap.put(Integer.valueOf(characters.getId()), request.getSession());
            this.logger.info(String.format("[%s] Login success %d", userName, (System.currentTimeMillis() - t)));
        }

        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        response.setStatus(302);
        response.setHeader("Location", url + "/#/avatar/");
    }
}
