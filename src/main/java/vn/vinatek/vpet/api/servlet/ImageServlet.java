package vn.vinatek.vpet.api.servlet;

import vn.vinatek.vpet.api.FileRest;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


@WebServlet(name = "ImageController", urlPatterns = {"/upload"})
public class ImageServlet extends HttpServlet {
    private static String UPLOAD_PATH = "";

    static {
        Properties prop = new Properties();
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream input = classLoader.getResourceAsStream("/upload.properties");

            prop.load(input);

            UPLOAD_PATH = prop.getProperty("uploadPath");

            input.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileRest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileRest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("link") == null) {
            response.setStatus(404);
            return;
        }

        String filePath = UPLOAD_PATH + "//" + request.getParameter("link");

        File file = new File(filePath);
        response.setContentLength((int) file.length());

        FileInputStream in = new FileInputStream(file);
        OutputStream out = response.getOutputStream();

        byte[] buf = new byte[1048576];
        int count = 0;
        while ((count = in.read(buf)) >= 0) {
            out.write(buf, 0, count);
        }
        out.close();
        in.close();
    }
}
