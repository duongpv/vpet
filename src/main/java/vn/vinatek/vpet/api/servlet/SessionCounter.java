package vn.vinatek.vpet.api.servlet;

import javax.servlet.*;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.HashMap;
import java.util.Map;

public class SessionCounter implements ServletContextListener, HttpSessionListener, ServletRequestListener {
    public static Map<Integer, HttpSession> sessions = new HashMap();

    public void contextInitialized(ServletContextEvent event) {
        event.getServletContext().setAttribute("vn.vinatek.vpet.api.provider.SessionCounter", this);
    }

    public void requestInitialized(ServletRequestEvent event) {
    }

    public void sessionDestroyed(HttpSessionEvent event) {
    }

    public void sessionCreated(HttpSessionEvent event) {
    }

    public void requestDestroyed(ServletRequestEvent event) {
    }

    public void contextDestroyed(ServletContextEvent event) {
    }

    public static SessionCounter getInstance(ServletContext context) {
        return (SessionCounter) context.getAttribute("vn.vinatek.vpet.api.provider.SessionCounter");
    }
}
