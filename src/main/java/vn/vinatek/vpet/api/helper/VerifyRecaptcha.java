package vn.vinatek.vpet.api.helper;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;


public class VerifyRecaptcha {
    private static final String url = "https://www.google.com/recaptcha/api/siteverify";
    private static final String secret = "6LcBOQYTAAAAAGLN4kQaNuel3bbXfRjrTAmnfXTM";
    private static final String USER_AGENT = "Mozilla/5.0";

    public static boolean verify(String gRecaptchaResponse) throws IOException {
        if ((gRecaptchaResponse == null) || ("".equals(gRecaptchaResponse))) {
            return false;
        }
        try {
            URL obj = new URL("https://www.google.com/recaptcha/api/siteverify");
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String postParams = "secret=6LcBOQYTAAAAAGLN4kQaNuel3bbXfRjrTAmnfXTM&response=" + gRecaptchaResponse;

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postParams);
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

            StringBuffer response = new StringBuffer();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString().contains("true");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
