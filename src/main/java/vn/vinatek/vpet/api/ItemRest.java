package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.CharacterClassDAO;
import vn.vinatek.vpet.database.dao.ItemClassDAO;
import vn.vinatek.vpet.database.dao.ItemDAO;
import vn.vinatek.vpet.database.pojo.CharacterClass;
import vn.vinatek.vpet.database.pojo.Item;
import vn.vinatek.vpet.database.pojo.ItemClass;
import vn.vinatek.vpet.database.pojo.ItemRecipe;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Path("/item")
public class ItemRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response getAll() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getAllItem();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/item/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response getItem(@PathParam("id") int id) throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        Item item = itemDAO.get(id);
        if (item != null) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(item)).build();
    }

    @GET
    @Path("/avatar/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response getAvatarItem(@PathParam("id") int id) throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        return Response.status(200).entity(new RestSuccess(itemDAO.get(id))).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response delete(@PathParam("id") int id) throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(itemDAO.delete(id)))).build();
    }

    @GET
    @Path("/pet/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response getPetItem(@PathParam("id") int id) throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        return Response.status(200).entity(new RestSuccess(itemDAO.get(id))).build();
    }

    @GET
    @Path("/other/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response getOtherItem(@PathParam("id") int id) throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        Item item = itemDAO.get(id);
        if (item != null) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }
        return Response.status(200).entity(new RestSuccess(item)).build();
    }

    @GET
    @Path("/avatar")
    @Produces({"application/json;charset=utf-8"})
    public Response getAllAvatarItem() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        return Response.status(200).entity(new RestSuccess(itemDAO.getAllAvatarItem())).build();
    }

    @GET
    @Path("/pet")
    @Produces({"application/json;charset=utf-8"})
    public Response getAllPetItem() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getAllPetItem();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/other")
    @Produces({"application/json;charset=utf-8"})
    public Response getAllOtherItem() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getAllOtherItem();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/evolution")
    @Produces({"application/json;charset=utf-8"})
    public Response getAllEvolutionItem() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        List<Item> items = itemDAO.getAllEvolutionItem();
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/repices")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response getAllItemRecipe() throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        return Response.status(200).entity(new RestSuccess(itemDAO.getAllItemRecipe())).build();
    }

    @POST
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response add(Item item) throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        ItemClassDAO itemClassDAO = (ItemClassDAO) getBean(ItemClassDAO.class);
        CharacterClassDAO characterClassDAO = (CharacterClassDAO) getBean(CharacterClassDAO.class);

        ItemClass itemClass = itemClassDAO.get(item.getItemClass().getId());
        CharacterClass characterClass = characterClassDAO.get(item.getCharacterClass().getId());

        item.setItemClass(itemClass);
        item.setCharacterClass(characterClass);

        itemDAO.save(item);

        return Response.status(200).entity(new RestSuccess(item)).build();
    }

    @PUT
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response update(Item item) throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        ItemClassDAO itemClassDAO = (ItemClassDAO) getBean(ItemClassDAO.class);

        ItemClass itemClass = itemClassDAO.get(item.getItemClass().getId());
        item.setItemClass(itemClass);

        Item itemExist = itemDAO.get(item.getId());

        copy(item, itemExist);

        itemDAO.update(itemExist);

        return Response.status(200).entity(new RestSuccess(itemExist)).build();
    }

    protected void copy(Item src, Item des) {
        des.setCrystalCostBuy(src.getCrystalCostBuy());
        des.setCrystalCostSell(src.getCrystalCostSell());
        des.setDefense(src.getDefense());
        des.setDescription(src.getDescription());
        des.setEndurance(src.getEndurance());
        des.setGoldCostBuy(src.getGoldCostBuy());
        des.setGoldCostSell(src.getGoldCostSell());
        des.setHp(src.getHp());
        des.setIncreasingAttribute(src.getIncreasingAttribute());
        des.setImageUrl(src.getImageUrl());
        des.setIntelligent(src.getIntelligent());
        des.setItemClass(src.getItemClass());
        des.setLevelRequire(src.getLevelRequire());
        des.setCharacterClass(src.getCharacterClass());
        des.setName(src.getName());
        des.setRateCatch(src.getRateCatch());
        des.setMagic(src.getMagic());
        des.setSell(src.isSell());
        des.setSpeed(src.getSpeed());
        des.setStrength(src.getStrength());
        des.setShopType(src.getShopType());
        des.setBcoinCostBuy(src.getBcoinCostBuy());
        des.setItemLevel(src.getItemLevel());
        des.setItemRecipes(src.getItemRecipes());
    }
}
