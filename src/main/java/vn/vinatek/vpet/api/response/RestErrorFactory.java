package vn.vinatek.vpet.api.response;


public class RestErrorFactory {

    public static final RestError MEMBER_REGISTER_CAPTCHA_INVALID = new RestError(1, "Captcha không hợp lệ");

    public static final RestError MEMBER_REGISTER_EMAIL_INVALID = new RestError(2, "Email không hợp lệ");

    public static final RestError MEMBER_REGISTER_ID_CARD_INVALID = new RestError(3, "CMND không hợp lệ");

    public static final RestError MEMBER_REGISTER_PASSWORD_INVALID = new RestError(4, "Mật khẩu không hợp lệ");

    public static final RestError MEMBER_REGISTER_PHONE_INVALID = new RestError(5, "Điện thoại không hợp lệ");

    public static final RestError MEMBER_REGISTER_FULL_NAME_INVALID = new RestError(6, "Tên không hợp lệ");

    public static final RestError MEMBER_REGISTER_USERNAME_INVALID = new RestError(7, "Tên tài khoản không hợp lệ");

    public static final RestError MEMBER_REGISTER_USERNAME_EXIST = new RestError(8, "Tên tài khoản đã tồn tại");

    public static final RestError CHARACTER_REGISTER_CHARACTER_NAME_EXIST = new RestError(257, "Tên nhân vật đã tồn tại");

    public static final RestError CHARACTER_REGISTER_CHARACTER_EXIST = new RestError(258, "Nhân vật đã tồn tại");

    public static final RestError CHARACTER_REGISTER_CHARACTER_NAME_INVALID = new RestError(259, "Tên nhân vật không hợp lệ");

    public static final RestError CHARACTER_REGISTER_PET_INVALID = new RestError(260, "Pet không hợp lệ");

    public static final RestError CHARACTER_NOT_EXIST = new RestError(261, "Nhân vật không tồn tại");

    public static final RestError CHARACTER_REGISTER_PET_NAME_INVALID = new RestError(262, "Tên thú cưng không hợp lệ");

    public static final RestError CHARACTER_REGISTER_NAME_TOO_LONG = new RestError(263, "Tên nhân vật dài hơn 14 kí tự");

    public static final RestError CHARACTER_NOT_FRIEND = new RestError(264, "Bạn chưa kết bạn với người nhận");

    public static final RestError CHARACTER_LEVEL_NOT_ENOUGH = new RestError(265, "Nhân vật chưa đủ level");

    public static final RestError CHARACTER_MAX = new RestError(272, "Nhân vật đạt cấp độ tối đa");

    public static final RestError BATTLE_NOT_EXIST = new RestError(513, "Trận đấu không tồn tại");

    public static final RestError BATTLE_ITEM_NOT_EXIST = new RestError(514, "Vật phẩm không tồn tại");

    public static final RestError BATTLE_ITEM_INVALID = new RestError(515, "Vật phẩm không hợp lệ");

    public static final RestError BATTLE_PET_DIE = new RestError(516, "Thú cưng đã hết máu");

    public static final RestError BATTLE_NO_ACTIVE_PET = new RestError(517, "Không có thú cưng được active");

    public static final RestError PET_NOT_EXIST = new RestError(769, "Thú cưng không tồn tại");

    public static final RestError PET_LESS_THAN_TWO = new RestError(770, "Số lượng thú cưng chỉ còn lại một, không thể phóng thích");

    public static final RestError PET_ACTIVE_LIMIT = new RestError(771, "Đã hết lượt thay đổi thú cưng");

    public static final RestError PET_POINT_LIMIT = new RestError(772, "Thú cưng đã max chỉ số");

    public static final RestError ITEM_NOT_EXIST = new RestError(1025, "Vật phẩm không tồn tại");

    public static final RestError ITEM_CANT_BUY = new RestError(1026, "Không thể mua");

    public static final RestError ITEM_BUY_GOLD_NOT_ENOUGH = new RestError(1027, "Vàng không đủ");

    public static final RestError ITEM_BUY_CRYSTAL_NOT_ENOUGH = new RestError(1028, "Kim cương không đủ");

    public static final RestError ITEM_CAN_NOT_USE = new RestError(1029, "Vật phẩm không thể sử dụng");

    public static final RestError ITEM_CANT_SEND = new RestError(1031, "Không thể gửi trang bị");

    public static final RestError ITEM_BUY_BCOIN_NOT_ENOUGH = new RestError(1032, "Tài phú không đủ");

    public static final RestError ITEM_INVALID = new RestError(1033, "Vật phẩm không hợp lệ");

    public static final RestError ITEM_CAN_NOT_CREATE = new RestError(1040, "Vật phẩm không thể chế tạo");

    public static final RestError ITEM_NOT_ENOUGH = new RestError(1041, "Vật phẩm không đủ");

    public static final RestError PET_EQUIP_ITEM_FULL = new RestError(1281, "Thú cưng đã trang bị đầy đủ item. Vui lòng tháo item không cần thiết.");

    public static final RestError PET_EQUIP_FAIL = new RestError(1282, "Không thể trang bị");

    public static final RestError PET_USE_FAIL = new RestError(1283, "Không thể sử dụng");

    public static final RestError PET_NOT_ENOUGH_LEVEL = new RestError(1284, "Thú cưng không đủ cấp độ");

    public static final RestError PET_END_EVOLUTION = new RestError(1285, "Thú cưng không thể tiến hóa nữa");

    public static final RestError PET_NOT_EVOLUTION = new RestError(1286, "Thú cưng không thể tiến hóa");

    public static final RestError PET_EQUIP_SUMMON_FULL = new RestError(1287, "Thú cưng đã trang bị đầy đủ linh thú. Vui lòng tháo linh thú không cần thiết.");

    public static final RestError HUNTING_GOLD_NOT_ENOUGH = new RestError(1537, "Vàng không đủ");

    public static final RestError HUNTING_CRYSTAL_NOT_ENOUGH = new RestError(1536, "Kim cương không đủ");

    public static final RestError HUNTING_NET_NOT_ENOUGH = new RestError(1538, "Lưới không đủ");

    public static final RestError HUNTING_PET_LIMIT = new RestError(1638, "Đã max số thú cưng, không thể bắt thêm.");

    public static final RestError SEND_NET_LIMIT = new RestError(1639, "Người nhận đã max số thú cưng, không thể gửi tặng.");

    public static final RestError SEND_SUMMON_LIMIT = new RestError(1639, "Người nhận đã max số linh thú, không thể gửi tặng.");

    public static final RestError UNEQUIPPET_SUMMON_LIMIT = new RestError(1639, "Đã max số linh thú, không thể gỡ.");

    public static final RestError HUNTING_SUMMON_LIMIT = new RestError(1639, "Đã max số linh thú, không thể bắt thêm.");

    public static final RestError HUNTING_DAY_LIMIT = new RestError(1539, "Hết lượt trong ngày");

    public static final RestError SUMMON_NOT_EXIST = new RestError(1793, "Linh thú không tồn tại");

    public static final RestError BANK_PASS_NOT_EXIST = new RestError(2049, "Bạn chưa có mật khẩu cấp 2");

    public static final RestError BANK_PASS_WRONG = new RestError(2050, "Sai mật khẩu cấp 1");

    public static final RestError BANK_ROOT_PASS_WRONG = new RestError(2051, "Sai mật khẩu cấp 2");

    public static final RestError BANK_FORBIDDEN = new RestError(2052, "Truy ngân hàng cập bị từ chối");

    public static final RestError BANK_GOLD_NOT_ENOUGH = new RestError(2053, "Không đủ vàng");

    public static final RestError BANK_CRYSTAL_NOT_ENOUGH = new RestError(2054, "Không đủ kim cương");

    public static final RestError BANK_MONEY_INVAILD = new RestError(2055, "Số tiền không hợp lệ");

    public static final RestError BANK_CHARACTER_NOT_EXIST = new RestError(2056, "Nhân vật không tồn tại");

    public static final RestError MONSTER_NOT_EXIST = new RestError(2305, "Quái vật không tồn tại");

    public static final RestError QUEST_TYPE_NOT_EXIST = new RestError(4097, "Nhiệm vụ không tồn tại");

    public static final RestError QUEST_ITEM_NOT_EXIST = new RestError(4098, "Không có thẻ nhận nhiệm vụ");

    public static final RestError QUEST_EXIST = new RestError(4099, "Bạn đã nhận nhiệm vụ khác");

    public static final RestError QUEST_NOT_COMPLETE = new RestError(4100, "Bạn chưa hoàn thành nhiệm vụ");

    public static final RestError QUEST_NOT_EXIST = new RestError(4101, "Nhiệm vụ không tồn tại");

    public static final RestError QUEST_TIME_OUT = new RestError(4102, "Nhiệm vụ quá thời gian");
}
