package vn.vinatek.vpet.api.response;


public class RestError extends RestResponse {

    private int errorCode;

    private String msg;

    public RestError(int errorCode, String msg) {
        this.errorCode = errorCode;
        this.msg = msg;
    }

    public RestError() {
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
