package vn.vinatek.vpet.api.response;

public class RestSuccess extends RestResponse {

    private Object data;

    public RestSuccess() {
    }

    public RestSuccess(Object data) {
        this.data = data;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
