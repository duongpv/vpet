package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.CharacterLevelDAO;
import vn.vinatek.vpet.database.pojo.CharacterLevel;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/characterlevel")
public class CharacterLevelRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response getAllCharacterLevel() throws IOException {
        CharacterLevelDAO characterLevelDAO = (CharacterLevelDAO) getBean(CharacterLevelDAO.class);
        return Response.status(200).entity(new RestSuccess(characterLevelDAO.getAll())).build();
    }

    @GET
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response getAllCharacterLevel(@PathParam("id") int id) throws IOException {
        CharacterLevelDAO characterLevelDAO = (CharacterLevelDAO) getBean(CharacterLevelDAO.class);
        return Response.status(200).entity(new RestSuccess(characterLevelDAO.get(id))).build();
    }

    @PUT
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response putArea(CharacterLevel characterLevel) throws IOException {
        CharacterLevelDAO characterLevelDAO = (CharacterLevelDAO) getBean(CharacterLevelDAO.class);
        CharacterLevel existCharacterLevel = characterLevelDAO.get(characterLevel.getId());

        existCharacterLevel.setCrystalRequire(characterLevel.getCrystalRequire());
        existCharacterLevel.setDelayRecover(characterLevel.getDelayRecover());
        existCharacterLevel.setDescription(characterLevel.getDescription());
        existCharacterLevel.setGoldRequire(characterLevel.getGoldRequire());
        existCharacterLevel.setLevelRequire(characterLevel.getLevelRequire());
        existCharacterLevel.setMaxCreateItemLevelId(characterLevel.getMaxCreateItemLevelId());
        existCharacterLevel.setMaxEggHatch(characterLevel.getMaxEggHatch());
        existCharacterLevel.setMaxForbiddenMap(characterLevel.getMaxForbiddenMap());
        existCharacterLevel.setMaxItemForPet(characterLevel.getMaxItemForPet());
        existCharacterLevel.setMaxItemLevelId(characterLevel.getMaxItemLevelId());
        existCharacterLevel.setMaxPet(characterLevel.getMaxPet());
        existCharacterLevel.setMaxSummon(characterLevel.getMaxSummon());
        existCharacterLevel.setMaxSummonForPet(characterLevel.getMaxSummonForPet());

        characterLevelDAO.update(existCharacterLevel);

        return Response.status(200).entity(new RestSuccess(existCharacterLevel)).build();
    }
}
