package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.MonsterDAO;
import vn.vinatek.vpet.database.pojo.Area;
import vn.vinatek.vpet.database.pojo.Item;
import vn.vinatek.vpet.database.pojo.Monster;
import vn.vinatek.vpet.database.pojo.MonsterRewardItem;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Path("/monsterarea")
public class MonsterAreaRest extends BaseRest {

    @GET
    @Path("/monsters")
    @Produces({"application/json;charset=utf-8"})
    public Response area() throws IOException {
        MonsterDAO monsterDAO = (MonsterDAO) getBean(MonsterDAO.class);
        List<Monster> monsters = monsterDAO.getMonstersInArea(1);
        for (Monster monster : monsters) {
            monster.setAreas(new ArrayList<Area>());
            monster.setItems(new ArrayList<Item>());
            monster.setMonsterRewardItems(new ArrayList<MonsterRewardItem>());
        }

        return Response.status(200).entity(new RestSuccess(monsters)).build();
    }
}