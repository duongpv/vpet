package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.helper.VerifyRecaptcha;
import vn.vinatek.vpet.api.pojo.RegisterInfo;
import vn.vinatek.vpet.api.response.RestError;
import vn.vinatek.vpet.api.response.RestErrorFactory;
import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.UserDAO;
import vn.vinatek.vpet.database.pojo.User;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/member")
public class MemberRest extends BaseRest {

    @POST
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response registerMember(RegisterInfo registerInfo) throws IOException {
        RestError restError = checkRegisterInfo(registerInfo);

        if (restError != null) {
            return Response.status(400).entity(restError).build();
        }

        UserDAO userDAO = (UserDAO) getBean(UserDAO.class);
        User user = toUser(registerInfo);
        userDAO.save(user);

        return Response.status(200).entity(new RestSuccess(user)).build();
    }

    private RestError checkRegisterInfo(RegisterInfo registerInfo) throws IOException {
        boolean t = VerifyRecaptcha.verify(registerInfo.getCaptcha());
        if (!t) {
            return RestErrorFactory.MEMBER_REGISTER_CAPTCHA_INVALID;
        }

        if (checkNullOrEmpty(registerInfo.getEmail())) {
            return RestErrorFactory.MEMBER_REGISTER_EMAIL_INVALID;
        }

        if (checkNullOrEmpty(registerInfo.getPassword())) {
            return RestErrorFactory.MEMBER_REGISTER_PASSWORD_INVALID;
        }

        if (checkNullOrEmpty(registerInfo.getUserName())) {
            return RestErrorFactory.MEMBER_REGISTER_USERNAME_INVALID;
        }

        UserDAO userDAO = (UserDAO) getBean(UserDAO.class);
        if (userDAO.exist(registerInfo.getUserName())) {
            return RestErrorFactory.MEMBER_REGISTER_USERNAME_EXIST;
        }

        return null;
    }

    private User toUser(RegisterInfo registerInfo) {
        User user = new User();
        user.setEmail(registerInfo.getEmail());
        user.setFullName(registerInfo.getFullName());
        user.setIdentityCard(registerInfo.getIdentityCard());
        user.setPassword(registerInfo.getPassword());
        user.setPhone(registerInfo.getPhone());
        user.setUserName(registerInfo.getUserName());

        return user;
    }
}
