package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.NewsDAO;
import vn.vinatek.vpet.database.pojo.News;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/news")
public class NewsRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response getAll() throws IOException {
        NewsDAO newsDAO = (NewsDAO) getBean(NewsDAO.class);
        return Response.status(200).entity(new RestSuccess(newsDAO.getAll())).build();
    }

    @GET
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response get(@PathParam("id") int id) throws IOException {
        NewsDAO newsDAO = (NewsDAO) getBean(NewsDAO.class);
        return Response.status(200).entity(new RestSuccess(newsDAO.get(id))).build();
    }

    @PUT
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response put(News news) throws IOException {
        NewsDAO newsDAO = (NewsDAO) getBean(NewsDAO.class);
        newsDAO.update(news);

        return Response.status(200).entity(new RestSuccess(news)).build();
    }

    @POST
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response post(News news) throws IOException {
        NewsDAO newsDAO = (NewsDAO) getBean(NewsDAO.class);
        newsDAO.save(news);

        return Response.status(200).entity(new RestSuccess(news)).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response delete(@PathParam("id") int id) throws IOException {
        NewsDAO newsDAO = (NewsDAO) getBean(NewsDAO.class);
        News news = newsDAO.get(id);
        if (null != news) {
            newsDAO.delete(news);
        }

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }
}
