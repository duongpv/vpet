package vn.vinatek.vpet.api;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import vn.vinatek.vpet.api.pojo.BattleContext;
import vn.vinatek.vpet.database.dao.CharacterPetDAO;
import vn.vinatek.vpet.database.dao.MonsterDAO;
import vn.vinatek.vpet.database.dao.PetDAO;
import vn.vinatek.vpet.database.pojo.*;
import vn.vinatek.vpet.database.service.CharacterService;
import vn.vinatek.vpet.queue.CacheClient;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


public class BattleBaseRest extends BaseRest {

    protected BattleContext battleContext;

    public BattleBaseRest(HttpServletRequest request, String key) {
        this.battleContext = getBattleContext(request, key);
    }

    protected CharacterPet getCharacterPet(int petId) {
        WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        CharacterPetDAO characterPetDAO = appContext.getBean(CharacterPetDAO.class);

        CharacterPet characterPet = characterPetDAO.get(petId);
        return initCharacterPet(characterPet);
    }

    protected CharacterPet getMaxPetByUserName(String userName) {
        WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        CharacterPetDAO characterPetDAO = appContext.getBean(CharacterPetDAO.class);
        CharacterService characterService = (CharacterService) getBean(CharacterService.class);

        CharacterPet characterPet = characterPetDAO.getMaxPetByUserName(userName);
        if (characterPet != null) {
            characterPet.setPetSummons(characterService.getPetSummons(characterPet.getId()));
        }

        return characterPet;
    }

    private CharacterPet initCharacterPet(CharacterPet characterPet) {
        if (characterPet != null) {
            WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
            CharacterService characterService = (CharacterService) getBean(CharacterService.class);
            PetDAO petDAO = appContext.getBean(PetDAO.class);

            characterPet.getPet().setAreas(new ArrayList<Area>());
            characterPet.getPet().setEvolutions(petDAO.getEvolutions(characterPet.getPet().getId()));
            characterPet.getPet().setPetSummons(new ArrayList<PetSummon>());

            characterPet.setPetSummons(characterService.getPetSummons(characterPet.getId()));
            characterPet.setPetEquipments(characterService.getPetEquipments(characterPet.getId()));
        }

        return characterPet;
    }

    protected Monster getMonster(int monsterId) {
        MonsterDAO monsterDAO = (MonsterDAO) getBean(MonsterDAO.class);
        Monster monster = monsterDAO.get(monsterId);
        if (monster != null) {
            List<MonsterRewardItem> rewardItems = monsterDAO.getMonsterRewardItems(monsterId);
            for (MonsterRewardItem rewardItem : rewardItems) {
                rewardItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
            }

            List<Item> items = monsterDAO.getItems(monsterId);
            for (Item item : items) {
                item.setItemRecipes(new ArrayList<ItemRecipe>());
            }

            monster.setMonsterRewardItems(rewardItems);
            monster.setAreas(monsterDAO.getAreas(monsterId));
            monster.setItems(items);
        }

        return monster;
    }

    protected int getCharacterMapId() {
        return characterDAO().getMapId(getUserName());
    }

    protected BattleContext initBattleContext(CharacterPet characterPet, Monster monster, Characters characters) {
//        try {
//            if (characterPet.getEvolutionIndex() < -1) {
//                AdminCharacterRest adminCharacterRest = new AdminCharacterRest();
//                adminCharacterRest.postBlockCharacter(characters.getId());
//
//                return null;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        CharacterService characterService = (CharacterService) getBean(CharacterService.class);
        characterPet = characterService.calculateTotalPoint(characterPet, characters);

        BattleContext battleContext = new BattleContext();
        battleContext.setCharacterPet(characterPet);
        battleContext.setMonster(monster);
        battleContext.setMonsterCurrentHp(monster.getBattleHp());

        int characterPetSpeed = characterPet.getTotalSpeed() % 100;
        int monsterSpeed = monster.getBattleSpeed() % 100;

        if (monsterSpeed > characterPetSpeed) {
            battleContext.setMonsterAttackFirst(true);
        }

        return battleContext;
    }

    protected void putBattleContext(BattleContext battleContext, String key) {
        WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        CacheClient cacheClient = appContext.getBean(CacheClient.class);

        cacheClient.push(getUserName(), key, battleContext);
    }

    protected BattleContext getBattleContext(HttpServletRequest request, String key) {
        WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        CacheClient cacheClient = appContext.getBean(CacheClient.class);

        return (BattleContext) cacheClient.get(getUserName(), key, BattleContext.class);
    }

    protected BattleContext getBattleContext(String key) {
        return getBattleContext(this.request, key);
    }

    protected void deleteBattleContext(HttpServletRequest request, String key) {
        WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        CacheClient cacheClient = appContext.getBean(CacheClient.class);

        cacheClient.delete(getUserName(), key);
    }
}
