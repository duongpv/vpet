package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.SkillClassDAO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("skillclass")
public class SkillClassRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response getAll() throws IOException {
        SkillClassDAO skillClassDAO = (SkillClassDAO) getBean(SkillClassDAO.class);
        return Response.status(200).entity(new RestSuccess(skillClassDAO.getAll())).build();
    }
}
