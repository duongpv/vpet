package vn.vinatek.vpet.api;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationContextUtils;
import vn.vinatek.vpet.database.dao.CharacterDAO;
import vn.vinatek.vpet.database.dao.UserDAO;
import vn.vinatek.vpet.database.pojo.Characters;
import vn.vinatek.vpet.database.pojo.User;
import vn.vinatek.vpet.queue.CacheClient;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;


public class BaseRest {

    protected Logger logger = Logger.getLogger(AdminCharacterRest.class);

    @Context
    protected HttpServletRequest request;

    protected org.springframework.security.core.userdetails.User user = null;

    public BaseRest() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            this.user = (org.springframework.security.core.userdetails.User) auth.getPrincipal();
        }
    }

    protected String getUserName() {
        if (user != null) {
            return user.getUsername();
        }

        return null;
    }

    protected User getUser() {
        if (user != null) {
            return userDAO().getByUserName(user.getUsername());
        }

        return null;
    }

    protected int getCharacterId() {
        CacheClient cacheClient = (CacheClient) getBean(CacheClient.class);
        String key = String.format("%s_characterid", getUserName());
        Object characterId = cacheClient.get(key);
        if (characterId != null) {
            return (Integer) characterId;
        }

        characterId = characterDAO().getId(getUserName());
        cacheClient.add(key, 900, characterId);
        return (Integer) characterId;
    }

    protected Characters getCurrentCharacter() {
        CharacterDAO characterDAO = characterDAO();

        String userName = user.getUsername();
        Characters characters = characterDAO().getCharacterByUserName(userName);
        if (characters != null) {
            characters.setCharacterEquipments(characterDAO.getCharacterEquipments(characters.getId()));
            characters.setNumberOfPets(characterDAO.getNumberOfPets(characters.getId()));
            characters.setNumberOfSummons(characterDAO.getNumberOfSummons(characters.getId()));
        }

        return characters;
    }

    public UserDAO userDAO() {
        return (UserDAO) getBean(UserDAO.class);
    }

    public CharacterDAO characterDAO() {
        return (CharacterDAO) getBean(CharacterDAO.class);
    }

    protected boolean checkNullOrEmpty(String text) {
        return (text == null) || (text.equals(""));
    }

    protected Object getBean(Class clazz) {
        ApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        return appContext.getBean(clazz);
    }
}
