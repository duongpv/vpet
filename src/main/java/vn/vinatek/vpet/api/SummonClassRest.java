package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.SummonClassDAO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/summonclass")
public class SummonClassRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response area() throws IOException {
        SummonClassDAO summonClassDAO = (SummonClassDAO) getBean(SummonClassDAO.class);
        return Response.status(200).entity(new RestSuccess(summonClassDAO.getAll())).build();
    }
}
