package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.pojo.CharacterRegisterInfo;
import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.api.servlet.SessionCounter;
import vn.vinatek.vpet.database.dao.ItemDAO;
import vn.vinatek.vpet.database.dao.PetDAO;
import vn.vinatek.vpet.database.pojo.*;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Path("/admincharacter")
public class AdminCharacterRest extends BaseRest {

    @GET
    @Path("/all")
    @Produces({"application/json;charset=utf-8"})
    public Response getCharacters() throws IOException {
        List<Characters> characters = characterDAO().getCharacters();
        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @GET
    @Path("/detail/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response getCharacter(@PathParam("id") int id) throws IOException {
        Characters characters = characterDAO().get(id);
        characters.setCharacterPets(new ArrayList<CharacterPet>());
        characters.setCharacterEquipments(new ArrayList<CharacterEquipment>());
        characters.setCharacterItems(new ArrayList<CharacterItem>());
        characters.setCharacterSummons(new ArrayList<CharacterSummon>());
        characters.setFriends(new ArrayList<Characters>());
        characters.setFriendRequest(new ArrayList<Characters>());

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/session/kill/{characterId}")
    @Produces({"application/json;charset=utf-8"})
    public Response postKillSession(@PathParam("characterId") int characterId) throws IOException {
        this.logger.info("Kill session of character: " + characterId);

        Map<Integer, HttpSession> sessionsMap = SessionCounter.sessions;
        HttpSession session = sessionsMap.get(Integer.valueOf(characterId));
        try {
            if (session != null) {
                sessionsMap.remove(Integer.valueOf(characterId));
                session.invalidate();
            }
        } catch (Exception localException) {
        }

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/addgold/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response postAddGold(@PathParam("id") int id, long gold) throws IOException {
        Characters characters = characterDAO().get(id);
        characters.setGold(characters.getGold() + gold);

        characterDAO().update(characters);

        characters.setCharacterPets(new ArrayList<CharacterPet>());
        characters.setCharacterEquipments(new ArrayList<CharacterEquipment>());
        characters.setCharacterItems(new ArrayList<CharacterItem>());
        characters.setCharacterSummons(new ArrayList<CharacterSummon>());
        characters.setFriends(new ArrayList<Characters>());
        characters.setFriendRequest(new ArrayList<Characters>());

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/addbcoin/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response postAddBcoin(@PathParam("id") int id, long bcoin) throws IOException {
        Characters characters = characterDAO().get(id);
        characters.setBcoin(characters.getBcoin() + bcoin);

        characterDAO().update(characters);

        characters.setCharacterPets(new ArrayList<CharacterPet>());
        characters.setCharacterEquipments(new ArrayList<CharacterEquipment>());
        characters.setCharacterItems(new ArrayList<CharacterItem>());
        characters.setCharacterSummons(new ArrayList<CharacterSummon>());
        characters.setFriends(new ArrayList<Characters>());
        characters.setFriendRequest(new ArrayList<Characters>());

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/addcrystal/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response postAddCrystal(@PathParam("id") int id, long crystal) throws IOException {
        Characters characters = characterDAO().get(id);
        characters.setCrystal(characters.getCrystal() + crystal);

        characterDAO().update(characters);

        characters.setCharacterPets(new ArrayList<CharacterPet>());
        characters.setCharacterEquipments(new ArrayList<CharacterEquipment>());
        characters.setCharacterItems(new ArrayList<CharacterItem>());
        characters.setCharacterSummons(new ArrayList<CharacterSummon>());
        characters.setFriends(new ArrayList<Characters>());
        characters.setFriendRequest(new ArrayList<Characters>());

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/additem/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response postAddItem(@PathParam("id") int id, int itemId) throws IOException {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        Item item = itemDAO.get(itemId);

        characterDAO().addCharacterItem(id, item);

        Characters characters = characterDAO().get(id);
        characters.setCharacterPets(new ArrayList<CharacterPet>());
        characters.setCharacterEquipments(new ArrayList<CharacterEquipment>());
        characters.setCharacterItems(new ArrayList<CharacterItem>());
        characters.setCharacterSummons(new ArrayList<CharacterSummon>());
        characters.setFriends(new ArrayList<Characters>());
        characters.setFriendRequest(new ArrayList<Characters>());

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/addpet/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response postAddPet(@PathParam("id") int id, CharacterRegisterInfo registerInfo) throws IOException {
        characterDAO().addPet(id, registerInfo.getPet().getId(), registerInfo.getPet().getName());

        Characters characters = characterDAO().get(id);
        characters.setCharacterPets(new ArrayList<CharacterPet>());
        characters.setCharacterEquipments(new ArrayList<CharacterEquipment>());
        characters.setCharacterItems(new ArrayList<CharacterItem>());
        characters.setCharacterSummons(new ArrayList<CharacterSummon>());
        characters.setFriends(new ArrayList<Characters>());
        characters.setFriendRequest(new ArrayList<Characters>());

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/addsummon/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response postAddSummon(@PathParam("id") int id, CharacterRegisterInfo registerInfo) throws IOException {
        characterDAO().addSummon(id, registerInfo.getSummon().getId(), registerInfo.getSummon().getName());

        Characters characters = characterDAO().get(id);
        characters.setCharacterPets(new ArrayList<CharacterPet>());
        characters.setCharacterEquipments(new ArrayList<CharacterEquipment>());
        characters.setCharacterItems(new ArrayList<CharacterItem>());
        characters.setCharacterSummons(new ArrayList<CharacterSummon>());
        characters.setFriends(new ArrayList<Characters>());
        characters.setFriendRequest(new ArrayList<Characters>());

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/changepass")
    @Produces({"application/json;charset=utf-8"})
    public Response postChangePass(String pass) throws IOException {
        vn.vinatek.vpet.database.pojo.User user = userDAO().getByUserName(getUserName());
        user.setPassword(pass);
        userDAO().update(user);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/block/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response postBlockCharacter(@PathParam("id") int id) throws IOException {

        postKillSession(id);

        vn.vinatek.vpet.database.pojo.User user = userDAO().getByCharacterId(id);
        user.setEnable(false);
        userDAO().update(user);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/unblock/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response postUnblockCharacter(@PathParam("id") int id) throws IOException {
        vn.vinatek.vpet.database.pojo.User user = userDAO().getByCharacterId(id);
        user.setEnable(true);
        userDAO().update(user);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }
}
