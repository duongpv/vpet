package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.pojo.BattleContext;
import vn.vinatek.vpet.api.pojo.BattleInfo;
import vn.vinatek.vpet.api.response.RestError;
import vn.vinatek.vpet.api.response.RestErrorFactory;
import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.pojo.Area;
import vn.vinatek.vpet.database.pojo.CharacterPet;
import vn.vinatek.vpet.database.pojo.Characters;
import vn.vinatek.vpet.database.pojo.Monster;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/areabattle")
public class AreaBattleRest extends BattleBaseRest {

    public AreaBattleRest(@Context HttpServletRequest request) {
        super(request, "AREA_BATTLE_CONTEXT");
    }

    @GET
    @Path("/current")
    @Produces({"application/json;charset=utf-8"})
    public Response getCurrentBattle() throws IOException {
        this.battleContext = getBattleContext("AREA_BATTLE_CONTEXT");
        return Response.status(200).entity(new RestSuccess(this.battleContext)).build();
    }

    @POST
    @Path("/init")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response initBattle(BattleInfo battleInfo) throws IOException {
        long t = System.currentTimeMillis();

        CharacterPet characterPet = getCharacterPet(battleInfo.getPetId());
        if (characterPet.isConsign()) {
            this.battleContext = null;
            return Response.status(400).entity(new RestError(519, "Thú cưng đang được ủy thác. Không thể vào đấu trường")).build();
        }
        if (characterPet.getCurrentHP() <= 0) {
            this.battleContext = null;
            return Response.status(400).entity(RestErrorFactory.BATTLE_PET_DIE).build();
        }

        Characters characters = getCurrentCharacter();
        this.battleContext = initBattleContext(characters, characterPet, battleInfo);
        if (this.battleContext == null) {
            return Response.status(400).entity(RestErrorFactory.MONSTER_NOT_EXIST).build();
        }

        putBattleContext(this.battleContext);

        logger.info(String.format("[%s] initBattle done %d ", getUserName(), (System.currentTimeMillis() - t)));

        return Response.status(200).entity(new RestSuccess(this.battleContext)).build();
    }

    private BattleContext initBattleContext(Characters characters, CharacterPet characterPet, BattleInfo battleInfo) {
        Monster monster = getMonster(battleInfo.getMonsterId());
        if ((monster == null) || (monster.isDelete())) {
            return null;
        }

        boolean isInArea = false;
        for (Area area : monster.getAreas()) {
            if (area.getId() == 1) {
                isInArea = true;
                break;
            }
        }

        if (!isInArea) {
            return null;
        }

        return initBattleContext(characterPet, monster, characters);
    }

    private void putBattleContext(BattleContext battleContext) {
        battleContext.setLastUpdate(System.currentTimeMillis());
        putBattleContext(battleContext, "AREA_BATTLE_CONTEXT");
    }
}
