package vn.vinatek.vpet.api;

import com.lthien.card.CardResponse;
import com.lthien.card.CardVerifier;
import com.lthien.card.megacard.MegaCardVerifier;
import vn.vinatek.vpet.api.pojo.*;
import vn.vinatek.vpet.api.response.RestError;
import vn.vinatek.vpet.api.response.RestErrorFactory;
import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.api.servlet.SessionCounter;
import vn.vinatek.vpet.database.dao.*;
import vn.vinatek.vpet.database.pojo.*;
import vn.vinatek.vpet.database.service.CharacterService;
import vn.vinatek.vpet.database.service.ConsignService;
import vn.vinatek.vpet.util.RandomHelper;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Path("/character")
public class CharacterRest extends BaseRest {

    @POST
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response createCharacter(CharacterRegisterInfo characterRegisterInfo) throws IOException {
        PetDAO petDAO = (PetDAO) getBean(PetDAO.class);
        String userName = this.user.getUsername();

        Characters characters = characterRegisterInfo.getCharacters();
        RestError restError = checkCreateCharacter(characters.getName());
        if (restError != null) {
            return Response.status(400).entity(restError).build();
        }

        Pet pet = petDAO.get(characterRegisterInfo.getPet().getId());
        if (pet == null || pet.getPetClass().getId() != 4) {
            return Response.status(400).entity(RestErrorFactory.CHARACTER_REGISTER_PET_INVALID).build();
        }

        CharacterLevel characterLevel = new CharacterLevel();
        characterLevel.setId(1);
        characters.setCharacterLevel(characterLevel);
        characters.setGold(10000L);

        characterDAO().save(characters);

        vn.vinatek.vpet.database.pojo.User user = userDAO().getByUserName(userName);
        user.getCharacters().add(characters);

        userDAO().update(user);

        characterDAO().addPet(getUserName(), pet.getId(), characterRegisterInfo.getPet().getName());

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/avatarupgrade/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postAvatarUpgrade() throws IOException {
        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        if (characters.getCharacterLevel().getId() >= 5) {
            return Response.status(400).entity(RestErrorFactory.CHARACTER_MAX).build();
        }

        int nextLevelId = characters.getCharacterLevel().getId() + 1;

        CharacterLevelDAO characterLevelDAO = (CharacterLevelDAO) getBean(CharacterLevelDAO.class);
        CharacterLevel characterLevel = characterLevelDAO.get(nextLevelId);

        if (characters.getLevel() < characterLevel.getLevelRequire()) {
            return Response.status(400).entity(RestErrorFactory.CHARACTER_LEVEL_NOT_ENOUGH).build();
        }

        long goldRequire = characterLevel.getGoldRequire();
        if (characters.getGold() < goldRequire) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_GOLD_NOT_ENOUGH).build();
        }

        long crystalRequire = characterLevel.getCrystalRequire();
        if (characters.getCrystal() < crystalRequire) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_CRYSTAL_NOT_ENOUGH).build();
        }

        characterDAO().upgradeAvatar(getUserName(), goldRequire, crystalRequire, characterLevel);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @GET
    @Path("/info")
    @Produces({"application/json;charset=utf-8"})
    public Response characterInfo() throws IOException {
        if (user == null) {
            return Response.status(200).entity(new RestSuccess(null)).build();
        }

        vn.vinatek.vpet.database.pojo.User user = userDAO().getByUserName(getUserName());

        return Response.status(200).entity(new RestSuccess(user.getCharacters())).build();
    }

    @GET
    @Path("/shortdetail")
    @Consumes({"application/json;charset=utf-8"})
    public Response getShortDetail() {
        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        UserShortDetail userShortDetail = new UserShortDetail(characters);

        return Response.status(200).entity(new RestSuccess(userShortDetail)).build();
    }

    @GET
    @Path("/viewdetail")
    @Consumes({"application/json;charset=utf-8"})
    public Response getViewDetail() {
        Characters characters = getCurrentCharacter();
        CharacterViewDetail characterViewDetail = null;
        if (characters != null) {
            characterViewDetail = new CharacterViewDetail(characters);
        }

        return Response.status(200).entity(new RestSuccess(characterViewDetail)).build();
    }

    @GET
    @Path("/pets")
    @Produces({"application/json;charset=utf-8"})
    public Response characterPets() throws IOException {
        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);

        List<CharacterPet> characterPets = characterPetDAO.getPets(getUserName());
        return Response.status(200).entity(new RestSuccess(characterPets)).build();
    }

    @GET
    @Path("/eggs")
    @Produces({"application/json;charset=utf-8"})
    public Response characterEggs() throws IOException {
        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);
        return Response.status(200).entity(new RestSuccess(characterPetDAO.getEggs(getCharacterId()))).build();
    }

    @POST
    @Path("/characteritemequip")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response characterItemEquip(CharacterItem characterItem) throws IOException {
        boolean canEquip = characterDAO().equipCharacterItem(getCharacterId(), characterItem);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(canEquip))).build();
    }

    @POST
    @Path("/characteritemunequip")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response characterItemUnequip(CharacterEquipment characterEquipment) throws IOException {
        boolean canUnequip = characterDAO().unequipCharacterItem(getCharacterId(), characterEquipment.getId());
        characterDAO().updateActivePetPoint(getUserName());

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(canUnequip))).build();
    }

    @POST
    @Path("/characteritemsell")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response characterItemSell(CharacterItem characterItem) throws IOException {
        boolean canSell = characterDAO().sellItem(getUserName(), characterItem);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(canSell))).build();
    }

    @POST
    @Path("/charactereggsell")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response characterEggSell(int characterPetId) throws IOException {
        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);
        CharacterPet characterPet = characterPetDAO.get(characterPetId);
        if (characterPet == null) {
            return Response.status(200).entity(new RestSuccess(Boolean.valueOf(false))).build();
        }

        characterPetDAO.delete(characterPet);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/profileimage")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postProfileImage(String profileimage) throws IOException {
        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        String imageTemp = characters.getImageUrl();

        characters.setImageUrl(profileimage);

        characterDAO().update(characters);

        FileRest.deleteImage(imageTemp);

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/equippetitem/{characterPetId}/{characterItemId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response equipPetItem(@PathParam("characterPetId") int characterPetId, @PathParam("characterItemId") int characterItemId) throws IOException {
        PetDAO petDAO = (PetDAO) getBean(PetDAO.class);

        CharacterLevel characterLevel = characterDAO().getCharacterLevel(getCharacterId());
        int numberItemsOfPet = petDAO.getNumberItemsOfPet(characterPetId);
        if (numberItemsOfPet > characterLevel.getMaxItemForPet()) {
            return Response.status(400).entity(RestErrorFactory.PET_EQUIP_ITEM_FULL).build();
        }

        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.get(characterItemId);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        Item item = characterItem.getItem();

        int itemClassId = item.getItemClass().getId();
        if ((itemClassId < 11) || (itemClassId > 20)) {
            return Response.status(400).entity(RestErrorFactory.PET_EQUIP_FAIL).build();
        }

        if (item.getItemLevel().getId() > characterLevel.getMaxItemLevelId()) {
            return Response.status(400).entity(RestErrorFactory.PET_EQUIP_FAIL).build();
        }

        characterDAO().equipPetItem(characterPetId, characterItem);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/unequippetitem/{characterPetId}/{petEquipmentId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response unequipPetItem(@PathParam("characterPetId") int characterPetId, @PathParam("petEquipmentId") int petEquipmentId) throws IOException {
        ConsignService consignService = (ConsignService) getBean(ConsignService.class);
        if (!consignService.checkAuth(characterPetId, getCharacterId())) {
            return Response.status(400).entity(new RestError(5002, "Bạn không có quyền với thú cưng này")).build();
        }

        PetEquipmentDAO petEquipmentDAO = (PetEquipmentDAO) getBean(PetEquipmentDAO.class);
        PetEquipment petEquipment = petEquipmentDAO.get(petEquipmentId);
        if (petEquipment == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        characterDAO().unequipPetItem(getCharacterId(), petEquipment);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/equippetsummon/{characterPetId}/{characterSummonId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response equipPetSummon(@PathParam("characterPetId") int characterPetId, @PathParam("characterSummonId") int characterSummonId) throws IOException {
        PetDAO petDAO = (PetDAO) getBean(PetDAO.class);
        CharacterLevel characterLevel = characterDAO().getCharacterLevel(getCharacterId());
        int numberSummonsOfPet = petDAO.getNumberSummonsOfPet(characterPetId);
        if (numberSummonsOfPet >= characterLevel.getMaxSummonForPet()) {
            return Response.status(400).entity(RestErrorFactory.PET_EQUIP_SUMMON_FULL).build();
        }

        characterDAO().equipPetSummon(characterPetId, characterSummonId);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/unequippetsummon/{characterPetId}/{petSummonId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response unequipPetSummon(@PathParam("characterPetId") int characterPetId, @PathParam("petSummonId") int petSummonId) throws IOException {
        ConsignService consignService = (ConsignService) getBean(ConsignService.class);
        if (!consignService.checkAuth(characterPetId, getCharacterId())) {
            return Response.status(400).entity(new RestError(5002, "Bạn không có quyền với thú cưng này")).build();
        }

        CharacterLevel characterLevel = characterDAO().getCharacterLevel(getCharacterId());
        int numberOfSummons = characterDAO().getNumberOfSummons(getCharacterId());
        if (numberOfSummons >= characterLevel.getMaxSummon()) {
            return Response.status(400).entity(RestErrorFactory.UNEQUIPPET_SUMMON_LIMIT).build();
        }

        characterDAO().unequipPetSummon(getCharacterId(), petSummonId);
        characterDAO().updateActivePetPoint(getUserName());

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/updateitem/{itemUpdateId}/{itemNeedId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response postUpdateItem(@PathParam("itemUpdateId") int itemUpdateId, @PathParam("itemNeedId") int itemNeedId) throws IOException {
        if (itemUpdateId == itemNeedId) {
            return Response.status(400).entity(RestErrorFactory.ITEM_INVALID).build();
        }

        ConfigDAO configDAO = (ConfigDAO) getBean(ConfigDAO.class);

        logger.info("Update itemUpdateId: " + itemUpdateId + " - itemNeedId: " + itemNeedId);

        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem itemUpdate = characterItemDAO.get(itemUpdateId);
        CharacterItem itemNeed = characterItemDAO.get(itemNeedId);
        if ((itemUpdate == null) || (itemNeed == null)) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        if ((itemUpdate.getItem().getItemClass().getId() > 10) || (itemNeed.getItem().getItemClass().getId() > 10)) {
            return Response.status(400).entity(RestErrorFactory.ITEM_INVALID).build();
        }

        if (itemUpdate.getItem().getId() != itemNeed.getItem().getId()) {
            return Response.status(400).entity(RestErrorFactory.ITEM_INVALID).build();
        }

        if (itemUpdate.getLevel() >= 10) {
            return Response.status(400).entity(RestErrorFactory.ITEM_INVALID).build();
        }

        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        long goldCost = itemUpdate.getUpdateGoldCost(configDAO.getItemUpdateGoldCost());
        if (characters.getGold() < goldCost) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_GOLD_NOT_ENOUGH).build();
        }

        characters.subGold(goldCost);

        int successRate = itemUpdate.getSuccessRate();
        int rate = RandomHelper.randInt(1, 100);
        if (rate <= successRate) {
            logger.info("Update success " + itemUpdate.getLevel() + " => " + (itemUpdate.getLevel() + 1));
            itemUpdate.setLevel(itemUpdate.getLevel() + 1);

            characterDAO().update(characters);
            characterItemDAO.delete(itemNeed);
        }

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(rate <= successRate))).build();
    }

    @POST
    @Path("/useitem/{characterItemId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response useItem(@PathParam("characterItemId") int characterItemId) throws IOException {
        logger.info("Use item: " + characterItemId);

        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.get(characterItemId);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        if (characterItem.getItem().getId() == 162) {
            Characters characters = characterDAO().getCharacterByUserName(getUserName());

            Item item = characterItem.getItem();
            long gold = RandomHelper.randInt(1, 200000);

            logger.info("Use item: " + item.getId() + " - " + item.getName() + " got " + gold);

            characters.addGold(gold);
            characterDAO().update(characters);

            characterItemDAO.delete(characterItem);

            return Response.status(200).entity(new RestSuccess("Bạn nhận được " + gold + " gold")).build();
        }

        return Response.status(400).entity(RestErrorFactory.ITEM_CAN_NOT_USE).build();
    }

    @POST
    @Path("/usepointitem/{characterPetId}/{characterItemId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response usePointItem(@PathParam("characterPetId") int characterPetId, @PathParam("characterItemId") int characterItemId) throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.get(characterItemId);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        int itemClassId = characterItem.getItem().getItemClass().getId();
        if (itemClassId != 42) {
            return Response.status(400).entity(RestErrorFactory.PET_USE_FAIL).build();
        }

        boolean isUsed = characterDAO().usePointItem(characterPetId, characterItemId);
        if (!isUsed) {
            return Response.status(400).entity(RestErrorFactory.PET_POINT_LIMIT).build();
        }

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/useevolutionitem/{characterPetId}/{characterItemId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response useEvolutionItem(@PathParam("characterPetId") int characterPetId, @PathParam("characterItemId") int characterItemId) throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.get(characterItemId);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        int itemClassId = characterItem.getItem().getItemClass().getId();
        if (itemClassId != 41) {
            return Response.status(400).entity(RestErrorFactory.PET_USE_FAIL).build();
        }

        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);
        PetDAO petDAO = (PetDAO) getBean(PetDAO.class);

        CharacterPet characterPet = characterPetDAO.get(characterPetId);
        List<Evolution> evolutions = petDAO.getEvolutions(characterPet.getPet().getId());
        if ((evolutions.size() <= 0) || (characterPet.getEvolutionIndex() >= evolutions.size() - 1)) {
            return Response.status(400).entity(RestErrorFactory.PET_END_EVOLUTION).build();
        }

        Item item = characterItem.getItem();
        Evolution nextEvolution = evolutions.get(characterPet.getEvolutionIndex() + 1);
        if (characterPet.getLevel() < nextEvolution.getRequireLevel()) {
            return Response.status(400).entity(RestErrorFactory.PET_NOT_ENOUGH_LEVEL).build();
        }

        if (nextEvolution.getItem().getId() != item.getId()) {
            return Response.status(400).entity(RestErrorFactory.PET_USE_FAIL).build();
        }

        characterPet.evolution(characterItem);
        characterPetDAO.update(characterPet);

        characterItemDAO.delete(characterItem);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/usemedicalitem/{characterPetId}/{characterItemId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response useMedicalItem(@PathParam("characterPetId") int characterPetId, @PathParam("characterItemId") int characterItemId) throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.get(characterItemId);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        int itemClassId = characterItem.getItem().getItemClass().getId();
        if ((itemClassId < 31) || (itemClassId > 40)) {
            return Response.status(400).entity(RestErrorFactory.PET_USE_FAIL).build();
        }

        characterDAO().useMedicalItem(getUserName(), characterPetId, characterItemId);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/sendpet/{characterId}/{characterPetId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response sendPet(@PathParam("characterId") int characterId, @PathParam("characterPetId") int characterPetId) throws IOException {
        ConsignService consignService = (ConsignService) getBean(ConsignService.class);
        if (!consignService.checkAuth(characterPetId, getCharacterId())) {
            return Response.status(400).entity(new RestError(5002, "Bạn không có quyền với thú cưng này")).build();
        }

        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);
        int numberOfPets = characterDAO().getNumberOfPets(getCharacterId());
        if (numberOfPets < 2) {
            return Response.status(400).entity(RestErrorFactory.PET_LESS_THAN_TWO).build();
        }

        CharacterLevel friendLevel = characterDAO().getCharacterLevel(characterId);
        int petsOfFriend = characterDAO().getNumberOfPets(characterId);
        if (petsOfFriend >= friendLevel.getMaxPet()) {
            return Response.status(400).entity(RestErrorFactory.SEND_NET_LIMIT).build();
        }

        CharacterPet characterPet = characterPetDAO.get(characterPetId);
        if (characterPet == null) {
            return Response.status(400).entity(RestErrorFactory.PET_NOT_EXIST).build();
        }

        characterPetDAO.sendPet(characterPet, characterId);
        logger.info("Send petId " + characterPet.getId() + " from " + getCharacterId() + " to " + characterId);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/sendsummon/{characterId}/{characterSummonId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response sendSummon(@PathParam("characterId") int characterId, @PathParam("characterSummonId") int characterSummonId) throws IOException {
        CharacterLevel friendLevel = characterDAO().getCharacterLevel(characterId);
        int summonsOfFriend = characterDAO().getNumberOfSummons(characterId);
        if (summonsOfFriend >= friendLevel.getMaxSummon()) {
            return Response.status(400).entity(RestErrorFactory.SEND_SUMMON_LIMIT).build();
        }

        CharacterSummonDAO characterSummonDAO = (CharacterSummonDAO) getBean(CharacterSummonDAO.class);
//        int numberOfSummons = characterDAO().getNumberOfSummons(getCharacterId());
//        if (numberOfSummons < 2) {
//            return Response.status(400).entity(RestErrorFactory.PET_LESS_THAN_TWO).build();
//        }

        CharacterSummon characterSummon = characterSummonDAO.get(characterSummonId);
        if (characterSummon == null) {
            return Response.status(400).entity(RestErrorFactory.SUMMON_NOT_EXIST).build();
        }

        characterSummonDAO.sendSummon(characterSummon, characterId);
        logger.info("Send summonId " + characterSummon.getId() + " from " + getCharacterId() + " to " + characterId);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/senditem/{characterId}/{characterItemId}")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response sendItem(@PathParam("characterId") int characterId, @PathParam("characterItemId") int characterItemId) throws IOException {
        Characters friend = characterDAO().getFriend(getCharacterId(), characterId);
        if (friend == null) {
            return Response.status(400).entity(RestErrorFactory.CHARACTER_NOT_FRIEND).build();
        }

        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.get(characterItemId);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        characterItemDAO.send(characterItem.getId(), characterId);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @GET
    @Path("/weaponitem")
    @Consumes({"application/json;charset=utf-8"})
    public Response weaponItems() throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        List<CharacterItem> items = characterItemDAO.getAllWeaponOfCharacter(getCharacterId());
        for (CharacterItem characterItem : items) {
            characterItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/evolutionitem")
    @Consumes({"application/json;charset=utf-8"})
    public Response getEvolutionItems() throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        List<CharacterItem> items = characterItemDAO.getAllEvolutionItemOfCharacter(getCharacterId());
        for (CharacterItem characterItem : items) {
            characterItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/fooditem")
    @Consumes({"application/json;charset=utf-8"})
    public Response getFoodItems() throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        List<CharacterItem> items = characterItemDAO.getAllFoodItemOfCharacter(getCharacterId());
        for (CharacterItem characterItem : items) {
            characterItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/otheritem")
    @Consumes({"application/json;charset=utf-8"})
    public Response getOtherItems() throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        List<CharacterItem> items = characterItemDAO.getAllOtherItemOfCharacter(getCharacterId());
        for (CharacterItem characterItem : items) {
            characterItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/medicalitem")
    @Consumes({"application/json;charset=utf-8"})
    public Response getMedicalItems() throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        List<CharacterItem> items = characterItemDAO.getAllMedicalItemOfCharacter(getCharacterId());
        for (CharacterItem characterItem : items) {
            characterItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/souveniritem")
    @Consumes({"application/json;charset=utf-8"})
    public Response getSouvenirItems() throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        List<CharacterItem> items = characterItemDAO.getAllSouvenirItemOfCharacter(getCharacterId());
        for (CharacterItem characterItem : items) {
            characterItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(items)).build();
    }

    @GET
    @Path("/pet")
    @Produces({"application/json;charset=utf-8"})
    public Response getPet() throws IOException {
        long t = System.currentTimeMillis();
        CharacterService characterService = (CharacterService) getBean(CharacterService.class);

        List<CharacterPet> characterPets = characterService.getCharacterPets(getUserName());
        logger.info(String.format("[%s] /character/pet %d", getUserName(), (System.currentTimeMillis() - t)));

        return Response.status(200).entity(new RestSuccess(characterPets)).build();
    }

    @GET
    @Path("/pet/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response getPet(@PathParam("id") int id) throws IOException {
        long t = System.currentTimeMillis();
        CharacterService characterService = (CharacterService) getBean(CharacterService.class);

        CharacterPet characterPet = characterService.getCharacterPet(id);
        if (characterPet == null) {
            return Response.status(400).entity(RestErrorFactory.PET_NOT_EXIST).build();
        }

        if (characterPet.isConsign()) {
            long consignTime = new Date().getTime() - characterPet.getLastConsign().getTime();
            characterPet.setConsignTime(consignTime - consignTime % (3600 * 1000));
        }

        Characters characters = getCurrentCharacter();

        calculateTotalPoint(characterPet, characters);

        logger.info(String.format("[%s] /character/pet/%d %d", getUserName(), id, (System.currentTimeMillis() - t)));

        return Response.status(200).entity(new RestSuccess(characterPet)).build();
    }

    @GET
    @Path("/summon")
    @Produces({"application/json;charset=utf-8"})
    public Response getSummon() throws IOException {
        List<CharacterSummon> summons = characterDAO().getCharacterSummons(getCharacterId());
        for (CharacterSummon summon : summons) {
            summon.getSummon().setAreas(new ArrayList<Area>());
        }

        return Response.status(200).entity(new RestSuccess(summons)).build();
    }

    @GET
    @Path("/summon/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response getSummon(@PathParam("id") int id) throws IOException {
        CharacterSummonDAO characterSummonDAO = (CharacterSummonDAO) getBean(CharacterSummonDAO.class);
        CharacterSummon characterSummon = characterSummonDAO.get(id);
        if (characterSummon == null) {
            return Response.status(400).entity(RestErrorFactory.SUMMON_NOT_EXIST).build();
        }

        characterSummon.getSummon().setAreas(new ArrayList<Area>());

        return Response.status(200).entity(new RestSuccess(characterSummon)).build();
    }

    @GET
    @Path("/characterpoint")
    @Produces({"application/json;charset=utf-8"})
    public Response getCharacterPoint() throws IOException {
        Characters characters = getCurrentCharacter();
        characters.setCharacterPets(new ArrayList<CharacterPet>());
        characters.setCharacterItems(new ArrayList<CharacterItem>());
//        characters.setCharacterEquipments(new ArrayList<CharacterEquipment>());
        characters.setCharacterSummons(new ArrayList<CharacterSummon>());

        for (CharacterEquipment characterEquipment : characters.getCharacterEquipments()) {
            characterEquipment.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        characters.setStrength(characters.getTotalStrength());
        characters.setDefense(characters.getTotalDefense());
        characters.setSpeed(characters.getTotalSpeed());
        characters.setIntelligent(characters.getTotalIntelligent());
        characters.setHp(characters.getTotalHp(true));
        characters.setFriendRequest(new ArrayList<Characters>());
        characters.setFriends(new ArrayList<Characters>());

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @GET
    @Path("/characteritem")
    @Produces({"application/json;charset=utf-8"})
    public Response characterItem() throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        List<CharacterItem> characterItems = characterItemDAO.getAllItemOfCharacter(getCharacterId());
        for (CharacterItem characterItem : characterItems) {
            characterItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return Response.status(200).entity(new RestSuccess(characterItems)).build();
    }

    @GET
    @Path("/item/{id}")
    @Consumes({"application/json;charset=utf-8"})
    public Response item(@PathParam("id") int id) throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.get(id);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        characterItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        return Response.status(200).entity(new RestSuccess(characterItem)).build();
    }

    @POST
    @Path("/activepet/{characterPetId}")
    @Consumes({"application/json;charset=utf-8"})
    public Response postActivePet(@PathParam("characterPetId") int characterPetId) throws IOException {
        ConsignService consignService = (ConsignService) getBean(ConsignService.class);
        if (!consignService.checkAuth(characterPetId, getCharacterId())) {
            return Response.status(400).entity(new RestError(5002, "Bạn không có quyền với thú cưng này")).build();
        }

        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);
        CharacterPet selectedCharacterPet = characterPetDAO.get(characterPetId);
        if (selectedCharacterPet == null) {
            return Response.status(400).entity(RestErrorFactory.PET_NOT_EXIST).build();
        }

        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        if (!canActivePet(characters.getLastActivePet())) {
            return Response.status(400).entity(RestErrorFactory.PET_ACTIVE_LIMIT).build();
        }

        if (characterPetDAO.activePet(characters.getId(), characterPetId)) {
            characters.setLastActivePet(new Date());
            characterDAO().update(characters);
        }

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/releasesummon/{characterSummonId}")
    @Consumes({"application/json;charset=utf-8"})
    public Response postReleaseSummon(@PathParam("characterSummonId") int characterSummonId) throws IOException {
        CharacterSummonDAO characterSummonDAO = (CharacterSummonDAO) getBean(CharacterSummonDAO.class);
        CharacterSummon characterSummon = characterSummonDAO.get(characterSummonId);
        if (characterSummon == null) {
            return Response.status(400).entity(RestErrorFactory.SUMMON_NOT_EXIST).build();
        }

        characterSummonDAO.delete(characterSummon);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/releasepet/{characterPetId}")
    @Consumes({"application/json;charset=utf-8"})
    public Response postReleasePet(@PathParam("characterPetId") int characterPetId) throws IOException {
        ConsignService consignService = (ConsignService) getBean(ConsignService.class);
        if (!consignService.checkAuth(characterPetId, getCharacterId())) {
            return Response.status(400).entity(new RestError(5002, "Bạn không có quyền với thú cưng này")).build();
        }

        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);
        CharacterPet selectedCharacterPet = characterPetDAO.get(characterPetId);
        if (selectedCharacterPet == null) {
            return Response.status(400).entity(RestErrorFactory.PET_NOT_EXIST).build();
        }

        int numberOfPets = characterDAO().getNumberOfPets(getCharacterId());
        if (numberOfPets < 2) {
            return Response.status(400).entity(RestErrorFactory.PET_LESS_THAN_TWO).build();
        }

        characterPetDAO.delete(selectedCharacterPet);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @GET
    @Path("/friends")
    @Consumes({"application/json;charset=utf-8"})
    public Response getFriends() {
        List<Characters> friends = characterDAO().getFriends(getCharacterId());
        return Response.status(200).entity(new RestSuccess(friends)).build();
    }

    @POST
    @Path("/unfriend/{id}")
    @Consumes({"application/json;charset=utf-8"})
    public Response postUnfriends(@PathParam("id") int id) {
        characterDAO().unFriend(getCharacterId(), id);
        characterDAO().unFriend(id, getCharacterId());

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @GET
    @Path("/friendrequests")
    @Consumes({"application/json;charset=utf-8"})
    public Response getFriendRequests() {
        List<Characters> friends = characterDAO().getFriendRequests(getCharacterId());
        return Response.status(200).entity(new RestSuccess(friends)).build();
    }

    @POST
    @Path("/friendaccept/{id}")
    @Consumes({"application/json;charset=utf-8"})
    public Response postFriendAccept(@PathParam("id") int id) {
        Characters friend = characterDAO().get(id);
        if (friend == null) {
            return Response.status(400).entity(RestErrorFactory.CHARACTER_NOT_EXIST).build();
        }

        characterDAO().acceptFriend(getCharacterId(), id);
        characterDAO().addFriend(friend.getId(), getCharacterId());

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/frienddeny/{id}")
    @Consumes({"application/json;charset=utf-8"})
    public Response postFriendDeny(@PathParam("id") int id) {
        characterDAO().denyFriendRequest(id, getCharacterId());
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @GET
    @Path("/friendfind/{searchText}")
    @Consumes({"application/json;charset=utf-8"})
    public Response getFriendResult(@PathParam("searchText") String searchText) {
        List<Characters> characters = characterDAO().findFriends(getCharacterId(), searchText);

        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @POST
    @Path("/addfriend/{id}")
    @Consumes({"application/json;charset=utf-8"})
    public Response postAddFriend(@PathParam("id") int id) {
        Characters friend = characterDAO().get(id);
        if (friend == null) {
            return Response.status(400).entity(RestErrorFactory.CHARACTER_NOT_EXIST).build();
        }

        characterDAO().addFriendRequest(getCharacterId(), friend.getId());
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @GET
    @Path("/recoverpet")
    @Consumes({"application/json;charset=utf-8"})
    public Response getRecoverPet() {
        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);
        Characters characters = getCurrentCharacter();
        List<CharacterPet> characterPets = characterPetDAO.getPetsByUserName(getUserName());
        for (CharacterPet characterPet : characterPets) {
            characterPet.setPetSummons(characterPetDAO.getPetSummons(characterPet.getId()));
        }

        characters.setCharacterPets(characterPets);
        long gold = getBestHp(characters) * 5;

        return Response.status(200).entity(new RestSuccess(Long.valueOf(gold))).build();
    }

    @POST
    @Path("/recoverpet")
    @Consumes({"application/json;charset=utf-8"})
    public Response postRecoverPet() {
        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);
        Characters characters = getCurrentCharacter();
        List<CharacterPet> characterPets = characterPetDAO.getPetsByUserName(getUserName());
        for (CharacterPet characterPet : characterPets) {
            characterPet.setPetSummons(characterPetDAO.getPetSummons(characterPet.getId()));
        }

        characters.setCharacterPets(characterPets);
        long gold = getBestHp(characters) * 5;

        if (characters.getGold() < gold) {
            return Response.status(400).entity(RestErrorFactory.ITEM_BUY_GOLD_NOT_ENOUGH).build();
        }

        gold = Math.max(0L, gold);

        characters.subGold(gold);

        for (CharacterPet characterPet : characters.getCharacterPets()) {
            int hp = getBestHp(characterPet, characters);
            characterPet.setCurrentHP(hp);
            characterPetDAO.update(characterPet);
        }

        characterDAO().update(characters);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/bankaccess")
    @Consumes({"application/json;charset=utf-8"})
    public Response postBankAccess(String pass) {
        vn.vinatek.vpet.database.pojo.User user = getUser();
        if (user.getRootPassword() == null) {
            return Response.status(400).entity(RestErrorFactory.BANK_PASS_NOT_EXIST).build();
        }

        if (user.getRootPassword().equals(pass)) {
            this.request.getSession().setAttribute("bankAccess", Boolean.valueOf(true));
            return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
        }

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(false))).build();
    }

    @POST
    @Path("/bankconfig")
    @Consumes({"application/json;charset=utf-8"})
    public Response postBankConfig(BankConfig bankConfig) {
        User user = getUser();
        if (!user.getPassword().equals(bankConfig.getPass())) {
            return Response.status(400).entity(RestErrorFactory.BANK_PASS_WRONG).build();
        }
        if ((user.getRootPassword() != null) && (!user.getRootPassword().equals(bankConfig.getOldRootpass()))) {
            return Response.status(400).entity(RestErrorFactory.BANK_ROOT_PASS_WRONG).build();
        }

        user.setRootPassword(bankConfig.getRootpass());
        userDAO().update(user);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/sendgold")
    @Consumes({"application/json;charset=utf-8"})
    public Response postSendGold(TradeInfo tradeInfo) {
        if (request.getSession().getAttribute("bankAccess") == null) {
            return Response.status(403).entity(RestErrorFactory.BANK_FORBIDDEN).build();
        }

        if (tradeInfo.getQuality() <= 0L) {
            return Response.status(400).entity(RestErrorFactory.BANK_MONEY_INVAILD).build();
        }

        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        if (characters.getGold() < tradeInfo.getQuality()) {
            return Response.status(400).entity(RestErrorFactory.BANK_GOLD_NOT_ENOUGH).build();
        }

        Characters receiver = characterDAO().get(tradeInfo.getReveiver());
        if (receiver == null) {
            return Response.status(400).entity(RestErrorFactory.BANK_CHARACTER_NOT_EXIST).build();
        }

        logger.info("Send " + tradeInfo.getQuality() + " gold from " + characters.getId() + " to " + tradeInfo.getReveiver());

        characters.subGold(tradeInfo.getQuality());
        characterDAO().update(characters);

        receiver.addGold((long) (tradeInfo.getQuality() * 0.9D));
        characterDAO().update(receiver);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/sendcrystal")
    @Consumes({"application/json;charset=utf-8"})
    public Response postSendCrystal(TradeInfo tradeInfo) {
        if (this.request.getSession().getAttribute("bankAccess") == null) {
            return Response.status(403).entity(RestErrorFactory.BANK_FORBIDDEN).build();
        }

        if (tradeInfo.getQuality() <= 0L) {
            return Response.status(400).entity(RestErrorFactory.BANK_MONEY_INVAILD).build();
        }

        Characters characters = characterDAO().getCharacterByUserName(getUserName());
        if (characters.getCrystal() < tradeInfo.getQuality()) {
            return Response.status(400).entity(RestErrorFactory.BANK_CRYSTAL_NOT_ENOUGH).build();
        }

        Characters receiver = characterDAO().get(tradeInfo.getReveiver());
        if (receiver == null) {
            return Response.status(400).entity(RestErrorFactory.BANK_CHARACTER_NOT_EXIST).build();
        }

        logger.info("Send " + tradeInfo.getQuality() + " crystal from " + characters.getId() + " to " + tradeInfo.getReveiver());

        characters.subCrystal(tradeInfo.getQuality());
        characterDAO().update(characters);

        receiver.addCrystal((long) (tradeInfo.getQuality() * 0.9D));
        characterDAO().update(receiver);

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/createitem/{itemId}")
    @Consumes({"application/json;charset=utf-8"})
    public Response postCreateItem(@PathParam("itemId") int itemId) {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);
        Item item = itemDAO.get(itemId);
        if (item == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        List<ItemRecipe> itemRecipes = itemDAO.getItemRecipes(item.getId());
        if (itemRecipes == null || itemRecipes.size() == 0) {
            return Response.status(400).entity(RestErrorFactory.ITEM_CAN_NOT_CREATE).build();
        }

        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        List<CharacterItem> characterItems = characterItemDAO.getItems(getCharacterId());

        for (ItemRecipe itemRecipe : itemRecipes) {
            if (!isHave(itemRecipe, characterItems)) {
                return Response.status(400).entity(RestErrorFactory.ITEM_NOT_ENOUGH).build();
            }
        }

        characterDAO().createItem(getCharacterId(), item);

        logger.info("Create item " + item.getId() + " success");
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/card")
    @Consumes({"application/json;charset=utf-8"})
    public Response postCheckCard(CardInfo cardInfo) throws IOException {
        try {
            logger.info("Check card Serial: " + cardInfo.getCardSerial() + " - Pin: " + cardInfo.getCardPin() + " - CardType: " + cardInfo.getCardType());
            CardVerifier cardVerifier = createCardVerifier();
            CardResponse cardResponse = cardVerifier.verify(cardInfo.getCardSerial(), cardInfo.getCardPin(), cardInfo.getCardType());
            logger.info("Check card Success: " + cardInfo.getCardSerial() + " - Pin: " + cardInfo.getCardPin() + " - CardType: " + cardInfo.getCardType());

            CardCheckDAO cardCheckDAO = (CardCheckDAO) getBean(CardCheckDAO.class);
            CardCheck cardCheck = new CardCheck();
            cardCheck.setCardPin(cardInfo.getCardPin());
            cardCheck.setCardSerial(cardInfo.getCardSerial());
            cardCheck.setCardType(cardInfo.getCardType().toString());
            cardCheck.setUser(getUser());
            cardCheck.setTransId(cardResponse.getTransId());
            cardCheck.setRealAmount(cardResponse.getRealAmount());

            cardCheckDAO.save(cardCheck);

            Characters characters = characterDAO().getCharacterByUserName(getUserName());
            characters.addCrystal(cardResponse.getRealAmount() / 1000);
            characters.addBcoin(cardResponse.getRealAmount() / 1000);
            characterDAO().update(characters);

            logger.info("nap card thanh cong");
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);

            vn.vinatek.vpet.database.pojo.User currentUser = getUser();
            long nowTick = new Date().getTime();
            long lastFailCheck = currentUser.getLastTimeCardCheckFail().getTime();
            int numberCheckFail = currentUser.getNumberCheckFail() + 1;

            if (nowTick - lastFailCheck < 1800000L) {
                if (numberCheckFail >= 3) {
                    postKillSession(getCurrentCharacter().getId());
                    currentUser.setEnable(false);
                }
            }
            else {
                numberCheckFail = 0;
            }

            currentUser.setNumberCheckFail(numberCheckFail);
            currentUser.setLastTimeCardCheckFail(new Date());
            userDAO().update(currentUser);

            if (currentUser.isEnable()) {
                logger.info("nap card that bai");
                return Response.status(400).entity(new RestSuccess(e.getMessage())).build();
            }
            return Response.status(403).entity(new RestSuccess(e.getMessage())).build();
        }

        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @GET
    @Path("/itemupdategoldcost/{characterItemId}")
    @Consumes({"application/json;charset=utf-8"})
    public Response getItemUpdateGoldCost(@PathParam("characterItemId") int characterItemId) throws IOException {
        CharacterItemDAO characterItemDAO = (CharacterItemDAO) getBean(CharacterItemDAO.class);
        CharacterItem characterItem = characterItemDAO.get(characterItemId);
        if (characterItem == null) {
            return Response.status(400).entity(RestErrorFactory.ITEM_NOT_EXIST).build();
        }

        ConfigDAO configDAO = (ConfigDAO) getBean(ConfigDAO.class);
        long goldCost = characterItem.getUpdateGoldCost(configDAO.getItemUpdateGoldCost());

        return Response.status(200).entity(new RestSuccess(Long.valueOf(goldCost))).build();
    }

    @GET
    @Path("/topgold")
    @Consumes({"application/json;charset=utf-8"})
    public Response getTopByGold() {
        List<Characters> characters = characterDAO().getTopCharactersByGold(20);
        for (Characters character : characters) {
            character.setCharacterEquipments(new ArrayList<CharacterEquipment>());
            character.setCharacterItems(new ArrayList<CharacterItem>());
            character.setCharacterPets(new ArrayList<CharacterPet>());
            character.setCharacterSummons(new ArrayList<CharacterSummon>());
            character.setFriendRequest(new ArrayList<Characters>());
            character.setFriends(new ArrayList<Characters>());
        }
        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @GET
    @Path("/topcrystal")
    @Consumes({"application/json;charset=utf-8"})
    public Response getTopByCrystal() {
        List<Characters> characters = characterDAO().getTopCharactersByCrystal(20);
        for (Characters character : characters) {
            character.setCharacterEquipments(new ArrayList<CharacterEquipment>());
            character.setCharacterItems(new ArrayList<CharacterItem>());
            character.setCharacterPets(new ArrayList<CharacterPet>());
            character.setCharacterSummons(new ArrayList<CharacterSummon>());
            character.setFriendRequest(new ArrayList<Characters>());
            character.setFriends(new ArrayList<Characters>());
        }
        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @GET
    @Path("/topexp")
    @Consumes({"application/json;charset=utf-8"})
    public Response getTopByExp() {
        List<Characters> characters = characterDAO().getTopCharactersByExp(20);
        for (Characters character : characters) {
            character.setCharacterEquipments(new ArrayList<CharacterEquipment>());
            character.setCharacterItems(new ArrayList<CharacterItem>());
            character.setCharacterPets(new ArrayList<CharacterPet>());
            character.setCharacterSummons(new ArrayList<CharacterSummon>());
            character.setFriendRequest(new ArrayList<Characters>());
            character.setFriends(new ArrayList<Characters>());
        }
        return Response.status(200).entity(new RestSuccess(characters)).build();
    }

    @GET
    @Path("/pet/top")
    @Consumes({"application/json;charset=utf-8"})
    public Response getTopPetByExp() {
        CharacterPetDAO characterPetDAO = (CharacterPetDAO) getBean(CharacterPetDAO.class);
        List<CharacterPet> characterPets = characterPetDAO.getTopCharacterPet(50);
        List<CharacterPet> result = new ArrayList<>();
        for (CharacterPet characterPet : characterPets) {
            result.add(characterPet);
        }

        return Response.status(200).entity(new RestSuccess(result)).build();
    }

    @POST
    @Path("/consign/{characterPetId}/{monsterId}")
    @Consumes({"application/json;charset=utf-8"})
    public Response consign(@PathParam("characterPetId") int characterPetId, @PathParam("monsterId") int monsterId) {
        ConsignService consignService = (ConsignService) getBean(ConsignService.class);

        if (!consignService.checkAuth(characterPetId, getCharacterId())) {
            return Response.status(400).entity(new RestError(5002, "Bạn không có quyền ủy thác với thú cưng này")).build();
        }

        if (!consignService.check(characterPetId, monsterId, getCurrentCharacter())) {
            return Response.status(400).entity(new RestError(5001, "Chỉ số nhân vật không đủ")).build();
        }

        consignService.consign(characterPetId, monsterId);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    @POST
    @Path("/unconsign/{characterPetId}")
    @Consumes({"application/json;charset=utf-8"})
    public Response unconsign(@PathParam("characterPetId") int characterPetId) {
        ConsignService consignService = (ConsignService) getBean(ConsignService.class);

        if (!consignService.checkAuth(characterPetId, getCharacterId())) {
            return Response.status(400).entity(new RestError(5002, "Bạn không có quyền với thú cưng này")).build();
        }

        consignService.unconsign(characterPetId);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    private void calculateTotalPoint(CharacterPet characterPet, Characters characters) {
        characterPet.setDefense(characterPet.getTotalDefense());
        characterPet.setHp(characterPet.getTotalHp());
        characterPet.setIntelligent(characterPet.getTotalIntelligent());
        characterPet.setSpeed(characterPet.getTotalSpeed());
        characterPet.setStrength(characterPet.getTotalStrength());

        CharacterService characterService = (CharacterService) getBean(CharacterService.class);
        characterService.calculateCharacterPetTotalPoint(characterPet, characters);
    }

    private RestError checkCreateCharacter(String characterName) throws IOException {
        if (checkNullOrEmpty(characterName)) {
            return RestErrorFactory.CHARACTER_REGISTER_CHARACTER_NAME_INVALID;
        }

        if (characterName.length() > 14) {
            return RestErrorFactory.CHARACTER_REGISTER_NAME_TOO_LONG;
        }

        if (characterDAO().getCharacterByUserName(getUserName()) != null) {
            return RestErrorFactory.CHARACTER_REGISTER_CHARACTER_EXIST;
        }

        if (characterDAO().exist(characterName)) {
            return RestErrorFactory.CHARACTER_REGISTER_CHARACTER_NAME_EXIST;
        }

        return null;
    }

    private boolean canActivePet(Date lastActive) {
        long lastActiveTick = lastActive.getTime();
        long nowTick = new Date().getTime();

        return lastActiveTick + 1440000L < nowTick;
    }

    private int getBestHp(Characters characters) {
        int bestHp = -1;

        for (CharacterPet characterPet : characters.getCharacterPets()) {
            int hp = getBestHp(characterPet, characters);

            if (hp > bestHp) {
                bestHp = hp;
            }
        }

        return bestHp;
    }

    private int getBestHp(CharacterPet characterPet, Characters characters) {
        int hp = characterPet.getTotalHp();

        if (characterPet.isActive()) {
            hp += characters.getTotalHp(true);
        }

        return hp;
    }

    private boolean isHave(ItemRecipe itemRecipe, List<CharacterItem> characterItems) {
        for (CharacterItem characterItem : characterItems) {
            if ((characterItem.getItem().getId() == itemRecipe.getItemNeed().getId()) && (characterItem.getLevel() == itemRecipe.getItemLevel())) {
                return true;
            }
        }

        return false;
    }

    private void postKillSession(int characterId) throws IOException {
        this.logger.info("Kill session of character: " + characterId);

        Map<Integer, HttpSession> sessionsMap = SessionCounter.sessions;
        HttpSession session = sessionsMap.get(Integer.valueOf(characterId));
        try {
            if (session != null) {
                sessionsMap.remove(Integer.valueOf(characterId));
                session.invalidate();
            }
        } catch (Exception localException) {
            logger.error("[postKillSession] " + localException.getMessage());
        }
    }

    private CardVerifier createCardVerifier() {
        return new MegaCardVerifier("10777", "windy1402@gmail.com", "cfde6c");
    }
}
