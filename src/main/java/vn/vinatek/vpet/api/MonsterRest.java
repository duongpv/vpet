package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.MonsterDAO;
import vn.vinatek.vpet.database.pojo.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Path("/monster")
public class MonsterRest extends BaseRest {

    @GET
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    public Response getAll() throws IOException {
        MonsterDAO monsterDAO = (MonsterDAO) getBean(MonsterDAO.class);
        List<Monster> monsters = monsterDAO.getAll();
        for (Monster monster : monsters) {
            monster.setAreas(new ArrayList<Area>());
            monster.setItems(new ArrayList<Item>());
            monster.setMonsterRewardItems(new ArrayList<MonsterRewardItem>());
        }

        return Response.status(200).entity(new RestSuccess(monsters)).build();
    }

    @POST
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response add(Monster monster) throws IOException {
        MonsterDAO monsterDAO = (MonsterDAO) getBean(MonsterDAO.class);
        monsterDAO.save(monster);

        return Response.status(200).entity(new RestSuccess(monster)).build();
    }

    @GET
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response get(@PathParam("id") int id) throws IOException {
        MonsterDAO monsterDAO = (MonsterDAO) getBean(MonsterDAO.class);
        List<Item> items = monsterDAO.getItems(id);
        for (Item item : items) {
            item.setItemRecipes(new ArrayList<ItemRecipe>());
        }
        List<MonsterRewardItem> monsterRewardItems = monsterDAO.getMonsterRewardItems(id);
        for (MonsterRewardItem monsterRewardItem : monsterRewardItems) {
            monsterRewardItem.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        List<Area> areas = monsterDAO.getAreas(id);

        Monster monster = monsterDAO.get(id);
        monster.setItems(items);
        monster.setMonsterRewardItems(monsterRewardItems);
        monster.setAreas(areas);

        return Response.status(200).entity(new RestSuccess(monster)).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces({"application/json;charset=utf-8"})
    public Response delete(@PathParam("id") int id) throws IOException {
        MonsterDAO monsterDAO = (MonsterDAO) getBean(MonsterDAO.class);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(monsterDAO.delete(id)))).build();
    }

    @PUT
    @Path("/")
    @Produces({"application/json;charset=utf-8"})
    @Consumes({"application/json;charset=utf-8"})
    public Response update(Monster monster) throws IOException {
        MonsterDAO monsterDAO = (MonsterDAO) getBean(MonsterDAO.class);
        Monster existMonster = monsterDAO.get(monster.getId());

//        copy(monster, existMonster);

        monsterDAO.update(monster);

        return Response.status(200).entity(new RestSuccess(monster)).build();
    }

    private void copy(Monster src, Monster des) {
        des.setAppearRate(src.getAppearRate());
        des.setAreas(src.getAreas());
        des.setDefense(src.getDefense());
        des.setDescription(src.getDescription());
        des.setHp(src.getHp());
        des.setImageUrl(src.getImageUrl());
        des.setIntelligent(src.getIntelligent());
        des.setItems(src.getItems());
        des.setLevel(src.getLevel());
        des.setName(src.getName());
        des.setMonsterRewardItems(src.getMonsterRewardItems());
        des.setPowerRate(src.getPowerRate());
        des.setSpeed(src.getSpeed());
        des.setStrength(src.getStrength());
        des.setQuest(src.isQuest());
    }
}
