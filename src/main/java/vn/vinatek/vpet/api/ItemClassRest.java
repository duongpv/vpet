package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.ItemClassDAO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/itemclass")
public class ItemClassRest extends BaseRest {

    @GET
    @Path("/avatar")
    @Produces({"application/json;charset=utf-8"})
    public Response getAllAvatarItem() throws IOException {
        ItemClassDAO itemClassDAO = (ItemClassDAO) getBean(ItemClassDAO.class);
        return Response.status(200).entity(new RestSuccess(itemClassDAO.getAllAvatarItemClass())).build();
    }

    @GET
    @Path("/pet")
    @Produces({"application/json;charset=utf-8"})
    public Response getAllPetItem() throws IOException {
        ItemClassDAO itemClassDAO = (ItemClassDAO) getBean(ItemClassDAO.class);
        return Response.status(200).entity(new RestSuccess(itemClassDAO.getAllPetItemClass())).build();
    }

    @GET
    @Path("/other")
    @Produces({"application/json;charset=utf-8"})
    public Response getAllOrtherItem() throws IOException {
        ItemClassDAO itemClassDAO = (ItemClassDAO) getBean(ItemClassDAO.class);
        return Response.status(200).entity(new RestSuccess(itemClassDAO.getAllOtherItemClass())).build();
    }
}
