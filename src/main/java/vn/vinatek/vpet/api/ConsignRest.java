package vn.vinatek.vpet.api;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.service.ConsignService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("/consign")
public class ConsignRest {

    @Context
    protected HttpServletRequest request;

    @POST
    @Path("/update/{partition}")
    @Consumes({"application/json;charset=utf-8"})
    public Response updateConsign(@PathParam("partition") int partition) {
        ConsignService consignService = (ConsignService) getBean(ConsignService.class);
        consignService.update(partition);
        return Response.status(200).entity(new RestSuccess(Boolean.valueOf(true))).build();
    }

    protected Object getBean(Class clazz) {
        ApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
        return appContext.getBean(clazz);
    }
}
