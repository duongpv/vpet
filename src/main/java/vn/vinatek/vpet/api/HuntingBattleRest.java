package vn.vinatek.vpet.api;

import vn.vinatek.vpet.api.pojo.BattleContext;
import vn.vinatek.vpet.api.pojo.Capture;
import vn.vinatek.vpet.api.pojo.CharacterRegisterInfo;
import vn.vinatek.vpet.api.pojo.UserPosition;
import vn.vinatek.vpet.api.response.RestError;
import vn.vinatek.vpet.api.response.RestErrorFactory;
import vn.vinatek.vpet.api.response.RestSuccess;
import vn.vinatek.vpet.database.dao.*;
import vn.vinatek.vpet.database.pojo.*;
import vn.vinatek.vpet.util.RandomHelper;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Path("/huntingbattle")
public class HuntingBattleRest extends BattleBaseRest {

    public HuntingBattleRest(@Context HttpServletRequest request) {
        super(request, "HUNTING_BATTLE_CONTEXT");
    }

    @POST
    @Path("/init/{mapid}")
    @Produces({"application/json;charset=utf-8"})
    public Response initBattle(@PathParam("mapid") int mapid, int feeType) throws IOException {
        long t = System.currentTimeMillis();
        if (feeType != 1 && feeType != 2) {
            return Response.status(400).entity(new RestError(1500, "Không thể đi săn")).build();
        }

        AreaDAO areaDAO = (AreaDAO) getBean(AreaDAO.class);
        CharacterDAO characterDAO = (CharacterDAO) getBean(CharacterDAO.class);

        Characters characters = getCurrentCharacter();
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());

        Calendar last = Calendar.getInstance();
        last.setTime(characters.getLastMapLimitRefresh());

        int dateNow = now.get(5);
        int dateLast = last.get(5);
        if (dateNow != dateLast) {
            logger.info(String.format("[%s] Reset limit", getUserName()));

            characters.setMapLimitOnDay(0);
            characters.setLastMapLimitRefresh(new Date());
        }

        Area area = areaDAO.get(mapid);
        if (feeType == 1 && (characters.getGold() < area.getGoldCost())) {
            return Response.status(400).entity(RestErrorFactory.HUNTING_GOLD_NOT_ENOUGH).build();
        }
        if (feeType == 2 && (characters.getCrystal() < area.getCrystalCost())) {
            return Response.status(400).entity(RestErrorFactory.HUNTING_CRYSTAL_NOT_ENOUGH).build();
        }

        if (area.isMapLimit()) {
            CharacterLevel characterLevel = characters.getCharacterLevel();
            int mapLimit = characters.getMapLimitOnDay();

            if (mapLimit >= characterLevel.getMaxForbiddenMap()) {
                logger.info(String.format("[%s] Day limit", getUserName()));

                return Response.status(400).entity(RestErrorFactory.HUNTING_DAY_LIMIT).build();
            }
            characters.setMapLimitOnDay(mapLimit + 1);
        }

        characters.setMapId(mapid);

        if (feeType == 1) {
            characters.setGold(characters.getGold() - area.getGoldCost());
        }
        if (feeType == 2) {
            characters.setCrystal(characters.getCrystal() - area.getCrystalCost());
        }
        characterDAO.update(characters);

        battleContext = new BattleContext();
        battleContext.setFeeType(feeType);
        battleContext.setCharacterId(characters.getId());
        battleContext.setMapID(mapid);
        battleContext.setExp(0);
        battleContext.setCurrentPos(new UserPosition(area.getStartRow(), area.getStartCol()));
        battleContext.setCharacterLevel(characters.getCharacterLevel());
        battleContext.setNumberOfPets(characters.getNumberOfPets());
        battleContext.setNumberOfSummons(characters.getNumberOfSummons());

        putBattleContext(battleContext);

        logger.info(String.format("[%s] initMapArea (mapId: %d) %d", getUserName(), mapid, (System.currentTimeMillis() - t)));
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @GET
    @Path("/current")
    @Produces({"application/json;charset=utf-8"})
    public Response getCurrentBattle() throws IOException {
        putBattleContext(battleContext);
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @POST
    @Path("/area/escape")
    @Produces({"application/json;charset=utf-8"})
    public Response postEscapeArea() throws IOException {
        CharacterDAO characterDAO = (CharacterDAO) getBean(CharacterDAO.class);
        putBattleContext(null);

        Characters characters = getCurrentCharacter();
        characters.setMapId(-1);
        characterDAO.update(characters);

        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @POST
    @Path("/battle/escape")
    @Produces({"application/json;charset=utf-8"})
    public Response postEscapeBattle() throws IOException {
        battleContext.setMonster(null);

        if (battleContext.getBattleState() == 1) {
            battleContext.setLegend(false);
        } else {
            battleContext.setPet(null);
        }

        battleContext.setBattleState(0);

        putBattleContext(battleContext);

        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @POST
    @Path("/battle/create")
    @Produces({"application/json;charset=utf-8"})
    public Response postCreateBattle(int petId) throws IOException {
        if (battleContext == null) {
            return Response.status(400).build();
        }

        Monster monster = getMonster(battleContext.getMonster().getId());
        CharacterPet characterPet = getMaxPetByUserName(getUserName());

        Monster.initPointFromPet(monster, characterPet);

        Response areaResponse = initBattle(petId, monster, 1, true);
        battleContext.setMonster(null);
        putBattleContext(battleContext);

        return areaResponse;
    }

    @POST
    @Path("/pet/escape")
    @Produces({"application/json;charset=utf-8"})
    public Response postEscapePet() throws IOException {
        battleContext.setPet(null);

        putBattleContext(battleContext);
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @POST
    @Path("/pet/capture/{netType}")
    @Produces({"application/json;charset=utf-8"})
    public Response postCapturePet(@PathParam("netType") int netType) throws IOException {
        if (battleContext.getNumberOfPets() >= battleContext.getCharacterLevel().getMaxPet()) {
            return Response.status(400).entity(RestErrorFactory.HUNTING_PET_LIMIT).build();
        }

        Capture capture = capture(netType);
        if (capture.getCode() == 400) {
            return Response.status(400).entity(RestErrorFactory.HUNTING_NET_NOT_ENOUGH).build();
        }

        int rate = RandomHelper.randInt(0, 100);
        if (capture.getCaptureRate() >= rate) {
            battleContext.setType(netType);
        } else if (capture.getRunRate() >= rate) {
            battleContext.setPet(null);
        }

        battleContext.setCaptureTurn(battleContext.getCaptureTurn() + 1);
        putBattleContext(battleContext);
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @POST
    @Path("/summon/escape")
    @Produces({"application/json;charset=utf-8"})
    public Response postEscapeSummon() throws IOException {
        battleContext.setSummon(null);

        putBattleContext(battleContext);
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @POST
    @Path("/summon/capture/{netType}")
    @Produces({"application/json;charset=utf-8"})
    public Response postCaptureSummon(@PathParam("netType") int netType) throws IOException {
        if (battleContext.getNumberOfSummons() >= battleContext.getCharacterLevel().getMaxSummon()) {
            return Response.status(400).entity(RestErrorFactory.HUNTING_SUMMON_LIMIT).build();
        }
        Capture capture = capture(netType);
        if (capture.getCode()== 400) {
            return Response.status(400).entity(RestErrorFactory.HUNTING_NET_NOT_ENOUGH).build();
        }

        int rate = RandomHelper.randInt(0, 100);
        if (capture.getCaptureRate() >= rate) {
            battleContext.setType(netType);
        } else if (capture.getRunRate() >= rate) {
            battleContext.setSummon(null);
        }

        battleContext.setCaptureTurn(battleContext.getCaptureTurn() + 1);
        putBattleContext(battleContext);
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    /*
    @POST
    @Path("/battle/petcreate")
    @Produces({"application/json;charset=utf-8"})
    public Response postCreateBattle() throws IOException {
        long t = System.currentTimeMillis();
        logger.info(String.format("[%s] postCreateBattle", getUserName()));
        Pet petAttack = battleContext.getPet();
        CharacterPet characterPet = getActivePet();

        Response areaResponse;
        if (characterPet == null) {
            areaResponse = Response.status(400).entity(RestErrorFactory.BATTLE_NO_ACTIVE_PET).build();
        } else {
            areaResponse = initBattle(characterPet, petAttack, true);
        }

        battleContext.setMonster(null);
        logger.info(String.format("[%s] postCreateBattle done %d", getUserName(), (System.currentTimeMillis() - t)));
        putBattleContext(battleContext);
        return areaResponse;
    }
    */

    @POST
    @Path("/pet/create")
    @Produces({"application/json;charset=utf-8"})
    public Response postCreatePet(CharacterRegisterInfo characterRegisterInfo) throws IOException {
        long t = System.currentTimeMillis();
        if (battleContext.getType() == 0) {
            return Response.status(400).entity(new RestSuccess(battleContext)).build();
        }

        String name = characterRegisterInfo.getPet().getName();
        if ((battleContext.getPet().getPetClass().getId() != 2) && ((name == null) || (name.equals("")))) {
            putBattleContext(battleContext);
            return Response.status(400).entity(RestErrorFactory.CHARACTER_REGISTER_PET_NAME_INVALID).build();
        }

        CharacterDAO characterDAO = (CharacterDAO) getBean(CharacterDAO.class);
        characterDAO.addPet(getUserName(), battleContext.getPet().getId(), name);

        int petId = battleContext.getPet().getId();

        battleContext.setPet(null);
        battleContext.setType(0);
        battleContext.setNumberOfPets(characterDAO().getNumberOfPets(battleContext.getCharacterId()));
        putBattleContext(battleContext);

        logger.info(String.format("[%s] postCreatePet (petId: %d, name: %s) done %d", getUserName(), petId, name, (System.currentTimeMillis() - t)));
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @POST
    @Path("/summon/create")
    @Produces({"application/json;charset=utf-8"})
    public Response postCreateSummon(CharacterRegisterInfo characterRegisterInfo) throws IOException {
        long t = System.currentTimeMillis();

        if (battleContext.getType() == 0) {
            return Response.status(400).entity(new RestSuccess(battleContext)).build();
        }

        String name = characterRegisterInfo.getSummon().getName();
        if ((name == null) || (name.equals(""))) {
            putBattleContext(battleContext);
            return Response.status(400).entity(RestErrorFactory.CHARACTER_REGISTER_PET_NAME_INVALID).build();
        }

        CharacterDAO characterDAO = (CharacterDAO) getBean(CharacterDAO.class);

        characterDAO.addSummon(getUserName(), battleContext.getSummon().getId(), name);

        battleContext.setSummon(null);
        battleContext.setType(0);
        battleContext.setNumberOfSummons(characterDAO().getNumberOfSummons(battleContext.getCharacterId()));
        putBattleContext(battleContext);

        logger.info(String.format("[%s] postCreateSummon done %d", getUserName(), (System.currentTimeMillis() - t)));
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @POST
    @Path("/battle/item")
    @Produces({"application/json;charset=utf-8"})
    public Response postGetItem() throws IOException {
        CharacterDAO characterDAO = (CharacterDAO) getBean(CharacterDAO.class);
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);

        Item item = itemDAO.get(battleContext.getRewardItems().get(0).getId());
        if (item != null) {
            characterDAO.addCharacterItem(getUserName(), item);
        }

        battleContext.setRewardItems(new ArrayList());
        putBattleContext(battleContext);
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    @POST
    @Path("/direction")
    @Produces({"application/json;charset=utf-8"})
    public Response postDirection(String direction) throws IOException {
        if (battleContext == null) {
            return Response.status(400).entity(null).build();
        }

        int step = battleContext.getStep();

        if (step > 0) {
            UserPosition currentPos = battleContext.getCurrentPos();
            switch (direction) {
                case "up":
                    currentPos.setRow(currentPos.getRow() - 1);
                    break;
                case "down":
                    currentPos.setRow(currentPos.getRow() + 1);
                    break;
                case "left":
                    currentPos.setCol(currentPos.getCol() - 1);
                    break;
                case "right":
                    currentPos.setCol(currentPos.getCol() + 1);
            }

            battleContext.setDropNet(-1);

            int rate = RandomHelper.randInt(1, 100);
            if (rate <= 5) {
                long t = System.currentTimeMillis();
                Monster monster = getAppearMonster();
                if (monster != null) {
                    logger.info(String.format("[%s] appearMonster %d", getUserName(), (System.currentTimeMillis() - t)));
                    battleContext.setMonster(monster);
                }
            }
            else if (rate <= 15) {
                if (RandomHelper.randInt(0, 1) == 0) {
                    Item item = getAreaRewardItems();
                    if (item != null) {
                        battleContext.getRewardItems().add(item);
                    }
                }
                else {
                    int netType = RandomHelper.randInt(0, 2);
                    battleContext.setDropNet(netType);

                    switch (battleContext.getDropNet()) {
                        case 0:
                            battleContext.setNormalNet(battleContext.getNormalNet() + 1);
                            break;
                        case 1:
                            battleContext.setElectricNet(battleContext.getElectricNet() + 1);
                            break;
                        case 2:
                            battleContext.setMagicNet(battleContext.getMagicNet() + 1);
                    }
                }
            }
            else if (rate <= 20) {
                Pet pet = getAppearPet();
                if ((pet != null) && (pet.getPetClass().getId() == 2)) {
                    battleContext.setLegend(true);
                } else {
                    battleContext.setLegend(false);
                }

                battleContext.setPet(pet);
            }
            else if (rate <= 25) {
                Summon summon = getAppearSummon();
                if (summon != null) {
                    battleContext.setSummon(summon);
                }
            }

            battleContext.setStep(step - 1);
        }

        if (battleContext.getStep() <= 0) {
            if (battleContext.getFeeType() == 2) {
                int exp = 1;

                Characters characters = characterDAO().getCharacterByUserName(getUserName());
                characters.addExperiences(exp);
                characterDAO().update(characters);

                battleContext.setExp(exp);
            }
        }

        putBattleContext(battleContext);
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    private Monster getAppearMonster() {
        MonsterDAO monsterDAO = (MonsterDAO) getBean(MonsterDAO.class);

        int mapId = getCharacterMapId();
        List<Integer> monsterIds = monsterDAO.getMonsterIdsInArea(mapId);
        if (monsterIds.isEmpty()) {
            return null;
        }

        return monsterDAO.getBasicInfo(monsterIds.get(RandomHelper.randInt(0, monsterIds.size() - 1)));
    }

    private Item getAreaRewardItems() {
        ItemDAO itemDAO = (ItemDAO) getBean(ItemDAO.class);

        int mapId = getCharacterMapId();
        List<Item> items = itemDAO.getMapItems(mapId);
        if (items.isEmpty()) {
            return null;
        }

        int rate = RandomHelper.randInt(0, 100);
        if (rate <= 30) {
            return getAreaGoodRewardItem(items);
        }

        return getAreaNormalRewardItem(items);
    }

    private Item getAreaGoodRewardItem(List<Item> areaRewardItems) {
        List<Item> items = new ArrayList<>();
        for (Item item : areaRewardItems) {
            if (item.getMagic() > 5) {
                items.add(item);
            }
        }

        return items.isEmpty() ? null : items.get(RandomHelper.randInt(0, items.size() - 1));
    }

    private Item getAreaNormalRewardItem(List<Item> areaRewardItems) {
        List<Item> items = new ArrayList<>();
        for (Item item : areaRewardItems) {
            if (item.getMagic() <= 5) {
                items.add(item);
            }
        }

        return items.isEmpty() ? null : items.get(RandomHelper.randInt(0, items.size() - 1));
    }

    private Pet getAppearPet() {
        AreaDAO areaDAO = (AreaDAO) getBean(AreaDAO.class);

        int mapId = getCharacterMapId();

        List<Pet> pets = areaDAO.getPetsByMapId(mapId);

        int rate = RandomHelper.randInt(0, 10);
        if (rate < 3) {
            return getAppearLegendPet(pets);
        }

        return getAppearNormalPet(pets);
    }

    private List<Pet> getAppearNormalPets(List<Pet> pets) {
        List<Pet> appearPets = new ArrayList();

        for (Pet pet : pets) {
            if ((pet.getPetClass().getId() == 1) || (pet.getPetClass().getId() == 4)) {
                appearPets.add(pet);
            }
        }

        return appearPets;
    }

    private Pet getAppearNormalPet(List<Pet> pets) {
        List<Pet> appearPets = getAppearNormalPets(pets);

        if (appearPets.isEmpty()) {
            return null;
        }

        return appearPets.get(RandomHelper.randInt(0, appearPets.size() - 1));
    }

    private Pet getAppearLegendPet(List<Pet> pets) {
        List<Pet> appearPets = getAppearLegendPets(pets);

        if (appearPets.isEmpty()) {
            return null;
        }

        int idx = RandomHelper.randInt(0, appearPets.size() - 1);

        return appearPets.get(idx);
    }

    private List<Pet> getAppearLegendPets(List<Pet> pets) {
        List<Pet> appearPets = new ArrayList();

        for (Pet pet : pets) {
            if (pet.getPetClass().getId() == 2) {
                appearPets.add(pet);
            }
        }

        return appearPets;
    }

    private Summon getAppearSummon() {
        AreaDAO areaDAO = (AreaDAO) getBean(AreaDAO.class);
        SummonDAO summonDAO = (SummonDAO) getBean(SummonDAO.class);

        int mapId = getCharacterMapId();
        List<Integer> summonIds = areaDAO.getSummonIds(mapId);
        if (summonIds.isEmpty()) {
            return null;
        }

        Summon summon = summonDAO.get(summonIds.get(RandomHelper.randInt(0, summonIds.size() - 1)));
        summon.setAreas(new ArrayList<Area>());
        return summon;
    }

    // TODO AreaBattleRest
    public Response initBattle(int petId, Monster monster, int type, boolean isHunting) throws IOException {
        long t = System.currentTimeMillis();

        CharacterPet characterPet = getCharacterPet(petId);
        if (characterPet.getCurrentHP() <= 0) {
            deleteBattleContext(request, "AREA_BATTLE_CONTEXT");
            return Response.status(400).entity(RestErrorFactory.BATTLE_PET_DIE).build();
        }

        Characters characters = getCurrentCharacter();

        if (type != 0) {
            monster.setLevel(characterPet.getLevel());
        }

        BattleContext battleContext = initBattleContext(characterPet, monster, characters);
        battleContext.setType(type);
        battleContext.setHunting(isHunting);
        putBattleContext(battleContext, "AREA_BATTLE_CONTEXT");

        logger.info(String.format("[%s] initBattle (petId: %d - monsterId: %d) done %d", getUserName(), petId, monster.getId(), (System.currentTimeMillis() - t)));
        return Response.status(200).entity(new RestSuccess(battleContext)).build();
    }

    // TODO AreaBattleRest
    public Response initBattle(CharacterPet characterPet, Pet petAttack, boolean isHunting) throws IOException {
        Monster monster = new Monster();
        monster.setName(petAttack.getName());
        monster.setLevel(0);
        monster.setImageUrl(petAttack.getImageAvatarUrl());
        monster.setPowerRate(150);

        Monster.initPointFromPet(monster, characterPet);

        return initBattle(characterPet.getId(), monster, 2, isHunting);
    }

    private void putBattleContext(BattleContext battleContext) {
        putBattleContext(battleContext, "HUNTING_BATTLE_CONTEXT");
    }

    private Capture capture(int netType) {
        Capture capture = new Capture();
        switch (netType) {
            case 1:
                if (battleContext.getNormalNet() <= 0) {
                    putBattleContext(battleContext);

                    capture.setCode(400);
                    return capture;
                }
                capture.setCaptureRate(10 - 2 * battleContext.getCaptureTurn());
                capture.setRunRate(20);

                battleContext.setNormalNet(battleContext.getNormalNet() - 1);
                break;
            case 2:
                if (battleContext.getElectricNet() <= 0) {
                    putBattleContext(battleContext);

                    capture.setCode(400);
                    return capture;
                }
                capture.setCaptureRate(15 - 3 * battleContext.getCaptureTurn());
                capture.setRunRate(30);

                battleContext.setElectricNet(battleContext.getElectricNet() - 1);
                break;
            case 3:
                if (battleContext.getMagicNet() <= 0) {
                    putBattleContext(battleContext);

                    capture.setCode(400);
                    return capture;
                }
                capture.setCaptureRate(20 - 4 * battleContext.getCaptureTurn());
                capture.setRunRate(50);

                battleContext.setMagicNet(battleContext.getMagicNet() - 1);
        }

        capture.setCode(200);
        return capture;
    }
}
