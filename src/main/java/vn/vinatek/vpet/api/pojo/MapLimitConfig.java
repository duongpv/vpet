package vn.vinatek.vpet.api.pojo;


public class MapLimitConfig {
    private long primary;

    private long preIntermediate;

    private long intermediate;

    private long high;

    private long supper;


    public long getPrimary() {
        return this.primary;
    }

    public void setPrimary(long primary) {
        this.primary = primary;
    }

    public long getPreIntermediate() {
        return this.preIntermediate;
    }

    public void setPreIntermediate(long preIntermediate) {
        this.preIntermediate = preIntermediate;
    }

    public long getIntermediate() {
        return this.intermediate;
    }

    public void setIntermediate(long intermediate) {
        this.intermediate = intermediate;
    }

    public long getHigh() {
        return this.high;
    }

    public void setHigh(long high) {
        this.high = high;
    }

    public long getSupper() {
        return this.supper;
    }

    public void setSupper(long supper) {
        this.supper = supper;
    }
}
