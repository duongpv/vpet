package vn.vinatek.vpet.api.pojo;

import com.lthien.card.CardType;

public class CardInfo {
    private CardType cardType;
    private String cardSerial;
    private String cardPin;

    public CardType getCardType() {
        return this.cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public String getCardSerial() {
        return this.cardSerial;
    }

    public void setCardSerial(String cardSerial) {
        this.cardSerial = cardSerial;
    }

    public String getCardPin() {
        return this.cardPin;
    }

    public void setCardPin(String cardPin) {
        this.cardPin = cardPin;
    }
}

