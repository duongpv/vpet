package vn.vinatek.vpet.api.pojo;


public class BattleInfo {
    private int petId;

    private int monsterId;

    public int getPetId() {
        return this.petId;
    }

    public void setPetId(int petId) {
        this.petId = petId;
    }

    public int getMonsterId() {
        return this.monsterId;
    }

    public void setMonsterId(int monsterId) {
        this.monsterId = monsterId;
    }
}
