package vn.vinatek.vpet.api.pojo;

import vn.vinatek.vpet.database.pojo.CharacterLevel;
import vn.vinatek.vpet.database.pojo.Characters;


public class UserShortDetail {
    private int id;
    private String name;
    private long gold;
    private long crystal;
    private long bcoin;
    private int level;
    private CharacterLevel characterLevel;

    public UserShortDetail(Characters characters) {
        this.id = characters.getId();
        this.name = characters.getName();
        this.gold = characters.getGold();
        this.crystal = characters.getCrystal();
        this.bcoin = characters.getBcoin();
        this.level = characters.getLevel();
        this.characterLevel = characters.getCharacterLevel();
    }

    public UserShortDetail() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getGold() {
        return this.gold;
    }

    public void setGold(long gold) {
        this.gold = gold;
    }

    public long getCrystal() {
        return this.crystal;
    }

    public void setCrystal(long crystal) {
        this.crystal = crystal;
    }

    public long getBcoin() {
        return this.bcoin;
    }

    public void setBcoin(long bcoin) {
        this.bcoin = bcoin;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public CharacterLevel getCharacterLevel() {
        return this.characterLevel;
    }

    public void setCharacterLevel(CharacterLevel characterLevel) {
        this.characterLevel = characterLevel;
    }
}
