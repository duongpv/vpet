package vn.vinatek.vpet.api.pojo;

/**
 * Created by duongpv on 5/10/2016.
 */
public class UserPosition {
    private int row;
    private int col;

    public UserPosition(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return this.row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return this.col;
    }

    public void setCol(int col) {
        this.col = col;
    }
}
