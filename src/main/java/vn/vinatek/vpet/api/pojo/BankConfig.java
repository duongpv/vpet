package vn.vinatek.vpet.api.pojo;


public class BankConfig {
    private String pass;

    private String oldRootpass;

    private String rootpass;

    public String getPass() {
        return this.pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getOldRootpass() {
        return this.oldRootpass;
    }

    public void setOldRootpass(String oldRootpass) {
        this.oldRootpass = oldRootpass;
    }

    public String getRootpass() {
        return this.rootpass;
    }

    public void setRootpass(String rootpass) {
        this.rootpass = rootpass;
    }
}
