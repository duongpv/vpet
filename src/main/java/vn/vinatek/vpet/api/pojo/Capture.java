package vn.vinatek.vpet.api.pojo;

/**
 * Created by duongpv on 6/3/16.
 */
public class Capture {

    private int code;

    private int captureRate;

    private int runRate;

    public Capture() {
        this.captureRate = 0;
        this.runRate = 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCaptureRate() {
        return captureRate;
    }

    public void setCaptureRate(int captureRate) {
        this.captureRate = captureRate;
    }

    public int getRunRate() {
        return runRate;
    }

    public void setRunRate(int runRate) {
        this.runRate = runRate;
    }
}
