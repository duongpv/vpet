package vn.vinatek.vpet.api.pojo;

import vn.vinatek.vpet.database.pojo.Characters;
import vn.vinatek.vpet.database.pojo.Pet;
import vn.vinatek.vpet.database.pojo.Summon;


public class CharacterRegisterInfo {
    private Summon summon;
    private Characters characters;
    private Pet pet;

    public Characters getCharacters() {
        return this.characters;
    }

    public void setCharacters(Characters characters) {
        this.characters = characters;
    }

    public Pet getPet() {
        return this.pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Summon getSummon() {
        return this.summon;
    }

    public void setSummon(Summon summon) {
        this.summon = summon;
    }
}