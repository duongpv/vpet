package vn.vinatek.vpet.api.pojo;

import vn.vinatek.vpet.database.pojo.Characters;
import vn.vinatek.vpet.database.pojo.IFightingEntity;
import vn.vinatek.vpet.database.pojo.Item;


public class BattleAttackInfo {
    private int strengthA;
    private int defenseA;
    private int intelligentA;
    private int speedA;
    private int defenseB;
    private int speedB;
    private int magic;

    public BattleAttackInfo() {
        this.magic = 1;
    }

    public BattleAttackInfo(Characters characters, IFightingEntity attacker, IFightingEntity defender, Item item) {
        this(characters, attacker, defender);
        this.magic = item.getMagic();
    }

    public BattleAttackInfo(Characters characters, IFightingEntity attacker, IFightingEntity defender) {
        this();

        this.strengthA = attacker.getBattleStrength();
        this.defenseA = attacker.getBattleDefense();
        this.intelligentA = attacker.getBattleIntelligent();
        this.speedA = attacker.getBattleSpeed();
        this.defenseB = defender.getBattleDefense();
        this.speedB = defender.getBattleSpeed();
    }

    public BattleAttackInfo(IFightingEntity attacker, IFightingEntity defender, Characters characters) {
        this();

        this.strengthA = attacker.getBattleStrength();
        this.defenseA = attacker.getBattleDefense();
        this.intelligentA = attacker.getBattleIntelligent();
        this.speedA = attacker.getBattleSpeed();
        this.defenseB = defender.getBattleDefense();
        this.speedB = defender.getBattleSpeed();
    }

    public BattleAttackInfo(IFightingEntity attacker, IFightingEntity defender, Characters characters, Item item) {
        this();

        this.strengthA = attacker.getBattleStrength();
        this.defenseA = attacker.getBattleDefense();
        this.intelligentA = attacker.getBattleIntelligent();
        this.speedA = attacker.getBattleSpeed();
        this.defenseB = defender.getBattleDefense();
        this.speedB = defender.getBattleSpeed();
        this.magic = item.getMagic();
    }

    public BattleAttackInfo(IFightingEntity attacker, IFightingEntity defender, Item item) {
        this(attacker, defender);
        this.magic = item.getMagic();
    }

    public BattleAttackInfo(IFightingEntity attacker, IFightingEntity defender) {
        this();

        this.strengthA = attacker.getBattleStrength();
        this.defenseA = attacker.getBattleDefense();
        this.intelligentA = attacker.getBattleIntelligent();
        this.speedA = attacker.getBattleSpeed();
        this.defenseB = defender.getBattleDefense();
        this.speedB = defender.getBattleSpeed();
    }

    public int getStrengthA() {
        return this.strengthA;
    }

    public void setStrengthA(int strengthA) {
        this.strengthA = strengthA;
    }

    public int getDefenseA() {
        return this.defenseA;
    }

    public void setDefenseA(int defenseA) {
        this.defenseA = defenseA;
    }

    public int getIntelligentA() {
        return this.intelligentA;
    }

    public void setIntelligentA(int intelligentA) {
        this.intelligentA = intelligentA;
    }

    public int getSpeedA() {
        return this.speedA;
    }

    public void setSpeedA(int speedA) {
        this.speedA = speedA;
    }

    public int getDefenseB() {
        return this.defenseB;
    }

    public void setDefenseB(int defenseB) {
        this.defenseB = defenseB;
    }

    public int getSpeedB() {
        return this.speedB;
    }

    public void setSpeedB(int speedB) {
        this.speedB = speedB;
    }

    public int getMagic() {
        return this.magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }
}