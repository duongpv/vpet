package vn.vinatek.vpet.api.pojo;

import vn.vinatek.vpet.database.pojo.User;


public class RegisterInfo extends User {

    private String captcha;

    public String getCaptcha() {
        return this.captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}