package vn.vinatek.vpet.api.pojo;

public class BattleLog {
    private int id;

    private String actionOne;

    private String actionTwo;

    private String regenHpEffect;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActionOne() {
        return this.actionOne;
    }

    public void setActionOne(String actionOne) {
        this.actionOne = actionOne;
    }

    public String getActionTwo() {
        return this.actionTwo;
    }

    public void setActionTwo(String actionTwo) {
        this.actionTwo = actionTwo;
    }

    public String getRegenHpEffect() {
        return this.regenHpEffect;
    }

    public void setRegenHpEffect(String regenHpEffect) {
        this.regenHpEffect = regenHpEffect;
    }
}
