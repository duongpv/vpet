package vn.vinatek.vpet.api.pojo;

import vn.vinatek.vpet.database.pojo.CharacterClass;
import vn.vinatek.vpet.database.pojo.CharacterEquipment;
import vn.vinatek.vpet.database.pojo.Characters;
import vn.vinatek.vpet.database.pojo.ItemRecipe;

import java.util.ArrayList;
import java.util.List;


public class CharacterViewDetail extends UserShortDetail {
    private String sex;
    private String imageUrl;
    private List<CharacterEquipment> characterEquipments;
    private CharacterClass characterClass;

    public CharacterViewDetail(Characters characters) {
        super(characters);

        this.imageUrl = characters.getImageUrl();
        this.characterEquipments = characters.getCharacterEquipments();
        this.characterClass = characters.getCharacterClass();
        this.sex = characters.getSex();

        for (CharacterEquipment characterEquipment : characterEquipments) {
            characterEquipment.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<CharacterEquipment> getCharacterEquipments() {
        return this.characterEquipments;
    }

    public void setCharacterEquipments(List<CharacterEquipment> characterEquipments) {
        this.characterEquipments = characterEquipments;
    }

    public CharacterClass getCharacterClass() {
        return this.characterClass;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
