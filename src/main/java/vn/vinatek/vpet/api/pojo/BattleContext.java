package vn.vinatek.vpet.api.pojo;

import vn.vinatek.vpet.database.pojo.*;

import java.util.ArrayList;
import java.util.List;

public class BattleContext {
    public static final int BATTLE_STARTING = 0;
    public static final int BATTLE_WIN = 1;
    public static final int BATTLE_LOSE = 2;
    public static final int BATTLE_DRAW = 3;
    private int characterId;
    private Monster monster;
    private CharacterPet characterPet;
    private int monsterCurrentHp;
    private Pet pet;
    private Summon summon;
    private int battleState = 0;
    private int experience;
    private int gold;
    private int type;
    private boolean monsterAttackFirst;
    private boolean monsterDefense;
    private UserPosition currentPos;
    private int mapID;
    private int step = 100;
    private int normalNet = 10;
    private int electricNet = 5;
    private int magicNet = 5;
    private int captureTurn = 0;
    private int dropNet = -1;
    private boolean legend = false;
    private boolean hunting = false;
    private CharacterLevel characterLevel;
    private int numberOfPets;
    private int numberOfSummons;
    private long lastUpdate;
    private int feeType;
    private boolean end;
    private int exp;

    private List<BattleLog> battleLogs = new ArrayList();

    private List<Item> rewardItems = new ArrayList();

    public int getCharacterId() {
        return characterId;
    }

    public void setCharacterId(int characterId) {
        this.characterId = characterId;
    }

    public Monster getMonster() {
        return this.monster;
    }

    public void setMonster(Monster monster) {
        this.monster = monster;
    }

    public int getBattleState() {
        return this.battleState;
    }

    public void setBattleState(int battleState) {
        this.battleState = battleState;
    }

    public int getMonsterCurrentHp() {
        return this.monsterCurrentHp;
    }

    public void setMonsterCurrentHp(int monsterCurrentHp) {
        this.monsterCurrentHp = monsterCurrentHp;
    }

    public List<Item> getRewardItems() {
        return this.rewardItems;
    }

    public void setRewardItems(List<Item> rewardItems) {
        this.rewardItems = rewardItems;
    }

    public List<BattleLog> getBattleLogs() {
        return this.battleLogs;
    }

    public void setBattleLogs(List<BattleLog> battleLogs) {
        this.battleLogs = battleLogs;
    }

    public int getExperience() {
        return this.experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public boolean isMonsterAttackFirst() {
        return this.monsterAttackFirst;
    }

    public void setMonsterAttackFirst(boolean monsterAttackFirst) {
        this.monsterAttackFirst = monsterAttackFirst;
    }

    public boolean isMonsterDefense() {
        return monsterDefense;
    }

    public void setMonsterDefense(boolean monsterDefense) {
        this.monsterDefense = monsterDefense;
    }

    public CharacterPet getCharacterPet() {
        return this.characterPet;
    }

    public void setCharacterPet(CharacterPet characterPet) {
        this.characterPet = characterPet;
    }

    public UserPosition getCurrentPos() {
        return this.currentPos;
    }

    public void setCurrentPos(UserPosition currentPos) {
        this.currentPos = currentPos;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getMapID() {
        return this.mapID;
    }

    public void setMapID(int mapID) {
        this.mapID = mapID;
    }

    public int getStep() {
        return this.step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public int getNormalNet() {
        return this.normalNet;
    }

    public void setNormalNet(int normalNet) {
        this.normalNet = normalNet;
    }

    public int getElectricNet() {
        return this.electricNet;
    }

    public void setElectricNet(int electricNet) {
        this.electricNet = electricNet;
    }

    public int getMagicNet() {
        return this.magicNet;
    }

    public void setMagicNet(int magicNet) {
        this.magicNet = magicNet;
    }

    public Pet getPet() {
        return this.pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Summon getSummon() {
        return this.summon;
    }

    public void setSummon(Summon summon) {
        this.summon = summon;
    }

    public int getCaptureTurn() {
        return this.captureTurn;
    }

    public void setCaptureTurn(int captureTurn) {
        this.captureTurn = captureTurn;
    }

    public int getDropNet() {
        return this.dropNet;
    }

    public void setDropNet(int dropNet) {
        this.dropNet = dropNet;
    }

    public int getGold() {
        return this.gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public boolean isLegend() {
        return this.legend;
    }

    public void setLegend(boolean legend) {
        this.legend = legend;
    }

    public boolean isHunting() {
        return this.hunting;
    }

    public void setHunting(boolean hunting) {
        this.hunting = hunting;
    }

    public CharacterLevel getCharacterLevel() {
        return characterLevel;
    }

    public void setCharacterLevel(CharacterLevel characterLevel) {
        this.characterLevel = characterLevel;
    }

    public int getNumberOfPets() {
        return numberOfPets;
    }

    public void setNumberOfPets(int numberOfPets) {
        this.numberOfPets = numberOfPets;
    }

    public int getNumberOfSummons() {
        return numberOfSummons;
    }

    public void setNumberOfSummons(int numberOfSummons) {
        this.numberOfSummons = numberOfSummons;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getFeeType() {
        return feeType;
    }

    public void setFeeType(int feeType) {
        this.feeType = feeType;
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }
}