package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.PetClass;

import java.util.List;


public class PetClassDAO extends BaseDAO<PetClass> {

    @Transactional(readOnly = true)
    public List getAll() {
        String query = "SELECT * FROM PetClass";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(PetClass.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public PetClass get(int id) {
        return super.get(id, PetClass.class);
    }

    @Transactional
    public PetClass save(PetClass petClass) {
        return super.save(petClass);
    }

    @Transactional
    public PetClass update(PetClass petClass) {
        return super.update(petClass);
    }
}
