package vn.vinatek.vpet.database.dao;

import com.google.gson.Gson;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.api.pojo.ItemUpdateGoldCost;
import vn.vinatek.vpet.database.pojo.Config;

import java.util.List;


public class ConfigDAO extends BaseDAO<Config> {

    @Autowired
    private Gson gson;

    @Transactional(readOnly = true)
    public Config getConfig(String id) {
        String sql = "SELECT * FROM Config WHERE Id = :id ";
        Session session = getSession();

        SQLQuery query = session.createSQLQuery(sql);
        query.addEntity(Config.class);
        query.setParameter("id", id);

        List result = query.list();

        return (Config) (result.isEmpty() ? null : result.get(0));
    }

    @Transactional(readOnly = true)
    public ItemUpdateGoldCost getItemUpdateGoldCost() {
        Config config = getConfig("ITEM_UPDATE_COST");

        return this.gson.fromJson(config.getData(), ItemUpdateGoldCost.class);
    }

    @Transactional
    public void saveItemUpdateGoldCost(ItemUpdateGoldCost itemUpdateGoldCost) {
        update("ITEM_UPDATE_COST", itemUpdateGoldCost);
    }

    @Transactional
    private void update(String name, Object o) {
        Config config = getConfig(name);

        config.setData(this.gson.toJson(o));

        super.update(config);
    }
}
