package vn.vinatek.vpet.database.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


public class BaseDAO<T> {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Transactional(readOnly = true)
    public T get(int id, Class clazz) {
        Session session = getSession();

        return (T) session.get(clazz, Integer.valueOf(id));
    }

    @Transactional
    public T save(T object) {
        Session session = getSession();
        session.save(object);

        return object;
    }

    @Transactional
    public T update(T object) {
        Session session = getSession();
        session.update(object);

        return object;
    }

    @Transactional
    public void delete(T object) {
        Session session = getSession();
        session.delete(object);
    }
}
