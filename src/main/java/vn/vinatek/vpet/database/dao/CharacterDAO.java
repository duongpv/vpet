package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.*;
import vn.vinatek.vpet.util.RandomHelper;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


public class CharacterDAO extends BaseDAO<Characters> {

    @Autowired
    private SummonDAO summonDAO;

    @Autowired
    private PetDAO petDAO;

    @Autowired
    private CharacterPetDAO characterPetDAO;

    @Autowired
    private CharacterItemDAO characterItemDAO;

    @Transactional(readOnly = true)
    public List<Characters> getAll() {
        String query = "SELECT CharacterUser.* FROM CharacterUser as CharacterUser, User as User WHERE User.Id = CharacterUser.UserId";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Characters.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Characters> getCharacters() {
        String query = "SELECT ch.id, ch.name, c.id as classid, c.name as classname FROM CharacterUser ch, CharacterClass c " +
                "WHERE ch.CharacterClassID = c.id";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        List<Object[]> objs = sqlQuery.list();
        if (objs == null) {
            return new ArrayList<>();
        }

        List<Characters> characters = new ArrayList<>();
        for (Object[] obj : objs) {
            CharacterClass characterClass = new CharacterClass();
            characterClass.setId((Integer) obj[2]);
            characterClass.setName((String) obj[3]);

            Characters character = new Characters();
            character.setId((Integer) obj[0]);
            character.setName((String) obj[1]);
            character.setCharacterClass(characterClass);
            characters.add(character);
        }

        return characters;
    }

    @Transactional(readOnly = true)
    public Characters get(int id) {
        return get(id, Characters.class);
    }

    @Transactional(readOnly = true)
    public Characters getCharacterByUserName(String userName) {
        String query = "SELECT * FROM CharacterUser c, User u WHERE c.UserId = u.Id AND u.UserName = :userName";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .addEntity(Characters.class)
                .setParameter("userName", userName)
                .setMaxResults(1);

        List<Characters> characters = sqlQuery.list();

        return characters.size() > 0 ? characters.get(0) : null;
    }

    @Transactional(readOnly = true)
    public List<Characters> getTopCharactersByGold(int top) {
        String query = "SELECT * FROM CharacterUser c ORDER BY gold DESC";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .addEntity(Characters.class)
                .setMaxResults(top);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Characters> getTopCharactersByCrystal(int top) {
        String query = "SELECT * FROM CharacterUser c ORDER BY crystal DESC";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .addEntity(Characters.class)
                .setMaxResults(top);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Characters> getTopCharactersByExp(int top) {
        String query = "SELECT * FROM CharacterUser c ORDER BY experience DESC";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .addEntity(Characters.class)
                .setMaxResults(top);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public int getId(String userName) {
        Session session = getSession();

        String query = "SELECT c.id FROM CharacterUser c, User u WHERE c.UserId = u.Id AND u.UserName = :userName";
        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query).setParameter("userName", userName);

        return (Integer) sqlQuery.uniqueResult();
    }

    @Transactional(readOnly = true)
    public int getMapId(String userName) {
        String query = "SELECT c.MapId FROM CharacterUser c, User u WHERE c.UserId = u.Id AND u.UserName = :userName";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query).setParameter("userName", userName);
        Object result = sqlQuery.uniqueResult();

        return result != null ? ((Integer) result).intValue() : 0;
    }

    @Transactional(readOnly = true)
    public boolean exist(String name) {
        String query = "SELECT * FROM CharacterUser WHERE Name = :name";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Characters.class);
        sqlQuery.setParameter("name", name);
        sqlQuery.setMaxResults(1);

        return sqlQuery.list().size() > 0;
    }

    @Transactional(readOnly = true)
    public CharacterLevel getCharacterLevel(int id) {
        String query = "SELECT l.* FROM CharacterUser c, CharacterLevel l " +
                "WHERE c.characterLevelId = l.id " +
                "AND c.id = :id";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .addEntity(CharacterLevel.class)
                .setParameter("id", id)
                .setMaxResults(1);

        List<CharacterLevel> characterLevels = sqlQuery.list();

        return characterLevels.size() > 0 ? characterLevels.get(0) : null;
    }

    @Transactional(readOnly = true)
    public int getNumberOfPets(int characterId) {
        String query = "SELECT count(*) FROM CharacterPet WHERE CharacterId = :characterId";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .setParameter("characterId", characterId);

        return ((BigInteger) sqlQuery.uniqueResult()).intValue();
    }

    @Transactional(readOnly = true)
    public int getNumberOfSummons(int characterId) {
        String query = "SELECT count(*) FROM CharacterSummon WHERE CharacterId = :characterId";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .setParameter("characterId", characterId);

        return ((BigInteger) sqlQuery.uniqueResult()).intValue();
    }

    @Transactional(readOnly = true)
    public List<CharacterEquipment> getCharacterEquipments(int characterId) {
        String query = "SELECT * FROM CharacterEquipment e WHERE e.CharacterId = :characterId";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .addEntity(CharacterEquipment.class)
                .setParameter("characterId", characterId);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<CharacterSummon> getCharacterSummons(int characterId) {
        String query = "SELECT * FROM CharacterSummon s WHERE s.CharacterId = :characterId";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .addEntity(CharacterSummon.class)
                .setParameter("characterId", characterId);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Characters> getFriends(int characterId) {
        String query = "SELECT cf.friendId, c.name FROM CharacterFriend cf, CharacterUser c " +
                "WHERE cf.friendId = c.id AND cf.characterId = :characterId";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query).setParameter("characterId", characterId);
        List<Object[]> result = sqlQuery.list();
        if (result == null) {
            return new ArrayList<>();
        }

        List<Characters> friends = new ArrayList<>();
        for (Object[] obj : result) {
            Characters character = new Characters();
            character.setId((Integer) obj[0]);
            character.setName((String) obj[1]);
            friends.add(character);
        }

        return friends;
    }

    @Transactional(readOnly = true)
    public Characters getFriend(int characterId, int friendId) {
        List<Characters> friends = getFriends(characterId);
        for (Characters characters : friends) {
            if (characters.getId() == friendId) {
                return characters;
            }
        }

        return null;
    }

    @Transactional(readOnly = true)
    public List<Characters> getFriendRequests(int characterId) {
        String query = "SELECT c.id, c.name FROM  CharacterUser c, FriendRequest r " +
                "WHERE c.id = r.CharacterId AND r.FriendId = :characterId";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query).setParameter("characterId", characterId);
        List<Object[]> result = sqlQuery.list();
        if (result == null) {
            return new ArrayList<>();
        }

        List<Characters> friends = new ArrayList<>();
        for (Object[] obj : result) {
            Characters character = new Characters();
            character.setId((Integer) obj[0]);
            character.setName((String) obj[1]);
            friends.add(character);
        }

        return friends;
    }

    @Transactional(readOnly = true)
    public List<Characters> findFriends(int characterId, String searchText) {
        String query = "SELECT cu.id, cu.Name FROM CharacterUser AS cu " +
                "WHERE cu.id <> :id " +
                "AND cu.name like :searchText " +
                "AND cu.id NOT IN (SELECT DISTINCT FriendId FROM CharacterFriend WHERE CharacterId = :id) " +
                "AND cu.id NOT IN (SELECT DISTINCT CharacterId FROM FriendRequest WHERE FriendId = :id)";

        Session session = getSession();
        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .setParameter("id", Integer.valueOf(characterId))
                .setParameter("searchText", "%" + searchText + "%")
                .setMaxResults(100);

        List<Object[]> rs = sqlQuery.list();
        if (rs == null) {
            return new ArrayList<>();
        }

        List<Characters> characters = new ArrayList<>();
        for (Object[] obj : rs) {
            Characters ch = new Characters();
            ch.setId((Integer) obj[0]);
            ch.setName((String) obj[1]);
            characters.add(ch);
        }

        return characters;
    }

    @Transactional
    public Characters save(Characters characters) {
        super.save(characters);
        return characters;
    }

    @Transactional
    public Characters update(Characters characters) {
        super.update(characters);
        return characters;
    }

    @Transactional
    public void delete(Characters characters) {
        super.delete(characters);
    }

    @Transactional
    public void addCharacterItems(int characterId, List<Item> items) {
        Session session = getSession();
        for (Item item : items) {
            SQLQuery query = session.createSQLQuery("INSERT INTO CharacterItem(CharacterId, ItemId, Endurance, Level) VALUES(:CharacterId, :ItemId, :Endurance, :Level)");
            query.setParameter("CharacterId", Integer.valueOf(characterId));
            query.setParameter("ItemId", Integer.valueOf(item.getId()));
            query.setParameter("Endurance", Integer.valueOf(item.getEndurance()));
            query.setParameter("Level", Integer.valueOf(0));

            query.executeUpdate();
        }
    }

    @Transactional
    public void addCharacterItem(String userName, Item item) {
        Integer characterId = getId(userName);
        List<Item> items = new ArrayList<>();
        items.add(item);

        addCharacterItems(characterId, items);
    }

    @Transactional
    public void addCharacterItem(int characterId , Item item) {
        List<Item> items = new ArrayList<>();
        items.add(item);

        addCharacterItems(characterId, items);
    }

    @Transactional
    public void addPet(String userName, int petId, String petName) {
        Session session = getSession();
        Characters characters = getCharacterByUserName(userName);
        if (characters != null) {
            Pet pet = petDAO.get(petId);
            CharacterPet characterPet = createCharacterPet(pet, petName);
            session.save(characterPet);
            characters.getCharacterPets().add(characterPet);
        }
    }

    @Transactional
    public void addPet(int characterId, int petId, String petName) {
        Session session = getSession();
        Characters characters = get(characterId);
        if (characters != null) {
            Pet pet = petDAO.get(petId);
            CharacterPet characterPet = createCharacterPet(pet, petName);
            session.save(characterPet);
            characters.getCharacterPets().add(characterPet);
        }
    }

    @Transactional
    public void addSummon(String userName, int summonId, String name) {
        Session session = getSession();
        Characters characters = getCharacterByUserName(userName);
        if (characters != null) {
            Summon summon = summonDAO.get(summonId);
            if (summon == null) {
                return;
            }

            CharacterSummon characterSummon = new CharacterSummon();
            characterSummon.setSummon(summon);
            session.save(characterSummon);
            characters.getCharacterSummons().add(characterSummon);
        }
    }

    @Transactional
    public void addSummon(int characterId, int summonId, String name) {
        Session session = getSession();
        Characters characters = get(characterId);
        if (characters != null) {
            Summon summon = summonDAO.get(summonId);
            if (summon == null) {
                return;
            }

            CharacterSummon characterSummon = new CharacterSummon();
            characterSummon.setSummon(summon);
            session.save(characterSummon);
            characters.getCharacterSummons().add(characterSummon);
        }
    }

    @Transactional
    public void equipPetSummon(int characterPetId, int characterSummonId) {
        Session session = getSession();
        SQLQuery query = session.createSQLQuery("SELECT SummonID FROM CharacterSummon WHERE id = :characterSummonId");
        query.setParameter("characterSummonId", Integer.valueOf(characterSummonId));
        Object sid = query.uniqueResult();
        if (sid == null) {
            return;
        }

        int summonId = ((Integer) sid).intValue();
        query = session.createSQLQuery("INSERT INTO PetSummon(PetID, SummonID) VALUES(:characterPetId, :summonId)");
        query.setParameter("characterPetId", Integer.valueOf(characterPetId));
        query.setParameter("summonId", Integer.valueOf(summonId));
        query.executeUpdate();

        query = session.createSQLQuery("DELETE FROM CharacterSummon WHERE id = :characterSummonId");
        query.setParameter("characterSummonId", Integer.valueOf(characterSummonId));
        query.executeUpdate();
    }

    @Transactional
    public void unequipPetSummon(int characterId, int petSummonId) {
        Session session = getSession();
        SQLQuery query = session.createSQLQuery("SELECT SummonID FROM PetSummon WHERE id = :petSummonId");
        query.setParameter("petSummonId", Integer.valueOf(petSummonId));
        Object sid = query.uniqueResult();
        if (sid == null) {
            return;
        }

        int summonId = ((Integer) sid).intValue();
        query = session.createSQLQuery("INSERT INTO CharacterSummon(CharacterId, SummonID) VALUES(:characterId, :summonId)");
        query.setParameter("characterId", Integer.valueOf(characterId));
        query.setParameter("summonId", Integer.valueOf(summonId));
        query.executeUpdate();

        query = session.createSQLQuery("DELETE FROM PetSummon WHERE id = :petSummonId");
        query.setParameter("petSummonId", Integer.valueOf(petSummonId));
        query.executeUpdate();
    }

    @Transactional
    public void equipPetItem(int characterPetId, CharacterItem characterItem) {
        Session session = getSession();

        SQLQuery query = session.createSQLQuery("INSERT INTO PetEquipment(PetID, ItemID, Endurance) VALUES(:petID, :itemID, :endurance)");
        query.setParameter("petID", Integer.valueOf(characterPetId));
        query.setParameter("itemID", Integer.valueOf(characterItem.getItem().getId()));
        query.setParameter("endurance", Integer.valueOf(characterItem.getEndurance()));
        query.executeUpdate();

        query = session.createSQLQuery("DELETE FROM CharacterItem WHERE id = :CharacterItemId");
        query.setParameter("CharacterItemId", Integer.valueOf(characterItem.getId()));
        query.executeUpdate();
    }

    @Transactional
    public void unequipPetItem(int characterId, PetEquipment petEquipment) {
        Session session = getSession();

        SQLQuery query = session.createSQLQuery("INSERT INTO CharacterItem(CharacterId, ItemId, Endurance, Level) VALUES(:characterId, :itemId, :endurance, 0)");
        query.setParameter("characterId", Integer.valueOf(characterId));
        query.setParameter("itemId", Integer.valueOf(petEquipment.getItem().getId()));
        query.setParameter("endurance", Integer.valueOf(petEquipment.getEndurance()));
        query.executeUpdate();

        query = session.createSQLQuery("DELETE FROM PetEquipment WHERE id = :petEquipmentId");
        query.setParameter("petEquipmentId", Integer.valueOf(petEquipment.getId()));
        query.executeUpdate();
    }

    @Transactional
    public boolean equipCharacterItem(int characterId, CharacterItem characterItem) {
        CharacterItem selectedItem = characterItemDAO.get(characterItem.getId());
        if (selectedItem == null) {
            return false;
        }

        Characters characters = get(characterId);
        CharacterClass characterClass = characterItem.getItem().getCharacterClass();
        if ((characterClass == null) || (characterClass.getId() != characters.getCharacterClass().getId())) {
            return false;
        }

        CharacterEquipment usedItem = getCharacterEquipmentItem(characters.getCharacterEquipments(), selectedItem.getItem());
        if (usedItem != null) {
            unequipCharacterItem(characters, usedItem);
        }

        characters.getCharacterItems().remove(selectedItem);

        usedItem = new CharacterEquipment();
        usedItem.setItem(selectedItem.getItem());
        usedItem.setLevel(selectedItem.getLevel());
        getSession().save(usedItem);

        characters.getCharacterEquipments().add(usedItem);
        update(characters);

        return true;
    }

    @Transactional
    public boolean unequipCharacterItem(int characterId, int id) {
        Characters characters = get(characterId);
        CharacterEquipment characterEquipment = getCharacterEquipment(characters, id);
        if (characterEquipment == null) {
            return false;
        }

        unequipCharacterItem(characters, characterEquipment);
        update(characters);

        return true;
    }

    @Transactional
    public void updateActivePetPoint(String userName) {
        Characters characters = getCharacterByUserName(userName);
        if (characters == null) {
            return;
        }

        CharacterPet characterPet = getActiveCharacterPet(characters);
        if (characterPet != null) {
            int totalHp = characterPet.getTotalHp() + characters.getTotalHp(true);
            totalHp = Math.min(characterPet.getCurrentHP(), totalHp);

            characterPet.setCurrentHP(totalHp);
            update(characters);
        }
    }

    @Transactional
    public void upgradeAvatar(String userName, long goldRequire, long crystalRequire, CharacterLevel characterLevel) {
        Characters characters = getCharacterByUserName(userName);
        if (characters == null) {
            return;
        }

        characters.subCrystal(crystalRequire);
        characters.subGold(goldRequire);
        characters.setCharacterLevel(characterLevel);

        CharacterPet characterPet = getActiveCharacterPet(characters);
        if (characterPet != null) {
            CharacterClass characterClass = characters.getCharacterClass();
            if (characterClass.getId() == 1) {
                characterPet.setStrength((float) (characterPet.getStrength() * 1.02));
            }
            else if (characterClass.getId() == 2) {
                characterPet.setDefense((float) (characterPet.getDefense() * 1.02));
            }
            else if (characterClass.getId() == 3) {
                characterPet.setIntelligent((float) (characterPet.getIntelligent() * 1.02));
            }
            else if (characterClass.getId() == 4) {
                characterPet.setSpeed((float) (characterPet.getSpeed() * 1.02));
            }
        }

        update(characters);
    }

    @Transactional
    public boolean sellItem(String userName, CharacterItem characterItem) {
        Characters characters = getCharacterByUserName(userName);
        if (characters == null) {
            return false;
        }
        CharacterItem selectedItem = characterItemDAO.get(characterItem.getId());
        if (selectedItem == null) {
            return false;
        }

        characters.setGold(characters.getGold() + selectedItem.getItem().getGoldCostSell());
        update(characters);

        characterItemDAO.delete(selectedItem);
        return true;
    }

    @Transactional
    public boolean useMedicalItem(String userName, int characterPetId, int characterItemId) {
        CharacterItem characterItem = characterItemDAO.get(characterItemId);
        if (characterItem == null) {
            return false;
        }

        Characters characters = getCharacterByUserName(userName);
        CharacterPet characterPet = characterPetDAO.get(characterPetId);

        int hp = characterPet.getCurrentHP() + characterItem.getItem().getMagic() * 100;
        int limit = characterPet.getTotalHp() + characters.getTotalHp(characterPet.isActive());

        hp = Math.min(hp, limit);

        characterPet.setCurrentHP(hp);

        update(characters);
        characterItemDAO.delete(characterItem);

        return true;
    }

    @Transactional
    public boolean usePointItem(int characterPetId, int characterItemId) {
        CharacterItem characterItem = characterItemDAO.get(characterItemId);
        if (characterItem == null) {
            return false;
        }

        CharacterPet characterPet = characterPetDAO.get(characterPetId);
        boolean isUsed = characterPet.usePointItem(characterItem);
        if (isUsed) {
            characterPetDAO.update(characterPet);
            characterItemDAO.delete(characterItem);
        }

        return isUsed;
    }

    @Transactional
    public boolean createItem(int characterId, Item item) {
        Characters characters = get(characterId);
        if (characters == null) {
            return false;
        }

        int rate = RandomHelper.randInt(0, 100);
        int successRate = 100 - item.getItemLevel().getId() * 10;

        List<ItemRecipe> itemRecipes = item.getItemRecipes();
        if (rate <= successRate) {
            for (ItemRecipe itemRecipe : itemRecipes) {
                removeItem(itemRecipe, characters);
            }

            CharacterItem characterItem = new CharacterItem();
            characterItem.setItem(item);
            characterItem.setEndurance(item.getEndurance());

            getSession().save(characterItem);
            characters.getCharacterItems().add(characterItem);
        }
        else {
            for (ItemRecipe itemRecipe : itemRecipes) {
                if (itemRecipe.getItemLevel() != 10) {
                    removeItem(itemRecipe, characters);
                }
            }
        }

        update(characters);
        return true;
    }

    @Transactional
    public boolean addFriendRequest(int characterId, int friendId) {
        Session session = getSession();
        SQLQuery query = session.createSQLQuery("INSERT INTO FriendRequest(CharacterId, FriendId) VALUES(:characterId, :friendId)");
        query.setParameter("characterId", Integer.valueOf(characterId));
        query.setParameter("friendId", Integer.valueOf(friendId));

        query.executeUpdate();
        return true;
    }

    @Transactional
    public boolean acceptFriend(int characterId, int friendId) {
        denyFriendRequest(friendId, characterId);
        addFriend(characterId, friendId);
        return true;
    }

    @Transactional
    public boolean addFriend(int characterId, int friendId) {
        Session session = getSession();
        SQLQuery query = session.createSQLQuery("INSERT INTO CharacterFriend(CharacterId, FriendId) VALUES(:characterId, :friendId)");
        query.setParameter("characterId", Integer.valueOf(characterId));
        query.setParameter("friendId", Integer.valueOf(friendId));

        query.executeUpdate();
        return true;
    }

    @Transactional
    public boolean denyFriendRequest(int characterId, int friendId) {
        Session session = getSession();
        SQLQuery query = session.createSQLQuery("DELETE FROM FriendRequest WHERE CharacterId = :characterId AND FriendId = :friendId");
        query.setParameter("characterId", Integer.valueOf(characterId));
        query.setParameter("friendId", Integer.valueOf(friendId));

        query.executeUpdate();
        return true;
    }

    @Transactional
    public boolean unFriend(int characterId, int friendId) {
        Session session = getSession();
        SQLQuery query = session.createSQLQuery("DELETE FROM CharacterFriend WHERE CharacterId = :characterId AND FriendId = :friendId");
        query.setParameter("characterId", Integer.valueOf(characterId));
        query.setParameter("friendId", Integer.valueOf(friendId));

        query.executeUpdate();
        return true;
    }

    private CharacterPet createCharacterPet(Pet pet, String name) {
        CharacterPet characterPet = new CharacterPet();
        characterPet.setPet(pet);
        characterPet.setName(name);
        characterPet.setDescription("");
        characterPet.setActive(true);

        initCharacterPet(characterPet);

        return characterPet;
    }

    private void initCharacterPet(CharacterPet characterPet) {
        Pet pet = characterPet.getPet();

        float growthRate = Pet.genGrowthRate(pet.getPetClass());
        float mutantPoint = Pet.genMutantPoint(pet.getPetClass());

        characterPet.setCurrentHP(pet.getHp());
        characterPet.setGrowthRate(growthRate);
        characterPet.setMutantPoint(mutantPoint);

        if (pet.getPetClass().getId() == 2) {
            characterPet.setEvolutionIndex(-3);
        }
    }

    private CharacterPet getActiveCharacterPet(Characters characters) {
        for (CharacterPet characterPet : characters.getCharacterPets()) {
            if (characterPet.isActive()) {
                return characterPet;
            }
        }

        return null;
    }

    private CharacterEquipment getCharacterEquipmentItem(List<CharacterEquipment> characterEquipments, Item item) {
        for (CharacterEquipment characterEquipment : characterEquipments) {
            if (characterEquipment.getItem().getItemClass().getId() == item.getItemClass().getId()) {
                return characterEquipment;
            }
        }

        return null;
    }

    private void unequipCharacterItem(Characters characters, CharacterEquipment characterEquipment) {
        characters.getCharacterEquipments().remove(characterEquipment);
        addCharacterItem(characters, characterEquipment);
    }

    private void addCharacterItem(Characters characters, CharacterEquipment characterEquipment) {
        CharacterItem characterItem = new CharacterItem();
        characterItem.setItem(characterEquipment.getItem());
        characterItem.setEndurance(characterEquipment.getItem().getEndurance());
        characterItem.setLevel(characterEquipment.getLevel());

        getSession().save(characterItem);
        characters.getCharacterItems().add(characterItem);
    }

    private CharacterEquipment getCharacterEquipment(Characters characters, int id) {
        for (CharacterEquipment characterEquipment : characters.getCharacterEquipments()) {
            if (characterEquipment.getId() == id) {
                return characterEquipment;
            }
        }

        return null;
    }

    public void removeItem(ItemRecipe itemRecipe, Characters characters) {
        CharacterItem removeItem = null;
        for (CharacterItem characterItem : characters.getCharacterItems()) {
            if ((characterItem.getItem().getId() == itemRecipe.getItemNeed().getId()) && (characterItem.getLevel() == itemRecipe.getItemLevel())) {
                removeItem = characterItem;
                break;
            }
        }

        if (removeItem != null) {
            characters.getCharacterItems().remove(removeItem);
        }
    }
}