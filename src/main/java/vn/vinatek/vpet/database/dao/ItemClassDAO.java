package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.ItemClass;

import java.util.List;

public class ItemClassDAO extends BaseDAO<ItemClass> {

    @Transactional(readOnly = true)
    public List getAllAvatarItemClass() {
        String query = "SELECT * FROM ItemClass where id < 11";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(ItemClass.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public ItemClass get(int id) {
        return get(id, ItemClass.class);
    }

    @Transactional(readOnly = true)
    public List getAllPetItemClass() {
        String query = "SELECT * FROM ItemClass where id > 10 AND id < 21";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(ItemClass.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List getAllOtherItemClass() {
        String query = "SELECT * FROM ItemClass where id > 20";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(ItemClass.class);

        return sqlQuery.list();
    }
}
