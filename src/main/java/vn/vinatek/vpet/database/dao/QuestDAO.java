package vn.vinatek.vpet.database.dao;

import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.Quest;


public class QuestDAO extends BaseDAO<Quest> {

    @Transactional(readOnly = true)
    public Quest get(int id) {
        return super.get(id, Quest.class);
    }

    @Transactional(readOnly = true)
    public boolean exist(int id) {
        Quest quest = get(id);

        return quest != null;
    }

    @Transactional
    public boolean delete(int id) {
        Quest quest = get(id);

        if (quest != null) {
            delete(quest);
        }

        return true;
    }
}