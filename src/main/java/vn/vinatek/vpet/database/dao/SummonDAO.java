package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.CharacterSummon;
import vn.vinatek.vpet.database.pojo.Summon;

import java.util.List;


public class SummonDAO extends BaseDAO<Summon> {

    @Transactional(readOnly = true)
    public List<Summon> getAll() {
        String query = "SELECT * FROM Summon WHERE IsDelete = FALSE";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Summon.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public Summon get(int id) {
        return super.get(id, Summon.class);
    }

    @Transactional
    public Summon save(Summon summon) {
        return super.save(summon);
    }

    @Transactional
    public Summon update(Summon summon) {
        return super.update(summon);
    }

    @Transactional
    public boolean delete(int id) {
        Summon summon = get(id);

        if (summon == null) {
            return false;
        }

        summon.setDelete(true);
        update(summon);

        return true;
    }

    @Transactional(readOnly = true)
    public List<Summon> getSummonShop() {
        Session session = getSession();
        String query = "SELECT * FROM Summon as s where s.IsSell = true ";

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Summon.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Summon> getPointSummonByMagic(int begin, int end) {
        String query = "SELECT * FROM Summon WHERE IsDelete = FALSE AND IsQuest = TRUE AND SummonClassID = 1 AND Magic >= :begin AND Magic <= :end";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Summon.class);
        sqlQuery.setParameter("begin", Integer.valueOf(begin));
        sqlQuery.setParameter("end", Integer.valueOf(end));

        return sqlQuery.list();
    }
}
