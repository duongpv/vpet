package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.Area;
import vn.vinatek.vpet.database.pojo.AreaRewardItem;
import vn.vinatek.vpet.database.pojo.Pet;
import vn.vinatek.vpet.database.pojo.PetClass;

import java.util.ArrayList;
import java.util.List;


public class AreaDAO extends BaseDAO<Area> {

    @Transactional(readOnly = true)
    public List<Area> getAll() {
        String query = "SELECT * FROM Area";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Area.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public Area get(int id) {
        return get(id, Area.class);
    }

    @Transactional(readOnly = true)
    public List<Pet> getPetsByMapId(int areaId) {
        String query = "SELECT Id, Name, imageAvatarUrl, HP, PetClassID FROM Pet " +
                "WHERE ID IN (SELECT DISTINCT(PetID) FROM PetArea WHERE AreaID = :areaId) " +
                "AND IsDelete = FALSE " +
                "AND IsCatched = TRUE";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("areaId", Integer.valueOf(areaId));

        List<Object[]> objs = sqlQuery.list();
        List<Pet> pets = new ArrayList<>();
        for (Object[] obj : objs) {
            PetClass petClass = new PetClass();
            petClass.setId((Integer) obj[4]);

            Pet pet = new Pet();
            pet.setId((Integer) obj[0]);
            pet.setName((String) obj[1]);
            pet.setImageAvatarUrl((String) obj[2]);
            pet.setHp(((Float) obj[3]).intValue());
            pet.setPetClass(petClass);
            pets.add(pet);
        }

        return pets;
    }

    @Transactional(readOnly = true)
    public List<Integer> getSummonIds(int areaId) {
        String query = "SELECT Summon.id FROM Area as Area, SummonArea as SummonArea, Summon as Summon " +
                "WHERE Area.id = SummonArea.AreaID " +
                "AND Summon.id = SummonArea.SummonID " +
                "AND Area.id = :areaId " +
                "AND Summon.IsDelete = FALSE";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("areaId", Integer.valueOf(areaId));

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<AreaRewardItem> getAreaRewardItems(int areaId) {
        String query = "SELECT * FROM AreaItem WHERE AreaID = :areaId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("areaId", Integer.valueOf(areaId));
        sqlQuery.addEntity(AreaRewardItem.class);

        return sqlQuery.list();
    }
}
