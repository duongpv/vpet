package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.News;

import java.util.List;

public class NewsDAO extends BaseDAO<News> {

    @Transactional(readOnly = true)
    public News get(int id) {
        return get(id, News.class);
    }

    @Transactional
    public News update(News news) {
        return super.update(news);
    }

    @Transactional(readOnly = true)
    public List<News> getAll() {
        String query = "SELECT * FROM News";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);

        sqlQuery.addEntity(News.class);

        return sqlQuery.list();
    }
}
