package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.Area;
import vn.vinatek.vpet.database.pojo.Item;
import vn.vinatek.vpet.database.pojo.Monster;
import vn.vinatek.vpet.database.pojo.MonsterRewardItem;

import java.util.ArrayList;
import java.util.List;


public class MonsterDAO extends BaseDAO<Monster> {

    @Transactional(readOnly = true)
    public List<Monster> getAll() {
        String query = "SELECT * FROM Monster WHERE IsDelete = false";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Monster.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Monster> getMonstersInArea(int areaID) {
        String query = "SELECT * FROM Monster as m, MonsterArea as ma WHERE m.IsDelete = false AND ma.AreaID = :areaID AND m.Id = ma.MonsterID ORDER BY m.Level";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("areaID", Integer.valueOf(areaID));
        sqlQuery.addEntity(Monster.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Integer> getMonsterIdsInArea(int areaID) {
        String query = "SELECT m.id FROM Monster as m, MonsterArea as ma WHERE m.IsDelete = false AND ma.AreaID = :areaID AND m.Id = ma.MonsterID ORDER BY m.Level";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("areaID", Integer.valueOf(areaID));

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public Monster get(int id) {
        return super.get(id, Monster.class);
    }

    @Transactional(readOnly = true)
    public Monster getBasicInfo(int id) {
        Session session = getSession();

        String query = "SELECT id, name, description, imageUrl " +
                "FROM Monster " +
                "WHERE id = :id";

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("id", Integer.valueOf(id));

        Object[] m = (Object[]) sqlQuery.uniqueResult();
        if (m == null) {
            return null;
        }

        Monster monster = new Monster();
        monster.setId((Integer) m[0]);
        monster.setName((String) m[1]);
        monster.setDescription((String) m[2]);
        monster.setImageUrl((String) m[3]);

        return monster;
    }

    @Transactional(readOnly = true)
    public List<Area> getAreas(int id) {
        Session session = getSession();

        String query = "SELECT AreaID FROM MonsterArea WHERE MonsterID = :id";

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("id", Integer.valueOf(id));

        List<Integer> areaIds = sqlQuery.list();
        List<Area> areas = new ArrayList<>();
        for (Integer areaId : areaIds) {
            Area area = new Area();
            area.setId(areaId);
            areas.add(area);
        }

        return areas;
    }

    @Transactional(readOnly = true)
    public List<Item> getItems(int id) {
        Session session = getSession();

        String query = "SELECT i.* FROM Item i, MonsterItem m WHERE i.id = m.itemID AND m.monsterID = :id";

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("id", Integer.valueOf(id));
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<MonsterRewardItem> getMonsterRewardItems(int id) {
        Session session = getSession();

        String query = "SELECT * FROM MonsterRewardItem WHERE MonsterId = :id";
        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("id", Integer.valueOf(id));
        sqlQuery.addEntity(MonsterRewardItem.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<MonsterRewardItem> getMonsterRewardItems(List<Integer> itemIds) {
        Session session = getSession();

        String query = "SELECT * FROM MonsterRewardItem WHERE ItemId IN (:itemIds)";
        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("itemIds", itemIds);
        sqlQuery.addEntity(MonsterRewardItem.class);

        return sqlQuery.list();
    }

    @Transactional
    public Monster update(Monster monster) {
//        List<Integer> itemIds = new ArrayList<>();
//        for (MonsterRewardItem monsterRewardItem : monster.getMonsterRewardItems()) {
//            itemIds.add(monsterRewardItem.getItem().getId());
//        }
//
//        List<MonsterRewardItem> monsterRewardItems = getMonsterRewardItems(itemIds);
//        monster.setMonsterRewardItems(monsterRewardItems);
//        Session session = getSession();
//        List<MonsterRewardItem> monsterRewardItems = monster.getMonsterRewardItems();
//        for (MonsterRewardItem item : monsterRewardItems) {
//            session.merge(item);
//        }
//
//        monster.setMonsterRewardItems(monsterRewardItems);
        return super.update(monster);
    }

    @Transactional
    public boolean delete(int id) {
        Monster monster = get(id);

        if (monster == null) {
            return false;
        }

        monster.setDelete(true);
        save(monster);

        return true;
    }

    @Transactional(readOnly = true)
    public List<Monster> getQuestMonster() {
        String query = "SELECT * FROM Monster WHERE IsDelete = false AND IsQuest = TRUE";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Monster.class);

        return sqlQuery.list();
    }
}