package vn.vinatek.vpet.database.dao;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CharacterPetDAO extends BaseDAO<CharacterPet> {

    @Transactional(readOnly = true)
    public CharacterPet get(int id) {
        return get(id, CharacterPet.class);
    }

//    @Transactional(readOnly = true)
//    public List<CharacterPet> getTopPetByExp(int top) {
//        String query = "SELECT * FROM CharacterPet cp ORDER BY experience DESC";
//
//        Session session = getSession();
//
//        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
//                .addEntity(Characters.class)
//                .setMaxResults(top);
//
//        return sqlQuery.list();
//    }

    @Transactional
    public CharacterPet save(CharacterPet characterPet) {
        super.save(characterPet);
        return characterPet;
    }

    @Transactional
    public CharacterPet update(CharacterPet characterPet) {
        super.update(characterPet);
        return characterPet;
    }

    @Transactional
    public void delete(CharacterPet characterPet) {
        super.delete(characterPet);
    }

    @Transactional(readOnly = true)
    public List<CharacterPet> getPets(String userName) {
        String query = "SELECT cp.id, cp.name FROM CharacterPet cp, Pet p, CharacterUser cu, User u " +
                "WHERE cp.PetId = p.id AND cp.CharacterId = cu.id AND cu.UserId = u.id AND u.UserName = :userName ";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("userName", userName);

        List<Object[]> cps = sqlQuery.list();
        if (cps == null) {
            return new ArrayList<>();
        }

        List<CharacterPet> characterPets = new ArrayList<>();
        for (Object[] cp : cps) {
            CharacterPet characterPet = new CharacterPet();
            characterPet.setId((Integer) cp[0]);
            characterPet.setName((String) cp[1]);
            characterPets.add(characterPet);
        }

        return characterPets;
    }

    @Transactional(readOnly = true)
    public List<CharacterPet> getPetsByUserName(String userName) {
        String query = "SELECT cp.* FROM CharacterPet cp, Pet p, CharacterUser cu, User u " +
                "WHERE cp.PetId = p.id AND cp.CharacterId = cu.id AND cu.UserId = u.id AND u.UserName = :userName ";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("userName", userName);
        sqlQuery.addEntity(CharacterPet.class);

        List<CharacterPet> characterPets = sqlQuery.list();
        if (characterPets == null) {
            return new ArrayList<>();
        }

        return characterPets;
    }

    @Transactional(readOnly = true)
    public List<PetEquipment> getPetEquipments(int characterPetId) {
        String query = "SELECT * FROM PetEquipment WHERE PetID = :characterPetId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterPetId", Integer.valueOf(characterPetId));
        sqlQuery.setFetchSize(10);

        sqlQuery.addEntity(PetEquipment.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<PetSummon> getPetSummons(int characterPetId) {
        String query = "SELECT * FROM PetSummon WHERE PetID = :characterPetId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterPetId", Integer.valueOf(characterPetId));
        sqlQuery.setFetchSize(10);

        sqlQuery.addEntity(PetSummon.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<CharacterPet> getEggs(int id) {
        String query = "SELECT * FROM CharacterPet WHERE EvolutionIndex = -3 AND CharacterId = :id";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("id", Integer.valueOf(id));

        sqlQuery.addEntity(CharacterPet.class);

        return sqlQuery.list();
    }

    @Transactional
    public void sendPet(CharacterPet characterPet, int friendId) {
        String query = "UPDATE CharacterPet SET CharacterId = :friendId WHERE Id = :characterPetId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("friendId", Integer.valueOf(friendId));
        sqlQuery.setParameter("characterPetId", Integer.valueOf(characterPet.getId()));

        sqlQuery.executeUpdate();
    }

    @Transactional(readOnly = true)
    public CharacterPet getMaxPetByUserName(String userName) {
        Session session = getSession();

        String query = "SELECT p.* FROM CharacterPet p, CharacterUser cu, User u " +
                "WHERE p.CharacterId = cu.Id AND cu.UserId = u.Id AND u.UserName = :userName " +
                "ORDER BY p.Level DESC";

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("userName", userName);
        sqlQuery.addEntity(CharacterPet.class);
        sqlQuery.setMaxResults(1);

        List<CharacterPet> rs = sqlQuery.list();
        if (rs == null || rs.isEmpty()) {
            return null;
        }

        return rs.get(0);
    }

    @Transactional
    public boolean activePet(int characterId, int petId) {
        Session session = getSession();

        Query q = session.createQuery("UPDATE CharacterPet SET IsActive = 0 WHERE CharacterId = :characterId");
        q.setParameter("characterId", characterId);
        q.executeUpdate();

        q = session.createQuery("UPDATE CharacterPet SET IsActive = 1 WHERE id = :petId");
        q.setParameter("petId", petId);
        q.executeUpdate();

        return true;
    }

    @Transactional(readOnly = true)
    public List<CharacterPet> getTopCharacterPet(int top) {
        String sql = "SELECT cp.id, cp.name, cp.experience, cp.level, pc.name as className, cu.name as characterName FROM CharacterPet cp " +
                "LEFT JOIN CharacterUser cu ON cp.characterId = cu.id " +
                "LEFT JOIN Pet p ON cp.petId = p.id " +
                "LEFT JOIN PetClass pc ON p.petClassID = pc.id " +
                "ORDER BY cp.experience DESC";

        Session session = getSession();
        Query sqlQuery = session.createSQLQuery(sql);
        sqlQuery.setMaxResults(top);

        List<Object[]> rs = sqlQuery.list();
        if (rs == null || rs.isEmpty()) {
            return new ArrayList<>();
        }

        List<CharacterPet> characterPets = new ArrayList<>();
        for (Object[] obj : rs) {
            PetClass petClass = new PetClass();
            petClass.setName((String) obj[4]);

            Characters characterUser = new Characters();
            characterUser.setName((String) obj[5]);

            CharacterPet cp = new CharacterPet();
            cp.setId((Integer) obj[0]);
            cp.setName(obj[1].toString());
            cp.setExperience(((BigInteger) obj[2]).longValue());
            cp.setLevel((Integer) obj[3]);
            cp.setPetClass(petClass);
            cp.setCharacters(characterUser);

            characterPets.add(cp);
        }

        return characterPets;
    }

    @Transactional(readOnly = true)
    public List<CharacterPet> getConsignedPet(int partition) {
        String sql = "SELECT id, CharacterId, monsterId, lastConsign FROM CharacterPet WHERE IsConsign = TRUE AND consign_partition = :partition";
        Session session = getSession();
        Query sqlQuery = session.createSQLQuery(sql);
        sqlQuery.setParameter("partition", Integer.valueOf(partition));

        List<Object[]> rs = sqlQuery.list();
        if (rs == null || rs.isEmpty()) {
            return new ArrayList<>();
        }

        List<CharacterPet> characterPets = new ArrayList<>();
        for (Object[] obj : rs) {
            Characters characterUser = new Characters();
            characterUser.setId((Integer) obj[1]);

            CharacterPet cp = new CharacterPet();
            cp.setId((Integer) obj[0]);
            cp.setCharacters(characterUser);
            cp.setMonsterId((Integer) obj[2]);
            cp.setLastConsign((Date) obj[3]);

            characterPets.add(cp);
        }

        return characterPets;
    }

    @Transactional(readOnly = true)
    public boolean checkAuth(int characterPetId, int characterId) {
        String sql = "SELECT id, CharacterId FROM CharacterPet WHERE id = :characterPetId AND CharacterId = :characterId";
        Session session = getSession();
        Query sqlQuery = session.createSQLQuery(sql);
        sqlQuery.setParameter("characterPetId", Integer.valueOf(characterPetId));
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));

        return sqlQuery.list().size() > 0;
    }
}
