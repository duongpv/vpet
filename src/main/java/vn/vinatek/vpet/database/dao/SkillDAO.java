package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.Skill;

import java.util.List;

public class SkillDAO extends BaseDAO<Skill> {

    @Transactional(readOnly = true)
    public List getAll() {
        String query = "SELECT * FROM Skill";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Skill.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public Skill get(int id) {
        return get(id, Skill.class);
    }
}
