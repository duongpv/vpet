package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.CharacterLevel;

import java.util.List;

public class CharacterLevelDAO extends BaseDAO<CharacterLevel> {

    @Transactional(readOnly = true)
    public CharacterLevel get(int id) {
        return (CharacterLevel) get(id, CharacterLevel.class);
    }

    @Transactional(readOnly = true)
    public List getAll() {
        String query = "SELECT * FROM CharacterLevel";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(CharacterLevel.class);

        return sqlQuery.list();
    }
}
