package vn.vinatek.vpet.database.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.Pet;
import vn.vinatek.vpet.database.pojo.PetEquipment;

import java.util.List;


public class PetEquipmentDAO extends BaseDAO<PetEquipment> {

    @Transactional(readOnly = true)
    public PetEquipment get(int id) {
        return super.get(id, PetEquipment.class);
    }

    @Transactional
    public boolean update(List<PetEquipment> petEquipments) {
        if (petEquipments == null) {
            return true;
        }

        for (PetEquipment petEquipment : petEquipments) {
            super.update(petEquipment);
        }

        return true;
    }

    @Transactional
    public void remove(int petEquipmentId) {
        Session session = getSession();
        Query q = session.createQuery("DELETE PetEquipment WHERE id = :petEquipmentId");
        q.setParameter("petEquipmentId", petEquipmentId);

        q.executeUpdate();
    }
}
