package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.CharacterSummon;

import java.util.List;

public class CharacterSummonDAO extends BaseDAO<CharacterSummon> {

    @Transactional(readOnly = true)
    public CharacterSummon get(int id) {
        return get(id, CharacterSummon.class);
    }

    @Transactional(readOnly = true)
    public List<CharacterSummon> getCharacterSummons(int characterId) {
        String query = "SELECT * FROM CharacterSummon WHERE characterId = :characterId";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));
        sqlQuery.addEntity(CharacterSummon.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public CharacterSummon getSummonQuest(int characterId, int id) {
        List<CharacterSummon> characterSummons = getCharacterSummons(characterId);
        for (CharacterSummon characterSummon : characterSummons) {
            if (characterSummon.getSummon().getId() == id) {
                return characterSummon;
            }
        }

        return null;
    }

    @Transactional
    public void sendSummon(CharacterSummon characterSummon, int friendId) {
        String query = "UPDATE CharacterSummon SET CharacterId = :friendId WHERE Id = :characterSummonId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("friendId", Integer.valueOf(friendId));
        sqlQuery.setParameter("characterSummonId", Integer.valueOf(characterSummon.getId()));

        sqlQuery.executeUpdate();
    }
}