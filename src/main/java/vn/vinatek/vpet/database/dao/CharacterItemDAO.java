package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.CharacterItem;

import java.util.List;


public class CharacterItemDAO extends BaseDAO<CharacterItem> {

    @Transactional(readOnly = true)
    public CharacterItem get(int id) {
        return get(id, CharacterItem.class);
    }

    @Transactional(readOnly = true)
    public List<CharacterItem> getItems(int characterId) {
        String query = "SELECT * FROM CharacterItem WHERE characterId = :characterId";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));
        sqlQuery.addEntity(CharacterItem.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<CharacterItem> getAllWeaponOfCharacter(int characterId) {
        String query = "SELECT * FROM CharacterItem as ci, Item as i , ItemClass as ic " +
                "WHERE i.Id = ci.ItemId AND ic.Id = i.ItemClassId " +
                "AND i.ItemClassId > 10 AND i.ItemClassId < 21 " +
                "AND ci.CharacterId = :characterId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));

        sqlQuery.addEntity(CharacterItem.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<CharacterItem> getAllEvolutionItemOfCharacter(int characterId) {
        String query = "SELECT * FROM CharacterItem as ci, Item as i , ItemClass as ic " +
                "WHERE i.Id = ci.ItemId " +
                "AND ic.Id = i.ItemClassId " +
                "AND i.ItemClassId = 41 " +
                "AND ci.CharacterId = :characterId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));

        sqlQuery.addEntity(CharacterItem.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<CharacterItem> getAllFoodItemOfCharacter(int characterId) {
        String query = "SELECT * FROM CharacterItem as ci, Item as i , ItemClass as ic " +
                "WHERE i.Id = ci.ItemId " +
                "AND ic.Id = i.ItemClassId " +
                "AND (i.ItemClassId = 21 OR i.ItemClassId = 61) " +
                "AND ci.CharacterId = :characterId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));

        sqlQuery.addEntity(CharacterItem.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<CharacterItem> getAllSouvenirItemOfCharacter(int characterId) {
        String query = "SELECT * FROM CharacterItem as ci, Item as i , ItemClass as ic " +
                "WHERE i.Id = ci.ItemId " +
                "AND ic.Id = i.ItemClassId " +
                "AND i.ItemClassId = 51 " +
                "AND ci.CharacterId = :characterId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));

        sqlQuery.addEntity(CharacterItem.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<CharacterItem> getAllMedicalItemOfCharacter(int characterId) {
        String query = "SELECT * FROM CharacterItem as ci, Item as i , ItemClass as ic " +
                "WHERE i.Id = ci.ItemId " +
                "AND ic.Id = i.ItemClassId " +
                "AND (i.ItemClassId = 31 OR i.ItemClassId = 41 OR  i.ItemClassId = 42) " +
                "AND ci.CharacterId = :characterId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));

        sqlQuery.addEntity(CharacterItem.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<CharacterItem> getAllItemOfCharacter(int characterId) {
        String query = "SELECT * FROM CharacterItem as ci, Item as i , ItemClass as ic " +
                "WHERE i.Id = ci.ItemId " +
                "AND ic.Id = i.ItemClassId " +
                "AND i.ItemClassId > 0 AND i.ItemClassId < 11 " +
                "AND ci.CharacterId = :characterId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));

        sqlQuery.addEntity(CharacterItem.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<CharacterItem> getAllOtherItemOfCharacter(int characterId) {
        String query = "SELECT * FROM CharacterItem as ci, Item as i , ItemClass as ic " +
                "WHERE i.Id = ci.ItemId " +
                "AND ic.Id = i.ItemClassId " +
                "AND ((i.ItemClassId > 70 AND i.ItemClassId <= 81) OR (i.ItemClassId = 51)) " +
                "AND ci.CharacterId = :characterId";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("characterId", Integer.valueOf(characterId));

        sqlQuery.addEntity(CharacterItem.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public CharacterItem getHeroQuestItem(int characterId, int questMode) {
        List<CharacterItem> characterItems = getItems(characterId);
        int questType = 30 + questMode;

        for (CharacterItem characterItem : characterItems) {
            if ((characterItem.getItem().getItemClass().getId() == 81) && (characterItem.getItem().getMagic() == questType)) {
                return characterItem;
            }
        }

        return null;
    }

    @Transactional(readOnly = true)
    public CharacterItem getSummonQuestItem(int characterId, int questMode) {
        List<CharacterItem> characterItems = getItems(characterId);
        int questType = 10 + questMode;

        for (CharacterItem characterItem : characterItems) {
            if ((characterItem.getItem().getItemClass().getId() == 81) && (characterItem.getItem().getMagic() == questType)) {
                return characterItem;
            }
        }

        return null;
    }

    @Transactional(readOnly = true)
    public CharacterItem getItemQuestItem(int characterId, int questMode) {
        List<CharacterItem> characterItems = getItems(characterId);
        int questType = 20 + questMode;

        for (CharacterItem characterItem : characterItems) {
            if ((characterItem.getItem().getItemClass().getId() == 81) && (characterItem.getItem().getMagic() == questType)) {
                return characterItem;
            }
        }

        return null;
    }

    @Transactional(readOnly = true)
    public CharacterItem getItemQuest(int characterId, int id) {
        List<CharacterItem> characterItems = getItems(characterId);

        for (CharacterItem characterItem : characterItems) {
            if (characterItem.getItem().getId() == id) {
                return characterItem;
            }
        }

        return null;
    }

    @Transactional
    public void send(int id, int receiver) {
        String query = "UPDATE CharacterItem SET characterId = :receiver WHERE id = :id";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("receiver", Integer.valueOf(receiver));
        sqlQuery.setParameter("id", Integer.valueOf(id));

        sqlQuery.executeUpdate();
    }
}
