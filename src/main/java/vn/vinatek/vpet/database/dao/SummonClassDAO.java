package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.SummonClass;

import java.util.List;

public class SummonClassDAO extends BaseDAO<SummonClass> {

    @Transactional(readOnly = true)
    public List getAll() {
        String query = "SELECT * FROM SummonClass";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(SummonClass.class);

        return sqlQuery.list();
    }
}
