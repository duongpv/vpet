package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.SkillClass;

import java.util.List;

public class SkillClassDAO extends BaseDAO<SkillClass> {

    @Transactional(readOnly = true)
    public List getAll() {
        String query = "SELECT * FROM SkillClass";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(SkillClass.class);

        return sqlQuery.list();
    }
}
