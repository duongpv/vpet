package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.Item;
import vn.vinatek.vpet.database.pojo.ItemRecipe;

import java.util.ArrayList;
import java.util.List;


public class ItemDAO extends BaseDAO<Item> {

    @Transactional(readOnly = true)
    public List getAllAvatarItem() {
        String query = "SELECT * FROM Item as i, ItemClass as ic where ic.Id = i.ItemClassId AND i.ItemClassId < 11 AND i.IsDelete = false";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public Item get(int id) {
        return get(id, Item.class);
    }

    @Transactional(readOnly = true)
    public List<Item> getAllPetItem() {
        String query = "SELECT * FROM Item as i, ItemClass as ic where ic.Id = i.ItemClassId AND (i.ItemClassId > 10 AND i.ItemClassId < 21) AND i.IsDelete = false";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getPetItemByMagic(int begin, int end) {
        String query = "SELECT * FROM Item as i, ItemClass as ic where ic.Id = i.ItemClassId AND (i.ItemClassId > 10 AND i.ItemClassId < 21) AND i.IsDelete = false\nAND Magic >= :begin \nAND Magic <= :end";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);
        sqlQuery.setParameter("begin", Integer.valueOf(begin));
        sqlQuery.setParameter("end", Integer.valueOf(end));

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getIncreaseItemByMagic(int begin, int end) {
        String query = "SELECT * FROM Item as i, ItemClass as ic where ic.Id = i.ItemClassId AND (i.ItemClassId = 42) AND i.IsDelete = false\nAND Magic >= :begin \nAND Magic <= :end";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);
        sqlQuery.setParameter("begin", Integer.valueOf(begin));
        sqlQuery.setParameter("end", Integer.valueOf(end));

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getAllItem() {
        String query = "SELECT * FROM Item WHERE IsDelete = false";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getAllOtherItem() {
        String query = "SELECT * FROM Item as i, ItemClass as ic where ic.Id = i.ItemClassId AND i.ItemClassId > 20 AND i.IsDelete = false";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional
    public boolean delete(int id) {
        Item item = get(id);
        if (item == null) {
            return false;
        }

        item.setDelete(true);
        save(item);

        return true;
    }

    @Transactional(readOnly = true)
    public List<Item> getAllEvolutionItem() {
        String query = "SELECT * FROM Item as i, ItemClass as ic where ic.Id = i.ItemClassId AND i.ItemClassId = 41 AND i.IsDelete = false";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getShopWeapon() {
        String query = "SELECT * FROM Item as i, ItemClass as ic where ic.Id = i.ItemClassId AND i.ItemClassId > 10 AND i.ItemClassId < 21 AND i.IsSell = true AND i.IsDelete = false AND i.ShopType = 1";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getShopMedical() {
        String query = "SELECT * FROM Item as i, ItemClass as ic where ic.Id = i.ItemClassId AND i.ItemClassId = 31 AND i.IsSell = true AND i.IsDelete = false AND i.ShopType = 2";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getPremiumShop() {
        String query = "SELECT * FROM Item as i where i.ShopType = 3 AND i.IsSell = true ";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getGroceryShop() {
        String query = "SELECT * FROM Item as i where i.ShopType = 4 AND i.IsSell = true ";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getGoldenShop() {
        String query = "SELECT * FROM Item as i where i.ShopType = 5 AND i.IsSell = true ";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getSpecialShop() {
        String query = "SELECT * FROM Item as i where i.ShopType = 6 AND i.IsSell = true ";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getAllItemRecipe() {
        String query = "SELECT * FROM Item AS i WHERE i.Id IN (SELECT DISTINCT ItemId FROM ItemRecipe) ORDER BY i.Name";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getItemByMagic(int begin, int end) {
        String query = "SELECT * FROM Item " +
                "WHERE IsDelete = FALSE " +
                "AND ItemClassId = 61 " +
                "AND Magic >= :begin AND Magic <= :end";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Item.class);
        sqlQuery.setParameter("begin", Integer.valueOf(begin));
        sqlQuery.setParameter("end", Integer.valueOf(end));

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public List<Item> getMapItems(int mapId) {
        String query = "SELECT i.id, i.name, i.imageUrl, i.magic FROM Item i, AreaItem ai " +
                "WHERE i.id = ai.ItemID " +
                "AND ai.AreaID = :mapId ";

        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("mapId", Integer.valueOf(mapId));

        List<Object[]> objs = sqlQuery.list();
        List<Item> items = new ArrayList<>();
        for (Object[] obj : objs) {
            Item item = new Item();
            item.setId((Integer) obj[0]);
            item.setName((String) obj[1]);
            item.setImageUrl((String) obj[2]);
            item.setMagic((Integer) obj[3]);

            items.add(item);
        }

        return items;
    }

    @Transactional
    public List<ItemRecipe> getItemRecipes(int itemId) {
        String query = "SELECT * FROM ItemRecipe WHERE itemId = :itemId ";

        Session session = getSession();
        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(ItemRecipe.class);
        sqlQuery.setParameter("itemId", Integer.valueOf(itemId));

        return sqlQuery.list();
    }
}