package vn.vinatek.vpet.database.dao;

public class QuestRequire {
    private int id;

    private int require;

    private int have;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRequire() {
        return this.require;
    }

    public void setRequire(int require) {
        this.require = require;
    }

    public int getHave() {
        return this.have;
    }

    public void setHave(int have) {
        this.have = have;
    }
}