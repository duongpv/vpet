package vn.vinatek.vpet.database.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class QuestInfo {
    private int questType;
    private List<QuestRequire> questRequires;
    private Date timeStart;
    private Date timeEnd;
    private int questMode;

    public QuestInfo() {
        this.questType = 2;
        this.questMode = 1;
        this.questRequires = new ArrayList();
    }

    public List<QuestRequire> getQuestRequires() {
        return this.questRequires;
    }

    public void setQuestRequires(List<QuestRequire> questRequires) {
        this.questRequires = questRequires;
    }

    public int getQuestType() {
        return this.questType;
    }

    public void setQuestType(int questType) {
        this.questType = questType;
    }

    public Date getTimeStart() {
        return this.timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Date getTimeEnd() {
        return this.timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public int getQuestMode() {
        return this.questMode;
    }

    public void setQuestMode(int questMode) {
        this.questMode = questMode;
    }
}
