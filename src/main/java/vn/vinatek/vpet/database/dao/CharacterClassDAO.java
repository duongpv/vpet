package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.CharacterClass;

import java.util.List;


public class CharacterClassDAO extends BaseDAO<CharacterClass> {

    @Transactional(readOnly = true)
    public List getAll() {
        String query = "SELECT * FROM CharacterClass";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(CharacterClass.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public CharacterClass get(int id) {
        return (CharacterClass) get(id, CharacterClass.class);
    }
}