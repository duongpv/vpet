package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.User;


public class UserDAO extends BaseDAO<User> {

    @Transactional(readOnly = true)
    public boolean exist(String userName) {
        return getByUserName(userName) != null;
    }

    @Transactional(readOnly = true)
    public User getByUserName(String userName) {
        String query = "SELECT * FROM User WHERE UserName = :userName";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(User.class);
        sqlQuery.setParameter("userName", userName);
        sqlQuery.setMaxResults(1);

        return (User) (sqlQuery.list().size() > 0 ? sqlQuery.list().get(0) : null);
    }

    @Transactional(readOnly = true)
    public User get(int id) {
        return get(id, User.class);
    }

    @Transactional(readOnly = true)
    public User getByCharacterId(int id) {
        String query = "select * FROM User as u, CharacterUser as cu WHERE u.Id = cu.UserId AND cu.Id = :id";

        Session session = getSession();
        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(User.class);
        sqlQuery.setParameter("id", Integer.valueOf(id));
        sqlQuery.setMaxResults(1);

        return (User) (sqlQuery.list().size() > 0 ? sqlQuery.list().get(0) : null);
    }
}