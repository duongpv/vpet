package vn.vinatek.vpet.database.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.Area;
import vn.vinatek.vpet.database.pojo.Evolution;
import vn.vinatek.vpet.database.pojo.Item;
import vn.vinatek.vpet.database.pojo.Pet;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class PetDAO extends BaseDAO<Pet> {

    @Transactional(readOnly = true)
    public List<Pet> getAll() {
        String query = "SELECT * FROM Pet WHERE IsDelete = false";
        Session session = getSession();

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.addEntity(Pet.class);

        return sqlQuery.list();
    }

    @Transactional(readOnly = true)
    public Pet get(int id) {
        return super.get(id, Pet.class);
    }

    @Transactional
    public Pet save(Pet samplePet) {
        return super.save(samplePet);
    }

    @Transactional
    public Pet update(Pet samplePet) {
        return super.update(samplePet);
    }

    @Transactional
    public boolean delete(int id) {
        Pet pet = get(id);
        if (pet == null) {
            return false;
        }

        pet.setDelete(true);
        update(pet);

        return true;
    }

    @Transactional
    public List<Area> getAreas(int petId) {
        Session session = getSession();
        String query = "SELECT a.* FROM Area a,  PetArea pa " +
                "WHERE pa.AreaID = a.id " +
                "AND pa.PetID = :petId " +
                "ORDER BY a.id ASC";

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("petId", Integer.valueOf(petId));
        sqlQuery.addEntity(Area.class);

        return sqlQuery.list();
    }

    @Transactional
    public List<Evolution> getEvolutions(int petId) {
        Session session = getSession();
        String query = "SELECT id, name, description, imageUrl, itemId, requireLevel FROM Evolution " +
                "WHERE petId = :petId " +
                "ORDER BY id ASC";

        SQLQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setParameter("petId", petId);
        sqlQuery.setFetchSize(10);

        List<Object[]> es = sqlQuery.list();
        if (es == null) {
            return new ArrayList<>();
        }

        List<Evolution> evolutions = new ArrayList<>();
        for (Object[] e : es) {
            Item item = new Item();
            item.setId((Integer) e[4]);

            Evolution evolution = new Evolution();
            evolution.setId((Integer) e[0]);
            evolution.setName((String) e[1]);
            evolution.setDescription((String) e[2]);
            evolution.setImageUrl((String) e[3]);
            evolution.setItem(item);
            evolution.setRequireLevel((Integer) e[5]);
            evolutions.add(evolution);
        }

        return evolutions;
    }

    @Transactional(readOnly = true)
    public int getNumberSummonsOfPet(int petId) {
        String query = "SELECT count(*) FROM PetSummon WHERE PetID = :petId";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .setParameter("petId", petId);

        return ((BigInteger) sqlQuery.uniqueResult()).intValue();
    }

    @Transactional(readOnly = true)
    public int getNumberItemsOfPet(int petId) {
        String query = "SELECT count(*) FROM PetEquipment WHERE PetID = :petId";
        Session session = getSession();

        SQLQuery sqlQuery = (SQLQuery) session.createSQLQuery(query)
                .setParameter("petId", petId);

        return ((BigInteger) sqlQuery.uniqueResult()).intValue();
    }
}