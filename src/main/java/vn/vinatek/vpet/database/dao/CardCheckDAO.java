package vn.vinatek.vpet.database.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.pojo.CardCheck;

import java.util.List;


public class CardCheckDAO extends BaseDAO<CardCheck> {

    @Transactional(readOnly = true)
    private List<CardCheck> getAll(int userId) {
        String query = "FROM CardCheck AS cc WHERE cc.user.id = :userId";
        Session session = getSession();

        Query sqlQuery = session.createQuery(query);
        sqlQuery.setParameter("userId", Integer.valueOf(userId));

        return sqlQuery.list();
    }
}
