package vn.vinatek.vpet.database.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import vn.vinatek.vpet.database.dao.CharacterDAO;
import vn.vinatek.vpet.database.dao.CharacterPetDAO;
import vn.vinatek.vpet.database.dao.PetDAO;
import vn.vinatek.vpet.database.dao.PetEquipmentDAO;
import vn.vinatek.vpet.database.pojo.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duongpv on 6/7/16.
 */
public class CharacterService {

    @Autowired
    private PetDAO petDAO;

    @Autowired
    private CharacterDAO characterDAO;

    @Autowired
    private PetEquipmentDAO petEquipmentDAO;

    @Autowired
    private CharacterPetDAO characterPetDAO;

    public List<PetEquipment> getPetEquipments(int characterPetId) {
        List<PetEquipment> petEquipments = characterPetDAO.getPetEquipments(characterPetId);
        if (petEquipments == null) {
            return new ArrayList<>();
        }

        for (PetEquipment petEquipment : petEquipments) {
            petEquipment.getItem().setItemRecipes(new ArrayList<ItemRecipe>());
        }

        return petEquipments;
    }

    public List<PetSummon> getPetSummons(int characterPetId) {
        List<PetSummon> petSummons = characterPetDAO.getPetSummons(characterPetId);
        if (petSummons == null) {
            return new ArrayList<>();
        }

        for (PetSummon petSummon : petSummons) {
            petSummon.getSummon().setAreas(new ArrayList<Area>());
        }

        return petSummons;
    }

    public List<CharacterPet> getCharacterPets(String userName) {
        List<CharacterPet> characterPets = characterPetDAO.getPetsByUserName(userName);
        for (CharacterPet characterPet : characterPets) {
            characterPet.getPet().setAreas(new ArrayList<Area>());
            characterPet.getPet().setEvolutions(petDAO.getEvolutions(characterPet.getPet().getId()));
            characterPet.getPet().setPetSummons(new ArrayList<PetSummon>());
            characterPet.setPetEquipments(new ArrayList<PetEquipment>());
            characterPet.setPetSummons(new ArrayList<PetSummon>());
        }

        return characterPets;
    }

    public CharacterPet getCharacterPet(int characterPetId) {
        CharacterPet characterPet = characterPetDAO.get(characterPetId);
        if (characterPet == null) {
            return null;
        }

        characterPet.getPet().setAreas(new ArrayList<Area>());
        characterPet.getPet().setEvolutions(petDAO.getEvolutions(characterPet.getPet().getId()));
        characterPet.getPet().setPetSummons(new ArrayList<PetSummon>());

        characterPet.setPetSummons(getPetSummons(characterPet.getId()));
        characterPet.setPetEquipments(getPetEquipments(characterPet.getId()));

        return characterPet;
    }

    public CharacterPet calculateTotalPoint(CharacterPet characterPetSrc, Characters characters) {
        CharacterPet characterPet = characterPetSrc.clone();

        characterPet.setDefense(characterPet.getTotalDefense());
        characterPet.setHp(characterPet.getTotalHp());
        characterPet.setIntelligent(characterPet.getTotalIntelligent());
        characterPet.setSpeed(characterPet.getTotalSpeed());
        characterPet.setStrength(characterPet.getTotalStrength());

        characterPet = calculateCharacterPetTotalPoint(characterPet, characters);

        return characterPet;
    }

    public CharacterPet calculateCharacterPetTotalPoint(CharacterPet characterPet, Characters characters) {
        if (characterPet.isActive()) {
            int point = (int) characterPet.getDefense() + characters.getTotalDefense();
            characterPet.setDefense(point);

            point = (int) characterPet.getHp() + characters.getTotalHp(true);
            characterPet.setHp(point);

            point = (int) characterPet.getIntelligent() + characters.getTotalIntelligent();
            characterPet.setIntelligent(point);

            point = (int) characterPet.getSpeed() + characters.getTotalSpeed();
            characterPet.setSpeed(point);

            point = (int) characterPet.getStrength() + characters.getTotalStrength();
            characterPet.setStrength(point);
        }

        return characterPet;
    }

    public void updateCharacter(Characters characters) {
        characterDAO.update(characters);
    }

    @Transactional
    public void updateCharacterPet(CharacterPet characterPet) {
        characterPetDAO.update(characterPet);
        petEquipmentDAO.update(characterPet.getPetEquipments());
    }
}
