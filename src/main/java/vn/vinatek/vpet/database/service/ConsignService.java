package vn.vinatek.vpet.database.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import vn.vinatek.vpet.database.dao.CharacterDAO;
import vn.vinatek.vpet.database.dao.CharacterPetDAO;
import vn.vinatek.vpet.database.dao.MonsterDAO;
import vn.vinatek.vpet.database.pojo.CharacterPet;
import vn.vinatek.vpet.database.pojo.Characters;
import vn.vinatek.vpet.database.pojo.Item;
import vn.vinatek.vpet.database.pojo.Monster;
import vn.vinatek.vpet.util.BattleUtil;

import java.util.Date;
import java.util.List;

/**
 * Created by duongpv on 8/11/2016.
 */
public class ConsignService {

    protected Logger logger = Logger.getLogger(ConsignService.class);

    @Autowired
    private CharacterDAO characterDAO;

    @Autowired
    private CharacterPetDAO characterPetDAO;

    @Autowired
    private MonsterDAO monsterDAO;

    @Autowired
    private CharacterService characterService;

    public boolean check(int characterPetId, int monsterId, Characters character) {
        CharacterPet characterPet = characterService.getCharacterPet(characterPetId);
        Monster monster = monsterDAO.get(monsterId);

        int totalPoint = characterPet.getTotalPoint();
        if (characterPet.isActive()) {
            totalPoint += character.getTotalDefense();
            totalPoint += character.getTotalSpeed();
            totalPoint += character.getTotalIntelligent();
            totalPoint += character.getTotalStrength();
        }

        return totalPoint >= monster.getTotalPoint() ;
    }

    public boolean checkAuth(int characterPetId, int characterId) {
        return characterPetDAO.checkAuth(characterPetId, characterId);
    }

    public boolean consign(int characterPetId, int monsterId) {
        CharacterPet characterPet = characterPetDAO.get(characterPetId);
        if (characterPet == null) {
            return false;
        }

        characterPet.setConsign(true);
        characterPet.setLastConsign(new Date());
        characterPet.setMonsterId(monsterId);
        characterPet.setPartition((int) (new Date().getTime() % 5));

        characterPetDAO.update(characterPet);
        return true;
    }

    public boolean unconsign(int characterPetId) {
        CharacterPet characterPet = characterPetDAO.get(characterPetId);
        if (characterPet == null) {
            return false;
        }

        characterPet.setConsign(Boolean.valueOf(false));
        characterPet.setMonsterId(Integer.valueOf(-1));
        characterPet.setPartition(Integer.valueOf(-1));

        characterPetDAO.update(characterPet);
        return true;
    }

    public void update(int partition) {
        List<CharacterPet> consignedCharacterPets = characterPetDAO.getConsignedPet(partition);
        for (CharacterPet characterPet : consignedCharacterPets) {
            int characterId = characterPet.getCharacters().getId();
            int characterPetId = characterPet.getId();

            update(characterId, characterPetId);
        }
    }

    private void update(int characterId, int characterPetId) {
        CharacterPet characterPet = characterPetDAO.get(characterPetId);
        if (characterPet == null || !characterPet.isConsign()) {
            return;
        }
        if (characterPet.getMonsterId() == -1) {
            return;
        }

        Characters characters = characterDAO.get(characterId);
        if (characters.getCrystal() < 3) {
            characterPet.setConsign(false);
            characterPet.setMonsterId(-1);

            characterPetDAO.update(characterPet);
            return;
        }

        characterPet.setPetSummons(characterService.getPetSummons(characterPet.getId()));
        characterPet.setPetEquipments(characterService.getPetEquipments(characterPet.getId()));

        Monster monster = monsterDAO.get(characterPet.getMonsterId());
        monster.setMonsterRewardItems(monsterDAO.getMonsterRewardItems(monster.getId()));

        int experience = BattleUtil.calculateExperience(characterPet.getLevel(), monster.getLevel()) * 720;
        int gold = (int) (experience * 2.5D);

        experience += characterPet.getExpIncreasing(experience);
        List<Item> rewardItems = monster.getRewardItems();

        characterPet.addExperiences(experience);

        characters.addGold(gold);
        characters.subCrystal(3);

        characterPetDAO.update(characterPet);
        characterDAO.update(characters);
        characterDAO.addCharacterItems(characters.getId(), rewardItems);

        logger.info(String.format("[Consign] update [character %d, characterPet %d] exp: %d, gold: %d, items: %d", characterId, characterPetId, experience, gold, rewardItems.size()));
    }
}
