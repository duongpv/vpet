package vn.vinatek.vpet.database.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Area implements Serializable {
    private int id;
    private String name;
    private String description;
    private int levelRequire;
    private int goldCost;
    private int crystalCost;
    private String mapElements;
    private String imageMapUrl;
    private String imageArtUrl;
    private int startRow;
    private int startCol;
    private boolean mapLimit;
    private List<AreaRewardItem> areaRewardItems;

    public Area() {
        this.areaRewardItems = new ArrayList();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLevelRequire() {
        return this.levelRequire;
    }

    public void setLevelRequire(int levelRequire) {
        this.levelRequire = levelRequire;
    }

    public int getGoldCost() {
        return this.goldCost;
    }

    public void setGoldCost(int goldCost) {
        this.goldCost = goldCost;
    }

    public int getCrystalCost() {
        return crystalCost;
    }

    public void setCrystalCost(int crystalCost) {
        this.crystalCost = crystalCost;
    }

    public String getMapElements() {
        return this.mapElements;
    }

    public void setMapElements(String mapElements) {
        this.mapElements = mapElements;
    }

    public List<AreaRewardItem> getAreaRewardItems() {
        return this.areaRewardItems;
    }

    public void setAreaRewardItems(List<AreaRewardItem> areaRewardItems) {
        this.areaRewardItems = areaRewardItems;
    }

    public int getStartRow() {
        return this.startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getStartCol() {
        return this.startCol;
    }

    public void setStartCol(int startCol) {
        this.startCol = startCol;
    }

    public String getImageMapUrl() {
        return this.imageMapUrl;
    }

    public void setImageMapUrl(String ImageMapUrl) {
        this.imageMapUrl = ImageMapUrl;
    }

    public String getImageArtUrl() {
        return this.imageArtUrl;
    }

    public void setImageArtUrl(String ImageArtUrl) {
        this.imageArtUrl = ImageArtUrl;
    }

    public boolean isMapLimit() {
        return this.mapLimit;
    }

    public void setMapLimit(boolean mapLimit) {
        this.mapLimit = mapLimit;
    }
}
