package vn.vinatek.vpet.database.pojo;


public class CharacterLevel {
    private int id;
    private String name;
    private String description;
    private int levelRequire;
    private long goldRequire;
    private long crystalRequire;
    private int maxPet;
    private int maxSummon;
    private int maxSummonForPet;
    private int maxItemForPet;
    private int delayRecover;
    private int maxEggHatch;
    private int maxItemLevelId;
    private int maxCreateItemLevelId;
    private int maxForbiddenMap;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLevelRequire() {
        return this.levelRequire;
    }

    public void setLevelRequire(int levelRequire) {
        this.levelRequire = levelRequire;
    }

    public long getGoldRequire() {
        return this.goldRequire;
    }

    public void setGoldRequire(long goldRequire) {
        this.goldRequire = goldRequire;
    }

    public long getCrystalRequire() {
        return this.crystalRequire;
    }

    public void setCrystalRequire(long crystalRequire) {
        this.crystalRequire = crystalRequire;
    }

    public int getMaxPet() {
        return this.maxPet;
    }

    public void setMaxPet(int maxPet) {
        this.maxPet = maxPet;
    }

    public int getMaxSummon() {
        return this.maxSummon;
    }

    public void setMaxSummon(int maxSummon) {
        this.maxSummon = maxSummon;
    }

    public int getMaxSummonForPet() {
        return this.maxSummonForPet;
    }

    public void setMaxSummonForPet(int maxSummonForPet) {
        this.maxSummonForPet = maxSummonForPet;
    }

    public int getMaxItemForPet() {
        return this.maxItemForPet;
    }

    public void setMaxItemForPet(int maxItemForPet) {
        this.maxItemForPet = maxItemForPet;
    }


    public int getDelayRecover() {
        return this.delayRecover;
    }


    public void setDelayRecover(int delayRecover) {
        this.delayRecover = delayRecover;
    }

    public int getMaxEggHatch() {
        return this.maxEggHatch;
    }

    public void setMaxEggHatch(int maxEggHatch) {
        this.maxEggHatch = maxEggHatch;
    }

    public int getMaxItemLevelId() {
        return this.maxItemLevelId;
    }

    public void setMaxItemLevelId(int maxItemLevelId) {
        this.maxItemLevelId = maxItemLevelId;
    }

    public int getMaxCreateItemLevelId() {
        return this.maxCreateItemLevelId;
    }

    public void setMaxCreateItemLevelId(int maxCreateItemLevelId) {
        this.maxCreateItemLevelId = maxCreateItemLevelId;
    }

    public int getMaxForbiddenMap() {
        return this.maxForbiddenMap;
    }

    public void setMaxForbiddenMap(int maxForbiddenMap) {
        this.maxForbiddenMap = maxForbiddenMap;
    }
}
