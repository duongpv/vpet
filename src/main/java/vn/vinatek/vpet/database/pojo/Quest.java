package vn.vinatek.vpet.database.pojo;

import com.google.gson.Gson;
import vn.vinatek.vpet.database.dao.QuestInfo;

public class Quest {
    public static final int QUEST_SUMMON = 1;
    public static final int QUEST_FIND_ITEM = 2;
    public static final int QUEST_HERO = 3;
    public static final int MODE_EASY = 1;
    public static final int MODE_NORMAL = 2;
    public static final int MODE_HARD = 3;
    private int id;
    private String data;

    public Quest() {
    }

    public Quest(int id, QuestInfo questInfo) {
        this.id = id;
        setQuestInfo(questInfo);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public QuestInfo getQuestInfo() {
        if (this.data == null) {
            return null;
        }

        return new Gson().fromJson(this.data, QuestInfo.class);
    }

    public void setQuestInfo(QuestInfo questInfo) {
        this.data = new Gson().toJson(questInfo);
    }
}
