/*     */ package vn.vinatek.vpet.database.pojo;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Skill
/*     */ {
/*     */   private int id;
/*     */   private String name;
/*     */   private String imageUrl;
/*     */   private String description;
/*     */   private float rate;
/*     */   private float rateStrength;
/*     */   private float rateDefense;
/*     */   private float rateIntelligent;
/*     */   private float rateSpeed;
/*     */   private float rateDameTaken;
/*     */   private float rateDameReflect;
/*     */   private float rateCritDame;
/*     */   private float ratePoisonDame;
/*     */   private int turn;
/*     */   private int turnFreeze;
/*     */   private float rateRegenHP;
/*     */   private float rateStealStrToDef;
/*     */   private float rateStealDefToStr;
/*     */   private float rateStealIntToSpeed;
/*     */   private float rateStealSpeedToInt;
/*     */   private SkillClass skillClass;
/*     */   private List<Area> areas;
/*     */   
/*     */   public Skill()
/*     */   {
/*  75 */     this.areas = new ArrayList();
/*     */   }
/*     */   
/*     */   public int getId() {
/*  79 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(int id) {
/*  83 */     this.id = id;
/*     */   }
/*     */   
/*     */   public String getName() {
/*  87 */     return this.name;
/*     */   }
/*     */   
/*     */   public void setName(String name) {
/*  91 */     this.name = name;
/*     */   }
/*     */   
/*     */   public String getImageUrl() {
/*  95 */     return this.imageUrl;
/*     */   }
/*     */   
/*     */   public void setImageUrl(String imageUrl) {
/*  99 */     this.imageUrl = imageUrl;
/*     */   }
/*     */   
/*     */   public float getRate() {
/* 103 */     return this.rate;
/*     */   }
/*     */   
/*     */   public void setRate(float rate) {
/* 107 */     this.rate = rate;
/*     */   }
/*     */   
/*     */   public float getRateStrength() {
/* 111 */     return this.rateStrength;
/*     */   }
/*     */   
/*     */   public void setRateStrength(float rateStrength) {
/* 115 */     this.rateStrength = rateStrength;
/*     */   }
/*     */   
/*     */   public float getRateDefense() {
/* 119 */     return this.rateDefense;
/*     */   }
/*     */   
/*     */   public void setRateDefense(float rateDefense) {
/* 123 */     this.rateDefense = rateDefense;
/*     */   }
/*     */   
/*     */   public float getRateIntelligent() {
/* 127 */     return this.rateIntelligent;
/*     */   }
/*     */   
/*     */   public void setRateIntelligent(float rateIntelligent) {
/* 131 */     this.rateIntelligent = rateIntelligent;
/*     */   }
/*     */   
/*     */   public float getRateSpeed() {
/* 135 */     return this.rateSpeed;
/*     */   }
/*     */   
/*     */   public void setRateSpeed(float rateSpeed) {
/* 139 */     this.rateSpeed = rateSpeed;
/*     */   }
/*     */   
/*     */   public float getRateDameTaken() {
/* 143 */     return this.rateDameTaken;
/*     */   }
/*     */   
/*     */   public void setRateDameTaken(float rateDameTaken) {
/* 147 */     this.rateDameTaken = rateDameTaken;
/*     */   }
/*     */   
/*     */   public float getRateDameReflect() {
/* 151 */     return this.rateDameReflect;
/*     */   }
/*     */   
/*     */   public void setRateDameReflect(float rateDameReflect) {
/* 155 */     this.rateDameReflect = rateDameReflect;
/*     */   }
/*     */   
/*     */   public float getRateCritDame() {
/* 159 */     return this.rateCritDame;
/*     */   }
/*     */   
/*     */   public void setRateCritDame(float rateCritDame) {
/* 163 */     this.rateCritDame = rateCritDame;
/*     */   }
/*     */   
/*     */   public float getRatePoisonDame() {
/* 167 */     return this.ratePoisonDame;
/*     */   }
/*     */   
/*     */   public void setRatePoisonDame(float ratePoisonDame) {
/* 171 */     this.ratePoisonDame = ratePoisonDame;
/*     */   }
/*     */   
/*     */   public int getTurn() {
/* 175 */     return this.turn;
/*     */   }
/*     */   
/*     */   public void setTurn(int turn) {
/* 179 */     this.turn = turn;
/*     */   }
/*     */   
/*     */   public int getTurnFreeze() {
/* 183 */     return this.turnFreeze;
/*     */   }
/*     */   
/*     */   public void setTurnFreeze(int turnFreeze) {
/* 187 */     this.turnFreeze = turnFreeze;
/*     */   }
/*     */   
/*     */   public float getRateRegenHP() {
/* 191 */     return this.rateRegenHP;
/*     */   }
/*     */   
/*     */   public void setRateRegenHP(float rateRegenHP) {
/* 195 */     this.rateRegenHP = rateRegenHP;
/*     */   }
/*     */   
/*     */   public float getRateStealStrToDef() {
/* 199 */     return this.rateStealStrToDef;
/*     */   }
/*     */   
/*     */   public void setRateStealStrToDef(float rateStealStrToDef) {
/* 203 */     this.rateStealStrToDef = rateStealStrToDef;
/*     */   }
/*     */   
/*     */   public float getRateStealDefToStr() {
/* 207 */     return this.rateStealDefToStr;
/*     */   }
/*     */   
/*     */   public void setRateStealDefToStr(float rateStealDefToStr) {
/* 211 */     this.rateStealDefToStr = rateStealDefToStr;
/*     */   }
/*     */   
/*     */   public float getRateStealIntToSpeed() {
/* 215 */     return this.rateStealIntToSpeed;
/*     */   }
/*     */   
/*     */   public void setRateStealIntToSpeed(float rateStealIntToSpeed) {
/* 219 */     this.rateStealIntToSpeed = rateStealIntToSpeed;
/*     */   }
/*     */   
/*     */   public float getRateStealSpeedToInt() {
/* 223 */     return this.rateStealSpeedToInt;
/*     */   }
/*     */   
/*     */   public void setRateStealSpeedToInt(float rateStealSpeedToInt) {
/* 227 */     this.rateStealSpeedToInt = rateStealSpeedToInt;
/*     */   }
/*     */   
/*     */   public SkillClass getSkillClass() {
/* 231 */     return this.skillClass;
/*     */   }
/*     */   
/*     */   public void setSkillClass(SkillClass skillClass) {
/* 235 */     this.skillClass = skillClass;
/*     */   }
/*     */   
/*     */   public List<Area> getAreas() {
/* 239 */     return this.areas;
/*     */   }
/*     */   
/*     */   public void setAreas(List<Area> areas) {
/* 243 */     this.areas = areas;
/*     */   }
/*     */   
/*     */   public String getDescription() {
/* 247 */     return this.description;
/*     */   }
/*     */   
/*     */   public void setDescription(String description) {
/* 251 */     this.description = description;
/*     */   }
/*     */ }


/* Location:              D:\ROOT\WEB-INF\classes\!\vn\vinatek\vpet\database\pojo\Skill.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */