/*    */ package vn.vinatek.vpet.database.pojo;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SkillClass
/*    */ {
/*    */   private int id;
/*    */   
/*    */ 
/*    */   private String name;
/*    */   
/*    */ 
/*    */   private String description;
/*    */   
/*    */ 
/*    */ 
/*    */   public int getId()
/*    */   {
/* 19 */     return this.id;
/*    */   }
/*    */   
/*    */   public void setId(int id) {
/* 23 */     this.id = id;
/*    */   }
/*    */   
/*    */   public String getName() {
/* 27 */     return this.name;
/*    */   }
/*    */   
/*    */   public void setName(String name) {
/* 31 */     this.name = name;
/*    */   }
/*    */   
/*    */   public String getDescription() {
/* 35 */     return this.description;
/*    */   }
/*    */   
/*    */   public void setDescription(String description) {
/* 39 */     this.description = description;
/*    */   }
/*    */ }


/* Location:              D:\ROOT\WEB-INF\classes\!\vn\vinatek\vpet\database\pojo\SkillClass.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */