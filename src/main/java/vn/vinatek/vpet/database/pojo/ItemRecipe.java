package vn.vinatek.vpet.database.pojo;

public class ItemRecipe {
    private int id;

    private Item itemNeed;

    private int itemLevel;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Item getItemNeed() {
        return this.itemNeed;
    }

    public void setItemNeed(Item itemNeed) {
        this.itemNeed = itemNeed;
    }

    public int getItemLevel() {
        return this.itemLevel;
    }

    public void setItemLevel(int itemLevel) {
        this.itemLevel = itemLevel;
    }
}
