package vn.vinatek.vpet.database.pojo;

import java.util.List;


public class Summon {
    private int id;
    private String name;
    private String description;
    private String imageUrl;
    private int magic;
    private int hpRegen;
    private boolean delete;
    private boolean hunting;
    private boolean increasingStrength;
    private boolean increasingDefense;
    private boolean increasingIntelligent;
    private boolean increasingSpeed;
    private boolean increasingHp;
    private int goldCostBuy;
    private int crystalCostBuy;
    private int bcoinCostBuy;
    private boolean sell;
    private boolean quest;
    private List<Area> areas;
    private SummonClass summonClass;

    public Summon() {
        this.delete = false;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isDelete() {
        return this.delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public SummonClass getSummonClass() {
        return this.summonClass;
    }

    public void setSummonClass(SummonClass summonClass) {
        this.summonClass = summonClass;
    }

    public int getMagic() {
        return this.magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }

    public int getHpRegen() {
        return this.hpRegen;
    }

    public void setHpRegen(int hpRegen) {
        this.hpRegen = hpRegen;
    }

    public boolean isHunting() {
        return this.hunting;
    }

    public void setHunting(boolean hunting) {
        this.hunting = hunting;
    }

    public List<Area> getAreas() {
        return this.areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public boolean isIncreasingStrength() {
        return this.increasingStrength;
    }

    public void setIncreasingStrength(boolean increasingStrength) {
        this.increasingStrength = increasingStrength;
    }

    public boolean isIncreasingDefense() {
        return this.increasingDefense;
    }

    public void setIncreasingDefense(boolean increasingDefense) {
        this.increasingDefense = increasingDefense;
    }

    public boolean isIncreasingIntelligent() {
        return this.increasingIntelligent;
    }

    public void setIncreasingIntelligent(boolean increasingIntelligent) {
        this.increasingIntelligent = increasingIntelligent;
    }

    public boolean isIncreasingSpeed() {
        return this.increasingSpeed;
    }

    public void setIncreasingSpeed(boolean increasingSpeed) {
        this.increasingSpeed = increasingSpeed;
    }

    public boolean isIncreasingHp() {
        return this.increasingHp;
    }

    public void setIncreasingHp(boolean increasingHp) {
        this.increasingHp = increasingHp;
    }

    public int getCatchedRate() {
        return 100;
    }

    public int getGoldCostBuy() {
        return this.goldCostBuy;
    }

    public void setGoldCostBuy(int goldCostBuy) {
        this.goldCostBuy = goldCostBuy;
    }

    public int getCrystalCostBuy() {
        return this.crystalCostBuy;
    }

    public void setCrystalCostBuy(int crystalCostBuy) {
        this.crystalCostBuy = crystalCostBuy;
    }

    public int getBcoinCostBuy() {
        return this.bcoinCostBuy;
    }

    public void setBcoinCostBuy(int bcoinCostBuy) {
        this.bcoinCostBuy = bcoinCostBuy;
    }

    public boolean isSell() {
        return this.sell;
    }

    public void setSell(boolean sell) {
        this.sell = sell;
    }

    public boolean isQuest() {
        return this.quest;
    }

    public void setQuest(boolean quest) {
        this.quest = quest;
    }
}


/* Location:              D:\ROOT\WEB-INF\classes\!\vn\vinatek\vpet\database\pojo\Summon.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */