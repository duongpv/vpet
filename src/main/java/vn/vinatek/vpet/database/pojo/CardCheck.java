/*    */ package vn.vinatek.vpet.database.pojo;
/*    */ 
/*    */ import java.util.Date;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CardCheck
/*    */ {
/*    */   int id;
/*    */   String transId;
/*    */   String cardSerial;
/*    */   String cardPin;
/*    */   String cardType;
/*    */   Date timeCheck;
/*    */   int realAmount;
/*    */   User user;
/*    */   
/*    */   public CardCheck()
/*    */   {
/* 27 */     this.timeCheck = new Date();
/*    */   }
/*    */   
/*    */   public int getId() {
/* 31 */     return this.id;
/*    */   }
/*    */   
/*    */   public void setId(int id) {
/* 35 */     this.id = id;
/*    */   }
/*    */   
/*    */   public String getTransId() {
/* 39 */     return this.transId;
/*    */   }
/*    */   
/*    */   public void setTransId(String transId) {
/* 43 */     this.transId = transId;
/*    */   }
/*    */   
/*    */   public String getCardSerial() {
/* 47 */     return this.cardSerial;
/*    */   }
/*    */   
/*    */   public void setCardSerial(String cardSerial) {
/* 51 */     this.cardSerial = cardSerial;
/*    */   }
/*    */   
/*    */   public String getCardPin() {
/* 55 */     return this.cardPin;
/*    */   }
/*    */   
/*    */   public void setCardPin(String cardPin) {
/* 59 */     this.cardPin = cardPin;
/*    */   }
/*    */   
/*    */   public String getCardType() {
/* 63 */     return this.cardType;
/*    */   }
/*    */   
/*    */   public void setCardType(String cardType) {
/* 67 */     this.cardType = cardType;
/*    */   }
/*    */   
/*    */   public Date getTimeCheck() {
/* 71 */     return this.timeCheck;
/*    */   }
/*    */   
/*    */   public void setTimeCheck(Date timeCheck) {
/* 75 */     this.timeCheck = timeCheck;
/*    */   }
/*    */   
/*    */   public User getUser() {
/* 79 */     return this.user;
/*    */   }
/*    */   
/*    */   public void setUser(User user) {
/* 83 */     this.user = user;
/*    */   }
/*    */   
/*    */   public int getRealAmount() {
/* 87 */     return this.realAmount;
/*    */   }
/*    */   
/*    */   public void setRealAmount(int realAmount) {
/* 91 */     this.realAmount = realAmount;
/*    */   }
/*    */ }


/* Location:              D:\ROOT\WEB-INF\classes\!\vn\vinatek\vpet\database\pojo\CardCheck.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */