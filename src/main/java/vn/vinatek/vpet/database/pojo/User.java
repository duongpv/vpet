package vn.vinatek.vpet.database.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class User {
    private int id;
    private String userName;
    private String password;
    private String rootPassword;
    private String role;
    private boolean enable;
    private String email;
    private String fullName = "";
    private String phone = "";
    private String identityCard = "";
    private List<Characters> characters;
    private Date lastTimeCardCheckFail;
    private int numberCheckFail;

    public User() {
        this.id = 0;
        this.role = "ROLE_USER";
        this.enable = true;
        this.characters = new ArrayList();
        this.lastTimeCardCheckFail = new Date();
        this.numberCheckFail = 0;
    }

    public User(String userName, String password) {
        this();

        this.userName = userName;
        this.password = password;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return this.role.equals("admin");
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isEnable() {
        return this.enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdentityCard() {
        return this.identityCard;
    }

    public void setIdentityCard(String identityCard) {
        this.identityCard = identityCard;
    }

    public List<Characters> getCharacters() {
        return this.characters;
    }

    public void setCharacters(List<Characters> characters) {
        this.characters = characters;
    }

    public String getRootPassword() {
        return this.rootPassword;
    }

    public void setRootPassword(String rootPassword) {
        this.rootPassword = rootPassword;
    }

    public Date getLastTimeCardCheckFail() {
        return this.lastTimeCardCheckFail;
    }

    public void setLastTimeCardCheckFail(Date lastTimeCardCheckFail) {
        this.lastTimeCardCheckFail = lastTimeCardCheckFail;
    }

    public int getNumberCheckFail() {
        return this.numberCheckFail;
    }

    public void setNumberCheckFail(int numberCheckFail) {
        this.numberCheckFail = numberCheckFail;
    }
}