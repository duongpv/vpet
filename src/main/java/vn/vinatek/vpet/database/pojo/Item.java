package vn.vinatek.vpet.database.pojo;

import java.util.ArrayList;
import java.util.List;


public class Item {
    private int id;
    private String name;
    private String description;
    private String imageUrl;
    private String increasingAttribute;
    private int goldCostSell;
    private int goldCostBuy;
    private int crystalCostSell;
    private int crystalCostBuy;
    private int bcoinCostBuy;
    private int shopType;
    private int hp;
    private int mp;
    private int defense;
    private int speed;
    private int intelligent;
    private int strength;
    private int magic;

    private int levelRequire;
    private List<ItemRecipe> itemRecipes = new ArrayList();
    private ItemLevel itemLevel;

    private int endurance;
    private float rateCatch;
    private ItemClass itemClass;
    private CharacterClass characterClass;
    private boolean sell;
    private boolean delete;

    public Item() {
        this.delete = false;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getGoldCostSell() {
        return this.goldCostSell;
    }

    public void setGoldCostSell(int goldCostSell) {
        this.goldCostSell = goldCostSell;
    }

    public int getGoldCostBuy() {
        return this.goldCostBuy;
    }

    public void setGoldCostBuy(int goldCostBuy) {
        this.goldCostBuy = goldCostBuy;
    }

    public int getCrystalCostSell() {
        return this.crystalCostSell;
    }

    public void setCrystalCostSell(int crystalCostSell) {
        this.crystalCostSell = crystalCostSell;
    }

    public int getCrystalCostBuy() {
        return this.crystalCostBuy;
    }

    public void setCrystalCostBuy(int crystalCostBuy) {
        this.crystalCostBuy = crystalCostBuy;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return this.mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getStrength() {
        return this.strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDefense() {
        return this.defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getIntelligent() {
        return this.intelligent;
    }

    public void setIntelligent(int intelligent) {
        this.intelligent = intelligent;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getEndurance() {
        return this.endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public float getRateCatch() {
        return this.rateCatch;
    }

    public void setRateCatch(float rateCatch) {
        this.rateCatch = rateCatch;
    }

    public ItemClass getItemClass() {
        return this.itemClass;
    }

    public void setItemClass(ItemClass itemClass) {
        this.itemClass = itemClass;
    }

    public boolean isSell() {
        return this.sell;
    }

    public void setSell(boolean sell) {
        this.sell = sell;
    }

    public int getLevelRequire() {
        return this.levelRequire;
    }

    public void setLevelRequire(int levelRequire) {
        this.levelRequire = levelRequire;
    }

    public int getMagic() {
        return this.magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }

    public CharacterClass getCharacterClass() {
        return this.characterClass;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public String getIncreasingAttribute() {
        return this.increasingAttribute;
    }

    public void setIncreasingAttribute(String increasingAttribute) {
        this.increasingAttribute = increasingAttribute;
    }

    public boolean isDelete() {
        return this.delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public int getBcoinCostBuy() {
        return this.bcoinCostBuy;
    }

    public void setBcoinCostBuy(int bcoinCostBuy) {
        this.bcoinCostBuy = bcoinCostBuy;
    }

    public int getShopType() {
        return this.shopType;
    }

    public void setShopType(int shopType) {
        this.shopType = shopType;
    }

    public ItemLevel getItemLevel() {
        return this.itemLevel;
    }

    public void setItemLevel(ItemLevel itemLevel) {
        this.itemLevel = itemLevel;
    }

    public List<ItemRecipe> getItemRecipes() {
        return this.itemRecipes;
    }

    public void setItemRecipes(List<ItemRecipe> itemRecipes) {
        this.itemRecipes = itemRecipes;
    }
}
