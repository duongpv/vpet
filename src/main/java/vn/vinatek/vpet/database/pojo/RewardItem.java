package vn.vinatek.vpet.database.pojo;


import java.io.Serializable;

public class RewardItem implements Serializable {
    private int id;

    private Item item;

    private int rate;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getRate() {
        return this.rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
