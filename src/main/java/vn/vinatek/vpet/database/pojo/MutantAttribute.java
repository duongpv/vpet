package vn.vinatek.vpet.database.pojo;

public class MutantAttribute
{
  public static final String STRENGTH = "Strength";
  public static final String DEFENSE = "Defense";
  public static final String INTELLIGENT = "Intelligent";
  public static final String SPEED = "Speed";
}


/* Location:              D:\ROOT\WEB-INF\classes\!\vn\vinatek\vpet\database\pojo\MutantAttribute.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */