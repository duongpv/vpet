package vn.vinatek.vpet.database.pojo;

import vn.vinatek.vpet.api.pojo.ItemUpdateGoldCost;


public class CharacterItem {
    private int id;
    private Item item;
    private int endurance;
    private int level;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getEndurance() {
        return this.endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getUpdateGoldCost(ItemUpdateGoldCost itemUpdateGoldCost) {
        long factor = 0L;

        switch (this.item.getItemLevel().getId()) {
            case 1:
                factor = itemUpdateGoldCost.getPrimary();
                break;
            case 2:
                factor = itemUpdateGoldCost.getPreIntermediate();
                break;
            case 3:
                factor = itemUpdateGoldCost.getIntermediate();
                break;
            case 4:
                factor = itemUpdateGoldCost.getHigh();
                break;
            case 5:
                factor = itemUpdateGoldCost.getSupper();
        }


        return getUpdateGoldCost(factor, this.level);
    }

    private long getUpdateGoldCost(long gold, int level) {
        for (int i = 1; i < level; i++) {
            gold = (long) ((float) gold * 1.2F);
        }

        return gold;
    }

    public int getSuccessRate() {
        return 100 - this.level * 7;
    }
}
