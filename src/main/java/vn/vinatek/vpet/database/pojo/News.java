/*    */ package vn.vinatek.vpet.database.pojo;
/*    */ 
/*    */ import java.util.Date;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class News
/*    */ {
/*    */   private int id;
/*    */   private String link;
/*    */   private String title;
/*    */   private String description;
/*    */   private Date publishDate;
/*    */   
/*    */   public int getId()
/*    */   {
/* 25 */     return this.id;
/*    */   }
/*    */   
/*    */   public void setId(int id) {
/* 29 */     this.id = id;
/*    */   }
/*    */   
/*    */   public String getLink() {
/* 33 */     return this.link;
/*    */   }
/*    */   
/*    */   public void setLink(String link) {
/* 37 */     this.link = link;
/*    */   }
/*    */   
/*    */   public String getTitle() {
/* 41 */     return this.title;
/*    */   }
/*    */   
/*    */   public void setTitle(String title) {
/* 45 */     this.title = title;
/*    */   }
/*    */   
/*    */   public String getDescription() {
/* 49 */     return this.description;
/*    */   }
/*    */   
/*    */   public void setDescription(String description) {
/* 53 */     this.description = description;
/*    */   }
/*    */   
/*    */   public Date getPublishDate() {
/* 57 */     return this.publishDate;
/*    */   }
/*    */   
/*    */   public void setPublishDate(Date publishDate) {
/* 61 */     this.publishDate = publishDate;
/*    */   }
/*    */ }


/* Location:              D:\ROOT\WEB-INF\classes\!\vn\vinatek\vpet\database\pojo\News.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */