package vn.vinatek.vpet.database.pojo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import vn.vinatek.vpet.util.RandomHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CharacterPet implements IFightingEntity {
    private int id;
    private String name;
    private String description;
    private Pet pet;
    private float mutantPoint;
    private float growthRate;
    private long experience;
    private float hp;
    private float mp;
    private float strength;
    private float defense;
    private float intelligent;
    private float speed;
    private int evolutionIndex;
    private int level;
    private int currentHP;
    private Date hatchTime;
    private List<PetEquipment> petEquipments;
    private List<PetSummon> petSummons;
    private transient Characters characters;
    private transient PetClass petClass;
    private boolean active;
    private boolean consign;
    private Date lastConsign;
    private int monsterId;
    private int partition;
    private transient long consignTime;

    public CharacterPet() {
        this.active = false;
        this.evolutionIndex = -1;
        this.level = 1;
        this.hatchTime = new Date();
        this.consign = false;
        this.monsterId = -1;
        this.partition = -1;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isConsign() {
        return consign;
    }

    public void setConsign(boolean consign) {
        this.consign = consign;
    }

    public Date getLastConsign() {
        return lastConsign;
    }

    public void setLastConsign(Date lastConsign) {
        this.lastConsign = lastConsign;
    }

    public int getMonsterId() {
        return monsterId;
    }

    public void setMonsterId(int monsterId) {
        this.monsterId = monsterId;
    }

    public int getPartition() {
        return partition;
    }

    public void setPartition(int partition) {
        this.partition = partition;
    }

    public long getConsignTime() {
        return consignTime;
    }

    public void setConsignTime(long consignTime) {
        this.consignTime = consignTime;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getExperience() {
        return this.experience;
    }

    public void setExperience(long experience) {
        this.experience = experience;
    }

    public float getGrowthRate() {
        return this.growthRate;
    }

    public void setGrowthRate(float growthRate) {
        this.growthRate = growthRate;
    }

    public Pet getPet() {
        return this.pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public float getMutantPoint() {
        return this.mutantPoint;
    }

    public void setMutantPoint(float mutantPoint) {
        this.mutantPoint = mutantPoint;
    }

    public float getHp() {
        return this.hp;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public float getMp() {
        return this.mp;
    }

    public void setMp(float mp) {
        this.mp = mp;
    }

    public float getStrength() {
        return this.strength;
    }

    public void setStrength(float strength) {
        this.strength = strength;
    }

    public float getDefense() {
        return this.defense;
    }

    public void setDefense(float defense) {
        this.defense = defense;
    }

    public float getIntelligent() {
        return this.intelligent;
    }

    public void setIntelligent(float intelligent) {
        this.intelligent = intelligent;
    }

    public float getSpeed() {
        return this.speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }


    public int getEvolutionIndex() {
        return this.evolutionIndex;
    }

    public void setEvolutionIndex(int evolutionIndex) {
        this.evolutionIndex = evolutionIndex;
    }

    public int getCurrentHP() {
        return this.currentHP;
    }

    public void setCurrentHP(int currentHP) {
        this.currentHP = currentHP;
    }

    public int getTotalHp() {
        int totalHp = (int) getHp() + this.pet.getHp();

        return totalHp + getHpIncreasing(totalHp);
    }

    public int getTotalStrength() {
        int totalStrength = (int) getStrength() + this.pet.getStrength();

        return totalStrength + getStrengthIncreasing(totalStrength);
    }

    public int getTotalDefense() {
        int totalDefense = (int) getDefense() + this.pet.getDefense();

        return totalDefense + getDefenseIncreasing(totalDefense);
    }

    public int getTotalSpeed() {
        int totalSpeed = (int) getSpeed() + this.pet.getSpeed();

        return totalSpeed + getSpeedIncreasing(totalSpeed);
    }

    public int getTotalIntelligent() {
        int totalIntelligent = (int) getIntelligent() + this.pet.getIntelligent();

        return totalIntelligent + getIntelligentIncreasing(totalIntelligent);
    }

    public int getTotalPoint() {
        return getTotalStrength() + getTotalDefense() + getTotalSpeed() + getTotalIntelligent();
    }

    public int getTotalMagic() {
        return 0;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<PetSummon> getPetSummons() {
        return this.petSummons;
    }

    public void setPetSummons(List<PetSummon> petSummons) {
        this.petSummons = petSummons;
    }

    public Characters getCharacters() {
        return characters;
    }

    public void setCharacters(Characters characters) {
        this.characters = characters;
    }

    public PetClass getPetClass() {
        return petClass;
    }

    public void setPetClass(PetClass petClass) {
        this.petClass = petClass;
    }

    public Date getHatchTime() {
        return this.hatchTime;
    }

    public void setHatchTime(Date hatchTime) {
        this.hatchTime = hatchTime;
    }

    public long addExperiences(long experiences) {
        this.experience += experiences;

        int tempLevel = calculateLevel(this.experience);

        if (tempLevel > this.level) {
            updatePoint(tempLevel - this.level);
        }

        return this.level = tempLevel;
    }

    private float getMaxPoint(int level, float growRate, float mutantPoint) {
        float factor = getPointFactor(level);

        return (5.0F + growRate + mutantPoint) * level * factor;
    }

    private float getMaxHpPoint(int level, float growRate, float mutantPoint) {
        return getMaxPoint(level, growRate, mutantPoint) * 2.0F;
    }

    private float getPointFactor(int level) {
        if ((level > 0) && (level < 501))
            return 1.5F;
        if ((level > 500) && (level < 1001))
            return 1.7F;
        if ((level > 1000) && (level < 2001))
            return 2.0F;
        if ((level > 2000) && (level < 4001)) {
            return 2.5F;
        }
        return 3.0F;
    }

    private void updatePoint(int numOfLevel) {
        for (int i = 0; i < numOfLevel; i++) {
            switch (this.pet.getPetClass().getId()) {
                case 1:
                case 2:
                case 3:
                case 4:
                    updatePointNormal();
            }
        }
    }

    private void updatePointNormal() {
        this.defense += 5.0F + this.growthRate;
        this.hp += 10.0F + this.growthRate * 2.0F;
        this.intelligent += 5.0F + this.growthRate;
        this.speed += 5.0F + this.growthRate;
        this.strength += 5.0F + this.growthRate;
    }

    private int calculateLevel(long experiences) {
        int level = 2;
        long nextEpx = 0L;
        for (; ; ) {
            nextEpx += level * level * level / 100 + 30 * level * level;

            if (nextEpx > experiences) {
                break;
            }

            level++;
        }

        return level - 1;
    }

    public long calculateExperiences(int level) {
        long exp = 0L;

        for (int i = 2; i <= level; i++) {
            exp += i * i * i / 100 + 30 * i * i;
        }

        return exp;
    }

    public List<PetEquipment> getPetEquipments() {
        return this.petEquipments;
    }

    public void setPetEquipments(List<PetEquipment> petEquipments) {
        this.petEquipments = petEquipments;
    }

    public boolean usePointItem(CharacterItem characterItem) {
        float maxPoint = getMaxPoint(this.level, this.growthRate, this.mutantPoint);
        float maxHpPoint = getMaxHpPoint(this.level, this.growthRate, this.mutantPoint);

        if ((characterItem.getItem().getDefense() > 0) && (maxPoint <= this.defense)) {
            return false;
        }

        if ((characterItem.getItem().getIntelligent() > 0) && (maxPoint <= this.intelligent)) {
            return false;
        }

        if ((characterItem.getItem().getSpeed() > 0) && (maxPoint <= this.speed)) {
            return false;
        }
        if ((characterItem.getItem().getStrength() > 0) && (maxPoint <= this.strength)) {
            return false;
        }

        if ((characterItem.getItem().getHp() > 0) && (maxPoint <= this.hp - maxPoint)) {
            return false;
        }

        this.defense = Math.min(maxPoint, this.defense + characterItem.getItem().getDefense());
        this.intelligent = Math.min(maxPoint, this.intelligent + characterItem.getItem().getIntelligent());
        this.speed = Math.min(maxPoint, this.speed + characterItem.getItem().getSpeed());
        this.strength = Math.min(maxPoint, this.strength + characterItem.getItem().getStrength());
        this.hp = Math.min(maxHpPoint, this.hp + characterItem.getItem().getHp());

        return true;
    }

    public void evolution(CharacterItem characterItem) {
        this.evolutionIndex += 1;

        updatePointNormal();

        int rate = RandomHelper.randInt(0, 3);

        if (rate > 0) {
            return;
        }

        rate = RandomHelper.randInt(0, 4);

        switch (rate) {
            case 0:
                this.defense += this.mutantPoint;
                break;
            case 1:
                this.intelligent += this.mutantPoint;
                break;
            case 2:
                this.speed += this.mutantPoint;
                break;
            case 3:
                this.strength += this.mutantPoint;
                break;
            case 4:
                this.hp += this.mutantPoint * 2.0F;
        }
    }

    private int getStrengthIncreasing(int point) {
        int increasing = 0;

        for (PetSummon petSummon : getPetSummons()) {
            Summon summon = petSummon.getSummon();
            if ((summon.getSummonClass().getId() == 1) && (summon.isIncreasingStrength())) {
                increasing += point * summon.getMagic() / 100;
            }
        }

        return increasing;
    }

    private int getDefenseIncreasing(int point) {
        int increasing = 0;

        for (PetSummon petSummon : getPetSummons()) {
            Summon summon = petSummon.getSummon();
            if ((summon.getSummonClass().getId() == 1) && (summon.isIncreasingDefense())) {
                increasing += point * summon.getMagic() / 100;
            }
        }

        return increasing;
    }

    private int getHpIncreasing(int point) {
        int increasing = 0;

        for (PetSummon petSummon : getPetSummons()) {
            Summon summon = petSummon.getSummon();
            if ((summon.getSummonClass().getId() == 1) && (summon.isIncreasingHp())) {
                increasing += point * summon.getMagic() / 100;
            }
        }

        return increasing;
    }

    private int getIntelligentIncreasing(int point) {
        int increasing = 0;

        for (PetSummon petSummon : getPetSummons()) {
            Summon summon = petSummon.getSummon();
            if ((summon.getSummonClass().getId() == 1) && (summon.isIncreasingIntelligent())) {
                increasing += point * summon.getMagic() / 100;
            }
        }

        return increasing;
    }

    private int getSpeedIncreasing(int point) {
        int increasing = 0;

        for (PetSummon petSummon : getPetSummons()) {
            Summon summon = petSummon.getSummon();
            if ((summon.getSummonClass().getId() == 1) && (summon.isIncreasingSpeed())) {
                increasing += point * summon.getMagic() / 100;
            }
        }

        return increasing;
    }

    public int getExpIncreasing(int point) {
        int increasing = 0;

        for (PetSummon petSummon : getPetSummons()) {
            Summon summon = petSummon.getSummon();
            if (summon.getSummonClass().getId() == 3) {
                increasing += point * summon.getMagic() / 100;
            }
        }

        return increasing;
    }

    public int getHpRegen() {
        return getHpRegen(getTotalHp());
    }

    public int getHpRegen(int point) {
        int increasing = 0;
        List<PetSummon> petSummons = getHpRegenSummons();
        int rate = RandomHelper.randInt(0, 100);

        for (PetSummon petSummon : petSummons) {
            Summon summon = petSummon.getSummon();

            if (rate <= summon.getMagic()) {
                increasing += point * summon.getHpRegen() / 100;
            }
        }

        return increasing;
    }

    public List<PetSummon> getHpRegenSummons() {
        List<PetSummon> petSummons = new ArrayList();

        for (PetSummon petSummon : getPetSummons()) {
            if (petSummon.getSummon().getSummonClass().getId() == 2) {
                petSummons.add(petSummon);
            }
        }

        return petSummons;
    }

    public void subExperiences(long experiences) {
        long floorExperience = calculateExperiences(this.level);
        long remain = this.experience - experiences;

        if (remain < floorExperience) {
            this.experience = floorExperience;
        } else {
            this.experience = remain;
        }
    }

    public CharacterPet clone() {
        Gson gson = new GsonBuilder().setDateFormat(0, 0).create();
        return gson.fromJson(gson.toJson(this), CharacterPet.class);
    }

    public int getBattleHp() {
        return (int) this.hp;
    }

    public int getBattleStrength() {
        return (int) this.strength;
    }

    public int getBattleDefense() {
        return (int) this.defense;
    }

    public int getBattleSpeed() {
        return (int) this.speed;
    }

    public int getBattleIntelligent() {
        return (int) this.intelligent;
    }

    public int getBattleMagic() {
        return 0;
    }
}
