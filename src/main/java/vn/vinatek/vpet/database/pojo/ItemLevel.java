/*    */ package vn.vinatek.vpet.database.pojo;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ItemLevel
/*    */ {
/*    */   public static final int PRIMARY = 1;
/*    */   
/*    */   public static final int PREINTERMEDIATE = 2;
/*    */   
/*    */   public static final int INTERMEDIATE = 3;
/*    */   
/*    */   public static final int HIGH = 4;
/*    */   
/*    */   public static final int SUPPER = 5;
/*    */   
/*    */   private int id;
/*    */   
/*    */   private String name;
/*    */   
/*    */ 
/*    */   public int getId()
/*    */   {
/* 24 */     return this.id;
/*    */   }
/*    */   
/*    */   public void setId(int id) {
/* 28 */     this.id = id;
/*    */   }
/*    */   
/*    */   public String getName() {
/* 32 */     return this.name;
/*    */   }
/*    */   
/*    */   public void setName(String name) {
/* 36 */     this.name = name;
/*    */   }
/*    */ }


/* Location:              D:\ROOT\WEB-INF\classes\!\vn\vinatek\vpet\database\pojo\ItemLevel.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */