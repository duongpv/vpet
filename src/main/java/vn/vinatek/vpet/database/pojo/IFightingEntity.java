package vn.vinatek.vpet.database.pojo;

public interface IFightingEntity {
    int getBattleHp();

    int getBattleStrength();

    int getBattleDefense();

    int getBattleSpeed();

    int getBattleIntelligent();

    int getBattleMagic();

    String getName();
}
