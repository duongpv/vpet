package vn.vinatek.vpet.database.pojo;


public class PetSummon {
    private int id;

    private Summon summon;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Summon getSummon() {
        return this.summon;
    }

    public void setSummon(Summon summon) {
        this.summon = summon;
    }
}
