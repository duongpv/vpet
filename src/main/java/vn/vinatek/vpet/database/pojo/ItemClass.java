package vn.vinatek.vpet.database.pojo;


public class ItemClass {
    public static final int MAGIC_WEAPON = 11;

    public static final int NORMAL_WEAPON = 12;

    public static final int DEFENSE_WEAPON = 13;

    private int id;

    private String name;

    private String description;

    private Boolean isPetEquipment;

    private Boolean isCharacterEquipment;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsPetEquipment() {
        return this.isPetEquipment;
    }

    public void setIsPetEquipment(Boolean isPetEquipment) {
        this.isPetEquipment = isPetEquipment;
    }

    public Boolean getIsCharacterEquipment() {
        return this.isCharacterEquipment;
    }

    public void setIsCharacterEquipment(Boolean isCharacterEquipment) {
        this.isCharacterEquipment = isCharacterEquipment;
    }
}
