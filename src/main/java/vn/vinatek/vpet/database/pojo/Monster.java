package vn.vinatek.vpet.database.pojo;

import vn.vinatek.vpet.util.RandomHelper;

import java.util.ArrayList;
import java.util.List;


public class Monster implements IFightingEntity {
    private int id;
    private String name;
    private String description;
    private String imageUrl;
    private int level;
    private int hp;
    private int strength;
    private int defense;
    private int intelligent;
    private int speed;
    private int powerRate;
    private int appearRate;
    private boolean quest;
    private boolean delete;
    private List<Area> areas;
    private List<Item> items;
    private List<MonsterRewardItem> monsterRewardItems;

    public Monster() {
        this.areas = new ArrayList();
        this.items = new ArrayList();
        this.monsterRewardItems = new ArrayList();
        this.delete = false;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getStrength() {
        return this.strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDefense() {
        return this.defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getIntelligent() {
        return this.intelligent;
    }

    public void setIntelligent(int intelligent) {
        this.intelligent = intelligent;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getTotalPoint() {
        return getStrength() + getDefense() + getSpeed() + getIntelligent();
    }

    public boolean isDelete() {
        return this.delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public List<Area> getAreas() {
        return this.areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<Item> getItems() {
        return this.items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<MonsterRewardItem> getMonsterRewardItems() {
        return this.monsterRewardItems;
    }

    public void setMonsterRewardItems(List<MonsterRewardItem> monsterRewardItems) {
        this.monsterRewardItems = monsterRewardItems;
    }

    public int getPowerRate() {
        return this.powerRate;
    }

    public void setPowerRate(int powerRate) {
        this.powerRate = powerRate;
    }

    public int getAppearRate() {
        return this.appearRate;
    }

    public void setAppearRate(int appearRate) {
        this.appearRate = appearRate;
    }

    public boolean isQuest() {
        return this.quest;
    }

    public void setQuest(boolean quest) {
        this.quest = quest;
    }

    public int getBattleHp() {
        int totalHp = getHp();

        return totalHp;
    }

    public int getBattleStrength() {
        int totalStrength = this.strength;

        return totalStrength;
    }

    public int getBattleDefense() {
        int totalDefense = this.defense;

        return totalDefense;
    }

    public int getBattleSpeed() {
        int totalSpeed = this.speed;

        return totalSpeed;
    }

    public int getBattleIntelligent() {
        int totalIntelligent = this.intelligent;

        return totalIntelligent;
    }

    public int getBattleMagic() {
        return 0;
    }


    public List<Item> getRewardItems() {
        List<MonsterRewardItem> rewardItems = getMonsterRewardItems();
        List<Item> temp = new ArrayList();

        for (MonsterRewardItem rewardItem : rewardItems) {
            int rate = RandomHelper.randInt(0, 100);
            if (rate <= rewardItem.getRate()) {
                temp.add(rewardItem.getItem());
            }
        }

        return temp;
    }

    public static void initPointFromPet(Monster monster, CharacterPet characterPet) {
        int defense = characterPet.getTotalDefense() * monster.getPowerRate() / 100;
        int hp = characterPet.getTotalHp() * monster.getPowerRate() / 100;
        int intelligent = characterPet.getTotalIntelligent() * monster.getPowerRate() / 100;
        int speed = characterPet.getTotalSpeed() * monster.getPowerRate() / 100;
        int strength = characterPet.getTotalStrength() * monster.getPowerRate() / 100;

        monster.setDefense(defense);
        monster.setHp(hp);
        monster.setIntelligent(intelligent);
        monster.setSpeed(speed);
        monster.setStrength(strength);
    }
}
