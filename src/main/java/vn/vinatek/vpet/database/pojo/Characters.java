package vn.vinatek.vpet.database.pojo;

import java.util.Date;
import java.util.List;


public class Characters {
    private int id;
    private String name;
    private String sex;
    private String imageUrl;
    private CharacterClass characterClass;
    private Date createdDate;
    private Date lastActivePet;
    private Date lastRecover;
    private long experience;
    private int level;
    private long crystal;
    private long gold;
    private long bcoin;
    private int hp;
    private int mp;
    private int strength;
    private int defense;
    private int intelligent;
    private int speed;
    private int mapId;
    private int mapLimitOnDay;
    private Date lastMapLimitRefresh;
    private CharacterLevel characterLevel;
    private List<CharacterPet> characterPets;
    private List<CharacterItem> characterItems;
    private List<CharacterEquipment> characterEquipments;
    private List<CharacterSummon> characterSummons;
    private transient List<Characters> friends;
    private transient List<Characters> friendRequest;
    private transient int numberOfPets;
    private transient int numberOfSummons;

    public Characters() {
        this.level = 1;
        this.lastMapLimitRefresh = new Date();
        this.createdDate = new Date();
        this.lastActivePet = new Date(0L);
        this.mapId = -1;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public CharacterClass getCharacterClass() {
        return this.characterClass;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public long getExperience() {
        return this.experience;
    }

    public void setExperience(long experience) {
        this.experience = experience;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getCrystal() {
        return this.crystal;
    }

    public void setCrystal(long crystal) {
        this.crystal = crystal;
    }

    public long getGold() {
        return this.gold;
    }

    public void setGold(long gold) {
        this.gold = gold;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return this.mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getStrength() {
        return this.strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDefense() {
        return this.defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getIntelligent() {
        return this.intelligent;
    }

    public void setIntelligent(int intelligent) {
        this.intelligent = intelligent;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public long getBcoin() {
        return this.bcoin;
    }

    public void setBcoin(long bcoin) {
        this.bcoin = bcoin;
    }

    public List<CharacterPet> getCharacterPets() {
        return this.characterPets;
    }

    public void setCharacterPets(List<CharacterPet> characterPets) {
        this.characterPets = characterPets;
    }

    public List<CharacterItem> getCharacterItems() {
        return this.characterItems;
    }

    public void setCharacterItems(List<CharacterItem> characterItems) {
        this.characterItems = characterItems;
    }

    public List<CharacterEquipment> getCharacterEquipments() {
        return this.characterEquipments;
    }

    public void setCharacterEquipments(List<CharacterEquipment> characterEquipments) {
        this.characterEquipments = characterEquipments;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getMapId() {
        return this.mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }

    public List<CharacterSummon> getCharacterSummons() {
        return this.characterSummons;
    }

    public void setCharacterSummons(List<CharacterSummon> characterSummons) {
        this.characterSummons = characterSummons;
    }

    public List<Characters> getFriends() {
        return this.friends;
    }

    public void setFriends(List<Characters> friends) {
        this.friends = friends;
    }

    public List<Characters> getFriendRequest() {
        return this.friendRequest;
    }

    public void setFriendRequest(List<Characters> friendRequest) {
        this.friendRequest = friendRequest;
    }

    public Date getLastActivePet() {
        return this.lastActivePet;
    }

    public void setLastActivePet(Date lastActivePet) {
        this.lastActivePet = lastActivePet;
    }

    public int getMapLimitOnDay() {
        return this.mapLimitOnDay;
    }

    public void setMapLimitOnDay(int mapLimitOnDay) {
        this.mapLimitOnDay = mapLimitOnDay;
    }

    public Date getLastMapLimitRefresh() {
        return this.lastMapLimitRefresh;
    }

    public void setLastMapLimitRefresh(Date lastMapLimitRefresh) {
        this.lastMapLimitRefresh = lastMapLimitRefresh;
    }

    public CharacterLevel getCharacterLevel() {
        return this.characterLevel;
    }

    public void setCharacterLevel(CharacterLevel characterLevel) {
        this.characterLevel = characterLevel;
    }

    public Date getLastRecover() {
        return this.lastRecover;
    }

    public void setLastRecover(Date lastRecover) {
        this.lastRecover = lastRecover;
    }

    /*
    private void updateActivePetPoint() {
        CharacterPet characterPet = getActiveCharacterPet();

        int totalHp = characterPet.getTotalHp() + getTotalHp(true);
        totalHp = Math.min(characterPet.getCurrentHP(), totalHp);

        characterPet.setCurrentHP(totalHp);
    }

    */

    public int getTotalHp(boolean petActive) {
        int totalHp = this.hp;

        if (!petActive) {
            return totalHp;
        }

        for (CharacterEquipment characterEquipment : this.characterEquipments) {
            int point = characterEquipment.getItem().getHp();
            point += (int) (point * (characterEquipment.getLevel() / 10.0F));

            totalHp += point;
        }

        return totalHp;
    }

    public int getTotalStrength() {
        int totalStrength = this.strength;

        for (CharacterEquipment characterEquipment : this.characterEquipments) {
            int point = characterEquipment.getItem().getStrength();
            point += (int) (point * (characterEquipment.getLevel() / 10.0F));

            totalStrength += point;
        }

        return totalStrength;
    }

    public int getTotalDefense() {
        int totalDefense = this.defense;

        for (CharacterEquipment characterEquipment : this.characterEquipments) {
            int point = characterEquipment.getItem().getDefense();
            point += (int) (point * (characterEquipment.getLevel() / 10.0F));

            totalDefense += point;
        }

        return totalDefense;
    }

    public int getTotalSpeed() {
        int totalSpeed = this.speed;

        for (CharacterEquipment characterEquipment : this.characterEquipments) {
            int point = characterEquipment.getItem().getSpeed();
            point += (int) (point * (characterEquipment.getLevel() / 10.0F));

            totalSpeed += point;
        }

        return totalSpeed;
    }

    public int getTotalIntelligent() {
        int totalIntelligent = this.intelligent;

        for (CharacterEquipment characterEquipment : this.characterEquipments) {
            int point = characterEquipment.getItem().getIntelligent();
            point += (int) (point * (characterEquipment.getLevel() / 10.0F));

            totalIntelligent += point;
        }

        return totalIntelligent;
    }

    public int getTotalMagic() {
        int totalMagic = 0;

        for (CharacterEquipment characterEquipment : this.characterEquipments) {
            int point = characterEquipment.getItem().getMagic();
            point += (int) (point * (characterEquipment.getLevel() / 10.0F));

            totalMagic += point;
        }

        return totalMagic;
    }

    public long addExperiences(long experiences) {
        this.experience += experiences;

        if (this.experience > 9500L) {
            this.experience = 9500L;
        }

        this.level = calculateLevel(this.experience);

        return this.level;
    }

    private int calculateLevel(long experiences) {
        int level = 2;
        long nextEpx = 0L;
        long factor = 50L;
        for (; ; ) {
            nextEpx += factor * (level - 1);

            if (nextEpx > experiences) {
                break;
            }

            level++;
        }

        return level - 1;
    }

    public long addGold(long gold) {
        return this.gold += gold;
    }

    public long subGold(long gold) {
        return this.gold = Math.max(0L, this.gold - gold);
    }

    public long addCrystal(long crystal) {
        return this.crystal += crystal;
    }

    public long addBcoin(long bcoin) {
        return this.bcoin += bcoin;
    }

    public long subCrystal(long crystal) {
        return this.crystal = Math.max(0L, this.crystal - crystal);
    }

    /*
    public boolean isFriend(Characters friend) {
        for (Characters characters : this.friends) {
            if (characters.getId() == friend.getId()) {
                return true;
            }
        }

        return false;
    }

    public boolean isFriendRequest(Characters friend) {
        for (Characters characters : this.friendRequest) {
            if (characters.getId() == friend.getId()) {
                return true;
            }
        }

        return false;
    }
    */

    public int getNumberOfSummons() {
        return numberOfSummons;
    }

    public void setNumberOfSummons(int numberOfSummons) {
        this.numberOfSummons = numberOfSummons;
    }

    public int getNumberOfPets() {
        return numberOfPets;
    }

    public void setNumberOfPets(int numberOfPets) {
        this.numberOfPets = numberOfPets;
    }
}