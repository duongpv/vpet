package vn.vinatek.vpet.database.pojo;

import vn.vinatek.vpet.util.RandomHelper;

import java.util.List;


public class Pet {
    private int id;
    private String name;
    private String description;
    private String imageAvatarUrl;
    private String imageEggUrl = "";

    private int defaultLevel;
    private int requireLevel;
    private PetClass petClass;
    private List<Area> areas;
    private List<Evolution> evolutions;
    private List<PetSummon> petSummons;
    private int hp;
    private int mp;
    private int strength;
    private int defense;
    private int intelligent;
    private int speed;
    private int hatchTime;
    private int catchedRate;
    private int appearRate;
    private boolean catched;
    private boolean evolved;
    private boolean delete;
    private String mutantAttribute;

    public Pet() {
        this.delete = false;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageAvatarUrl() {
        return this.imageAvatarUrl;
    }

    public void setImageAvatarUrl(String imageAvatarUrl) {
        this.imageAvatarUrl = imageAvatarUrl;
    }

    public String getImageEggUrl() {
        return this.imageEggUrl;
    }

    public void setImageEggUrl(String imageEggUrl) {
        this.imageEggUrl = imageEggUrl;
    }

    public int getHatchTime() {
        return this.hatchTime;
    }

    public void setHatchTime(int hatchTime) {
        this.hatchTime = hatchTime;
    }

    public boolean isCatched() {
        return this.catched;
    }

    public void setCatched(boolean catched) {
        this.catched = catched;
    }

    public boolean isEvolved() {
        return this.evolved;
    }

    public void setEvolved(boolean evolved) {
        this.evolved = evolved;
    }

    public List<Area> getAreas() {
        return this.areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public int getDefaultLevel() {
        return this.defaultLevel;
    }

    public void setDefaultLevel(int defaultLevel) {
        this.defaultLevel = defaultLevel;
    }

    public int getRequireLevel() {
        return this.requireLevel;
    }

    public void setRequireLevel(int requireLevel) {
        this.requireLevel = requireLevel;
    }

    public PetClass getPetClass() {
        return this.petClass;
    }

    public void setPetClass(PetClass petClass) {
        this.petClass = petClass;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return this.mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getStrength() {
        return this.strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDefense() {
        return this.defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getIntelligent() {
        return this.intelligent;
    }

    public void setIntelligent(int intelligent) {
        this.intelligent = intelligent;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getMutantAttribute() {
        return this.mutantAttribute;
    }

    public void setMutantAttribute(String mutantAttribute) {
        this.mutantAttribute = mutantAttribute;
    }

    public boolean isDelete() {
        return this.delete;
    }

    public void setDelete(boolean remove) {
        this.delete = remove;
    }

    public List<Evolution> getEvolutions() {
        return this.evolutions;
    }

    public void setEvolutions(List<Evolution> evolutions) {
        this.evolutions = evolutions;
    }

    public List<PetSummon> getPetSummons() {
        return this.petSummons;
    }

    public void setPetSummons(List<PetSummon> petSummons) {
        this.petSummons = petSummons;
    }

    public int getCatchedRate() {
        return this.catchedRate;
    }

    public void setCatchedRate(int catchedRate) {
        this.catchedRate = catchedRate;
    }

    public int getAppearRate() {
        return this.appearRate;
    }

    public void setAppearRate(int appearRate) {
        this.appearRate = appearRate;
    }

    public static float genGrowthRate(PetClass petClass) {
        int petClassId = petClass.getId();

        float growthRate = 1.0F;
        switch (petClassId) {
            case 4:
                growthRate = 1.0F;
                break;
            case 3:
                growthRate = 3.0F;
                break;
            case 2:
                growthRate = RandomHelper.randInt(16, 20);
                growthRate /= 10.0F;
                break;
            case 1:
                growthRate = 1.0F;
        }


        return growthRate;
    }

    public static float genMutantPoint(PetClass petClass) {
        int petClassId = petClass.getId();

        float mutantPoint = 0.0F;
        switch (petClassId) {
            case 2:
                mutantPoint = RandomHelper.randInt(5, 10);
                mutantPoint /= 10.0F;
        }


        return mutantPoint;
    }
}
