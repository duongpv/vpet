package vn.vinatek.vpet.database.pojo;


public class Evolution {
    private int id;

    private String name;

    private String description;

    private String imageUrl;

    private Item item;

    private int requireLevel;

    private boolean delete;

    public Evolution() {
        this.delete = false;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Item getItem() {
        return this.item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getRequireLevel() {
        return this.requireLevel;
    }

    public void setRequireLevel(int requireLevel) {
        this.requireLevel = requireLevel;
    }

    public boolean isDelete() {
        return this.delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
