package vn.vinatek.vpet.database.pojo;

public class PetClass {
    public static final int NORMAL = 1;

    public static final int LEGEND = 2;

    public static final int GOD = 3;

    public static final int DEFAULT = 4;

    private int id;

    private String name;

    private String description;

    private int hp;

    private int mp;

    private int strength;

    private int defense;

    private int intelligent;

    private int speed;

    private float mutantPointFrom;

    private float mutantPointTo;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return this.mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public int getStrength() {
        return this.strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDefense() {
        return this.defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getIntelligent() {
        return this.intelligent;
    }

    public void setIntelligent(int intelligent) {
        this.intelligent = intelligent;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public float getMutantPointFrom() {
        return this.mutantPointFrom;
    }

    public void setMutantPointFrom(float mutantPointFrom) {
        this.mutantPointFrom = mutantPointFrom;
    }

    public float getMutantPointTo() {
        return this.mutantPointTo;
    }

    public void setMutantPointTo(float mutantPointTo) {
        this.mutantPointTo = mutantPointTo;
    }
}
