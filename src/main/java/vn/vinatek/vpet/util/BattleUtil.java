package vn.vinatek.vpet.util;


public class BattleUtil {

  public static int calculateNormalAttackDamage(int strengthA, int defenseA, int intelligentA, int speedA, int defenseB, int speedB, int magic) {
    return (int) calculateNormalAttackDamage((float) strengthA, (float) defenseA, (float) intelligentA, (float) speedA, (float) defenseB, (float) speedB, (float) magic);
  }

  private static float calculateNormalAttackDamage(float pa, float da, float ia, float sa, float db, float sb, float x) {
    return (pa + da - db + 0.5F * (ia + sa - sb)) * calculateFactor(x);
  }

  public static int calculateMagicAttackDamage(int strengthA, int defenseA, int intelligentA, int speedA, int defenseB, int speedB, int magic) {
    return (int) calculateMagicAttackDamage((float) strengthA, (float) defenseA, (float) intelligentA, (float) speedA, (float) defenseB, (float) speedB, (float) magic);
  }

  private static float calculateMagicAttackDamage(float pa, float da, float ia, float sa, float db, float sb, float x) {
    return (0.5F * (pa + da - db) + (ia + sa - sb)) * calculateFactor(x);
  }

  public static int calculateNormalDefDamage(int strengthA, int defenseA, int intelligentA, int speedA, int intelligentB, int strengthB, int magic) {
    return (int) calculateNormalDefDamage((float) strengthA, (float) defenseA, (float) intelligentA, (float) speedA, (float) strengthB, (float) intelligentB, (float) magic);
  }

  private static float calculateNormalDefDamage(float pa, float da, float ia, float sa, float pb, float ib, float x) {
    return (da + pa - pb + 0.5F * (sa + ia - ib)) * calculateFactor(x);
  }

  public static int calculateMagicDefDamage(int strengthA, int defenseA, int intelligentA, int speedA, int strengthB, int intelligentB, int magic) {
    return (int) calculateMagicDefDamage((float) strengthA, (float) defenseA, (float) intelligentA, (float) speedA, (float) intelligentB, (float) strengthB, (float) magic);
  }

  private static float calculateMagicDefDamage(float pa, float da, float ia, float sa, float pb, float ib, float x) {
    return (0.5F * (da + pa - pb) + (sa + ia - ib)) * calculateFactor(x);
  }

  public static int calculateExperience(int petLevel, int monsterLevel) {
    return (int) calculateExperience((float) petLevel, (float) monsterLevel);
  }

  private static float calculateExperience(float x, float a) {
    float M = Math.max(a - x, 0.0F);
    return a * a / 1000.0F + 3.0F * a + (a * M * M / 16000.0F + M);
  }

  private static float getR() {
    return RandomHelper.randInt(95, 105) / 100.0F;
  }

  private static float calculateFactor(float x) {
    float r = getR();
    return (2.0F * x + 7.0F) * r / 18.0F;
  }
}
