package vn.vinatek.vpet.cfg;

import com.sun.jersey.spi.container.servlet.ServletContainer;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class Initializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
        ctx.register(WebAppConfig.class);

        ServletRegistration.Dynamic servlet = servletContext.addServlet("dispatcher", new DispatcherServlet(ctx));
        servlet.addMapping("/");
        servlet.setAsyncSupported(true);
        servlet.setLoadOnStartup(1);

        ServletRegistration.Dynamic jerseyServlet = servletContext.addServlet("jerseyServlet", new ServletContainer());
        jerseyServlet.setInitParameter("com.sun.jersey.config.property.packages", "vn.vinatek.vpet.api");
        jerseyServlet.addMapping("/api/*");
        jerseyServlet.setLoadOnStartup(1);

        servletContext.addListener(new ContextLoaderListener(ctx));
    }
}
