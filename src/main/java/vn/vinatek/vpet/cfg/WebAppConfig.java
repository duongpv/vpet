package vn.vinatek.vpet.cfg;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import vn.vinatek.vpet.queue.CacheClient;
import vn.vinatek.vpet.queue.CacheClientImpl;
import vn.vinatek.vpet.ws.handler.AreaBattleHandler;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan("vn.vinatek.vpet")
//@PropertySource(value = {"classpath:application.properties"})
@ImportResource({"classpath:/hibernate.xml", "classpath:/spring-security.xml"})
@Import({SocketConfig.class})
class WebAppConfig extends WebMvcConfigurerAdapter {

    @Resource
    private Environment env;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("/");
    }

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
//      viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");

        return viewResolver;
    }

    @Bean
    public AreaBattleHandler areaBattleHandler() {
        return new AreaBattleHandler();
    }

    @Bean
    public Gson gson() {
        return new Gson();
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss'Z'"));

        return mapper;
    }

    @Bean
    public CacheClient cacheClient() {
        return new CacheClientImpl("127.0.0.1:11211");
    }
}