package vn.vinatek.vpet.log.pojo;

import com.google.gson.Gson;
import vn.vinatek.vpet.api.provider.GsonProvider;
import vn.vinatek.vpet.database.pojo.PetSummon;
import vn.vinatek.vpet.database.pojo.Summon;

import java.util.ArrayList;
import java.util.List;


public class PetSummonLog extends PetSummon {
    public static PetSummonLog create(PetSummon petSummon) {
        Gson gson = GsonProvider.getInstance();
        String json = gson.toJson(petSummon);

        PetSummonLog petSummonLog = gson.fromJson(json, PetSummonLog.class);

        Summon summon = new Summon();
        summon.setId(petSummonLog.getSummon().getId());
        petSummonLog.setSummon(summon);

        return petSummonLog;
    }

    public static List<PetSummonLog> create(List<PetSummon> petSummons) {
        List<PetSummonLog> petSummonLogs = new ArrayList();

        for (PetSummon petSummon : petSummons) {
            petSummonLogs.add(create(petSummon));
        }

        return petSummonLogs;
    }
}