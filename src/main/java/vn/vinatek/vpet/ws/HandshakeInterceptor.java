package vn.vinatek.vpet.ws;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.socket.WebSocketHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by duongpv on 5/14/16.
 */
public class HandshakeInterceptor implements org.springframework.web.socket.server.HandshakeInterceptor {

//    private Logger logger = Logger.getLogger(HandshakeInterceptor.class);

    @Override
    public boolean beforeHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Map<String, Object> map) throws Exception {
        if (serverHttpRequest instanceof ServletServerHttpRequest) {
            HttpServletRequest servletRequest = ((ServletServerHttpRequest) serverHttpRequest).getServletRequest();
//            map.put("servletRequest", servletRequest);
//            if (servletRequest.getSession() != null) {
//                map.put("AREA_BATTLE_CONTEXT", servletRequest.getSession().getAttribute("AREA_BATTLE_CONTEXT"));
//            }

            map.put("servletContext", servletRequest.getServletContext());
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (!(auth instanceof AnonymousAuthenticationToken)) {
                User user = (org.springframework.security.core.userdetails.User) auth.getPrincipal();
                map.put("currentUser", user.getUsername());
            }
        }

        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Exception e) {

    }
}
