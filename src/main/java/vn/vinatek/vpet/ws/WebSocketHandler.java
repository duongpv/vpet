package vn.vinatek.vpet.ws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import vn.vinatek.vpet.api.pojo.BattleContext;
import vn.vinatek.vpet.database.pojo.CharacterPet;
import vn.vinatek.vpet.database.pojo.Characters;
import vn.vinatek.vpet.database.service.CharacterService;
import vn.vinatek.vpet.queue.CacheClient;
import vn.vinatek.vpet.ws.handler.AreaBattleHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by duongpv on 9/30/2015.
 */
public class WebSocketHandler extends TextWebSocketHandler {

    private Logger logger = Logger.getLogger(getClass());

    private String currentUser;

    private BattleContext battleContext;

    private Characters characters;

    private CharacterPet realcharacterPet;

    private CharacterPet characterPet;

    @Autowired
    private CacheClient cacheClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AreaBattleHandler areaBattleHandler;

    @Autowired
    private CharacterService characterService;

    public WebSocketHandler() {
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        long t = System.currentTimeMillis();
        currentUser = (String) session.getAttributes().get("currentUser");
        battleContext = (BattleContext) cacheClient.get(currentUser, "AREA_BATTLE_CONTEXT", BattleContext.class);

        characters = areaBattleHandler.getCurrentCharacters(currentUser);
        realcharacterPet = areaBattleHandler.getCharacterPet(battleContext.getCharacterPet().getId());

        characterPet = characterService.calculateTotalPoint(realcharacterPet, characters);

        logger.info(String.format("[%s] connected %d", currentUser, (System.currentTimeMillis() - t)));
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException {
        battleContext = areaBattleHandler.handle(message.getPayload(), currentUser, battleContext, characters, characterPet, realcharacterPet);
        try {
            session.sendMessage(new TextMessage(buildMessage(battleContext)));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        BattleContext lastBattleContext = (BattleContext) cacheClient.get(currentUser, "AREA_BATTLE_CONTEXT", BattleContext.class);
        if (lastBattleContext.getLastUpdate() != battleContext.getLastUpdate()) {
            return;
        }

        putBattleContext();
        logger.info(String.format("[%s] disconnected", currentUser));
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
        BattleContext lastBattleContext = (BattleContext) cacheClient.get(currentUser, "AREA_BATTLE_CONTEXT", BattleContext.class);
        if (lastBattleContext.getLastUpdate() != battleContext.getLastUpdate()) {
            return;
        }

        putBattleContext();
        logger.error(String.format("[%s] ws error: %s", currentUser, exception.getMessage()), exception);
    }

    private void putBattleContext() {
        areaBattleHandler.putBattleContext(battleContext, characters, realcharacterPet, currentUser, "AREA_BATTLE_CONTEXT");
    }

    private String buildMessage(Object data) throws JsonProcessingException {
        Map<String, Object> msg = new HashMap<>();
        msg.put("data", data);

        return objectMapper.writeValueAsString(msg);
    }
}
