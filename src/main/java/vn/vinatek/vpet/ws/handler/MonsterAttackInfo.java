package vn.vinatek.vpet.ws.handler;

import vn.vinatek.vpet.database.pojo.Item;

/**
 * Created by duongpv on 6/30/16.
 */
public class MonsterAttackInfo {

    private int damage;

    private String type;

    private Item item;

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
