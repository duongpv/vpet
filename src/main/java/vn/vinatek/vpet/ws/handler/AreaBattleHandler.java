package vn.vinatek.vpet.ws.handler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import vn.vinatek.vpet.api.pojo.BattleAttackInfo;
import vn.vinatek.vpet.api.pojo.BattleContext;
import vn.vinatek.vpet.api.pojo.BattleLog;
import vn.vinatek.vpet.database.dao.CharacterDAO;
import vn.vinatek.vpet.database.dao.QuestDAO;
import vn.vinatek.vpet.database.dao.QuestInfo;
import vn.vinatek.vpet.database.dao.QuestRequire;
import vn.vinatek.vpet.database.pojo.*;
import vn.vinatek.vpet.database.service.CharacterService;
import vn.vinatek.vpet.queue.CacheClient;
import vn.vinatek.vpet.util.BattleUtil;
import vn.vinatek.vpet.util.RandomHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by duongpv on 5/17/16.
 */
public class AreaBattleHandler {

    private Logger logger = Logger.getLogger(AreaBattleHandler.class);

    @Autowired
    private CacheClient cacheClient;

    @Autowired
    private QuestDAO questDAO;

    @Autowired
    private CharacterDAO characterDAO;

    @Autowired
    private CharacterService characterService;

    public BattleContext handle(String message, String currentUser, BattleContext battleContext, Characters characters, CharacterPet characterPet, CharacterPet realcharacterPet) throws IOException {
        IFightingEntity monster = battleContext.getMonster();
        int itemId = Integer.valueOf(message.split(":")[1]);

        if (itemId < 0) {
            PetEquipment petEquipment = new PetEquipment();
            switch (itemId) {
                case -1:
                    normalAttack(currentUser, battleContext, characters, characterPet, monster, petEquipment);
                    break;
                case -2:
                    magicAttack(currentUser, battleContext, characters, characterPet, monster, petEquipment);
                    break;
                case -3:
                    defense(currentUser, battleContext, characters, characterPet, monster, petEquipment);
            }
        } else {
            PetEquipment petEquipment = null;
            for (PetEquipment temp : characterPet.getPetEquipments()) {
                if (temp.getId() == itemId) {
                    petEquipment = temp;
                    break;
                }
            }
//            if (petEquipment == null) {
//              putBattleContext(this.battleContext);
//              return Response.status(400).entity(RestErrorFactory.BATTLE_ITEM_NOT_EXIST).build();
//            }

            Item item = petEquipment.getItem();
            switch (item.getItemClass().getId()) {
                case 12:
                    normalAttack(currentUser, battleContext, characters, characterPet, monster, petEquipment);
                    break;
                case 11:
                    magicAttack(currentUser, battleContext, characters, characterPet, monster, petEquipment);
                    break;
                case 13:
                    defense(currentUser, battleContext, characters, characterPet, monster, petEquipment);
            }

            if (petEquipment.getItem().getEndurance() >= 0) {
                int endurance = petEquipment.getEndurance() - 1;

                petEquipment.setEndurance(endurance);
                if (endurance <= 0) {
                    characterPet.getPetEquipments().remove(petEquipment);
                }
            }
        }

        if ((battleContext.getMonsterCurrentHp() <= 0) && (characterPet.getCurrentHP() > 0)) {
            realcharacterPet.addExperiences(characterPet.getExperience());
        }
        if ((characterPet.getCurrentHP() <= 0) && (battleContext.getMonsterCurrentHp() > 0)) {
            realcharacterPet.subExperiences(characterPet.getExperience());
        }

        realcharacterPet.setPetEquipments(characterPet.getPetEquipments());
        realcharacterPet.setCurrentHP(characterPet.getCurrentHP());

        if (battleContext.isEnd()) {
            putBattleContext(battleContext, characters, realcharacterPet, currentUser, "AREA_BATTLE_CONTEXT");
        }

        return battleContext;
    }

    public CharacterPet getCharacterPet(int petId) {
        return characterService.getCharacterPet(petId);
    }

    public Characters getCurrentCharacters(String currentUser) {
        Characters characters = characterDAO.getCharacterByUserName(currentUser);
        if (characters != null) {
            characters.setCharacterEquipments(characterDAO.getCharacterEquipments(characters.getId()));
        }

        return characters;
    }

    public boolean normalAttack(String currentUser, BattleContext battleContext, Characters characters, CharacterPet characterPet, IFightingEntity monster, PetEquipment petEquipment) throws IOException {
        BattleLog battleLog = createBattleLog();
        if (battleContext.getBattleLogs().size() == 0) {
            battleContext.getBattleLogs().add(battleLog);
        }
        else {
            battleContext.getBattleLogs().set(0, battleLog);
        }

        if (petEquipment.getItem() == null) {
            battleContext = normalAttackWithoutWeapon(battleContext, characters, characterPet, monster);
        } else {
            battleContext = normalAttackWithWeapon(battleContext, characters, characterPet, monster, petEquipment.getItem());
        }

        if (!battleContext.isMonsterDefense()) {
            battleContext = monsterAttack(battleContext, characterPet, monster);
        }

        battleContext = check(currentUser, battleContext, characterPet, characters, monster);
        battleContext.setCharacterPet(characterPet);

        return battleContext.isEnd();
    }

    public boolean magicAttack(String currentUser, BattleContext battleContext, Characters characters, CharacterPet characterPet, IFightingEntity monster, PetEquipment petEquipment) throws IOException {
        BattleLog battleLog = createBattleLog();
        if (battleContext.getBattleLogs().size() == 0) {
            battleContext.getBattleLogs().add(battleLog);
        }
        else {
            battleContext.getBattleLogs().set(0, battleLog);
        }

        if (petEquipment.getItem() == null) {
            battleContext = magicAttackWithoutWeapon(battleContext, characters, characterPet, monster);
        } else {
            battleContext = magicAttackWithWeapon(battleContext, characters, characterPet, monster, petEquipment.getItem());
        }

        if (!battleContext.isMonsterDefense()) {
            battleContext = monsterAttack(battleContext, characterPet, monster);
        }

        battleContext = check(currentUser, battleContext, characterPet, characters, monster);
        battleContext.setCharacterPet(characterPet);

        return battleContext.isEnd();
    }

    public boolean defense(String currentUser, BattleContext battleContext, Characters characters, CharacterPet characterPet, IFightingEntity monster, PetEquipment petEquipment) throws IOException {
        BattleLog battleLog = createBattleLog();
        if (battleContext.getBattleLogs().size() == 0) {
            battleContext.getBattleLogs().add(battleLog);
        }
        else {
            battleContext.getBattleLogs().set(0, battleLog);
        }

        if (battleContext.isMonsterAttackFirst()) {
            battleLog.setActionTwo(characterPet.getName() + " phòng thủ");
        } else {
            battleLog.setActionOne(characterPet.getName() + " phòng thủ");
        }

        if (petEquipment.getItem() != null) {
            battleContext = monsterAttackDefenseWithItem(battleContext, characters, characterPet, monster, petEquipment);
        } else {
            battleContext = monsterAttackDefense(battleContext, characters, characterPet, monster);
        }

        battleContext = check(currentUser, battleContext, characterPet, characters, monster);
        battleContext.setCharacterPet(characterPet);

        return battleContext.isEnd();
    }

    public BattleContext check(String currentUser, BattleContext battleContext, CharacterPet characterPet, Characters characters, IFightingEntity monster) {
        if ((characterPet.getCurrentHP() <= 0) && (battleContext.getMonsterCurrentHp() <= 0)) {
            battleDraw(currentUser, battleContext, characters, characterPet, (Monster) monster);
            battleContext.setEnd(true);
        }
        else if (characterPet.getCurrentHP() <= 0) {
            battleLose(currentUser, battleContext, characters, characterPet, (Monster) monster);
            characterPet.setCurrentHP(0);
            battleContext.setEnd(true);
        }
        else if (battleContext.getMonsterCurrentHp() <= 0) {
            battleWin(currentUser, battleContext, characters, characterPet, (Monster) monster);
            battleContext.setEnd(true);
        }
        else {
            int hp = characterPet.getHpRegen();
            if (hp > 0) {
                String effect = "Bạn được hồi " + hp;
                hp = Math.min(characterPet.getCurrentHP() + hp, characterPet.getTotalHp());
                characterPet.setCurrentHP(hp);
                int last = battleContext.getBattleLogs().size() - 1;
                battleContext.getBattleLogs().get(last).setRegenHpEffect(effect);
            }
        }

        return battleContext;
    }

    public BattleContext normalAttackWithoutWeapon(BattleContext battleContext, Characters characters, CharacterPet characterPet, IFightingEntity monster) {
        BattleAttackInfo battleAttackInfo;
        if (characterPet.isActive()) {
            battleAttackInfo = new BattleAttackInfo(characters, characterPet, monster);
        } else {
            battleAttackInfo = new BattleAttackInfo(characterPet, monster);
        }

        int damage = BattleUtil.calculateNormalAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());
        MonsterAttackInfo attackInfo = calculateMonsterDefense((Monster) monster, characterPet);
        if (attackInfo != null) {
            setMonsterLog(battleContext, monster.getName() + " phòng thủ");
            int def = attackInfo.getDamage();
            damage = Math.max(0, damage) - def;

            battleContext.setMonsterDefense(true);
        }
        else {
            battleContext.setMonsterDefense(false);
        }

        String temp = "";
        if (damage <= 0) {
            temp = temp + characterPet.getName() + " tấn công vật lý gây ra 0 sát thương cho " + monster.getName();
            damage = 0;
        } else {
            temp = temp + characterPet.getName() + " tấn công vật lý gây ra " + damage + " sát thương cho " + monster.getName();
        }
        int currentHp = battleContext.getMonsterCurrentHp() - damage;

        battleContext.setMonsterCurrentHp(currentHp);

        setCharacterPetLog(battleContext, temp);

        return battleContext;
    }

    public BattleContext normalAttackWithWeapon(BattleContext battleContext, Characters characters, CharacterPet characterPet, IFightingEntity monster, Item item) {
        BattleAttackInfo battleAttackInfo;
        if (characterPet.isActive()) {
            battleAttackInfo = new BattleAttackInfo(characters, characterPet, monster, item);
        } else {
            battleAttackInfo = new BattleAttackInfo(characterPet, monster, item);
        }

        int damage = BattleUtil.calculateNormalAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());
        MonsterAttackInfo attackInfo = calculateMonsterDefense((Monster) monster, characterPet);
        if (attackInfo != null) {
            setMonsterLog(battleContext, monster.getName() + " phòng thủ");
            int def = attackInfo.getDamage();
            damage = Math.max(0, damage) - def;

            battleContext.setMonsterDefense(true);
        }
        else {
            battleContext.setMonsterDefense(false);
        }

        String temp = "";
        if (damage <= 0) {
            temp = temp + characterPet.getName() + " dùng " + item.getName() + " tấn công gây ra 0 sát thương cho " + monster.getName();
            damage = 0;
        } else {
            temp = temp + characterPet.getName() + " dùng " + item.getName() + " tấn công gây ra " + damage + " sát thương cho " + monster.getName();
        }
        int currentHp = battleContext.getMonsterCurrentHp() - damage;

        battleContext.setMonsterCurrentHp(currentHp);

        setCharacterPetLog(battleContext, temp);

        return battleContext;
    }

    public BattleContext magicAttackWithoutWeapon(BattleContext battleContext, Characters characters, CharacterPet characterPet, IFightingEntity monster) {
        BattleAttackInfo battleAttackInfo;
        if (characterPet.isActive()) {
            battleAttackInfo = new BattleAttackInfo(characters, characterPet, monster);
        } else {
            battleAttackInfo = new BattleAttackInfo(characterPet, monster);
        }

        int damage = BattleUtil.calculateMagicAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());
        MonsterAttackInfo attackInfo = calculateMonsterDefense((Monster) monster, characterPet);
        if (attackInfo != null) {
            setMonsterLog(battleContext, monster.getName() + " phòng thủ");
            int def = attackInfo.getDamage();
            damage = Math.max(0, damage) - def;

            battleContext.setMonsterDefense(true);
        }
        else {
            battleContext.setMonsterDefense(false);
        }

        String temp = "";
        if (damage <= 0) {
            temp = temp + characterPet.getName() + " tấn công ma pháp gây ra 0 sát thương cho " + monster.getName();
            damage = 0;
        } else {
            temp = temp + characterPet.getName() + " tấn công ma pháp gây ra " + damage + " sát thương cho " + monster.getName();
        }
        int currentHp = battleContext.getMonsterCurrentHp() - damage;

        battleContext.setMonsterCurrentHp(currentHp);

        setCharacterPetLog(battleContext, temp);

        return battleContext;
    }

    public BattleContext magicAttackWithWeapon(BattleContext battleContext, Characters characters, CharacterPet characterPet, IFightingEntity monster, Item item) {
        BattleAttackInfo battleAttackInfo;
        if (characterPet.isActive()) {
            battleAttackInfo = new BattleAttackInfo(characters, characterPet, monster, item);
        } else {
            battleAttackInfo = new BattleAttackInfo(characterPet, monster, item);
        }

        int damage = BattleUtil.calculateMagicAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());
        MonsterAttackInfo attackInfo = calculateMonsterDefense((Monster) monster, characterPet);
        if (attackInfo != null) {
            setMonsterLog(battleContext, monster.getName() + " phòng thủ");
            int def = attackInfo.getDamage();
            damage = Math.max(0, damage) - def;

            battleContext.setMonsterDefense(true);
        }
        else {
            battleContext.setMonsterDefense(false);
        }

        String temp = "";
        if (damage <= 0) {
            temp = temp + characterPet.getName() + " dùng " + item.getName() + " tấn công gây ra 0 sát thương cho " + monster.getName();
            damage = 0;
        } else {
            temp = temp + characterPet.getName() + " dùng " + item.getName() + " tấn công gây ra " + damage + " sát thương cho " + monster.getName();
        }
        int currentHp = battleContext.getMonsterCurrentHp() - damage;

        battleContext.setMonsterCurrentHp(currentHp);

        setCharacterPetLog(battleContext, temp);

        return battleContext;
    }

    public BattleContext monsterAttackDefense(BattleContext battleContext, Characters characters, CharacterPet characterPet, IFightingEntity monster) {
        BattleAttackInfo battleAttackInfo;
        if (characterPet.isActive()) {
            battleAttackInfo = new BattleAttackInfo(characterPet, monster, characters);
        } else {
            battleAttackInfo = new BattleAttackInfo(characterPet, monster);
        }

        int def = Math.max(0, BattleUtil.calculateNormalDefDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic()));
//        if (characterPet.isActive()) {
//            battleAttackInfo = new BattleAttackInfo(monster, characterPet, characters);
//        } else {
//            battleAttackInfo = new BattleAttackInfo(monster, characterPet);
//        }
//        int damageAttack = BattleUtil.calculateNormalAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());

        MonsterAttackInfo attackInfo = calculateMonsterAttackDamage((Monster) monster, characterPet);
        int damageAttack = attackInfo.getDamage();

        int damage = Math.max(0, damageAttack) - def;

        String temp = "";
        if (damage < 0) {
            temp = temp + monster.getName() + " bị phản lại " + -damage + " sát thương từ " + characterPet.getName();

            int currentHp = battleContext.getMonsterCurrentHp() + damage;
            battleContext.setMonsterCurrentHp(currentHp);
        }
        else {
            if (attackInfo.getItem() != null) {
                temp = temp + monster.getName() + " sử dụng " + attackInfo.getItem().getName() + " tấn công gây ra " + damage + " sát thương cho " + characterPet.getName();
            }
            else {
                temp = temp + monster.getName() + " tấn công gây ra " + damage + " sát thương cho " + characterPet.getName();
            }

            int currentHp = characterPet.getCurrentHP() - damage;
            characterPet.setCurrentHP(currentHp);
        }

        setMonsterLog(battleContext, temp);

        return battleContext;
    }

    public BattleContext monsterAttackDefenseWithItem(BattleContext battleContext, Characters characters, CharacterPet characterPet, IFightingEntity monster, PetEquipment petEquipment) {
        BattleAttackInfo battleAttackInfo;
        if (characterPet.isActive()) {
            battleAttackInfo = new BattleAttackInfo(characterPet, monster, characters, petEquipment.getItem());
        } else {
            battleAttackInfo = new BattleAttackInfo(characterPet, monster, petEquipment.getItem());
        }

        int def = Math.max(0, BattleUtil.calculateNormalDefDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic()));

//        if (characterPet.isActive()) {
//            battleAttackInfo = new BattleAttackInfo(monster, characterPet, characters);
//        } else {
//            battleAttackInfo = new BattleAttackInfo(monster, characterPet);
//        }
//        int damageAttack = BattleUtil.calculateNormalAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());
        MonsterAttackInfo attackInfo = calculateMonsterAttackDamage((Monster) monster, characterPet);
        int damageAttack = attackInfo.getDamage();

        int damage = Math.max(0, damageAttack) - def;

        String temp = "";

        if (damage < 0) {
            temp = temp + monster.getName() + " bị phản lại " + -damage + " sát thương từ " + characterPet.getName();

            int currentHp = battleContext.getMonsterCurrentHp() + damage;
            battleContext.setMonsterCurrentHp(currentHp);
        }
        else {
            if (attackInfo.getItem() != null) {
                temp = temp + monster.getName() + " sử dụng " + attackInfo.getItem().getName() + " tấn công gây ra " + damage + " sát thương cho " + characterPet.getName();
            }
            else {
                temp = temp + monster.getName() + " tấn công gây ra " + damage + " sát thương cho " + characterPet.getName();
            }

            int currentHp = characterPet.getCurrentHP() - damage;
            characterPet.setCurrentHP(currentHp);
        }

        setMonsterLog(battleContext, temp);

        return battleContext;
    }

    public BattleContext monsterAttack(BattleContext battleContext, CharacterPet characterPet, IFightingEntity monster) {
//        BattleAttackInfo battleAttackInfo = new BattleAttackInfo(monster, characterPet);
//        int damage = BattleUtil.calculateNormalAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());

        MonsterAttackInfo attackInfo = calculateMonsterAttackDamage((Monster) monster, characterPet);
        int damage = attackInfo.getDamage();

        String temp = "";
        if (damage <= 0) {
            temp = temp + monster.getName() + " tấn công gây ra 0 sát thương cho " + characterPet.getName();
            damage = 0;
        }
        else {
            if (attackInfo.getItem() != null) {
                temp = temp + monster.getName() + " sử dụng " + attackInfo.getItem().getName() + " tấn công gây ra " + damage + " sát thương cho " + characterPet.getName();
            }
            else {
                temp = temp + monster.getName() + " tấn công gây ra " + damage + " sát thương cho " + characterPet.getName();
            }
        }
        int currentHp = characterPet.getCurrentHP() - damage;
        characterPet.setCurrentHP(currentHp);

        setMonsterLog(battleContext, temp);

        return battleContext;
    }

    public BattleContext battleWin(String currentUser, BattleContext battleContext, Characters characters, CharacterPet characterPet, Monster monster) {
        battleContext.setBattleState(1);
        battleContext.setMonsterCurrentHp(0);

        /*
        if (battleContext.getType() == 2) {
            BattleContext huntingBattleContext = (BattleContext) this.request.getSession().getAttribute("HUNTING_BATTLE_CONTEXT");
            huntingBattleContext.setBattleState(1);
            characterPet.setExperience(0L);

            this.logger.info("battleWin - legend pet");

            return battleContext;
        }
        */

        if (battleContext.isHunting()) {
            Quest quest = questDAO.get(characters.getId());
            if (quest != null) {
                QuestInfo questInfo = quest.getQuestInfo();
                if (questInfo.getQuestType() == 3) {
                    QuestRequire questRequire = questInfo.getQuestRequires().get(0);
                    questRequire.setHave(questRequire.getHave() + 1);

                    quest.setQuestInfo(questInfo);
                    questDAO.update(quest);
                }
            }
        }

        List<Item> rewardItems = monster.getRewardItems();

        int experience = BattleUtil.calculateExperience(characterPet.getLevel(), monster.getLevel());
        int gold = (int) (experience * 2.5D);

        experience += characterPet.getExpIncreasing(experience);

        battleContext.setGold(gold);
        battleContext.setExperience(experience);
        battleContext.setRewardItems(rewardItems);

        characters.addGold(gold);
        characterPet.setExperience(experience);

        logger.info(String.format("[%s] battleWin - gold: %s - exp: %s", currentUser, gold, experience));
        return battleContext;
    }

    public BattleContext battleDraw(String currentUser, BattleContext battleContext, Characters characters, CharacterPet characterPet, Monster monster) {
        characterPet.setCurrentHP(0);

        battleContext.setMonsterCurrentHp(0);
        battleContext.setBattleState(3);

        logger.info(String.format("[%s] battle draw", currentUser));
        return battleContext;
    }

    public BattleContext battleLose(String currentUser, BattleContext battleContext, Characters characters, CharacterPet characterPet, Monster monster) {
        characterPet.setCurrentHP(0);

        battleContext.setBattleState(2);

        int experience = BattleUtil.calculateExperience(characterPet.getLevel(), monster.getLevel());
        int gold = experience * 3;

        battleContext.setGold(gold);
        battleContext.setExperience(experience);

        characters.subGold(gold);
        characterPet.setExperience(experience);

        logger.info(String.format("[%s] battleLose - gold:  %s - exp: %s", currentUser, gold, experience));
        return battleContext;
    }

    public BattleLog createBattleLog() {
        BattleLog battleLog = new BattleLog();
        battleLog.setId(1);

        return battleLog;
    }

    public BattleLog setCharacterPetLog(BattleContext battleContext, String log) {
        BattleLog battleLog = battleContext.getBattleLogs().get(0);

        if (battleContext.isMonsterAttackFirst()) {
            battleLog.setActionTwo(log);
        } else {
            battleLog.setActionOne(log);
        }

        return battleLog;
    }

    public BattleLog setMonsterLog(BattleContext battleContext, String log) {
        BattleLog battleLog = battleContext.getBattleLogs().get(0);

        if (battleContext.isMonsterAttackFirst()) {
            battleLog.setActionOne(log);
        } else {
            battleLog.setActionTwo(log);
        }

        return battleLog;
    }

    public void putBattleContext(BattleContext battleContext, Characters characters, CharacterPet realcharacterPet, String hash, String key) {
        characterService.updateCharacter(characters);
        characterService.updateCharacterPet(realcharacterPet);
        characterDAO.addCharacterItems(characters.getId(), battleContext.getRewardItems());

        battleContext.setLastUpdate(System.currentTimeMillis());
        cacheClient.push(hash, key, battleContext);
    }

    private MonsterAttackInfo calculateMonsterAttackDamage(Monster monster, CharacterPet characterPet) {
        List<Item> items = new ArrayList<>();
        if (monster.getItems().size() > 0) {
            for (Item item : monster.getItems()) {
                if (item.getItemClass().getId() == 11 || item.getItemClass().getId() == 12) {
                    items.add(item);
                }
            }
        }

        boolean useItem = RandomHelper.randInt(0, 1) == 1;
        if (items.size() == 0) {
            useItem = false;
        }

        MonsterAttackInfo monsterAttackInfo = new MonsterAttackInfo();

        int damage;
        if (useItem) {
            Item item = items.get(RandomHelper.randInt(0, items.size() - 1));
            monsterAttackInfo.setItem(item);

            BattleAttackInfo battleAttackInfo = new BattleAttackInfo(monster, characterPet, item);
            if (item.getItemClass().getId() == 11) {
                damage = BattleUtil.calculateMagicAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());
                monsterAttackInfo.setType("magic");
            }
            else {
                damage = BattleUtil.calculateNormalAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());
                monsterAttackInfo.setType("normal");
            }
        }
        else {
            BattleAttackInfo battleAttackInfo = new BattleAttackInfo(monster, characterPet);
            if (RandomHelper.randInt(0, 1) == 0) {
                damage = BattleUtil.calculateMagicAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());
                monsterAttackInfo.setType("magic");
            }
            else {
                damage = BattleUtil.calculateNormalAttackDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic());
                monsterAttackInfo.setType("normal");
            }
        }

        monsterAttackInfo.setDamage(damage);

        return monsterAttackInfo;
    }

    private MonsterAttackInfo calculateMonsterDefense(Monster monster, CharacterPet characterPet) {
        if (RandomHelper.randInt(0, 1) == 0) {
            return null;
        }

        BattleAttackInfo battleAttackInfo = new BattleAttackInfo(monster, characterPet);
        int def = Math.max(0, BattleUtil.calculateNormalDefDamage(battleAttackInfo.getStrengthA(), battleAttackInfo.getDefenseA(), battleAttackInfo.getIntelligentA(), battleAttackInfo.getSpeedA(), battleAttackInfo.getDefenseB(), battleAttackInfo.getSpeedB(), battleAttackInfo.getMagic()));

        MonsterAttackInfo monsterAttackInfo = new MonsterAttackInfo();
        monsterAttackInfo.setDamage(def);

        return monsterAttackInfo;
    }
}
