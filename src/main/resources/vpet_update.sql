ALTER TABLE `Area`
ADD `CrystalCost` int(11) NULL AFTER `GoldCost`;

ALTER TABLE `characterpet`
ADD `IsConsign` tinyint NULL DEFAULT 0 AFTER `HatchTime`,
ADD `lastConsign` datetime NULL AFTER `IsConsign`,
ADD `monsterId` int NULL DEFAULT -1 AFTER `lastConsign`,
ADD `consign_partition` int(11) NOT NULL DEFAULT '-1';

