<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div style="width: 100%; text-align: center">
    <table style="margin: auto">
        <tr >
            <td class="headerred">Tùy chọn vật phẩm</td>
        </tr>
        <tr >
            <td>
                <img style="width: 80px; height: 80px;" ng-if="summon.summon.imageUrl" img-pre-load="{{summon.summon.imageUrl}}"/>
            </td>
        </tr>
        <tr >
            <td>
                <table cellpadding="5" cellspacing="0" class="">
                    <tr>
                        <td style="padding-top:14px;">
                            <select ng-model="actionType" class="textfield" >
                                <option value="1">Trang bị cho thú cưng</option>
                                <option value="2">Thả</option>
                                <option value="3">Tặng cho bạn bè</option>
                            </select>
                        </td>
                    </tr>
                    <tr ng-show="actionType == 1">
                        <td>
                            <select ng-model="selectedCharacterPetId" class="textfield">
                                <option ng-repeat="characterPet in characterPets" value="{{characterPet.id}}">{{characterPet.name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr ng-show="actionType == 3">
                        <td>
                            <select ng-model="selectedCharacterId" class="textfield">
                                <option ng-repeat="character in characters" value="{{character.id}}">{{character.name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img ng-click="action(actionType)" ng-src="images/button/button_ok_size_s1.png"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>