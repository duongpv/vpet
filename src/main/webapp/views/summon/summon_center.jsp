<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div style="height: 710px; overflow: hidden">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Linh thú của tôi</td>
        </tr>
        <tr>
            <td class="contentred" style="padding-top: 0px">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <!-- list items in current category -->
                    <tr ng-repeat="summonRow in summonRows" style="height: 100px">
                        <td valign="top" class="backgroundorange"  >
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr >
                                    <td style="width: 250px;" ng-repeat="summonUser in summonRow.summons">
                                        <table ng-init="summon = summonUser.summon"
                                               style="font-style:italic;cellspacing:10px;cellpadding:0;width: 100%;">
                                            <tr style="text-align:center;">
                                                <td >
                                                    <a ui-sref="summonAction({id: {{summonUser.id}}})" >
                                                        <img img-pre-load="{{summon.imageUrl}}" title="{{summon.name}}" width="100" height="101" />
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr style="text-align:center;">
                                                <td><strong>{{summon.name}}</strong></td>
                                            </tr>
                                            <tr style="text-align:center;">
                                                <td><strong>Loại:&nbsp;</strong>{{summon.summonClass.name}}&nbsp;</td>
                                            </tr>
                                            <tr style="text-align:center;">
                                                <td><strong>Ma thuật:&nbsp;</strong>{{summon.magic}}&nbsp;</td>
                                            </tr>
                                            <!--
                                            <tr style="text-align:center;">
                                                <td><strong>Miêu tả:&nbsp;</strong>{{summon.description}}&nbsp;</td>
                                            </tr>
                                            -->
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


    </table>
</div>

<div class="contentred" style="padding: 0px; margin: 0px">
    <div id="pagination-box"></div>
</div>