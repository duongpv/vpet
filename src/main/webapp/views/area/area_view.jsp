<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Chào mừng đến với đấu trường!<br>Hãy chọn đối thủ</td>
        </tr>
        <tr>
            <td class="contentred">
                <table width="100%" cellspacing="0" cellpadding="0" ng-repeat="row in monstersArray">
                    <tr>
                        <td ng-repeat="col in [0, 1]" width="50%" >
                            <div ng-init="monster = monsters[2 * row + col]">
                                <div ng-if="monster">
                                    <img width="150px" height="150px" img-pre-load="{{monster.imageUrl}}"/>
                                    <br>
                                    <a ui-sref="areaBattlePetChose({id: {{monster.id}}})">
                                        <strong>{{monster.name}}</strong> - Level {{monster.level}}
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Paging here</td>
        </tr>
    </table>
</div>
