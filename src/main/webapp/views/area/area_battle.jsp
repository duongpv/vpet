<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<style>
    .tool-tip-center-text{
        text-align: center;
    }
</style>
<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Chiến đấu với {{monster.name}}</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="contentred">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" >
                                <tr>
                                    <td width="308" align="center" valign="top">
                                        <table width="308" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center" valign="middle">
                                                    <div ng-if="characterPet.evolutionIndex < 0">
                                                        <img img-pre-load="{{characterPet.pet.imageAvatarUrl}}" width="150" height="150"  />
                                                    </div>
                                                    <div ng-if="characterPet.evolutionIndex >= 0">
                                                        <img img-pre-load="{{characterPet.pet.evolutions[characterPet.evolutionIndex].imageUrl}}" width="150" height="150"  />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentpink">{{characterPet.name}}</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="120" align="left" class="contentredtable">Cấp độ:</td>
                                                            <td class="contentgreen">{{characterPet.level}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Máu:</td>
                                                            <td class="contentwhite">
                                                                <table width="100%" cellpadding="2" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td align="center" class="contentpink">{{battleContext.characterPet.currentHP}} / {{floatToInt(characterPet.hp)}}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Sức mạnh:</td>
                                                            <td class="contentpink">{{floatToInt(characterPet.strength)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Phòng thủ:</td>
                                                            <td class="contentpink">{{floatToInt(characterPet.defense)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Trí tuệ:</td>
                                                            <td class="contentpink">{{floatToInt(characterPet.intelligent)}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Tốc độ:</td>
                                                            <td class="contentpink">{{floatToInt(characterPet.speed)}}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="103" align="left" valign="top">
                                        <img src="images/vs.png" width="103" height="323" />
                                    </td>
                                    <td align="center" valign="top">
                                        <table width="308" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center" valign="middle">
                                                    <img img-pre-load="{{monster.imageUrl}}" alt="" width="150" height="150" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentpink">{{monster.name}}</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="120" align="left" class="contentredtable">Cấp độ:</td>
                                                            <td class="contentgreen">{{monster.level}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Máu:</td>
                                                            <td class="contentwhite">
                                                                <table width="100%" cellpadding="2" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td align="center" class="contentpink">{{battleContext.monsterCurrentHp}} / {{monster.hp}}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Sức mạnh:</td>
                                                            <td class="contentpink">{{monster.strength}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Phòng thủ:</td>
                                                            <td class="contentpink">{{monster.defense}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Trí tuệ:</td>
                                                            <td class="contentpink">{{monster.intelligent}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" class="contentredtable">Tốc độ:</td>
                                                            <td class="contentpink">{{monster.speed}}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" valign="top" class="tablegray">
                                        <div style="height: 50px; overflow: auto;">
                                            <%--<label style="width: 100%" ng-repeat="battleLog in battleContext.battleLogs">--%>
                                                <%--Lượt {{battleLog.id}}<ul>--%>
                                                    <%--<li ng-if="battleLog.actionOne">{{battleLog.actionOne}}</li>--%>
                                                    <%--<li ng-if="battleLog.actionTwo">{{battleLog.actionTwo}}</li>--%>
                                                    <%--<li ng-if="battleLog.regenHpEffect">{{battleLog.regenHpEffect}}</li>--%>
                                                <%--</ul>--%>
                                            <%--</label>--%>
                                            <label style="width: 100%">
                                            <ul>
                                                <li ng-if="battleContext.battleLogs[0].actionOne">{{battleContext.battleLogs[0].actionOne}}</li>
                                                <li ng-if="battleContext.battleLogs[0].actionTwo">{{battleContext.battleLogs[0].actionTwo}}</li>
                                                <li ng-if="battleContext.battleLogs[0].regenHpEffect">{{battleContext.battleLogs[0].regenHpEffect}}</li>
                                            </ul>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                <style>
                                    .battle-action{
                                        cursor: pointer;
                                        margin-left: 50px;
                                    }
                                    .battle-action img{
                                        width: 75px;
                                        height: 75px;
                                    }
                                </style>
                                <tr ng-if="battleContext.battleState == 0">
                                    <td colspan="3" style="text-align: center">
                                        <span class="battle-action" ng-click="normalAttack()">
                                            <img src="images/sword.png"/>
                                        </span>
                                        <span class="battle-action"  ng-click="magicAttack()">
                                            <img src="images/wand.png"/>
                                        </span>
                                        <span class="battle-action" ng-click="defend()">
                                            <img src="images/shield.png"/>
                                        </span>
                                    </td>
                                </tr>
                                <tr ng-if="battleContext.battleState == 0">
                                    <td colspan="3" class="backgroundform">
                                        <table width="100%" class="contentred">
                                            <tr>
                                                <td colspan="2" align="left" valign="bottom">
                                                    <table width="445" border="0" cellpadding="0" cellspacing="0" class="tableborderwhite">
                                                        <tr>
                                                            <td>
                                                                <table width="445" border="0" cellpadding="0" cellspacing="0" class="tableborderwhite1">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                                                <tr>
                                                                                    <td align="center" width="100" height="101" ng-repeat="i in [0, 1, 2, 3]">
                                                                                        <a ng-if="characterPet.petSummons[i]">
                                                                                            <section class="ng-tooltip-section">
                                                                                                <img width="100" height="101" img-pre-load="{{characterPet.petSummons[i].summon.imageUrl}}"
                                                                                                     data-tooltip-trigger data-html="true" data-tooltip-text="{{renderTooltipSummon(characterPet.petSummons[i])}}"/>
                                                                                                <!-- Active pg-ng-tooltip lib-->
                                                                                                <div data-pg-ng-tooltip class="ng-tooltip"></div>
                                                                                            </section>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="445" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="150" class="tableborderwhitebottomleft">&nbsp;&nbsp;&nbsp;Linh thú trang bị</td>
                                                            <td class="tableborderwhitebottomcenter">&nbsp;</td>
                                                            <td class="tableborderwhitebottomright">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right" valign="bottom">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="right" valign="bottom">
                                                                <table width="445" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="150" height="15" class="tableborderwhitebottomleft1">&nbsp;&nbsp;&nbsp;</td>
                                                                        <td class="tableborderwhitebottomcenter">&nbsp;</td>
                                                                        <td class="tableborderwhitebottomright1">Vũ khí trang bị&nbsp;&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="5">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right" valign="top">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="right" valign="top">
                                                                <table width="445" border="0" cellpadding="0" cellspacing="0" class="tableborderwhite1">
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                                                <tr>
                                                                                    <td align="center" width="100" height="101" ng-repeat="i in [0, 1, 2, 3]">
                                                                                        <section class="ng-tooltip-section" ng-if="characterPet.petEquipments[i] != null">
                                                                                            <img style="margin-left: 20px;" width="80" height="80" 
                                                                                                 img-pre-load="{{characterPet.petEquipments[i].item.imageUrl}}"
                                                                                                 ng-click="itemUse(characterPet.petEquipments[i])"
                                                                                                 data-tooltip-trigger data-html="true" data-tooltip-text="{{renderTooltip(characterPet.petEquipments[i])}}"/>
                                                                                            <!-- Active pg-ng-tooltip lib-->
                                                                                            <div data-pg-ng-tooltip class="ng-tooltip tool-tip-center-text"></div>
                                                                                        </section>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="5">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" valign="top">&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr ng-if="battleContext.battleState == 0">
                                    <td ng-if="battleContext.type == 0">
                                        <a href ng-click="areaBattleEscape()">
                                            <img ng-src="images/button/bochay.png"/>
                                        </a>
                                    </td>
                                    <td ng-if="battleContext.type == 1">
                                        <a href ng-click="huntingBattleEscape()">
                                            <img ng-src="images/button/bochay.png"/>
                                        </a>
                                    </td>
                                </tr>

                                <%-- Win text --%>
                                <tr ng-if="battleContext.battleState == 1">
                                    <td colspan="3">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="3" valign="top" class="tablegray">
                                                    <div style="height: 75px; overflow: auto;">
                                                        <label>Chúc mừng! Bạn đánh thắng {{monster.name}}</label>
                                                        <br>
                                                        <label>
                                                            <ul>
                                                                <li>Đạt được {{battleContext.experience}} điểm kinh nghiệm</li>
                                                                <li>Đạt được {{battleContext.gold}} vàng</li>
                                                            </ul>
                                                        </label>
                                                        <br>
                                                        <label ng-if="rewardItems != undefined && rewardItems != null && rewardItems.length > 0">
                                                            Bạn nhặt được
                                                            <ul ng-repeat="item in rewardItems">
                                                                <li>{{item.name}}</li>
                                                            </ul>
                                                        </label>											
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ng-if="battleContext.type == 0">
                                                <td colspan="3" valign="center" style="text-align: center">
                                                    <br>
                                                    <a ui-sref="areaView">
                                                        <image ng-src="images/button/dautruong_button_red.png"/>
                                                    </a>
                                                    <a>
                                                        <image ng-src="images/button/trangcanhan_button.png"/>
                                                    </a>
                                                    <a href="javascript:void(0);" ng-click="createBattle()">
                                                        <image ng-src="images/button/tieptucdanh_button.png"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr ng-if="battleContext.type > 0">
                                                <td colspan="3" valign="center" style="text-align: center">
                                                    <a href ng-click="huntingBattleEscape()">
                                                        <image ng-src="images/button/button_ok.png"/>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <%-- Lose text --%>
                                <tr ng-if="battleContext.battleState == 2">
                                    <td colspan="3">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="3" valign="top" class="tablegray">
                                                    <div style="height: 150px">
                                                        <label>Buồn quá! Bạn đánh thua {{monster.name}} rồi</label>
                                                        </label>
                                                        <br>
                                                        <label>
                                                            <ul>
                                                                <li>Bạn mất {{battleContext.experience}} điểm kinh nghiệm</li>
                                                            </ul>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ng-if="battleContext.type == 0">
                                                <td colspan="3" valign="center" style="text-align: center">
                                                    <br>
                                                    <a ui-sref="areaView">
                                                        <image ng-src="images/button/dautruong_button_red.png"/>
                                                    </a>
                                                    <a ui-sref="memberDetail">
                                                        <image ng-src="images/button/trangcanhan_button.png"/>
                                                    </a>
                                                    <a href="javascript:void(0);" ng-click="createBattle()">
                                                        <image ng-src="images/button/tieptucdanh_button.png"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr ng-if="battleContext.type > 0">
                                                <td colspan="3" valign="center" style="text-align: center">
                                                    <a href ng-click="huntingBattleEscape()">
                                                        <image ng-src="images/button/button_ok.png"/>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <%-- Draw text --%>
                                <tr ng-if="battleContext.battleState == 3">
                                    <td colspan="3">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="3" valign="top" class="tablegray">
                                                    <div style="height: 150px">
                                                        <label>Buồn quá! Bạn đánh hòa với {{monster.name}} rồi</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr ng-if="battleContext.type == 0">
                                                <td colspan="3" valign="center" style="text-align: center">
                                                    <br>
                                                    <a ui-sref="areaView">
                                                        <image ng-src="images/button/dautruong_button_red.png"/>
                                                    </a>
                                                    <a ui-sref="memberDetail">
                                                        <image ng-src="images/button/trangcanhan_button.png"/>
                                                    </a>
                                                    <a href="javascript:void(0);" ng-click="createBattle()">
                                                        <image ng-src="images/button/tieptucdanh_button.png"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr ng-if="battleContext.type > 0">
                                                <td colspan="3" valign="center" style="text-align: center">
                                                    <a href ng-click="huntingBattleEscape()">
                                                        <image ng-src="images/button/button_ok.png"/>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="contentred">&nbsp;</td>
        </tr>
    </table>
</div>
