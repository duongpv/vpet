<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top" class="headerred">Chế tạo trang bị</td>
        </tr>
    </table>

    <div style="height: 550px">
        <table width="100%" >
            <tr ng-repeat="item in rows">
                <td width="150px" style="padding-top: 10px; padding-bottom: 10px;">
                    <div style="text-align: center" class="creator-item">
                        <img img-pre-load="{{item.imageUrl}}" width="100" height="101" />
                        <br>
                        {{item.name}}
                        <br>
                        Độ bền: {{item.endurance}}
                        <br>
                        Ma thuật: {{item.magic}}
                    </div>
                </td>
                <td width="130px" algin="center" >
                    <img title="Chế tạo" ng-click="create(item.id)" src="images/smith/arrow_left.png" style="padding-left: 15px;padding-right: 15px;cursor: pointer;"/>
                </td>
                <td width="100%">
                    <table width="100%">
                        <tr align="right">
                            <td ng-repeat="itemRecipe in item.itemRecipes">
                                <div ng-init="itemNeed = itemRecipe.itemNeed" class="creator-item">
                                    <div style="top: -10px; right: 0px" class="item-level-text label" ng-if="itemRecipe.itemLevel > 0">+{{itemRecipe.itemLevel}}</div>
                                    <img img-pre-load="{{itemNeed.imageUrl}}" width="100" height="101" />
                                    <br>
                                    {{itemNeed.name}}
                                </div>
                            </td> 
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div class="contentred" style="padding: 0px; margin: 0px">
        <div id="pagination-box" style="margin: 0px 0px 0px 0px !important"></div>
    </div>
</div>

<style>
    .creator-item{
        text-align: center; 
        position: relative;
        margin-right: 25px;
        width: 100px;
    }
</style>