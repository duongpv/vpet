<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top" class="headerred">Nâng cấp trang bị</td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top">
                <img  src="images/smith/update_center_logo.png"/>
            </td>
        </tr>


        <tr>
            <td colspan="3" align="center" valign="top" class="headerred">Trang bị cần nâng cấp</td>
        </tr>
        <tr>
            <td align="center">
                <select ng-model="selectedUpdateId" class="textfield" ng-change="selectedChange()">
                    <option ng-repeat="characterItem in characterItems" value="{{characterItem.id}}">{{characterItem.level > 0 ? characterItem.item.name + "+ " + characterItem.level : characterItem.item.name}}</option>
                </select>
            </td>
        </tr>

        <tr>
            <td colspan="3" align="center" valign="top">
                <img style="padding-top: 25px; padding-bottom: 25px;" src="images/smith/arrow_up.png"/>
            </td>
        </tr>

        <tr>
            <td colspan="3" align="center" valign="top" class="headerred">Nâng cấp nguyên liệu</td>
        </tr>
        <tr>
            <td align="center">
                <select ng-model="selectedNeedlId" class="textfield">
                    <option ng-repeat="characterItem in needItems" value="{{characterItem.id}}">{{characterItem.level > 0 ? characterItem.item.name + "+ " + characterItem.level : characterItem.item.name}}</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top" class="headerred">Số tiền cần: {{goldCost}}</td>
        </tr>

        <tr>
            <td colspan="3" align="center" valign="top">
                <img ng-click="updateItem()" style="padding-top: 25px;" src="images/button/button_ok_size_s.png"/>
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>