<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Top Character
                <a href="#/topcharacter/gold">Gold</a>
                <a href="#/topcharacter/crystal">Crystal</a>
                <a href="#/topcharacter/exp">Experience</a>
                <a href="#/topcharacter/pet">Pet Experience</a>
            </td>
        </tr>
        <tr>

        </tr>
        <tr>
            <td ng-if="topType == 'char'" class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Name</td>
                        <td>Experience</td>
                        <td>Level</td>
                        <td>Class</td>
                        <td>Gold</td>
                        <td>Crystal</td>
                        <%--<td>&nbsp;</td>--%>
                    </tr>
                    <tr ng-repeat="character in characterList">
                        <td>{{$index + 1}}</td>
                        <td>{{character.name}}</td>
                        <td>{{character.experience}}</td>
                        <td>{{character.level}}</td>
                        <td>{{character.characterClass.name}}</td>
                        <td>{{character.gold}}</td>
                        <td>{{character.crystal}}</td>
                        <%--<td>--%>
                            <%--<a ui-sref="adminCharacterDetail({id: {{character.id}}})">View</a>--%>
                        <%--</td>--%>
                    </tr>
                </table>
            </td>
            <td ng-if="topType == 'pet'" class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Pet Name</td>
                        <td>Experience</td>
                        <td>Level</td>
                        <td>Class</td>
                        <td>Character Name</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="characterPet in characterPets">
                        <td>{{$index + 1}}</td>
                        <td>{{characterPet.name}}</td>
                        <td>{{characterPet.experience}}</td>
                        <td>{{characterPet.level}}</td>
                        <td>{{characterPet.petClass.name}}</td>
                        <td>{{characterPet.characters.name}}</td>
                        <td>
                        <a ui-sref="petDetail({id: {{characterPet.id}}})">View</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
