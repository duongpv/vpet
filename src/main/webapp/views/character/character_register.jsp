<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="contentred">H&atilde;y l&#7921;a ch&#7885;n nh&acirc;n v&#7853;t sau &#273;&oacute; nh&#7853;n m&#7897;t th&uacute; c&#432;ng &#273;&#7875; b&#7855;t &#273;&#7847;u chuy&#7871;n phi&ecirc;u l&#432;u!</td>
        </tr>
        <tr>
            <td class="contentred">
                <form>
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="32">
                                            <a href ng-click="prevAvatar()"><img src="images/arrow_back.png" width="32" height="63" /></a>
                                        </td>
                                        <td ng-if="characterClass.length > 0">
                                            <section class="ng-tooltip-section">
                                                <img ng-if="character.sex == 'Male'" static-img-pre-load="{{imageAvatarSource + '_men.png'}}" width="300px" height="500px"  data-tooltip-trigger data-tooltip-text="{{avatarDescription}}"/>
                                                <img ng-if="character.sex == 'Female'" static-img-pre-load="{{imageAvatarSource + '_women.png'}}" width="300px" height="500px"  data-tooltip-trigger data-tooltip-text="{{avatarDescription}}"/>
                                                <!-- Active pg-ng-tooltip lib-->
                                                <div data-pg-ng-tooltip class="ng-tooltip"></div>
                                            </section>
                                        </td>
                                        <td width="32">
                                            <a href ng-click="nextAvatar()"><img src="images/arrow_next.png" width="32" height="63" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="bottom">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="32">
                                            <a href ng-click="prevPet()"><img src="images/arrow_back.png" width="32" height="63" /></a>
                                        </td>
                                        <td ng-if="petList.length > 0">
                                            <section class="ng-tooltip-section">
                                                <img static-img-pre-load="{{imagePetSource}}" width="300px" height="296px" data-tooltip-trigger data-tooltip-text="{{petDescription}}"/>
                                                <!-- Active pg-ng-tooltip lib-->
                                                <div data-pg-ng-tooltip class="ng-tooltip"></div>
                                            </section>
                                        </td>
                                        <td width="32">
                                            <a href ng-click="nextPet()"><img src="images/arrow_next.png" width="32" height="63" /></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="65" align="center">
                                <a href ng-click="setSex('Male')">
                                    <img src="images/man_icon.png" width="57" height="52" />
                                </a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href ng-click="setSex('Female')">
                                    <img src="images/woman_icon.png" width="57" height="53" />
                                </a>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <input type="text" class="textfield" ng-model="character.name" size="30" />
                            </td>
                            <td align="center">
                                <input type="text" class="textfield" ng-model="pet.name" size="30" />
                            </td>
                        </tr>
                        <tr>
                            <td height="70" colspan="2" align="center">
                                <a href ng-click="doCreate()"><img src="images/button/button_ok.png" width="148" height="54" /></a>
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
    </table>
</div>