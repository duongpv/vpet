<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<style>
    .dropdown-menu-custom{
        background: transparent;
        border: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        min-width:0px;
    }
</style>

<div class="dropdown" id="context-menu">
    <ul class="dropdown-menu dropdown-menu-custom" role="menu">
        <li>
            <img ng-click="equip()" ng-src="images/button/trang_bi_button.png" />
        </li>
        <li>
            <img ng-click="send()" ng-src="images/button/tang_button.png" />
        </li>
        <li>
            <img ng-click="sell()" ng-src="images/button/ban_ve_chai_button.png" />
        </li>
    </ul>
</div>
<div id="avatarViewDiv">
    <div ng-show="!isShowDetail">
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" class="headerred">Avatar</td>
            </tr>
            <tr>
                <td class="contentred">
                    <table width="735" height="600" cellspacing="0" cellpadding="0">
                        <tr class="backgroundform">
                            <td align="center" valign="top">
                                <table cellpadding="3" cellspacing="0" class="contentredtable">
                                    <tr>
                                        <td width="110" rowspan="2" align="center" valign="top">
                                            <table  style="margin-top: 50px" width="100%" cellspacing="0" cellpadding="0">
                                                <tr ng-repeat="equipmentItem in characterEquipmentsItem">
                                                    <td width="80" height="80">
                                                        <div ng-init="item = equipmentItem.item"  style="margin-top: 10px">
                                                            <div ng-show="!(equipmentItem.id == -1)">
                                                                <section class="ng-tooltip-section">
                                                                    <div style="position: relative">
                                                                        <label ng-if="equipmentItem.level > 0" class="item-level-text">+{{equipmentItem.level}}</label>
                                                                        <img ng-if="item" ng-dblClick="unequip(equipmentItem)" width="80" height="80" img-pre-load="{{item.imageUrl}}"
                                                                             data-tooltip-trigger data-html="true" data-tooltip-text="{{renderTooltip(equipmentItem)}}"/>
                                                                    </div>
                                                                    <!-- Active pg-ng-tooltip lib-->
                                                                    <div data-pg-ng-tooltip class="ng-tooltip"></div>
                                                                </section>
                                                            </div> 
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="right" class="contentred">
                                            <a href ng-click="showDetail()">Xem thông tin</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300" align="center" valign="top" bgcolor="white">
                                            <img ng-if="character.sex == 'Male'" ng-src="{{character.characterClass.imageUrl + '_men.png'}}" width="300" height="500" />
                                            <img ng-if="character.sex == 'Female'" ng-src="{{character.characterClass.imageUrl + '_women.png'}}" width="300" height="500" />
                                        </td>
                                    </tr>
                                </table>
                            </td>

                            <td >
                                <table width="240" cellpadding="0" cellspacing="0" class="contentredtable">
                                    <tr>
                                        <td align="right" class="contentred">Vật phẩm</td>
                                    </tr>
                                    <tr>
                                        <td style="height: 450px" valign="top">
                                            <table align="top" cellspacing="0" cellpadding="0" class="contentredtable" ng-repeat="row in itemRows">
                                                <tr>
                                                    <td ng-if="row.items[0]" width="120" height="112"  align="center" >
                                                        <div ng-init="item = row.items[0].item">
                                                            <section class="ng-tooltip-section">
                                                                <div style="position: relative">
                                                                    <label ng-if="row.items[0].level > 0" class="item-level-text">+{{row.items[0].level}}</label>
                                                                    <img ng-if="item" ng-dblClick="itemEquip(row.items[0])" id="item-{{row.items[0].id}}" ng-right-click="showContextMenu(row.items[0])"  width="80" height="80" img-pre-load="{{item.imageUrl}}" 
                                                                         data-tooltip-trigger data-html="true" data-tooltip-text="{{renderTooltip(row.items[0])}}" />
                                                                </div>
                                                                <!-- Active pg-ng-tooltip lib-->
                                                                <div data-pg-ng-tooltip class="ng-tooltip"></div>
                                                            </section>
                                                        </div>
                                                    </td>
                                                    <td ng-if="row.items[1]" width="120" height="112"  align="center">
                                                        <div ng-init="item = row.items[1].item">
                                                            <section class="ng-tooltip-section">
                                                                <div style="position: relative">
                                                                    <label ng-if="row.items[1].level > 0" class="item-level-text">+{{row.items[1].level}}</label>
                                                                    <img ng-if="item" ng-dblClick="itemEquip(row.items[1])" id="item-{{row.items[1].id}}" ng-right-click="showContextMenu(row.items[1])"  width="80" height="80" img-pre-load="{{item.imageUrl}}"
                                                                         data-tooltip-trigger data-html="true" data-tooltip-text="{{renderTooltip(row.items[1])}}"/>
                                                                </div>
                                                                <!-- Active pg-ng-tooltip lib-->
                                                                <div data-pg-ng-tooltip class="ng-tooltip"></div>
                                                            </section>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <div id="pagination-box"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="avatarDetailDiv" ng-show="isShowDetail" class="ng-hide">
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" class="headerred">Th&ocirc;ng tin Avatar</td>
            </tr>
            <tr>
                <td class="contentred">
                    <table style="width: 100%" cellspacing="0" cellpadding="0">
                        <tr class="backgroundform">
                            <td valign="top">
                                <table style="width: 100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" class="headerred">{{characterInfo.name}}</td>
                                    </tr>
                                    <tr >
                                        <td class="contentredtable"><strong>SỨC MẠNH</strong></td>
                                        <td class="contentredtable">{{characterInfo.strength}}</td>
                                    </tr>
                                    <tr >
                                        <td class="contentredtable"><strong>PHÒNG THỦ</strong></td>
                                        <td class="contentredtable">{{characterInfo.defense}}</td>
                                    </tr>
                                    <tr >
                                        <td class="contentredtable"><strong>TỐC ĐỘ</strong></td>
                                        <td class="contentredtable">{{characterInfo.speed}}</td>
                                    </tr>
                                    <tr >
                                        <td class="contentredtable"><strong>TRÍ TUỆ</strong></td>
                                        <td class="contentredtable">{{characterInfo.intelligent}}</td>
                                    </tr>
                                    <tr >
                                        <td class="contentredtable"><strong>MÁU</strong></td>
                                        <td class="contentredtable">{{characterInfo.hp}}</td>
                                    </tr>
                                    <tr >
                                        <td class="contentredtable"><strong>KINH NGHIỆM</strong></td>
                                        <td class="contentredtable">{{characterInfo.experience}}</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="56%" align="right" valign="top">
                                <table width="405" cellpadding="3" cellspacing="0" class="contentredtable">
                                    <tr>
                                        <td colspan="2" align="right">
                                            <table width="73%" align="right" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="buttonyellow">Danh Hiệu</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td width="105"  >
                                            <div ng-repeat="equipmentItem in characterEquipmentsItem">
                                                <div ng-init="item = equipmentItem.item"  style="margin-top: 10px">
                                                    <div ng-show="!(equipmentItem.id == -1)">
                                                        <section class="ng-tooltip-section">
                                                            <div style="position: relative">
                                                                <label ng-if="equipmentItem.level > 0" class="item-level-text">+{{equipmentItem.level}}</label>

                                                                <img ng-if="item" width="102" height="100" img-pre-load="{{item.imageUrl}}"
                                                                     data-tooltip-trigger data-html="true" data-tooltip-text="{{renderTooltip(equipmentItem)}}"/>
                                                            </div>
                                                            <!-- Active pg-ng-tooltip lib-->
                                                            <div data-pg-ng-tooltip class="ng-tooltip"></div>
                                                        </section>
                                                    </div> 
                                                </div>
                                            </div>
                                        </td>
                                        <td width="320" rowspan="5" align="center" valign="middle">
                                            <img ng-if="characterInfo.imageUrl != null" img-pre-load="{{characterInfo.imageUrl}}" width="302" height="498" />
                                            <img ng-if="characterInfo.imageUrl == null" ng-src="images/profile_default.jpg" width="302" height="498" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>