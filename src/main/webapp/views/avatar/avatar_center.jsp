<%@page import="org.springframework.security.core.authority.SimpleGrantedAuthority"%>
<%@page import="java.util.Collection"%>
<%@page import="org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper"%>
<%@page import="java.lang.String"%>
<%@page import="org.springframework.security.core.GrantedAuthority"%>
<%@page import="java.util.List"%>
<%@page import="org.springframework.security.core.Authentication"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td height="80px">&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Bảng quản lý</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="contentred">
                <table width="100%" border="0" cellspacing="0" cellpadding="10">
                    <tr height="110">
                        <td align="center">
                            <a ui-sref="memberDetail">
                                <img src="images/button/thong_tin_ca_nhan_button.png" width="198" height="79" />
                            </a>
                        </td>
                        <td align="center">
                            <a ui-sref="petCenter">
                                <img src="images/button/thu_cung_button.png" width="198" height="79" />
                            </a>
                        </td>
                        <td align="center">
                            <a ui-sref="summonCenter">
                                <img src="images/button/linh_thu_button.png" width="198" height="79" />
                            </a>
                        </td>
                    </tr>
                    <tr height="110">
                        <td align="center">
                            <a ui-sref="itemCenter">
                                <img src="images/button/vat_pham_button.png" width="198" height="79" />
                            </a>
                        </td>
                        <td align="center">
                            <a ui-sref="friendCenter">
                                <img src="images/button/ban_be_button.png" width="198" height="79" />
                            </a>
                        </td>
                        <td align="center">
                            <img src="images/button/trien_lam_button.png" width="198" height="79" /></td>
                    </tr>
                    <tr height="110">
                        <td align="center">
                            <img src="images/button/nhat_ky_button.png" width="198" height="79" />
                        </td>
                        <td align="center">
                            <a ui-sref="avatarView">
                                <img src="images/button/avatar_button.png" width="198" height="79" />
                            </a>
                        </td>
                    </tr>
                    <tr height="110">
                        <%
                            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                            Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
                            boolean authorized = authorities.contains(new SimpleGrantedAuthority("ROLE_ADMIN"));
                            if (authorized) {
                        %>

                        <td align="center">
                            <a href="#/admin/">
                                <img src="images/button/admin_button.png" width="198" height="79" />
                            </a>
                        </td>
                        <%
                            }
                        %>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>