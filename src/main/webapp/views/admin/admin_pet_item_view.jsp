<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên item</td>
            <td><input type="text" ng-model="item.name" /></td>
        </tr>
        <tr>
            <td>Mô tả</td>
            <td><textarea ng-model="item.description" style="text-align:left; height: 120px;width:174px" multiple="multiple"></textarea></td>
        </tr>
        <tr>
            <td>Loại</td>
            <td>
                <table>
                    <tr>
                        <td >
                            <select ng-model="item.itemClass.id">
                                <option ng-repeat="itemClass in itemClasses" ng-value="itemClass.id" ng-selected="itemClass.id == $parent.item.itemClass.id">{{itemClass.name}}</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Hình ảnh</td>
            <td>
                <div>
                    <img ng-src="{{imageSource}}" style="width:96px;height:96px;">
                    <!--<div ng-if="uploadedImages['image1'] != ''" style="margin: 50px 0 0;">
                        <img ng-src="{{uploadedImages['image1']}}" style="width:96px;height:96px;"/>
                    </div>-->
                    <div ng-if="itemUploader">
                        <input type="file" nv-file-select uploader="itemUploader"/>
                    </div>
                    <div class="progress" >
                        <div class="progress-bar" role="progressbar" ng-style="{ 'width': itemUploader.progress + '%' }"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Cấp độ yêu cầu</td>
            <td><input type="number" ng-model="item.levelRequire" /></td>
        </tr>
        <tr>
            <td>Chỉ số ma thuật</td>
            <td><input type="number" ng-model="item.magic" /></td>
        </tr>
        <tr>
            <td>Độ bền</td>
            <td><input type="number" ng-model="item.endurance" /></td>
        </tr>
        <tr>
            <td>Có thể mua</td>
            <td>
                <input type="checkbox" ng-model="item.sell" ng-value="pet.catched"/>
            </td>
        </tr>
        <tr>
            <td>Bán tại</td>
            <td>
                <select ng-model="item.shopType" ng-value="0">
                    <option value="0">Ẩn</option>
                    <option value="1">Cửa hàng vũ khí</option>
                    <option value="2">Cửa hàng dược phẩm</option>
                    <option value="3">Cửa hàng cao cấp</option>
                    <option value="4">Cửa hàng tạp hóa</option>
                    <option value="5">Cửa hàng hoàng kim</option>
                    <option value="6">Cửa hàng đặc biệt</option>
                </select>
            </td>
        </tr>
        <tr ng-show="item.itemClass.id == 14">
            <td>Thuộc tính tăng chỉ số</td>
            <td>
                <select ng-model="item.increasingAttribute" >
                    <option ng-selected="item.increasingAttribute == 'Strength'">Strength</option>
                    <option ng-selected="item.increasingAttribute == 'Defense'">Defense</option>
                    <option ng-selected="item.increasingAttribute == 'Intelligent'">Intelligent</option>
                    <option ng-selected="item.increasingAttribute == 'Speed'">Speed</option>
                </select>
            </td>
        </tr>

        <tr ng-show="item.sell">
            <td>Giá mua (Crystal)</td>
            <td><input type="number" ng-model="item.crystalCostBuy" /></td>
        </tr>
        <tr>
            <td>Giá bán (Crystal)</td>
            <td><input type="number" ng-model="item.crystalCostSell" /></td>
        </tr>
        <tr ng-show="item.sell">
            <td>Giá mua (Gold)</td>
            <td><input type="number" ng-model="item.goldCostBuy" /></td>
        </tr>
        <tr>
            <td>Giá bán (Gold)</td>
            <td><input type="number" ng-model="item.goldCostSell" /></td>
        </tr>
        <tr ng-show="item.sell">
            <td>Giá mua (Tài phú)</td>
            <td><input type="number" ng-model="item.bcoinCostBuy" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="Update" ng-click="submit()" />
            </td>
        </tr>
    </table>
</div>