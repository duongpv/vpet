<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên loài</td>
            <td><input type="text" ng-model="summon.name" /></td>
        </tr>
        <tr>
            <td>Mô tả</td>
            <td><textarea ng-model="summon.description" style="text-align:left; height: 120px;width:174px" multiple="multiple"></textarea></td>
        </tr>
        <tr>
            <td>Lớp</td>
            <td>
                <table>
                    <tr>
                        <td ng-repeat="summonClass in summonClasses" ng-init="summon.petClass = summonClasses[0]">
                            <input type="radio" name="summonClass" ng-value="summonClass.id" ng-model="$parent.summonClassId" ng-change="summonClassChange(summonClass)"/>
                            <label>{{summonClass.name}}</label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Linh thú nhiệm vụ</td>
            <td><input type="checkbox" ng-model="summon.quest" /></td>
        </tr>
        <tr>
            <td>Có thể mua</td>
            <td>
                <input type="checkbox" ng-model="summon.sell" ng-value="summon.sell"/>
            </td>
        </tr>
        <tr ng-show="summon.sell">
            <td>Giá mua (Crystal)</td>
            <td><input type="number" ng-model="summon.crystalCostBuy" /></td>
        </tr>
        <tr ng-show="summon.sell">
            <td>Giá mua (Gold)</td>
            <td><input type="number" ng-model="summon.goldCostBuy" /></td>
        </tr>
        <tr ng-show="summon.sell">
            <td>Giá mua (Tài phú)</td>
            <td><input type="number" ng-model="summon.bcoinCostBuy" /></td>
        </tr>

        <tr>
            <td>Có săn</td>
            <td>
                <input type="checkbox" ng-model="summon.hunting" ng-value="summon.hunting"/>
            </td>
        </tr>
        <tr>
            <td>Map</td>
            <td>
                <table>
                    <tr ng-repeat="summonArea in summon.areas">
                        <td>
                            <select ng-model="summonArea.id" >
                                <option ng-repeat="area in areas" ng-selected="summonArea.id == area.id" ng-value="area.id">
                                    {{area.name}}
                                </option>
                            </select>
                            <div></div>
                            <button ng-click="removeMap(summonArea.id)">Remove map</button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr> 
            <td></td>
            <td>
                <button ng-click="addMap()">Add map</button>
            </td>
        </tr>
        <tr>
            <td>Hình ảnh linh thú</td>
            <td>
                <div>
                    <img ng-src="{{imageSource}}" style="width:96px;height:96px;">
                    <!--<div ng-if="uploadedImages['image1'] != ''" style="margin: 50px 0 0;">
                        <img ng-src="{{uploadedImages['image1']}}" style="width:96px;height:96px;"/>
                    </div>-->
                    <div ng-if="imageUploader">
                        <input type="file" nv-file-select uploader="imageUploader"/>
                    </div>
                    <div class="progress" >
                        <div class="progress-bar" role="progressbar" ng-style="{ 'width': imageUploader.progress + '%' }"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Chỉ số phép thuật</td>
            <td><input type="number" ng-model="summon.magic" /></td>
        </tr>
        <tr ng-show="summon.summonClass.id == 1">
            <td>Thuộc tính tăng chỉ số</td>
            <td>
                <input type="checkbox" ng-model="summon.increasingStrength">Strength<br>
                <input type="checkbox" ng-model="summon.increasingDefense">Defense<br>
                <input type="checkbox" ng-model="summon.increasingIntelligent">Intelligent<br>
                <input type="checkbox" ng-model="summon.increasingSpeed">Speed<br>
                <input type="checkbox" ng-model="summon.increasingHp">Hp<br>
            </td>
        </tr>
        <tr ng-show="summon.summonClass.id == 2">
            <td>Số máu hồi(%)</td>
            <td><input type="number" ng-model="summon.hpRegen" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="Update" ng-click="submit()" />
            </td>
        </tr>
    </table>
</div>