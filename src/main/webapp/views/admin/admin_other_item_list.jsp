<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Other Item List</td>
        </tr>
        <tr>
            <td class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>Loại</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="item in itemes">
                        <td>{{$index + 1}}</td>
                        <td>{{item.name}}</td>
                        <td>{{item.description}}</td>
                        <td>{{item.itemClass.name}}</td>
                        <td>
                            <a ui-sref="adminOtherItemDetail({id: {{item.id}}})">View - </a>
                            <a href ng-click="delete(item.id, $index)">Delete</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <a ui-sref="adminOtherItemAdd">Create</a>
</div>