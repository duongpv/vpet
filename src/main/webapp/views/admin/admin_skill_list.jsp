<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- petSampleController -->
<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Skill List</td>
        </tr>
        <tr>
            <td class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>Class</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="skill in skillList">
                        <td>{{$index + 1}}</td>
                        <td>{{skill.name}}</td>
                        <td>{{skill.description}}</td>
                        <td>{{skill.skillClass.name}}</td>
                        <td>
                            <a ui-sref="adminSkillDetail({id: {{skill.id}}})">View</a>
                            <!--
                            -
                            <a href ng-click="delete(skill.id, $index)">Delete</a>
                            -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <a ui-sref="adminSkillAdd">Create</a>
    <a ui-sref="adminCenter">Back</a>
</div>
