<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên loài</td>
            <td><input type="text" ng-model="monster.name" /></td>
        </tr>
        <tr>
            <td>Mô tả</td>
            <td><textarea ng-model="monster.description" style="text-align:left; height: 120px;width:174px" multiple="multiple"></textarea></td>
        </tr>
        <tr>
            <td>Map</td>
            <td>
                <table>
                    <tr ng-repeat="monsterArea in monster.areas">
                        <td>
                            <select ng-model="monsterArea.id" >
                                <option ng-repeat="area in areas" ng-selected="monsterArea.id == area.id" ng-value="area.id">
                                    {{area.name}}
                                </option>
                            </select>
                            <div></div>
                            <button ng-click="removeMap(monsterArea.id)">Remove map</button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr> 
            <td></td>
            <td>
                <button ng-click="addMap()">Add map</button>
            </td>
        </tr>
        <tr>
            <td>Tỉ lệ xuất hiện</td>
            <td><input type="number" ng-model="monster.appearRate" /></td>
        </tr>
        <tr>
            <td>Quái nhiệm vụ</td>
            <td><input type="checkbox" ng-model="monster.quest" /></td>
        </tr>
        <tr>
            <td>Hình ảnh quái</td>
            <td>
                <div>
                    <img ng-src="{{imageSource}}" style="width:96px;height:96px;">
                    <!--<div ng-if="uploadedImages['image1'] != ''" style="margin: 50px 0 0;">
                        <img ng-src="{{uploadedImages['image1']}}" style="width:96px;height:96px;"/>
                    </div>-->
                    <div ng-if="imageUploader">
                        <input type="file" nv-file-select uploader="imageUploader"/>
                    </div>
                    <div class="progress" >
                        <div class="progress-bar" role="progressbar" ng-style="{ 'width': imageUploader.progress + '%' }"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Cấp độ</td>
            <td><input type="number" ng-model="monster.level" /></td>
        </tr>
        <tr>
            <td>Chỉ số % sức mạnh</td>
            <td><input type="number" ng-model="monster.powerRate" /></td>
        </tr>
        <tr>
            <td>Chỉ số HP</td>
            <td><input type="number" ng-model="monster.hp" /></td>
        </tr>
        <tr>
            <td>Chỉ số SM</td>
            <td><input type="number" ng-model="monster.strength" /></td>
        </tr>
        <tr>
            <td>Chỉ số phòng thủ</td>
            <td><input type="number" ng-model="monster.defense" /></td>
        </tr>
        <tr>
            <td>Chỉ số thông minh</td>
            <td><input type="number" ng-model="monster.intelligent" /></td>
        </tr>
        <tr>
            <td>Chỉ số tốc độ</td>
            <td><input type="number" ng-model="monster.speed" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="Update" ng-click="submit()" />
            </td>
        </tr>
    </table>
</div>