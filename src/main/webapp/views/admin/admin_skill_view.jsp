<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên loài</td>
            <td><input type="text" ng-model="skill.name" /></td>
        </tr>
        <tr>
            <td>Mô tả</td>
            <td><textarea ng-model="skill.description" style="text-align:left; height: 120px;width:174px" multiple="multiple"></textarea></td>
        </tr>
        <tr>
            <td>Skill class</td>
            <td>
                <table>
                    <tr >
                        <td>
                            <select ng-model="skill.skillClass.id" >
                                <option ng-repeat="skill in skillClasses" ng-selected="skillClass.id == skill.id"  ng-value="skill.id">
                                    {{skill.name}}
                                </option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Map</td>
            <td>
                <table>
                    <tr ng-repeat="skillArea in skill.areas">
                        <td>
                            <select ng-model="skillArea.id" >
                                <option ng-repeat="area in areas" ng-selected="skillArea.id == area.id" ng-value="area.id">
                                    {{area.name}}
                                </option>
                            </select>
                            <div></div>
                            <button ng-click="removeMap(skillArea.id)">Remove map</button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr> 
            <td></td>
            <td>
                <button ng-click="addMap()">Add map</button>
            </td>
        </tr>

        <tr>
            <td>Hình skill</td>
            <td>
                <div>
                    <img ng-src="{{imageSource}}" style="width:96px;height:96px;">
                    <div ng-if="imageUploader">
                        <input type="file" nv-file-select uploader="imageUploader"/>
                    </div>
                    <div class="progress" >
                        <div class="progress-bar" role="progressbar" ng-style="{ 'width': imageUploader.progress + '%' }"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Khả năng xảy ra</td>
            <td><input type="number" ng-model="skill.rate" /></td>
        </tr>

        <tr>
            <td>Tăng sức mạnh(%)</td>
            <td><input type="number" ng-model="skill.rateStrength" /></td>
        </tr>
        <tr>
            <td>Tăng phòng thủ(%)</td>
            <td><input type="number" ng-model="skill.rateDefense" /></td>
        </tr>
        <tr>
            <td>Tăng thông minh(%)</td>
            <td><input type="number" ng-model="skill.rateIntelligent" /></td>
        </tr>
        <tr>
            <td>Tăng tốc độ(%)</td>
            <td><input type="number" ng-model="skill.rateSpeed" /></td>
        </tr>
        <tr>
            <td>Chuyển hóa sát thương thành máu(%)</td>
            <td><input type="number" ng-model="skill.rateDameTaken" /></td>
        </tr>
        <tr>
            <td>Phản damage(%)</td>
            <td><input type="number" ng-model="skill.rateDameReflect" /></td>
        </tr>
        <tr>
            <td>Crit damage(%)</td>
            <td><input type="number" ng-model="skill.rateCritDame" /></td>
        </tr>
        <tr>
            <td>Gây độc % máu</td>
            <td><input type="number" ng-model="skill.ratePoisonDame" /></td>
        </tr>
        <tr>
            <td>Số lượt gây độc</td>
            <td><input type="number" ng-model="skill.turn" /></td>
        </tr>
        <tr>
            <td>Số lượt đóng băng</td>
            <td><input type="number" ng-model="skill.turnFreeze" /></td>
        </tr>
        <tr>
            <td>Hồi máu %</td>
            <td><input type="number" ng-model="skill.rateRegenHP" /></td>
        </tr>
        <tr>
            <td>Ăn cắp SM thành PT %</td>
            <td><input type="number" ng-model="skill.rateStealStrToDef" /></td>
        </tr>
        <tr>
            <td>Ăn cắp PT thành SM %</td>
            <td><input type="number" ng-model="skill.rateStealDefToStr" /></td>
        </tr>
        <tr>
            <td>Ăn cắp TM thành TD %</td>
            <td><input type="number" ng-model="skill.rateStealIntToSpeed" /></td>
        </tr>
        <tr>
            <td>Ăn cắp TD thành TM %</td>
            <td><input type="number" ng-model="skill.rateStealSpeedToInt" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="Update" ng-click="submit()" />
            </td>
        </tr>
    </table>
</div>