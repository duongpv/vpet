<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên</td>
            <td><input type="text" readonly ng-model="characterLevel.name" /></td>
        </tr>
        <tr>
            <td>Mô tả</td>
            <td><textarea ng-model="characterLevel.description" style="text-align:left; height: 120px;width:174px" multiple="multiple"></textarea></td>
        </tr>
        <tr>
            <td>Cấp độ nâng cấp</td>
            <td><input type="number" ng-model="characterLevel.levelRequire" /></td>
        </tr>
        <tr>
            <td>Vàng nâng cấp</td>
            <td><input type="number" ng-model="characterLevel.goldRequire" /></td>
        </tr>
        <tr>
            <td>Kim cương nâng cấp</td>
            <td><input type="number" ng-model="characterLevel.crystalRequire" /></td>
        </tr>
        <tr>
            <td>Số thú cưng</td>
            <td><input type="number" ng-model="characterLevel.maxPet" /></td>
        </tr>
        <tr>
            <td>Số linh thú</td>
            <td><input type="number" ng-model="characterLevel.maxSummon" /></td>
        </tr>
        <tr>
            <td>Số linh thú cho pet</td>
            <td><input type="number" ng-model="characterLevel.maxSummonForPet" /></td>
        </tr>
        <tr>
            <td>Số trang bị cho pet</td>
            <td><input type="number" ng-model="characterLevel.maxItemForPet" /></td>
        </tr>
        <tr>
            <td>Delay uống nước sông (phút)</td>
            <td><input type="number" ng-model="characterLevel.delayRecover" /></td>
        </tr>
        <tr>
            <td>Số trứng ấp</td>
            <td><input type="number" ng-model="characterLevel.maxEggHatch" /></td>
        </tr>
        <tr>
            <td>Loại item được xài (1 - 5)</td>
            <td><input type="number" ng-model="characterLevel.maxItemLevelId" /></td>
        </tr>
        <tr>
            <td>Loại item được chế (1 - 5)</td>
            <td><input type="number" ng-model="characterLevel.maxCreateItemLevelId" /></td>
        </tr>
        <tr>
            <td>Số lần vào vường cấm</td>
            <td><input type="number" ng-model="characterLevel.maxForbiddenMap" /></td>
        </tr>
        <tr>
            <td>
                <br>
                <button ng-click="submit()">Update</button>
            </td>
            <td></td>
        </tr>

    </table>
</div>