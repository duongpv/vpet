<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- petSampleController -->
<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Monster List</td>
        </tr>
        <tr>
            <td class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>Map</td>
                        <td>HP</td>
                        <td>SM</td>
                        <td>PT</td>
                        <td>TT</td>
                        <td>TD</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="monster in monsters">
                        <td>{{$index + 1}}</td>
                        <td>{{monster.name}}</td>
                        <td>{{monster.description}}</td>
                        <td>{{monster.areas[0].name}}</td>
                        <td>{{monster.hp}}</td>
                        <td>{{monster.strength}}</td>
                        <td>{{monster.defense}}</td>
                        <td>{{monster.intelligent}}</td>
                        <td><span>{{monster.speed}}</span></td>
                        <td>
                            <a ui-sref="adminMonsterItemDetail({id: {{monster.id}}})">View</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <a ui-sref="adminCenter">Back</a>
</div>