<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Character Level</td>
        </tr>
        <tr>
            <td class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="characterLevel in characterLevels">
                        <td>{{$index + 1}}</td>
                        <td>{{characterLevel.name}}</td>
                        <td>{{characterLevel.description}}</td>
                        <td>
                            <a ui-sref="adminCharacterLevelDetail({id: {{characterLevel.id}}})">View</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>