<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- petSampleController -->
<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Character List</td>
        </tr>
        <tr>
            <td class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Name</td>
                        <td>Class</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="character in characterList">
                        <td>{{$index + 1}}</td>
                        <td>{{character.name}}</td>
                        <td>{{character.characterClass.name}}</td>
                        <td>
                            <a ui-sref="adminCharacterDetail({id: {{character.id}}})">View</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <a ui-sref="adminCenter">Back</a>
</div>
