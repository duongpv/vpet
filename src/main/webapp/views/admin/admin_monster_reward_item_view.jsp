<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên loài</td>
            <td><input type="text" ng-model="monster.name" /></td>
        </tr>

        <tbody  ng-repeat="rewardItem in monster.monsterRewardItems">
            <tr>
                <td><hr></td>
                <td><hr></td>
            </tr>
            <tr>
                <td>Tên vật phẩm</td>
                <td>
                    <div>
                        <strong>{{rewardItem.item.name}}</strong>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Hình ảnh</td>
                <td>
                    <div>
                        <img ng-src="{{imageSources[$index]}}" style="width:96px;height:96px;">
                    </div>
                </td>
            </tr>
            <tr>
                <td>Tỉ lệ rớt</td>
                <td>
                    <div>
                        <input type="number" ng-model="rewardItem.rate"/>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" value="Delete" ng-click="delete($index)" />
                </td>
            </tr>  
        </tbody>

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>
        <tr>
            <td>Vật phẩm</td>
            <td>
                <table>
                    <tr>
                        <td >
                            <select ng-change="itemChange(tempItem.id)" ng-model="tempItem.id">
                                <option ng-repeat="item in items" ng-value="item.id">{{item.name}}</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        <tr>     
        <tr>
            <td>Hình ảnh</td>
            <td>
                <div>
                    <img ng-src="{{imageTempSources}}" style="width:96px;height:96px;">
                </div>
            </td>
        </tr>
        <tr>
            <td>Tỉ lệ rớt (%)</td>
            <td>
                <div>
                    <input type="number" ng-model="tempRate"/>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="Add" ng-click="add()" />
                <input type="button" value="Update" ng-click="submit()" />
            </td>
        </tr>
    </table>
</div>