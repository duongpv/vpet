<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên loài</td>
            <td><input type="text" ng-model="pet.name" /></td>
        </tr>
        <tr>
            <td>Mô tả</td>
            <td><textarea ng-model="pet.description" style="text-align:left; height: 120px;width:174px" multiple="multiple"></textarea></td>
        </tr>
        <tr>
            <td>Lớp</td>
            <td>
                <table>
                    <tr>
                        <td ng-repeat="petClass in petClasses" ng-init="pet.petClass = petClasses[0]">
                            <input type="radio" name="petClassId" ng-value="petClass.id" ng-model="$parent.petClassId" ng-change="petClassChange(petClass)"/>
                            <label>{{petClass.name}}</label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr ng-if="pet.petClass.id == 2">
            <td>Thuộc tính biến dị</td>
            <td>
                <select ng-model="pet.mutantAttribute" >
                    <option ng-selected="pet.mutantAttribute == 'Strength'">Strength</option>
                    <option ng-selected="pet.mutantAttribute == 'Defense'">Defense</option>
                    <option ng-selected="pet.mutantAttribute == 'Intelligent'">Intelligent</option>
                    <option ng-selected="pet.mutantAttribute == 'Speed'">Speed</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Có thể bắt</td>
            <td>
                <input type="checkbox" ng-model="pet.catched" ng-value="pet.catched"/>
            </td>
        </tr>  
        <tr ng-if="pet.catched">
            <td>Tỉ lệ bắt</td>
            <td>
                <input type="number" ng-model="pet.catchedRate" ng-value="pet.catchedRate"/>
            </td>
        </tr>

        <tr>
            <td>Có thể tiến hóa</td>
            <td>
                <input type="checkbox" ng-model="pet.evolved" ng-value="pet.evolved"/>
            </td>
        </tr>
        <tr>
            <td>Map</td>
            <td>
                <table>
                    <tr ng-repeat="petArea in pet.areas">
                        <td>
                            <select ng-model="petArea.id" >
                                <option ng-repeat="area in areas" ng-selected="petArea.id == area.id" ng-value="area.id">
                                    {{area.name}}
                                </option>
                            </select>
                            <div></div>
                            <button ng-click="removeMap(petArea.id)">Remove map</button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Tỉ lệ xuất hiện</td>
            <td>
                <input type="number" ng-model="pet.appearRate" ng-value="pet.appearRate"/>
            </td>
        </tr>
        <tr> 
            <td></td>
            <td>
                <button ng-click="addMap()">Add map</button>
            </td>
        </tr>

        <tr>
            <td>Hình ảnh thú</td>
            <td>
                <div>
                    <img ng-src="{{imageAvatarSource}}" style="width:96px;height:96px;">
                    <!--<div ng-if="uploadedImages['image1'] != ''" style="margin: 50px 0 0;">
                        <img ng-src="{{uploadedImages['image1']}}" style="width:96px;height:96px;"/>
                    </div>-->
                    <div ng-if="petAvatarUploader">
                        <input type="file" nv-file-select uploader="petAvatarUploader"/>
                    </div>
                    <div class="progress" >
                        <div class="progress-bar" role="progressbar" ng-style="{ 'width': petAvatarUploader.progress + '%' }"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Hình ảnh trứng</td>
            <td>
                <div>
                    <img ng-src="{{imageEggSource}}" style="width:96px;height:96px;">
                    <!--<div ng-if="uploadedImages['image1'] != ''" style="margin: 50px 0 0;">
                        <img ng-src="{{uploadedImages['image1']}}" style="width:96px;height:96px;"/>
                    </div>-->
                    <div ng-if="petEggUploader">
                        <input type="file" nv-file-select uploader="petEggUploader"/>
                    </div>
                    <div class="progress" >
                        <div class="progress-bar" role="progressbar" ng-style="{ 'width': petEggUploader.progress + '%' }"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Cấp độ mặc định</td>
            <td><input type="number" ng-model="pet.defaultLevel" /></td>
        </tr>
        <tr>
            <td>Cấp độ mặc định</td>
            <td><input type="number" ng-model="pet.defaultLevel" /></td>
        </tr>
        <tr>
            <td>Cấp độ yêu cầu</td>
            <td><input type="number" ng-model="pet.requireLevel" /></td>
        </tr>
        <tr>
            <td>Chỉ số HP</td>
            <td><input type="number" ng-model="pet.hp" /></td>
        </tr>
        <tr>
            <td>Chỉ số SM</td>
            <td><input type="number" ng-model="pet.strength" /></td>
        </tr>
        <tr>
            <td>Chỉ số phòng thủ</td>
            <td><input type="number" ng-model="pet.defense" /></td>
        </tr>
        <tr>
            <td>Chỉ số thông minh</td>
            <td><input type="number" ng-model="pet.intelligent" /></td>
        </tr>
        <tr>
            <td>Chỉ số tốc độ</td>
            <td><input type="number" ng-model="pet.speed" /></td>
        </tr>
        <tr>
            <td>Thời gian ấp (phút)</td>
            <td><input type="number" ng-model="pet.hatchTime" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="Update" ng-click="submit()" />
            </td>
        </tr>
    </table>
</div>