<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>

        <tr>
            <td>Sơ cấp</td>
            <td><input type="number" ng-model="config.primary" /></td>
        </tr>
        <tr>
            <td>Hạ cấp</td>
            <td><input type="number" ng-model="config.preIntermediate" /></td>
        </tr>
        <tr>
            <td>Trung cấp</td>
            <td><input type="number" ng-model="config.intermediate" /></td>
        </tr>
        <tr>
            <td>Cao Cấp</td>
            <td><input type="number" ng-model="config.high" /></td>
        </tr>
        <tr>
            <td>Siêu cấp</td>
            <td><input type="number" ng-model="config.supper" /></td>
        </tr>

        <tr>
            <td>
                <br>
                <button ng-click="submit()">Update</button>
            </td>
            <td></td>
        </tr>

    </table>
</div>