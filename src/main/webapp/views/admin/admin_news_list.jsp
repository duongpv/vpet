<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">News List</td>
        </tr>
        <tr>
            <td class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Title</td>
                        <td>Description</td>
                        <td>Link</td>
                        <td>Publish Date</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="news in newss">
                        <td>{{$index + 1}}</td>
                        <td>{{news.title}}</td>
                        <td>{{news.description}}</td>
                        <td>{{news.link}}</td>
                        <td>{{news.publishDate}}</td>
                        <td>
                            <a ui-sref="adminNewsDetail({id: {{news.id}}})">View - </a>
                            <a href ng-click="delete(news.id, $index)">Delete</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <a ui-sref="adminNewsAdd">Create</a>
</div>