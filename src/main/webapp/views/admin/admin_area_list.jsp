<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Map List</td>
        </tr>
        <tr>
            <td class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="area in areas">
                        <td>{{$index + 1}}</td>
                        <td>{{area.name}}</td>
                        <td>{{area.description}}</td>
                        <td>
                            <a ui-sref="adminAreaDetail({id: {{area.id}}})">View</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>