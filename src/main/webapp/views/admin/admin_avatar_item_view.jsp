<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên item</td>
            <td><input type="text" ng-model="item.name" /></td>
        </tr>
        <tr>
            <td>Mô tả</td>
            <td><textarea ng-model="item.description" style="text-align:left; height: 120px;width:174px" multiple="multiple"></textarea></td>
        </tr>
        <tr>
            <td>Loại</td>
            <td>
                <table>
                    <tr>
                        <td >
                            <select ng-model="item.itemClass.id">
                                <option ng-repeat="itemClass in itemClasses" ng-value="itemClass.id" ng-selected="itemClass.id == $parent.item.itemClass.id">{{itemClass.name}}</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Cấp bậc</td>
            <td>
                <table>
                    <tr>
                        <td >
                            <select ng-model="item.itemLevel.id">
                                <option value="1">Sơ Cấp</option>
                                <option value="2">Hạ Cấp</option>
                                <option value="3">Trung Cấp</option>
                                <option value="4">Thượng Cấp</option>
                                <option value="5">Siêu Cấp</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>
        <tr >
            <td>Nguyên liệu</td>
            <td >
                <div ng-repeat="itemRecipe in item.itemRecipes">
                    <div>{{itemRecipe.itemNeed.name}}</div>
                    Cấp độ <input ng-model="itemRecipe.itemLevel" type="number"/>
                    <button ng-click="deleteItemRecipe(itemRecipe.itemNeed.id)">Delete</button>
                    <hr>
                </div>
            </td>
        </tr>

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>
        <tr>
            <td>Chế tạo từ</td>
            <td>
                Tên <select ng-model="selectedItemNeed">
                    <option ng-repeat="item in items" ng-value="item.id">{{item.name}}</option>
                </select>
                <br>
                Cấp độ <input ng-model="selectedItemLevel" type="number"/>
                <br>
                <button ng-click="add()">Add</button>
            </td>
        </tr>

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>

        <tr>
            <td>Lớp nhân vật</td>
            <td>
                <table>
                    <tr>
                        <td >
                            <select ng-model="item.characterClass.id">
                                <option ng-repeat="characterClass in characterClasses" ng-value="characterClass.id" ng-selected="characterClass.id == $parent.item.characterClass.id">{{characterClass.name}}</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Hình ảnh</td>
            <td>
                <div>
                    <img ng-src="{{imageSource}}" style="width:96px;height:96px;">
                    <!--<div ng-if="uploadedImages['image1'] != ''" style="margin: 50px 0 0;">
                        <img ng-src="{{uploadedImages['image1']}}" style="width:96px;height:96px;"/>
                    </div>-->
                    <div ng-if="itemUploader">
                        <input type="file" nv-file-select uploader="itemUploader"/>
                    </div>
                    <div class="progress" >
                        <div class="progress-bar" role="progressbar" ng-style="{
                                    'width': itemUploader.progress + '%' }"></div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Cấp độ yêu cầu</td>
            <td><input type="number" ng-model="item.levelRequire" /></td>
        </tr>
        <tr>
            <td>Chỉ số HP</td>
            <td><input type="number" ng-model="item.hp" /></td>
        </tr>
        <tr>
            <td>Chỉ số sức mạnh</td>
            <td><input type="number" ng-model="item.strength" /></td>
        </tr>
        <tr>
            <td>Chỉ số phòng thủ</td>
            <td><input type="number" ng-model="item.defense" /></td>
        </tr>
        <tr>
            <td>Chỉ số thông minh</td>
            <td><input type="number" ng-model="item.intelligent" /></td>
        </tr>
        <tr>
            <td>Chỉ số tốc độ</td>
            <td><input type="number" ng-model="item.speed" /></td>
        </tr>
        <tr>
            <td>Chỉ số ma thuật</td>
            <td><input type="number" ng-model="item.magic" /></td>
        </tr>
        <tr>
            <td>Độ bền</td>
            <td><input type="number" ng-model="item.endurance" /></td>
        </tr>
        <tr>
            <td>Có thể mua</td>
            <td>
                <input type="checkbox" ng-model="item.sell" ng-value="pet.catched"/>
            </td>
        </tr>
        <tr>
            <td>Bán tại</td>
            <td>
                <select ng-model="item.shopType" ng-value="0">
                    <option value="0">Ẩn</option>
                    <option value="1">Cửa hàng vũ khí</option>
                    <option value="2">Cửa hàng dược phẩm</option>
                    <option value="3">Cửa hàng cao cấp</option>
                    <option value="4">Cửa hàng tạp hóa</option>
                    <option value="5">Cửa hàng hoàng kim</option>
                    <option value="6">Cửa hàng đặc biệt</option>
                </select>
            </td>
        </tr>
        <tr ng-show="item.sell">
            <td>Giá mua (Crystal)</td>
            <td><input type="number" ng-model="item.crystalCostBuy" /></td>
        </tr>
        <tr>
            <td>Giá bán (Crystal)</td>
            <td><input type="number" ng-model="item.crystalCostSell" /></td>
        </tr>
        <tr ng-show="item.sell">
            <td>Giá mua (Gold)</td>
            <td><input type="number" ng-model="item.goldCostBuy" /></td>
        </tr>
        <tr>
            <td>Giá bán (Gold)</td>
            <td><input type="number" ng-model="item.goldCostSell" /></td>
        </tr>
        <tr ng-show="item.sell">
            <td>Giá mua (Tài phú)</td>
            <td><input type="number" ng-model="item.bcoinCostBuy" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" value="Update" ng-click="submit()" />
            </td>
        </tr>
    </table>
</div>