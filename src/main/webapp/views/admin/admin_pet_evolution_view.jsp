<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên loài</td>
            <td><input type="text" ng-model="pet.name" /></td>
        </tr>

        <tbody  ng-repeat="evolution in pet.evolutions">
            <tr>
                <td><hr></td>
                <td><hr></td>
            </tr>
            <tr>
                <td>Tên</td>
                <td><input type=text" ng-model="evolution.name"/></td>
            </tr>
            <tr>
                <td>Mô tả</td>
                <td><input type="text" ng-model="evolution.description"/></td>
            </tr>  
            <tr>
                <td>Cấp độ yêu cầu</td>
                <td><input type="number" ng-model="evolution.requireLevel"/></td>
            </tr>  
            <tr>
                <td>Vật phẩm</td>
                <td>
                    <table>
                        <tr>
                            <td >
                                <select ng-model="evolution.item.id">
                                    <option ng-repeat="item in items" ng-value="item.id" ng-selected="item.id == evolution.item.id">{{item.name}}</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>Hình ảnh thú</td>
                <td>
                    <div>
                        <img ng-src="{{imageSources[$index]}}" style="width:96px;height:96px;">
                        <div ng-if="imageUploaders[$index]">
                            <input type="file" nv-file-select uploader="imageUploaders[$index]"/>
                        </div>
                        <div class="progress" >
                            <div class="progress-bar" role="progressbar" ng-style="{ 'width': imageUploaders[$index].progress + '%' }"></div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" value="Delete" ng-click="delete($index)" />
                </td>
            </tr>  
        </tbody>
        <tr>
            <td colspan="2">
                <input type="button" value="Add" ng-click="add()" />
                <input type="button" value="Update" ng-click="submit()" />
            </td>
        </tr>
    </table>
</div>