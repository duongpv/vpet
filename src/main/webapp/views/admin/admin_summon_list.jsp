<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- petSampleController -->
<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Summon List</td>
        </tr>
        <tr>
            <td class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>Class</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="summon in summons">
                        <td>{{$index + 1}}</td>
                        <td>{{summon.name}}</td>
                        <td>{{summon.description}}</td>
                        <td>{{summon.summonClass.name}}</td>
                        <td>
                            <a ui-sref="adminSummonDetail({id: {{summon.id}}})">View</a>
                            -
                            <a href ng-click="delete(summon.id, $index)">Delete</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <a ui-sref="adminSummonAdd">Create</a>
    <a ui-sref="adminCenter">Back</a>
</div>
