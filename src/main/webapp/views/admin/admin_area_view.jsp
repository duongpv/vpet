<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên bản đồ</td>
            <td><input type="text" ng-model="area.name" /></td>
        </tr>
        <tr>
            <td>Mô tả</td>
            <td><textarea ng-model="area.description" style="text-align:left; height: 120px;width:174px" multiple="multiple"></textarea></td>
        </tr>
        <tr>
            <td>Hạn chế</td>
            <td><input type="checkbox" ng-model="area.mapLimit" /></td>
        </tr>
        <tr>
            <td>Cấp độ yêu cầu</td>
            <td><input type="number" ng-model="area.levelRequire" /></td>
        </tr>
        <tr>
            <td>Vàng yêu cầu</td>
            <td><input type="number" ng-model="area.goldCost" /></td>
        </tr>
        <tr>
            <td>Kim cương yêu cầu</td>
            <td><input type="number" ng-model="area.crystalCost" /></td>
        </tr>
        <tr ng-if="area">
            <td>Map</td>
            <td valign="top" style="position: relative">
                <img ng-src="{{area.imageMapUrl}}"/>

                <div style="position: absolute;top: 0">
                    <div ng-repeat="(row, rowlement)  in mapElements">
                        <span ng-repeat="(col, colElement) in rowlement" ng-class="{'hunting-not-able': colElement.barrier == true}" class="hunting-box hunting-box-border">
                            <div id="r{{row}}c{{col}}">
                                <!--r{{row}}c{{col}}-->
                            </div>
                        </span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Vị trí dòng bắt đầu</td>
            <td><input type="number" ng-model="area.startRow" /></td>
        </tr>
        <tr>
            <td>Vị trí cột bắt đầu</td>
            <td><input type="number" ng-model="area.startCol" /></td>
        </tr>
        <tr>
            <td>Bản đồ</td>
            <td>
                <table>
                    <tr ng-repeat="row in mapElements">
                        <td ng-repeat="col in row">
                            <div><input type="checkbox" ng-model="col.barrier"></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>

        <tr>
            <td>Vị trí dòng</td>
            <td>
                <input ng-model="rowIdx" type="number"/>
            </td>
        </tr>
        <tr>
            <td>Vị trí cột</td>
            <td>
                <input ng-model="colIdx" type="number" value="0"/>
            </td>
        </tr>
        <tr>
            <td><button ng-click="addRow()"> Thêm dòng</button></td>
            <td><button ng-click="addCol()"> Thêm cột</button></td>
        </tr>
        <tr>
            <td><button ng-click="deleteRow()"> Xóa dòng</button></td>
            <td><button ng-click="deleteCol()"> Xóa cột</button></td>
        </tr>

        <tr>
            <td>
                <br>
                <button ng-click="submit()">Update</button>
            </td>
            <td></td>
        </tr>

    </table>
</div>