<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- petSampleController -->
<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Pet Sample List</td>
        </tr>
        <tr>
            <td class="contentred backgroundorange">
                <table width="99%" border="1" cellspacing="0" class="tableborderblack4px">
                    <tr align="center" valign="middle" class="headerblack">
                        <td>#</td>
                        <td>Name</td>
                        <td>Description</td>
                        <td>Class</td>
                        <td>HP</td>
                        <td>SM</td>
                        <td>PT</td>
                        <td>TT</td>
                        <td>TD</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr ng-repeat="pet in petList">
                        <td>{{$index + 1}}</td>
                        <td>{{pet.name}}</td>
                        <td>{{pet.description}}</td>
                        <td>{{pet.petClass.name}}</td>
                        <td>{{pet.hp}}</td>
                        <td>{{pet.strength}}</td>
                        <td>{{pet.defense}}</td>
                        <td>{{pet.intelligent}}</td>
                        <td><span>{{pet.speed}}</span></td>
                        <td>
                            <a ui-sref="adminPetDetail({id: {{pet.id}}})">View</a>
                            -
                            <a href ng-click="delete(pet.id, $index)">Delete</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <a ui-sref="adminPetAdd">Create</a>
    <a ui-sref="adminCenter">Back</a>
</div>
