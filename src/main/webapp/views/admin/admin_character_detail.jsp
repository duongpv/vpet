<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
    <table>
        <tr>
            <td>Tên nhân vật</td>
            <td><input type="text" ng-model="character.name" disabled/></td>
        </tr>
        <tr>
            <td>Vàng</td>
            <td>
                <input type="number" ng-model="character.gold" ng-value="character.gold" disabled/>
            </td>
        </tr>
        <tr>
            <td>Kim cương</td>
            <td>
                <input type="number" ng-model="character.crystal" ng-value="character.crystal" disabled/>
            </td>
        </tr>  
        <tr>
            <td>Tài phú</td>
            <td>
                <input type="number" ng-model="character.bcoin" ng-value="character.bcoin" disabled/>
            </td>
        </tr>  
        <tr>
            <td></td>
            <td>
                <button ng-click="block(character.id)">Block</button>
                <button ng-click="unblock(character.id)">Unblock</button>
            </td>
        </tr>  

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>
        <tr>
            <td>Thêm Vàng</td>
            <td>
                <input type="number" ng-model="gold"/>
            </td>
        </tr>  
        <tr>
            <td>
                <button ng-click="addGold(gold)">Thêm Vàng</button>
            </td>
        </tr>  

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>
        <tr>
            <td>Thêm Kim Cương</td>
            <td>
                <input type="number" ng-model="crystal"/>
            </td>
        </tr>  
        <tr>
            <td>
                <button ng-click="addCrystal(crystal)">Thêm Kim Cương</button>
            </td>
        </tr>

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>
        <tr>
            <td>Thêm Tài Phú</td>
            <td>
                <input type="number" ng-model="bcoin"/>
            </td>
        </tr>  
        <tr>
            <td>
                <button ng-click="addBcoin(bcoin)">Thêm Tài Phú</button>
            </td>
        </tr>

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>
        <tr>
            <td>Vật phẩm</td>
            <td>
                <table>
                    <tr>
                        <td >
                            <select ng-change="itemChange(tempItem.id)" ng-model="tempItem.id">
                                <option ng-repeat="item in items" ng-value="item.id">{{item.name}}</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        <tr>     
        <tr>
            <td>Hình ảnh</td>
            <td>
                <div>
                    <img ng-src="{{imageTempSources}}" style="width:96px;height:96px;">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <button ng-click="addItem(tempItem.id)">Thêm item</button>
            </td>
        </tr>  

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>
        <tr>
            <td>Tên thú cưng</td>
            <td><input type="text" ng-model="petName"/></td>
        </tr>
        <tr>
            <td>Thú cưng</td>
            <td>
                <table>
                    <tr>
                        <td >
                            <select ng-change="petChange(tempPet.id)" ng-model="tempPet.id">
                                <option ng-repeat="pet in pets" ng-value="pet.id">{{pet.name}}</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        <tr>     
        <tr>
            <td>Hình ảnh</td>
            <td>
                <div>
                    <img ng-src="{{petTempSources}}" style="width:96px;height:96px;">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <button ng-click="addPet(tempPet.id, petName)">Thêm thú cưng</button>
            </td>
        </tr>  

        <tr>
            <td><hr></td>
            <td><hr></td>
        </tr>
        <tr>
            <td>Tên linh thú</td>
            <td><input type="text" ng-model="summonName"/></td>
        </tr>
        <tr>
            <td>Linh thú</td>
            <td>
                <table>
                    <tr>
                        <td >
                            <select ng-change="summonChange(tempSummon.id)" ng-model="tempSummon.id">
                                <option ng-repeat="summon in summons" ng-value="summon.id">{{summon.name}}</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        <tr>     
        <tr>
            <td>Hình ảnh</td>
            <td>
                <div>
                    <img ng-src="{{summonTempSources}}" style="width:96px;height:96px;">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <button ng-click="addSummon(tempSummon.id, summonName)">Thêm linh thú</button>
            </td>
        </tr>  
    </table>
</div>