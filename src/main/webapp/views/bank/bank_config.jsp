<%@page contentType="text/html" pageEncoding="UTF-8"%>

<style>
    .field-margin{

    }
    .field-margin td{
        padding: 20px 20px 20px 20px;

        font-family: Arial, Helvetica, sans-serif;
        font-size: 18px;
    }
</style>
<div >
    <table width="100%" cellspacing="0" cellpadding="0" >
        <tr>
            <td height="80px">&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Thiết lập pass 2</td>
        </tr>
        <tr>
            <td align="center">
                <table class="backgroundform">
                    <tr class="field-margin">
                        <td>
                            Pass 1:
                        </td>
                        <td>
                            <input type="password" class="textfield" ng-model="bankConfig.pass">
                        </td>
                    </tr>
                    <tr class="field-margin">
                        <td>
                            Pass 2 cũ (nếu có):
                        </td>
                        <td>
                            <input type="password" class="textfield" ng-model="bankConfig.oldRootpass">
                        </td>
                    </tr>
                    <tr class="field-margin">
                        <td>
                            Pass 2 mới:
                        </td>
                        <td>
                            <input type="password" class="textfield" ng-model="bankConfig.rootpass">
                        </td>
                    </tr>
                    <tr class="field-margin">
                        <td>
                            Nhập lại pass 2:
                        </td>
                        <td>
                            <input type="password" class="textfield" ng-model="bankConfig.repeatRootpass">
                        </td>
                    </tr>

                    <tr class="field-margin" align="center">
                        <td colspan="2">
                            <a href ng-click="setBankConfig(bankConfig)">
                                <img src="../../images/button/ok_button_black.png" width="150px"/>
                            </a>
                            <a ui-sref="bank" style="margin-left: 50px;">
                                <img src="../../images/button/huy_button_black.png" width="150px"/>
                            </a>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
