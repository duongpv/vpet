<%@page contentType="text/html" pageEncoding="UTF-8"%>

<style>
    .field-margin{

    }
    .field-margin td{
        padding: 20px 20px 0px 20px;

        font-family: Arial, Helvetica, sans-serif;
        font-size: 18px;
    }
</style>
<div >
    <table width="100%" cellspacing="0" cellpadding="0" >
        <tr>
            <td height="80px">&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: 18pt;" align="center" valign="top" class="headerred">Chuyển tiền</td>
        </tr>
        <tr>
            <td>
                <div >
                    <table class="backgroundform" style="margin: 0 auto; width: 570px;">
                        <tr class="field-margin">
                            <td>Nhập số vàng muốn gửi</td>
                            <td>
                                <input type="number" ng-model="goldSend" class="textfield"/>
                            </td>
                        </tr>
                        <tr class="field-margin">
                            <td>Chọn bạn bè</td>
                            <td>
                                <select ng-model="goldFriendId" class="textfield">
                                    <option ng-repeat="friend in friends" ng-value="friend.id">
                                        {{friend.name}}
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr class="field-margin">
                            <td colspan="2" style="text-align: center; padding-bottom: 20px;" >
                                <a href ng-click="sendGold(goldSend, goldFriendId)">
                                    <img src="images/button/gui_button.png" width="150px"/>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <br>
                <br>
            </td>
            <td>
                <br>
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <div >
                    <table class="backgroundform"  style="margin: 0 auto; width: 570px;">
                        <tr class="field-margin">
                            <td>Nhập số kim cương muốn gửi</td>
                            <td>
                                <input type="number" ng-model="crystalSend" class="textfield"/>
                            </td>
                        </tr>
                        <tr class="field-margin">
                            <td>Chọn bạn bè</td>
                            <td>
                                <select ng-model="crystalFriendId" class="textfield">
                                    <option ng-repeat="friend in friends" ng-value="friend.id">
                                        {{friend.name}}
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr class="field-margin">
                            <td colspan="2" style="text-align: center; padding-bottom: 20px;" >
                                <a href ng-click="sendCrystal(crystalSend, crystalFriendId)">
                                    <img src="images/button/gui_button.png" width="150px"/>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
                <br>
                <a ui-sref="bankCenter">
                    <img src="images/button/quay_lai_button_long.png" width="150px">
                </a>
            </td>
        </tr>
    </table>
</div>
