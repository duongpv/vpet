<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="dialog-wrap" ng-show="display">

    <div class="dialog-box contentwhite">
        <button type="button" class="btn-dialog-close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">×</button>

        <div style="text-align: center">
            Bạn có chắc chắn bán mua với giá:
            <span ng-if="item.goldCostBuy > 0">{{item.goldCostBuy}} vàng </span>
            <span ng-if="item.crystalCostBuy > 0"> <span ng-if="item.crystalCostBuy > 0"> + </span> {{item.crystalCostBuy}} kim cương </span> 
            <span ng-if="item.bcoinCostBuy > 0"><span ng-if="item.goldCostBuy > 0 || item.crystalCostBuy > 0"> + </span> {{item.bcoinCostBuy}} tài phú </span>
            ?
        </div>
        <div style="text-align: center">
            <br>
            <img ng-click="close(true)" src="../../images/button/button_ok_size_s.png" />
        </div>
    </div>

</div>
<div class="dialog-fade" ng-show="display"></div>