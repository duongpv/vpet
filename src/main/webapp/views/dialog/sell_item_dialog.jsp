<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="dialog-wrap" ng-show="display">

    <div class="dialog-box contentwhite">
        <button type="button" class="btn-dialog-close" ng-click="close(false)" data-dismiss="modal" aria-hidden="true">×</button>

        <div style="text-align: center">Bạn có chắc chắn bán ?</div>
        <div style="text-align: center">
            <br>
            <img ng-click="close(true)" src="../../images/button/button_ok_size_s.png" />
        </div>
    </div>

</div>
<div class="dialog-fade" ng-show="display"></div>