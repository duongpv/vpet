<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div >
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred">Cửa hàng {{shopName}}</td>
        </tr>

        <tr>
            <td class="contentred" style="padding-top: 0px">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <!-- list items in current category -->
                    <tr ng-repeat="itemRow in itemRows" style="height: 100px">
                        <td valign="top" style="padding-bottom: 50px">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 250px;" ng-repeat="item in itemRow">
                                        <table
                                            style="color:#000; font-style:italic;cellspacing:10px;cellpadding:0;width: 100%;">
                                            <tr style="text-align:center;">
                                                <td >
                                                    <a >
                                                        <img img-pre-load="{{item.imageUrl}}" title="{{item.name}}" width="100" height="101" />
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr style="text-align:center;">
                                                <td><strong>{{item.name}}</strong></td>
                                            </tr>
                                            <tr style="text-align:center;">
                                                <td><strong>Ma thuật:&nbsp;</strong>{{item.magic}}&nbsp;</td>
                                            </tr>
                                            <tr style="text-align:center;" ng-if="item.endurance">
                                                <td><strong>Độ bền:&nbsp;</strong>{{item.endurance}}&nbsp;</td>
                                            </tr>
                                            <tr  style="text-align:center;">
                                                <td>
                                                    <strong>Giá&nbsp;</strong>
                                                    <span ng-if="item.goldCostBuy > 0"> 
                                                        {{item.goldCostBuy}} vàng
                                                    </span>
                                                    <span ng-if="item.crystalCostBuy > 0"> 
                                                        <span ng-if="item.goldCostBuy > 0"> + </span> {{item.crystalCostBuy}} kim cương
                                                    </span>
                                                    <span ng-if="item.bcoinCostBuy > 0"> 
                                                        <span ng-if="item.crystalCostBuy > 0 || item.goldCostBuy > 0"> + </span> {{item.bcoinCostBuy}} tài phú
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr style="text-align:center;">
                                                <td>
                                                    <div ng-click="buy(item)"  style="cursor: pointer"> 
                                                        <strong>Mua</strong>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div>
        <span>
            <a href ui-sref="shopCenter">
                <img style="margin-left: 30px;" src="images/button/quay_lai_button_large.png"/>
            </a>
        </span>
        <span style="float: right">
            <a href ui-sref="itemCenter">
                <img style="margin-right: 30px;" src="images/button/trang_vat_pham_button.png"/>
            </a>
        </span>
    </div>
</div>