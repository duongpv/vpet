<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top" class="headerred">Danh sách cửa hàng</td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top">
                <img  src="images/shop_logo.png"/>
            </td>
        </tr>

        <tr align="center" >
            <td style="padding-top: 25px;">
                <a href ui-sref="shopDetail({type: 1})">
                    <img  src="images/button/shop/cua_hang_vu_khi.png"/>
                </a>
            </td>
            <td style="padding-top: 25px;">
                <a href ui-sref="shopDetail({type: 2})">
                    <img  src="images/button/shop/cua_hang_duoc_pham.png"/>
                </a>
            </td>
            <td style="padding-top: 25px;">
                <a href ui-sref="shopDetail({type: 3})">
                    <img  src="images/button/shop/cua_hang_cao_cap.png"/>
                </a>
            </td>
        </tr>
        <tr  align="center" >
            <td style="padding-top: 25px;">
                <a href ui-sref="shopDetail({type: 4})">
                    <img  src="images/button/shop/cua_hang_tap_hoa.png"/>
                </a>
            </td>
            <td style="padding-top: 25px;">
                <a href ui-sref="shopDetail({type: 5})">
                    <img  src="images/button/shop/cua_hang_hoang_kim.png"/>
                </a>
            </td>
            <td style="padding-top: 25px;">
                <a href ui-sref="shopDetail({type: 6})">
                    <img  src="images/button/shop/cua_hang_dac_biet.png"/>
                </a>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>