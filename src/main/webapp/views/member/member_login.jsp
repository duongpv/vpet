<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div id="login">
    <br />
    <br />
    <br />
    <div  class="container" style="width:400px;">
        <form id="loginForm" name="loginForm" action="j_spring_security_check" method="POST" novalidate>
            <table width="100%" cellpadding="0" cellspacing="0" class="backgroundform">
                <tr>
                    <td class="backgroundform">
                        <label for="txtUsername"><strong>Tên tài khoản:</strong></label>
                    </td>
                    <td>
                        <input type="text" name="username" class="textfield" id="txtUsername" size="30" ng-model="loginInfo.username" required/>
                    </td>
                </tr>
                <tr>
                    <td class="backgroundform">
                        <label for="txtPassword"><strong>Mật khẩu:</strong></label>
                    </td>
                    <td>
                        <input type="password" name="password" class="textfield" id="txtPassword" size="30" ng-model="loginInfo.password" required/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center" class="backgroundform">
                        <div onclick="document.getElementById('loginForm').submit();">
                            <img src="images/button/button_ok.png" alt="" width="148" height="54" />
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
