<%-- 
    Document   : login_callback
    Created on : May 7, 2015, 6:21:20 PM
    Author     : Jeremy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <%
            String status = request.getParameter("error");
            String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();

            if (status != null) {
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", url + "/#/member/login");
            } else {
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", url + "/#/avatar/");
            }
        %>

        <%=url + "/#/member/login"%>
    </body>
</html>
