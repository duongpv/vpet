<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td align="center" class="headerred">Thông tin cá nhân</td>
    </tr>
    <tr>
        <td align="center" class="contentredtable">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="50"class="headerblack">{{character.name}}</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href ng-click="alert('view cua hang')">
                                        <img src="images/button/cua_hang_red_button.png" style="width:286px;height:58px;"/>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href ng-click="alert('view phong trien lam')">
                                        <img src="images/button/phong_trien_lam_button.png" style="width:287px;height:58px;"/>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href ng-click="upgrade()">
                                        <img src="images/button/member/update_button.png" style="width:287px;height:58px;"/>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td width="405" align="left" valign="top">
                        <table id="profile_info" width="100%" border="0" cellspacing="0" cellpadding="5">
                            <tr>
                                <td colspan="2">
                                    <!-- container -->
                                    <div style="position: relative;width:405px;height:399px;">
                                        <!-- background-element -->
                                        <div width="100%" height="100%">
                                            <img ng-if="character.imageUrl != null" img-pre-load="{{character.imageUrl}}" width="405" height="399" />
                                            <img ng-if="character.imageUrl == null" ng-src="images/profile_default.jpg" width="405" height="399"/>
                                        </div>
                                        <!-- overlay-element -->
                                        <div style="position:absolute;width:100px;height:100px;top:2px;left:2px;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td style="opacity:0.6;" 
                                                        onmouseover="this.style.setProperty('opacity', '1.0')" 
                                                        onmouseout="this.style.setProperty('opacity', '0.6')">
                                                        <a href ng-click="chooseImage = !chooseImage">
                                                            <img src="images/camera_icon.png" title="Thay đổi ảnh" width="39" height="32" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr ng-if="chooseImage == true">
                                                    <td height="30" class="backgroundyellowtransparency">
                                                        <div ng-if="imageUploader">
                                                            <a href ng-click="uploadeProfile()" style="text-decoration:none;">Tải ảnh lên</a>
                                                        </div>
                                                        <div ng-if="imageUploader" style="display: none">
                                                            <input id="imageUploader" type="file" nv-file-select uploader="imageUploader"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr ng-if="chooseImage == true">
                                                    <td height="30" class="backgroundyellowtransparency">
                                                        <a href ng-click="changeProfileImage('url')" style="text-decoration:none;">Chèn liên kết</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="120" class="contentblack1"><strong>Cấp độ:</strong></td>
                                <td>{{character.level}}</td>
                            </tr>
                            <tr>
                                <td width="120" class="contentblack1"><strong>Danh hiệu:</strong></td>
                                <td>{{character.characterLevel.name}}</td>
                            </tr>
                            <tr>
                                <td width="120" class="contentblack1"><strong>Bang hội:</strong></td>
                                <td>[???]</td>
                            </tr>
                            <tr>
                                <td width="120" class="contentblack1"><strong>Lớp nhân vật:</strong></td>
                                <td>{{character.characterClass.name}}</td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="text-align: center;">
                    <td colspan="2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <a ui-sref="petCenter">
                                        <img src="images/button/danh_sach_thu_cung_tab.png" 
                                             style="width:265px;height:62px;" />
                                    </a>
                                    <a href ng-click="tabs.active('petpet')">
                                        <img src="images/button/danh_sach_linh_thu_tab.png"
                                             style="opacity:{{tabs.petpet.opacity}};filter:alpha(opacity={{100 * tabs.petpet.opacity}});width:264px;height:62px;"/>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="tablegray">&nbsp;</td>
    </tr>
</table>