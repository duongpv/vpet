<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<br>
<br>
<br>
<div  class="container" style="width:400px">
    <form
        name="registerMemberForm"
        novalidate>
        <table width="100%" cellpadding="0" cellspacing="0" class="backgroundform">
            <tr >
                <td colspan="2" class="headerblack">Thông tin bắt buộc</td>
            </tr>
            <tr>
                <td class="backgroundform"><strong>Bí danh:</strong></td>
                <td style="padding-right: 10px !important;"><input type="text" class="textfield" size="30" placeholder="Bí danh dài từ 3-14 ký tự"
                                                                   ng-model="registerInfo.userName" ng-minlength="3" ng-maxlength="14" required/>
                </td>
            </tr>
            <tr>
                <td class="backgroundform"><strong>Mật khẩu:</strong></td>
                <td><input type="password" class="textfield" size="30" placeholder="Mật khẩu dài từ 3-14 ký tự"
                           ng-model="registerInfo.password" ng-minlength="3" ng-maxlength="14" required/>
                </td>
            </tr>
            <tr>
                <td class="backgroundform"><strong>Xác nhận mật khẩu:</strong></td>
                <td><input type="password" name="passwordRetype" class="textfield" size="30" placeholder="Mật khẩu dài từ 3-14 ký tự"
                           ng-model="passwordRetype" required/>
                    <p ng-show="submitted && (registerMemberForm.passwordRetype.$invalid || registerInfo.password != passwordRetype)" class="help-block">Mật khẩu không đúng.</p>	
                </td>
            </tr>
            <tr>
                <td class="backgroundform"><strong>Email:</strong></td>
                <td>
                    <input type="email" name="email" class="textfield" size="30" 
                           ng-model="registerInfo.email" required />
                    <p ng-show="submitted && registerMemberForm.email.$invalid" class="help-block">Email không hợp lệ.</p>
                </td>
            </tr>
            <!--
            <tr>
                <td class="backgroundform"><strong>Họ và tên:</strong></td>
                <td><input type="text" class="textfield" size="30" 
                           ng-model="registerInfo.fullName" required/>
                </td>
            </tr>
            <tr>
                <td class="backgroundform"><strong>Số điện thoại:</strong></td>
                <td><input type="tel" class="textfield" size="30" 
                           ng-model="registerInfo.phone" ng-minlength="7" ng-maxlength="16" required/>
                </td>
            </tr>
            <tr>
                <td class="backgroundform"><p><strong>Số CMND:</strong></p></td>
                <td><input type="text" class="textfield" size="30" 
                           ng-model="registerInfo.identityCard" ng-minlength="9" ng-maxlength="12" required/>
                </td>
            </tr>
            -->
            <tr>
                <td align="center" colspan="2" class="backgroundform">
                    <div vc-recaptcha key="model.key" 
                         on-success="setResponse(response)"></div>
                    <p ng-show="submitted && (registerInfo.captcha === null)" class="help-block">Chưa xác nhận captcha</p>
                    <p ng-show="submitted && (registerInfo.captcha === null)" class="help-block">Xác nhận captcha thất bại</p>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="backgroundform">
                    <a href ng-click="submitted = true;
                        submit()">
                        <img src="images/button/button_ok.png" alt="" width="148" height="54" />
                    </a>
                </td>
            </tr>
        </table>
    </form>
</div>