<%-- 
    Document   : hunter_art
    Created on : Oct 3, 2015, 7:50:33 PM
    Author     : Jeremy
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div ng-show="isHunting == false">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">{{area.name}}</td>
        </tr>
        <tr ng-if="area">
            <td align="center" valign="top" class="headerred">
                <img src="{{area.imageArtUrl}}" />
            </td>
        </tr>
        <tr>
            <td align="center" style="padding-bottom:14px;">
                <select ng-model="feeType" class="textfield" >
                    <option value="1" ng-show="area.goldCost != 0">Đóng {{area.goldCost}} gold lệ phí để đi săn</option>
                    <option value="2" ng-show="area.crystalCost != 0" >Đóng {{area.crystalCost}} kim cương lệ phí để đi săn</option>
                </select>
            </td>
            <%--<td align="center">Đóng {{area.goldCost}} gold lệ phí để đi săn</td>--%>
        </tr>
        <tr ng-if="area" >
            <td align="middle">
                <a href ng-click="ok(area.id, feeType)">
                    <img src="images/button/button_ok_size_s.png"/>
                </a>
            </td>
        </tr>
    </table>
</div>

<div ng-show="isHunting == true" align="center" ng-show="hasInProgressHunt == true" >
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" class="headerred">
                <div>
                    Bạn chưa kết thúc chuyến săn trước ở khu vực {{map.name}}.
                    <br />
                    OK - tiếp tục chuyến săn.
                    <br />
                    Bỏ chạy - kết thúc chuyến săn trước.
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">&nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <a ui-sref="worldHuntingArea">
                    <img src="images/button/button_ok_size_s.png" width="101" height="45" />
                </a>
                &nbsp;
                <a href ng-click="escape()">
                    <img src="images/button/bochay_button.png" width="101" height="45" />
                </a>
            </td>
        </tr>
    </table>
</div>