<%-- 
    Document   : hunter_art
    Created on : Oct 3, 2015, 7:50:33 PM
    Author     : Jeremy
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Bạn ném lưới {{netType}} vào con {{pet.name}}</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Con {{pet.name}} đã bị thu phục</td>
        </tr>
        <tr>
            <td align="middle">
                <table width="400" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center" valign="middle" >
                            <table width="100%" cellpadding="5" cellspacing="0" class="backgroundform">
                                <tr>
                                    <td align="center"  colspan="3">
                                        <img ng-if="pet.petClass.id != 2" img-pre-load="{{pet.imageAvatarUrl}}"  
                                             style="margin: 20px;"
                                             width="200px" height="200px" />

                                        <img ng-if="pet.petClass.id == 2" img-pre-load="{{pet.imageEggUrl}}"  
                                             style="margin: 20px;"
                                             width="200px" height="200px" />
                                    </td>
                                </tr>
                                <tr ng-if="pet.petClass.id != 2">
                                    <td align="center" class="headerred">
                                        Đặt tên cho con {{pet.name}}
                                    </td>
                                </tr>
                                <tr ng-show="pet.petClass.id != 2">
                                    <td align="center" colspan="2">
                                        <input type="text" width="100%" ng-model="name"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="3">
                                        <a href ng-click="submit()">
                                            <img src="images/button/button_ok_size_s.png" 
                                                 alt="" width="115" height="42" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>