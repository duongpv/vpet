<%-- 
    Document   : hunter_art
    Created on : Oct 3, 2015, 7:50:33 PM
    Author     : Jeremy
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Bạn gặp {{monster.name}} trong lúc săn</td>
        </tr>
        <tr>
            <td align="middle">
                <table width="400" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center" valign="middle" >
                            <table width="100%" cellpadding="5" cellspacing="0" class="backgroundform">
                                <tr>
                                    <td align="center"  colspan="2">
                                        <img ng-if="monster" img-pre-load="{{monster.imageUrl}}"  
                                             style="margin: 20px;"
                                             width="200px" height="200px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" >
                                        <a href ng-click="ok()">
                                            <img src="images/button/thach_dau_button.png" 
                                                 alt="" width="115" height="42"/>
                                        </a>
                                    </td>
                                    <td align="right">
                                        <a href ng-click="escape()">
                                            <img src="images/button/bo_chay_button_black.png" 
                                                 alt="" width="115" height="42" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>