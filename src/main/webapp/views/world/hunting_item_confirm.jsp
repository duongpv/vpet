<%-- 
    Document   : hunter_art
    Created on : Oct 3, 2015, 7:50:33 PM
    Author     : Jeremy
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Bạn nhặt được {{rewardItem.name}} trong lúc săn</td>
        </tr>
        <tr>
            <td align="middle">
                <table width="400" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center" valign="middle" >
                            <table id="hunting_item_confirm_tbl" width="100%" cellpadding="5" cellspacing="0" class="backgroundform">
                                <tr>
                                    <td align="center"  colspan="2">
                                        <img ng-if="rewardItem" img-pre-load="{{rewardItem.imageUrl}}"  
                                             style="margin: 20px;"
                                             width="200px" height="200px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" >
                                        <a href ng-click="ok()">
                                            <img src="images/button/button_ok_size_s.png" 
                                                 alt="" width="115" height="42"/>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>