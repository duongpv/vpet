<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div align="center">
    <br />
    <br />
    <br />
    <table width="400" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" valign="middle" class="backgroundred">
                <table width="100%" cellpadding="5" cellspacing="0" class="backgroundform">
                    <tr>
                        <td align="center" style="padding-top:14px;padding-bottom:8px;">
                            <strong>Chọn thú cưng để chiến đấu</strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><label for="select"></label>
                            <select name="select" class="textfield" required
                                    ng-model="battleInfo.petId"
                                    ng-options="pet.id as pet.name for pet in pets">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding-top:14px;padding-bottom:8px;">
                            <a href ng-click="createBattle()">
                                <img src="images/button/button_ok_size_s.png" alt="" width="115" height="42" />
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>