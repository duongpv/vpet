<%-- 
    Document   : hunter_art
    Created on : Oct 3, 2015, 7:50:33 PM
    Author     : Jeremy
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Bạn gặp {{pet.name}} trong lúc săn</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">
                <table style="width: 80%">
                    <tr>
                        <td>Lưới thường {{battleContext.normalNet}}</td>
                        <td>Lưới điện {{battleContext.electricNet}}</td>
                        <td>Lưới phép {{battleContext.magicNet}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="middle">
                <table width="400" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center" valign="middle" >
                            <table width="100%" cellpadding="5" cellspacing="0" class="backgroundform">
                                <tr>
                                    <td align="center"  colspan="3">
                                        <img ng-if="pet" img-pre-load="{{pet.imageAvatarUrl}}"  
                                             style="margin: 20px;"
                                             width="200px" height="200px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" >
                                        <a href ng-click="capture(1)">
                                            <img src="images/button/luoi_thuong_button.png" 
                                                 alt="" width="115" height="42"/>
                                        </a>
                                    </td>
                                    <td align="center" >
                                        <a href ng-click="capture(2)">
                                            <img src="images/button/luoi_dien_button.png" 
                                                 alt="" width="115" height="42"/>
                                        </a>
                                    </td>
                                    <td align="center">
                                        <a href ng-click="capture(3)">
                                            <img src="images/button/luoi_ma_thuat_button.png" 
                                                 alt="" width="115" height="42" />
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="3">
                                        <a href ng-click="escape()">
                                            <img src="images/button/bo_chay_button_black.png" 
                                                 alt="" width="115" height="42" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>