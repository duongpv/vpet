<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div style="text-align: center" id="hunting-area">
    <div class="headerred">Chuyến đi săn bắt đầu</div>
    <div class="headerred">Số bước còn lại {{mapContext.step}}</div>

    <div class="navigation-button">
        <a href ng-click="moveUp()">Đi lên</a>
    </div>
    <div class="navigation-button">
        <a href ng-click="moveLeft()">Qua trái</a> <a href ng-click="moveRight()" style="margin-left: 100px;">Qua phải</a>
    </div>
    <div class="navigation-button">
        <a href ng-click="moveDown()">Đi xuống</a>
    </div>

    <div class="hunting-able">
        <div ng-repeat="(row, rowlement)  in mapElements">
            <span ng-repeat="(col, colElement) in rowlement" class="hunting-box">
                <div id="r{{row}}c{{col}}">
                    <!--r{{row}}c{{col}}-->
                </div>
            </span>
        </div>
    </div>
    <br>
    <a href>
        <img ng-click="escape()" src="images/button/thoat_button.png" />
    </a>
</div>