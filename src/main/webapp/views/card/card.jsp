<%@page contentType="text/html" pageEncoding="UTF-8"%>

<br>
<br>
<br>
<div  class="container" style="width:400px">
    <table width="100%" cellpadding="0" cellspacing="0" class="backgroundform">
        <tr >
            <td colspan="2" class="headerblack">Thông tin bắt buộc</td>
        </tr>
        <tr>
            <td class="backgroundform"><strong>Loại card</strong></td>
            <td style="padding-right: 10px !important;">
                <select  ng-model="cardInfo.cardType" class="textfield">
                    <option value="VINA_PHONE">Vinaphone</option>
                    <option value="MOBI_FONE">MobiFone</option>
                    <option value="VTC_VCOIN">VTC Vcoin</option>
                    <option value="FPT_GATE">FPT Gate</option>
                    <option value="VIETTEL">Viettel</option>
                    <option value="MEGACARD">Megacard</option>
                    <option value="ONCASH">Oncash</option>
                </select>
            </td>
        </tr>

        <tr>
            <td class="backgroundform"><strong>Card serial</strong></td>
            <td style="padding-right: 10px !important;">
                <input ng-model="cardInfo.cardSerial" type="text" class="textfield" size="30"/>
            </td>
        </tr>

        <tr>
            <td class="backgroundform"><strong>Card pin</strong></td>
            <td style="padding-right: 10px !important;">
                <input ng-model="cardInfo.cardPin" type="text" class="textfield" size="30"/>
            </td>
        </tr>

        <tr>
            <td colspan="2" align="center" class="backgroundform">
                <a href ng-click="send()">
                    <img src="images/button/button_ok.png" alt="" width="148" height="54" />
                </a>
            </td>
        </tr>
    </table>
</div>
