<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="3"  valign="top" class="headerred">Nhận Nhiệm Vụ Anh Hùng</td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top">
                <img  src="images/quest/quest_hero_logo.png"/>
            </td>
        </tr>

        <tr>
            <td align="center" colspan="3" class="headerred">
                Yêu cầu đánh đủ số quái vật khi đi săn để về trả nhiệm vụ. Có 3 cấp độ để làm.  <br>
                Dễ: Đánh 100 quái <br>
                Trung Bình: Đánh 300 quái <br>
                Khó: Đánh 500 quái <br>
            </td>
        </tr>
        <tr align="center" >
            <td style="padding-top: 25px;">
                <a ng-click="accept(1)" href>
                    <img src="images/quest/quest_hero_easy.png"/>
                </a>
                <br>
                <span class="headerred">Dễ</span>
            </td>
            <td style="padding-top: 25px;">
                <a ng-click="accept(2)" href>
                    <img  src="images/quest/quest_hero_normal.png"/>
                </a>
                <br>
                <span class="headerred">Trung bình</span>
            </td>
            <td style="padding-top: 25px;">
                <a ng-click="accept(3)" href>
                    <img  src="images/quest/quest_hero_hard.png"/>
                </a>
                <br>
                <span class="headerred">Khó</span>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
</div>