<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="3"  valign="top" class="headerred">Nhận Nhiệm Vụ Tìm kiếm linh thú</td>
        </tr>
        <tr>
            <td colspan="3" align="center" valign="top">
                <img  src="images/quest/quest_summon_logo.png"/>
            </td>
        </tr>

        <tr>
            <td align="center" colspan="3" class="headerred">
                Thu thập đủ 4 linh thú theo yêu cầu, phần thưởng sẽ là 1 linh thú cấp cao hơn và điểm hoạt động. Có 3 cấp độ để làm
            </td>
        </tr>
        <tr align="center" >
            <td style="padding-top: 25px;">
                <a ng-click="accept(1)" href>
                    <img src="images/quest/quest_summon_easy.png"/>
                </a>
                <br>
                <span class="headerred">Dễ</span>
            </td>
            <td style="padding-top: 25px;">
                <a ng-click="accept(2)" href>
                    <img  src="images/quest/quest_summon_normal.png"/>
                </a>
                <br>
                <span class="headerred">Trung bình</span>
            </td>
            <td style="padding-top: 25px;">
                <a ng-click="accept(3)" href>
                    <img  src="images/quest/quest_summon_hard.png"/>
                </a>
                <br>
                <span class="headerred">Khó</span>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
</div>