<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0" ng-if="showCenter">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="3"  valign="top" class="headerred">Nhận Nhiệm Vụ</td>
        </tr>

        <tr align="center" >
            <td style="padding-top: 25px;">
                <a ui-sref="questItemCenter">
                    <img src="images/quest/quest_item.png"/>
                </a>
                <br>
            </td>
            <td style="padding-top: 25px;">
                <a ui-sref="questSummonCenter">
                    <img  src="images/quest/quest_summon.png"/>
                </a>
                <br>
            </td>
            <td style="padding-top: 25px;">
                <a ui-sref="questHeroCenter">
                    <img  src="images/quest/quest_hero.png"/>
                </a>
                <br>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>

    <!-- Hero quest -->
    <table width="100%" cellspacing="2" cellpadding="0" ng-if="quest.questType == 3 && !complete">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="2"  valign="top" class="headerred">Quái cần tiêu diệt</td>
        </tr>
        <!--
        <tr ng-if="quest.monster.imageUrl" class="headerred">
            <td align="center" colspan="2">
                <img img-pre-load="{{quest.monster.imageUrl}}">
            </td>
        </tr>
        -->
        <tr>
            <td align="center" colspan="2" valign="top" class="headerred">
               <span style="color: #00FF00">{{quest.questRequires[0].have}}</span> / {{quest.questRequires[0].require}}
            </td>
        </tr>

        <tr>
            <td align="center" colspan="1">
                <img ng-click="remove()" src="images/button/huy_button_red.png" width="117px" height="43px"/> 
            </td>
            <td align="center" colspan="1">
                <img ng-click="heroQuestComplete()" src="images/button/button_ok.png" width="117px" height="43px"/> 
            </td>
        </tr>
    </table>

    <!-- Summon quest -->
    <table width="100%" cellspacing="2" cellpadding="0" ng-if="quest.questType == 1">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td ng-if="!completeSummon" align="center" colspan="2" valign="top" class="headerred">Linh thú cần có</td>
            <td ng-if="completeSummon" align="center" colspan="2" valign="top" class="headerred">Linh thú nhận được</td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table>
                    <tr>
                        <td align="center" width="220px" ng-repeat="summon in summons">
                            <span >
                                <div>
                                    <img width="80px" ng-if="summon.imageUrl" img-pre-load="{{summon.imageUrl}}">
                                </div>
                                <div class="headerred">
                                    {{summon.name}}
                                </div>
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr ng-show="notTimeOut && !completeSummon">
            <td align="center" colspan="2" class="headerred" id="countDown">
            </td>
        </tr>
        <tr ng-if="!notTimeOut">
            <td align="center" colspan="2" class="headerred">
                Quá thời gian cho phép
            </td>
        </tr>
        <tr ng-if="!completeSummon">
            <td align="center" colspan="1">
                <img ng-click="remove()" src="images/button/huy_button_red.png" width="117px" height="43px"/> 
            </td>
            <td align="center" colspan="1" ng-if="notTimeOut">
                <img ng-click="summonQuestComplete()" src="images/button/button_ok.png" width="117px" height="43px"/> 
            </td>
        </tr>
        <tr ng-if="completeSummon">
            <td align="center" colspan="2">
                <a ng-click="reload()" href>
                    <img src="images/button/button_ok.png" width="117px" height="43px"/> 
                </a>
            </td>
        </tr>
    </table>

    <!-- item quest -->
    <table width="100%" cellspacing="2" cellpadding="0" ng-if="quest.questType == 2 && !complete">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td ng-if="!complete" align="center" colspan="2" valign="top" class="headerred">Vật phẩm cần có</td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table>
                    <tr>
                        <td align="center" width="220px" ng-repeat="item in items">
                            <span>
                                <div>
                                    <img width="80px" ng-if="item.imageUrl" img-pre-load="{{item.imageUrl}}">
                                </div>
                                <div class="headerred">
                                    {{item.name}}
                                </div>
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="1">
                <img ng-click="remove()" src="images/button/huy_button_red.png" width="117px" height="43px"/> 
            </td>
            <td align="center" colspan="1" >
                <img ng-click="itemQuestComplete()" src="images/button/button_ok.png" width="117px" height="43px"/> 
            </td>
        </tr>
    </table>

    <!-- item quest reward -->
    <table width="100%" cellspacing="2" cellpadding="0" ng-if="complete">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td ng-if="complete" align="center" colspan="2" valign="top" class="headerred">Vật phẩm nhận được</td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <table>
                    <tr>
                        <td align="center" width="220px" ng-repeat="item in items">
                            <span>
                                <div>
                                    <img width="80px" ng-if="item.imageUrl" img-pre-load="{{item.imageUrl}}">
                                </div>
                                <div class="headerred">
                                    {{item.name}}
                                </div>
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr ng-if="!complete">
            <td align="center" colspan="1">
                <img ng-click="remove()" src="images/button/huy_button_red.png" width="117px" height="43px"/> 
            </td>
            <td align="center" colspan="1" >
                <img ng-click="itemQuestComplete()" src="images/button/button_ok.png" width="117px" height="43px"/> 
            </td>
        </tr>
        <tr ng-if="complete">
            <td align="center" colspan="2">
                <a ng-click="reload()" href>
                    <img src="images/button/button_ok.png" width="117px" height="43px"/> 
                </a>
            </td>
        </tr>
    </table>
</div>