<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div >
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Hồ sơ thú cưng</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="contentred">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" class="backgroundorange">
                                <tr>
                                    <td width="705" align="left" valign="top">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="120" align="left" class="contentredtable">Loài:</td>
                                                <td class="contentwhite">
                                                    <span ng-if="characterPet.evolutionIndex < 0">{{characterPet.pet.name}}</span>
                                                    <span ng-if="characterPet.evolutionIndex >= 0">{{characterPet.pet.evolutions[characterPet.evolutionIndex].name}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Hạng:</td>
                                                <td class="contentgreen">
                                                    <span >N/A</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Cấp độ:</td>
                                                <td class="contentwhite">
                                                    <span >{{characterPet.level}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Sức mạnh:</td>
                                                <td class="contentwhite">
                                                    <span >{{characterPet.strength}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Phòng thủ:</td>
                                                <td class="contentwhite">
                                                    <span >{{characterPet.defense}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Tốc độ:</td>
                                                <td class="contentwhite">
                                                    <span >{{characterPet.speed}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Trí tuệ:</td>
                                                <td class="contentwhite">
                                                    <span >{{characterPet.intelligent}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Ngày sinh:</td>
                                                <td class="contentwhite">
                                                    <span >N/A</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Máu:</td>
                                                <td class="contentwhite">
                                                    <table width="100%" cellpadding="2" cellspacing="0" class="tablenumber">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color: #00FF00;">
                                                                    <tr>
                                                                        <td >
                                                                            <div  style="width: {{(characterPet.currentHP) / (characterPet.hp) * 100}}%" class="blood">
                                                                                {{(characterPet.currentHP)}} / {{(characterPet.hp)}}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Kinh nghiệm:</td>
                                                <td class="contentwhite">
                                                    <table width="100%" cellpadding="2" cellspacing="0" class="tablenumber">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color: #00FF00;" >
                                                                    <tr>
                                                                        <td >
                                                                            <div  style="width: {{(characterPet.experience) / characterPet.nextExperience * 100}}%" class="experience">
                                                                                {{(characterPet.experience)}} / {{characterPet.nextExperience}}
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="contentredtable">Liên kết:</td>
                                                <td class="contentwhite">
                                                    <span>http://thuao.com/#/pet/detail/{{characterPet.id}}</span>
                                                </td>
                                            </tr>
                                            <tr ng-if="characterPet.consign">
                                                <td align="left" class="contentredtable">Ủy thác</td>
                                                <td class="contentwhite">
                                                    <span>{{characterPet.consignTime / (3600 * 1000)}} giờ </span> <a href ng-click="unConsign(characterPet)">Kết thúc</a>
                                                </td>
                                            </tr>
                                        </table></td>
                                    <td width="317" align="center" valign="top">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td ng-if="characterPet.pet.petClass.id == 1" width="300" align="center">
                                                    <img ng-src="images/thuong_thu.png" width="168" height="44" />
                                                </td>
                                                <td ng-if="characterPet.pet.petClass.id == 2" width="300" align="center">
                                                    <img ng-src="images/huyen_thoai.png" width="168" height="44" />
                                                </td>
                                                <td ng-if="characterPet.pet.petClass.id == 3" width="300" align="center">
                                                    <img ng-src="images/than_thu.png" width="168" height="44" />
                                                </td>
                                                <td ng-if="characterPet.pet.petClass.id == 4" width="300" align="center">
                                                    <img ng-src="images/thuong_thu.png" width="168" height="44" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="300" align="center">
                                                    <div ng-if="characterPet.evolutionIndex < 0">
                                                        <img img-pre-load="{{characterPet.pet.imageAvatarUrl}}" width="300" height="300" />
                                                    </div>
                                                    <div ng-if="characterPet.evolutionIndex >= 0">
                                                        <img img-pre-load="{{characterPet.pet.evolutions[characterPet.evolutionIndex].imageUrl}}" width="300" height="300" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <table width="100%" cellspacing="0" cellpadding="5">
                                                        <tr>
                                                            <td width="50" ng-repeat="i in [0, 1, 2, 3, 4]">
                                                                <img ng-src="{{::$parent.skills[i].imagePath}}" title="{{::$parent.skills[i].name}}&nbsp;{{::$parent.skills[i].description}}" alt="{{::$parent.skills[i].name}}" width="50" height="51" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="60" colspan="2" valign="top">
                                        <a href ng-click="setActive(characterPet)">
                                            <img src="images/button/hoat_dong_chinh_button.png" width="147" height="41" />
                                        </a>
                                        &nbsp;
                                        <a href ng-click="sendFriend(characterPet)">
                                            <img src="images/button/tang_button.png" width="147" height="41" />
                                        </a>
                                        &nbsp;
                                        <a href ng-click="showReleasePetModal(characterPet)">
                                            <img src="images/button/phong_thich_button.png" width="147" height="42" />
                                        </a>
                                        &nbsp;
                                        <a href ng-if="characterPet.consign == false" ng-click="petConsign(characterPet)">
                                            <img src="images/button/uythac.png" width="147" height="42" />
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left" valign="bottom">
                                        <table width="445"  style="margin-left: 2px;" border="0" cellpadding="0" cellspacing="0" class="tableborderwhite">
                                            <tr>
                                                <td>
                                                    <table width="445" border="0" cellpadding="0" cellspacing="0" class="tableborderwhite1">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                                    <tr>
                                                                        <td align="center" width="100" height="101" ng-repeat="i in [0, 1, 2, 3]">
                                                                            <a ng-if="petSummons[i]" href ng-click="showUnusePetSummonModal(petSummons[i])">
                                                                                <section class="ng-tooltip-section">
                                                                                    <img width="100" height="101" img-pre-load="{{petSummons[i].summon.imageUrl}}"
                                                                                         data-tooltip-trigger data-html="true" data-tooltip-text="{{renderTooltipSummon(petSummons[i])}}"/>
                                                                                    <!-- Active pg-ng-tooltip lib-->
                                                                                    <div data-pg-ng-tooltip class="ng-tooltip"></div>
                                                                                </section>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table style="margin-left: 2px;" width="445" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="150" class="tableborderwhitebottomleft">&nbsp;&nbsp;&nbsp;Linh thú trang bị</td>
                                                <td class="tableborderwhitebottomcenter">&nbsp;</td>
                                                <td class="tableborderwhitebottomright">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right" valign="bottom">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="right" valign="bottom">
                                                    <table width="445" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="150" height="15" class="tableborderwhitebottomleft1">&nbsp;&nbsp;&nbsp;</td>
                                                            <td class="tableborderwhitebottomcenter">&nbsp;</td>
                                                            <td class="tableborderwhitebottomright1">Vũ khí trang bị&nbsp;&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="5">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right" valign="top">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="right" valign="top">
                                                    <table width="445" border="0" cellpadding="0" cellspacing="0" class="tableborderwhite1">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                                    <tr>
                                                                        <td align="center" width="100" height="101" ng-repeat="i in [0, 1, 2, 3]">
                                                                            <a ng-if="petEquipments[i]" href ng-click="showUnusePetItemModal(petEquipments[i])">
                                                                                <section class="ng-tooltip-section">
                                                                                    <img width="100" height="101" img-pre-load="{{petEquipments[i].item.imageUrl}}"
                                                                                         data-tooltip-trigger data-html="true" data-tooltip-text="{{renderTooltip(petEquipments[i])}}"/>
                                                                                    <!-- Active pg-ng-tooltip lib-->
                                                                                    <div data-pg-ng-tooltip class="ng-tooltip"></div>
                                                                                </section>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="5">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tablegray">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="contentred">&nbsp;</td>
        </tr>
    </table>
</div>
