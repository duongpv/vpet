<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div style="width: 100%; text-align: center">
    <table style="margin: auto">
        <tr >
            <td class="headerred">Tùy chọn vật phẩm</td>
        </tr>
        <tr>
            <td>
                <img style="width: 80px; height: 80px;" ng-if="characterPet.pet.imageAvatarUrl" img-pre-load="{{characterPet.pet.imageAvatarUrl}}"/>
            </td>
        </tr>
        <tr>
            <td class="headerred">
                {{characterPet.name}}
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="5" cellspacing="0" class="">
                    <tr>
                        <td>
                            <select ng-model="selectedCharacterId" class="textfield">
                                <option ng-repeat="character in characters" value="{{character.id}}">{{character.name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img ng-click="send()" ng-src="images/button/button_ok_size_s1.png"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>