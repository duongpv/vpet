<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" valign="top" class="headerred">Thú cưng của tôi</td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding-top:14px;">
                Hiển thị theo <select ng-init="sortBy = 'auto'" ng-model="sortBy" class="textfield" style="width: auto" ng-change="dataSort(sortBy, orderBy)" >
                    <option value="auto">Tự động</option>
                    <option value="name">Tên</option>
                    <option value="level">Level</option>
                </select>
                Sắp xếp theo <select ng-init="orderBy = 'asc'" ng-model="orderBy" class="textfield" style="width: auto" ng-change="dataSort(sortBy, orderBy)" >
                    <option value="asc" >Tăng dần</option>
                    <option value="desc">Giảm dần</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="contentred">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr ng-repeat="row in rows">
                        <td width="50%" ng-repeat="pet in row.pets">
                            <table width="100%" cellspacing="0" cellpadding="0" ng-if="pet != null">
                                <tr>
                                    <td align="center" valign="middle">
                                        <a ui-sref="petDetail({id: {{pet.id}}})">
                                            <div ng-if="pet.evolutionIndex < 0">
                                                <img img-pre-load="{{pet.pet.imageAvatarUrl}}" width="300" height="300" />
                                            </div>
                                            <div ng-if="pet.evolutionIndex >= 0">
                                                <img img-pre-load="{{pet.pet.evolutions[pet.evolutionIndex].imageUrl}}" width="300" height="300" />
                                            </div>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentpink">
                                        <div>{{pet.name}}</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div class="contentred" style="padding: 0px; margin: 0px">
        <div id="pagination-box"></div>
    </div>
</div>