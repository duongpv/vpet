<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div style="width: 100%; text-align: center">
    <table style="margin: auto">
        <tr >
            <td class="headerred">Tùy chọn vật phẩm</td>
        </tr>
        <tr >
            <td>
                <img style="width: 80px; height: 80px;" ng-if="characterPet.pet.imageEggUrl" img-pre-load="{{characterPet.pet.imageEggUrl}}"/>
            </td>
        </tr>
        <tr >
            <td>
                <table cellpadding="5" cellspacing="0" class="">
                    <tr>
                        <td style="padding-top:14px;">
                            <select ng-model="actionType" class="textfield" >
                                <option value="1">Tặng cho bạn bè</option>
                                <option value="2">Bán ve chai</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img ng-click="action(actionType)" ng-src="images/button/button_ok_size_s1.png"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>