<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div style="height: 810px; overflow: hidden">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="center" class="headerred" style="padding-bottom: 50px">Vật phẩm của tôi</td>
        </tr>
        <tr>
            <td align="center">
                <a>
                    <img ng-click="setItemsData(weaponItems)" ng-src="images/button/vu_khi_button.png"/>
                </a>
                <a>
                    <img ng-click="setItemsData(medicalItems)" ng-src="images/button/duoc_pham_button.png"/>
                </a>
                <a>
                    <img ng-click="setItemsData(foodItems)" ng-src="images/button/thuc_an_button.png"/>
                </a>
                <!--
                <a>
                    <img ng-click="setItemsData(evolutionItems)" ng-src="images/button/tien_hoa_button.png"/>
                </a>
                -->
                <a>
                    <img ng-click="setItemsData(otherItems)" ng-src="images/button/khac_button.png"/>
                </a>
                <a>
                    <img ng-src="images/button/hang_loat_button.png"/>
                </a>
            </td>
        </tr>
        <tr>
            <td class="contentred" style="padding-top: 0px">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <!-- list items in current category -->
                    <tr ng-repeat="itemRow in itemRows" style="height: 100px">
                        <td valign="top" class="backgroundorange"  >
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 250px;" ng-repeat="characterItem in itemRow.items">
                                        <table ng-init="item = characterItem.item"
                                               style="font-style:italic;cellspacing:10px;cellpadding:0;width: 100%;">
                                            <tr style="text-align:center;">
                                                <td >
                                                    <a ng-if="!characterItem.egg" ui-sref="itemAction({id: {{characterItem.id}} })" >
                                                        <img img-pre-load="{{item.imageUrl}}" title="{{item.name}}" width="100" height="101" />
                                                    </a>

                                                    <a ng-if="characterItem.egg" ui-sref="eggAction({id: {{characterItem.id}} })" >
                                                        <img img-pre-load="{{item.imageUrl}}" title="{{item.name}}" width="100" height="101" />
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr style="text-align:center;">
                                                <td><strong>{{item.name}}</strong></td>
                                            </tr>
                                            <tr ng-if="!characterItem.egg" style="text-align:center;">
                                                <td><strong>Loại:&nbsp;</strong>{{item.itemClass.name}}&nbsp;</td>
                                            </tr>
                                            <tr ng-if="!characterItem.egg" style="text-align:center;">
                                                <td><strong>Ma thuật:&nbsp;</strong>{{item.magic}}&nbsp;</td>
                                            </tr>
                                            <tr ng-if="!characterItem.egg" style="text-align:center;" ng-if="item.itemClass.id > 10 && item.itemClass.id < 21">
                                                <td>
                                                    <strong>Độ bền:&nbsp;</strong> 
                                                    <span ng-if="item.endurance >= 0">{{characterItem.endurance}}/{{item.endurance}}&nbsp;</span>
                                                    <span ng-if="item.endurance < 0"> Vô hạn&nbsp;</span>
                                                </td>
                                            </tr>
                                            <tr style="text-align:center;">
                                                <td><strong>Miêu tả:&nbsp;</strong>{{item.description}}&nbsp;</td>
                                            </tr>
                                            <!--
                                            <tr>
                                                <td>
                                                    <input type="checkbox" id="{{category.props.id + ':' + category.items[idx].id}}" 
                                                           ng-model="category.items[idx].isSelected" 
                                                           ng-true-value="true" ng-false-value="false" 
                                                           ng-change="category.selectItem(category.items[idx])"/>
                                                </td>
                                            </tr>
                                            -->
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


    </table>
</div>

<div class="contentred" style="padding: 0px; margin: 0px">
    <div id="pagination-box"></div>
</div>