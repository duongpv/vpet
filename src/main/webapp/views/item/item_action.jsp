<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div style="width: 100%; text-align: center">
    <table style="margin: auto">
        <tr >
            <td class="headerred">Tùy chọn vật phẩm</td>
        </tr>
        <tr >
            <td>
                <img style="width: 80px; height: 80px;" ng-if="item.item.imageUrl" img-pre-load="{{item.item.imageUrl}}"/>
            </td>
        </tr>
        <tr >
            <td>
                <table cellpadding="5" cellspacing="0" class="">
                    <tr>
                        <td style="padding-top:14px;">
                            <select ng-model="actionType" class="textfield" >
                                <option value="1">Trang bị cho thú cưng</option>
                                <option value="2">Chữa trị</option>
                                <option value="3">Tiến hóa thú cưng</option>
                                <option value="4">Tăng chỉ số </option>
                                <option value="5">Tặng cho bạn bè</option>
                                <option value="6">Sử dụng</option>
                                <option value="7">Cho vào cửa hàng</option>
                                <option value="8">Cho vào phòng triển lãm</option>
                                <option value="9">Bán ve chai</option>
                            </select>
                        </td>
                    </tr>
                    <tr ng-show="actionType == 1 || actionType == 2 || actionType == 3 || actionType == 4">
                        <td>
                            <select ng-model="selectedCharacterPetId" class="textfield">
                                <option ng-repeat="characterPet in characterPets" value="{{characterPet.id}}">{{characterPet.name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr ng-show="actionType == 5">
                        <td>
                            <select ng-model="selectedCharacterId" class="textfield">
                                <option ng-repeat="character in characters" value="{{character.id}}">{{character.name}}</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img ng-click="action(actionType)" ng-src="images/button/button_ok_size_s1.png"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>