define(["angular", "angularUIRouter", "routeResolverServices", "angular-file-upload", "angularRecaptcha", "ngProgress", "loading-bar", "ngAnimate", "pg-ng-tooltip", "angularModalService"], function () {
    /**
     * chieffancypants.loadingBar for http request
     */
    var app = angular.module('app', ["ngRoute", "ui.router", "vcRecaptcha", "angularFileUpload", "routeResolverServices", "ngProgress", "chieffancypants.loadingBar", "ngAnimate", "pg-ng-tooltip", "angularModalService"]);

    /*
     * Progress for ui route
     */
    app.run(function ($rootScope, ngProgress) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if (toState.resolve) {
                ngProgress.start();
            }
        });
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (toState.resolve) {
                ngProgress.complete();
            }
        });
    });

    app.config(function ($stateProvider, $urlRouterProvider, $controllerProvider, routeResolverProvider) {
        var route = routeResolverProvider.route; // Create route resolve function

        app.registerCtrl = $controllerProvider.register; // Create attribute to register controller

        $urlRouterProvider.otherwise('admin/'); // Mọi đường dẫn không hợp lệ đều được chuyển đến state home

        $stateProvider
                .state('home', {// Định ngĩa 1 state home
                    url: '/home', // khai báo Url hiển thị
                    templateUrl: 'home.jsp'// đường dẫn view
                })
                .state('adminCenter', {
                    url: '/admin/',
                    templateUrl: 'views/admin/admin_center.jsp'
                })
                .state('adminPetList', {
                    url: '/admin/pet/',
                    templateUrl: 'views/admin/admin_pet_list.jsp',
                    resolve: route("adminPetListCtrl"),
                    controller: "adminPetListCtrl"
                })
                .state('adminPetAdd', {
                    url: '/admin/pet/add',
                    templateUrl: 'views/admin/admin_pet_view.jsp',
                    resolve: route("adminPetAddCtrl"),
                    controller: "adminPetAddCtrl"
                })
                .state('adminPetDetail', {
                    url: '/admin/pet/:id',
                    templateUrl: 'views/admin/admin_pet_view.jsp',
                    resolve: route("adminPetDetailCtrl"),
                    controller: "adminPetDetailCtrl"
                })
                .state('adminAvatarItemList', {
                    url: '/admin/item/avatar',
                    templateUrl: 'views/admin/admin_avatar_item_list.jsp',
                    resolve: route("adminAvatarItemListCtrl"),
                    controller: "adminAvatarItemListCtrl"
                })
                .state('adminAvatarItemAdd', {
                    url: '/admin/item/avatar/add',
                    templateUrl: 'views/admin/admin_avatar_item_view.jsp',
                    resolve: route("adminAvatarItemAddCtrl"),
                    controller: "adminAvatarItemAddCtrl"
                })
                .state('adminAvatarItemDetail', {
                    url: '/admin/item/avatar/:id',
                    templateUrl: 'views/admin/admin_avatar_item_view.jsp',
                    resolve: route("adminAvatarItemDetailCtrl"),
                    controller: "adminAvatarItemDetailCtrl"
                })
                .state('adminPetItemList', {
                    url: '/admin/item/pet',
                    templateUrl: 'views/admin/admin_pet_item_list.jsp',
                    resolve: route("adminPetItemListCtrl"),
                    controller: "adminPetItemListCtrl"
                })
                .state('adminPetItemAdd', {
                    url: '/admin/item/pet/add',
                    templateUrl: 'views/admin/admin_pet_item_view.jsp',
                    resolve: route("adminPetItemAddCtrl"),
                    controller: "adminPetItemAddCtrl"
                })
                .state('adminPetItemDetail', {
                    url: '/admin/item/pet/:id',
                    templateUrl: 'views/admin/admin_pet_item_view.jsp',
                    resolve: route("adminPetItemDetailCtrl"),
                    controller: "adminPetItemDetailCtrl"
                })
                .state('adminOtherItemList', {
                    url: '/admin/item/other',
                    templateUrl: 'views/admin/admin_other_item_list.jsp',
                    resolve: route("adminOtherItemListCtrl"),
                    controller: "adminOtherItemListCtrl"
                })
                .state('adminOtherItemAdd', {
                    url: '/admin/item/other/add',
                    templateUrl: 'views/admin/admin_other_item_view.jsp',
                    resolve: route("adminOtherItemAddCtrl"),
                    controller: "adminOtherItemAddCtrl"
                })
                .state('adminOtherItemDetail', {
                    url: '/admin/item/other/:id',
                    templateUrl: 'views/admin/admin_other_item_view.jsp',
                    resolve: route("adminOtherItemDetailCtrl"),
                    controller: "adminOtherItemDetailCtrl"
                })
                .state('adminPetEvolutionList', {
                    url: '/admin/evolution',
                    templateUrl: 'views/admin/admin_pet_evolution_list.jsp',
                    resolve: route("adminPetEvolutionListCtrl"),
                    controller: "adminPetEvolutionListCtrl"
                })
                .state('adminPetEvolutionDetail', {
                    url: '/admin/evolution/:id',
                    templateUrl: 'views/admin/admin_pet_evolution_view.jsp',
                    resolve: route("adminPetEvolutionDetailCtrl"),
                    controller: "adminPetEvolutionDetailCtrl"
                })
                .state('adminMonsterList', {
                    url: '/admin/monster',
                    templateUrl: 'views/admin/admin_monster_list.jsp',
                    resolve: route("adminMonsterListCtrl"),
                    controller: "adminMonsterListCtrl"
                })
                .state('adminMonsterDetail', {
                    url: '/admin/monster/detail/:id',
                    templateUrl: 'views/admin/admin_monster_view.jsp',
                    resolve: route("adminMonsterDetailCtrl"),
                    controller: "adminMonsterDetailCtrl"
                })
                .state('adminMonsterAdd', {
                    url: '/admin/monster/add',
                    templateUrl: 'views/admin/admin_monster_view.jsp',
                    resolve: route("adminMonsterAddCtrl"),
                    controller: "adminMonsterAddCtrl"
                })
                .state('adminMonsterItemList', {
                    url: '/admin/monster/item',
                    templateUrl: 'views/admin/admin_monster_item_list.jsp',
                    resolve: route("adminMonsterItemListCtrl"),
                    controller: "adminMonsterItemListCtrl"
                })
                .state('adminMonsterItemDetail', {
                    url: '/admin/monster/item/:id',
                    templateUrl: 'views/admin/admin_monster_item_view.jsp',
                    resolve: route("adminMonsterItemDetailCtrl"),
                    controller: "adminMonsterItemDetailCtrl"
                })
                .state('adminMonsterRewardItemList', {
                    url: '/admin/monster/rewarditem',
                    templateUrl: 'views/admin/admin_monster_reward_item_list.jsp',
                    resolve: route("adminMonsterRewardItemListCtrl"),
                    controller: "adminMonsterRewardItemListCtrl"
                })
                .state('adminMonsterRewardItemDetail', {
                    url: '/admin/monster/rewarditem/:id',
                    templateUrl: 'views/admin/admin_monster_reward_item_view.jsp',
                    resolve: route("adminMonsterRewardItemDetailCtrl"),
                    controller: "adminMonsterRewardItemDetailCtrl"
                })
                .state('adminSummonList', {
                    url: '/admin/summon/',
                    templateUrl: 'views/admin/admin_summon_list.jsp',
                    resolve: route("adminSummonListCtrl"),
                    controller: "adminSummonListCtrl"
                })
                .state('adminSummonAdd', {
                    url: '/admin/summon/add',
                    templateUrl: 'views/admin/admin_summon_view.jsp',
                    resolve: route("adminSummonAddCtrl"),
                    controller: "adminSummonAddCtrl"
                })
                .state('adminSummonDetail', {
                    url: '/admin/summon/:id',
                    templateUrl: 'views/admin/admin_summon_view.jsp',
                    resolve: route("adminSummonDetailCtrl"),
                    controller: "adminSummonDetailCtrl"
                })
                .state('adminAreaDetail', {
                    url: '/admin/area/:id',
                    templateUrl: 'views/admin/admin_area_view.jsp',
                    resolve: route("adminAreaDetailCtrl"),
                    controller: "adminAreaDetailCtrl"
                })
                .state('adminAreaList', {
                    url: '/admin/area',
                    templateUrl: 'views/admin/admin_area_list.jsp',
                    resolve: route("adminAreaListCtrl"),
                    controller: "adminAreaListCtrl"
                })
                .state('adminAreaRewardItemList', {
                    url: '/admin/arearewarditem',
                    templateUrl: 'views/admin/admin_area_reward_item_list.jsp',
                    resolve: route("adminAreaRewardItemListCtrl"),
                    controller: "adminAreaRewardItemListCtrl"
                })
                .state('adminAreaRewardItemDetail', {
                    url: '/admin/arearewarditem/:id',
                    templateUrl: 'views/admin/admin_area_reward_item_view.jsp',
                    resolve: route("adminAreaRewardItemDetailCtrl"),
                    controller: "adminAreaRewardItemDetailCtrl"
                })
                .state('adminCharacterList', {
                    url: '/admin/character/',
                    templateUrl: 'views/admin/admin_character_list.jsp',
                    resolve: route("adminCharacterListCtrl"),
                    controller: "adminCharacterListCtrl"
                })
                .state('adminCharacterDetail', {
                    url: '/admin/character/:id',
                    templateUrl: 'views/admin/admin_character_detail.jsp',
                    resolve: route("adminCharacterDetailCtrl"),
                    controller: "adminCharacterDetailCtrl"
                })
                .state('adminSkillList', {
                    url: '/admin/skill/',
                    templateUrl: 'views/admin/admin_skill_list.jsp',
                    resolve: route("adminSkillListCtrl"),
                    controller: "adminSkillListCtrl"
                })
                .state('adminSkillAdd', {
                    url: '/admin/skill/add',
                    templateUrl: 'views/admin/admin_skill_view.jsp',
                    resolve: route("adminSkillAddCtrl"),
                    controller: "adminSkillAddCtrl"
                })
                .state('adminSkillDetail', {
                    url: '/admin/skill/:id',
                    templateUrl: 'views/admin/admin_skill_view.jsp',
                    resolve: route("adminSkillDetailCtrl"),
                    controller: "adminSkillDetailCtrl"
                })
                .state('adminNewsList', {
                    url: '/admin/news/',
                    templateUrl: 'views/admin/admin_news_list.jsp',
                    resolve: route("adminNewsListCtrl"),
                    controller: "adminNewsListCtrl"
                })
                .state('adminNewsAdd', {
                    url: '/admin/news/add',
                    templateUrl: 'views/admin/admin_news_view.jsp',
                    resolve: route("adminNewsAddCtrl"),
                    controller: "adminNewsAddCtrl"
                })
                .state('adminNewsDetail', {
                    url: '/admin/news/:id',
                    templateUrl: 'views/admin/admin_news_view.jsp',
                    resolve: route("adminNewsDetailCtrl"),
                    controller: "adminNewsDetailCtrl"
                })
                .state('adminChangePass', {
                    url: '/admin/changepass',
                    templateUrl: 'views/admin/admin_change_pass.jsp',
                    resolve: route("adminChangePassCtrl"),
                    controller: "adminChangePassCtrl"
                })
                .state('adminConfigUpdateGold', {
                    url: '/admin/config/updategold',
                    templateUrl: 'views/admin/admin_config_update_gold.jsp',
                    resolve: route("adminConfigUpdateGoldCtrl"),
                    controller: "adminConfigUpdateGoldCtrl"
                })
                .state('adminCharacterLevelList', {
                    url: '/admin/characterlevel/',
                    templateUrl: 'views/admin/admin_character_level_list.jsp',
                    resolve: route("adminCharacterLevelListCtrl"),
                    controller: "adminCharacterLevelListCtrl"
                })
                .state('adminCharacterLevelDetail', {
                    url: '/admin/characterlevel/:id',
                    templateUrl: 'views/admin/admin_character_level_view.jsp',
                    resolve: route("adminCharacterLevelDetailCtrl"),
                    controller: "adminCharacterLevelDetailCtrl"
                });
    });

    return app; // Return for other define
});