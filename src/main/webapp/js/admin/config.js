/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require.config({
    baseUrl: "app/controller",
    deps: ["main"], // Load main module
    urlArgs: "time=" + new Date().getTime(),
    paths: {
        angular: "../../js/libs/angular/angular.min",
        angularRoute: "../../js/libs/angular-route/angular-route.min",
        angularUIRouter: "../../js/libs/angular-ui-router/angular-ui-router.min",
        routeResolverServices: "../services/routeResolverServices",
        imgPreLoad: "../directives/imgPreLoad",
        ngRightClick: "../directives/ngRightClick",
        "angular-file-upload": "../../js/libs/angular-file-upload/angular-file-upload.min",
        "pg-ng-tooltip": "../../js/libs/pg-ng-tooltip/pg-ng-tooltip.min",
        angularRecaptcha: "../../js/libs/angular-recaptcha/angular-recaptcha.min",
        ngProgress: "../../js/libs/ngProgress/ngProgress.min",
        angularModalService: "../../js/libs/angular-modal-service/angular-modal-service.min",
        "loading-bar": "../../js/libs/angular-loading-bar/loading-bar.min",
        ngAnimate: "../../js/libs/angular-animate/angular-animate.min",
        jquery: "../../js/libs/jquery/jquery.min",
        twbsPagination: "../../js/libs/twbsPagination/jquery.twbsPagination.min",
        app: "../../js/admin/app",
        main: "../../js/main"
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angularUIRouter': {
            deps: ["angularRoute"]
        },
        'angularRoute': {
            deps: ["angular"]
        },
        'angularRecaptcha': {
            deps: ["angular"]
        },
        'routeResolverServices': {
            deps: ["angular"]
        },
        'ngProgress': {
            deps: ["angular"]
        },
        'loading-bar': {
            deps: ["angular"]
        },
        'ngAnimate': {
            deps: ["angular"]
        },
        'imgPreLoad': {
            deps: ["angular"]
        },
        'pg-ng-tooltip': {
            deps: ["angular"]
        },
        angularModalService: {
            deps: ["angular"]
        },
        'twbsPagination': {
            deps: ["jquery"]
        }
    }
});