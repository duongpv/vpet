define(["angular", "angularUIRouter", "routeResolverServices", "angular-file-upload", "angularRecaptcha", "ngProgress", "loading-bar", "ngAnimate", "pg-ng-tooltip", "angularModalService", "sockjs"], function () {
    /**
     * chieffancypants.loadingBar for http request
     */
    var app = angular.module('app', ["ngRoute", "ui.router", "vcRecaptcha", "angularFileUpload", "routeResolverServices", "ngProgress", "chieffancypants.loadingBar", "ngAnimate", "pg-ng-tooltip", "angularModalService"]);

    /*
     * Progress for ui route
     */
    app.run(function ($rootScope, ngProgress) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if (toState.resolve) {
                ngProgress.start();
            }
        });
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (toState.resolve) {
                ngProgress.complete();
            }
        });
    });

    app.config(function ($stateProvider, $urlRouterProvider, $controllerProvider, routeResolverProvider) {
        var route = routeResolverProvider.route; // Create route resolve function

        app.registerCtrl = $controllerProvider.register; // Create attribute to register controller

        $urlRouterProvider.otherwise('home'); // Mọi đường dẫn không hợp lệ đều được chuyển đến state home

        $stateProvider
            .state('home', {// Định ngĩa 1 state home
                url: '/home', // khai báo Url hiển thị
                templateUrl: 'home.jsp', // đường dẫn view
                resolve: route("homeCtrl"),
                controller: "homeCtrl"
            })
            .state('login', {
                url: '/member/login',
                templateUrl: 'views/member/member_login.jsp'
            })
            .state('loginOption', {
                url: '/member/',
                templateUrl: 'views/member/member_login_option.jsp'
            })
            .state('memberRegister', {
                url: '/member/register',
                templateUrl: 'views/member/member_register.jsp',
                resolve: route("memberRegisterCtrl"),
                controller: "memberRegisterCtrl"
            })
            .state('memberDetail', {
                url: '/member/detail',
                templateUrl: 'views/member/member_detail.jsp',
                resolve: route("memberDetailCtrl"),
                controller: "memberDetailCtrl"
            })
            .state('avatarCenter', {
                url: '/avatar/',
                templateUrl: 'views/avatar/avatar_center.jsp',
                resolve: route("avatarCenterCtrl"),
                controller: "avatarCenterCtrl"
            })
            .state('avatarView', {
                url: '/avatar/view',
                templateUrl: 'views/avatar/avatar_view.jsp',
                resolve: route("avatarViewCtrl"),
                controller: "avatarViewCtrl"
            })
            .state('characterRegister', {
                url: '/character/register',
                templateUrl: 'views/character/character_register.jsp',
                resolve: route("characterRegisterCtrl"),
                controller: "characterRegisterCtrl"
            })
            .state('panel', {
                url: '/panel/',
                templateUrl: 'views/avatar/avatar_center.jsp',
                resolve: route("avatarCenterCtrl"),
                controller: "avatarCenterCtrl"
            })
            .state('petCenter', {
                url: '/pet/',
                templateUrl: 'views/pet/pet_center.jsp',
                resolve: route("petCenterCtrl"),
                controller: "petCenterCtrl"
            })
            .state('petDetail', {
                url: '/pet/detail/:id',
                templateUrl: 'views/pet/pet_detail.jsp',
                resolve: route("petDetailCtrl"),
                controller: "petDetailCtrl"
            })
            .state('petSend', {
                url: '/pet/send/:id',
                templateUrl: 'views/pet/pet_send.jsp',
                resolve: route("petSendCtrl"),
                controller: "petSendCtrl"
            })
            .state('petConsign', {
                url: '/pet/consign/:id',
                templateUrl: 'views/pet/pet_consign.jsp',
                resolve: route("petConsignCtrl"),
                controller: "petConsignCtrl"
            })
            .state('summonCenter', {
                url: '/summon/',
                templateUrl: 'views/summon/summon_center.jsp',
                resolve: route("summonCenterCtrl"),
                controller: "summonCenterCtrl"
            })
            .state('summonAction', {
                url: '/summon/action/:id',
                templateUrl: 'views/summon/summon_action.jsp',
                resolve: route("summonActionCtrl"),
                controller: "summonActionCtrl"
            })
            .state('itemCenter', {
                url: '/item/',
                templateUrl: 'views/item/item_center.jsp',
                resolve: route("itemCenterCtrl"),
                controller: "itemCenterCtrl"
            })
            .state('itemAction', {
                url: '/item/action/:id?callbackView',
                templateUrl: 'views/item/item_action.jsp',
                resolve: route("itemActionCtrl"),
                controller: "itemActionCtrl"
            })
            .state('eggAction', {
                url: '/egg/action/:id',
                templateUrl: 'views/item/egg_action.jsp',
                resolve: route("eggActionCtrl"),
                controller: "eggActionCtrl"
            })
            .state('areaView', {
                url: '/area/',
                templateUrl: 'views/area/area_view.jsp',
                resolve: route("areaViewCtrl"),
                controller: "areaViewCtrl"
            })
            .state('areaBattlePetChose', {
                url: '/area/battle/:id/petchose',
                templateUrl: 'views/area/area_pet_chose.jsp',
                resolve: route("areaBattlePetChoseCtrl"),
                controller: "areaBattlePetChoseCtrl"
            })
            .state('areaBattle', {
                url: '/area/battle/',
                templateUrl: 'views/area/area_battle.jsp',
                resolve: route("areaBattleCtrl"),
                controller: "areaBattleCtrl"
            })
            .state('world', {
                url: '/world/',
                templateUrl: 'views/world/world.jsp',
                resolve: route("worldCtrl"),
                controller: "worldCtrl"
            })
            .state('worldHuntingGate', {
                url: '/world/hunting/gate/{id}',
                templateUrl: 'views/world/hunting_gate.jsp',
                resolve: route("worldHuntingGateCtrl"),
                controller: "worldHuntingGateCtrl"
            })
            .state('worldHuntingArea', {
                url: '/world/hunting/area/',
                templateUrl: 'views/world/hunting_area.jsp',
                resolve: route("worldHuntingAreaCtrl"),
                controller: "worldHuntingAreaCtrl"
            })
            .state('worldHuntingMonsterConfirm', {
                url: '/world/hunting/battle/confirm',
                templateUrl: 'views/world/hunting_monster_confirm.jsp',
                resolve: route("worldHuntingMonsterConfirmCtrl"),
                controller: "worldHuntingMonsterConfirmCtrl"
            })
            .state('worldHuntingBattlePetChose', {
                url: '/world/hunting/battle/pet/chose',
                templateUrl: 'views/world/hunting_pet_chose.jsp',
                resolve: route("worldHuntingBattlePetChoseCtrl"),
                controller: "worldHuntingBattlePetChoseCtrl"
            })
            .state('worldHuntingItemConfirm', {
                url: '/world/hunting/item/confirm',
                templateUrl: 'views/world/hunting_item_confirm.jsp',
                resolve: route("worldHuntingItemConfirmCtrl"),
                controller: "worldHuntingItemConfirmCtrl"
            })
            .state('worldHuntingPetConfirm', {
                url: '/world/hunting/pet/confirm',
                templateUrl: 'views/world/hunting_pet_confirm.jsp',
                resolve: route("worldHuntingPetConfirmCtrl"),
                controller: "worldHuntingPetConfirmCtrl"
            })
            .state('worldHuntingPetBattleConfirm', {
                url: '/world/hunting/pet/battle',
                templateUrl: 'views/world/hunting_pet_battle_confirm.jsp',
                resolve: route("worldHuntingPetBattleConfirmCtrl"),
                controller: "worldHuntingPetBattleConfirmCtrl"
            })
            .state('worldHuntingPetCreate', {
                url: '/world/hunting/pet/create',
                templateUrl: 'views/world/hunting_pet_create.jsp',
                resolve: route("worldHuntingPetCreateCtrl"),
                controller: "worldHuntingPetCreateCtrl"
            })
            .state('worldHuntingSummonConfirm', {
                url: '/world/hunting/summon/confirm',
                templateUrl: 'views/world/hunting_summon_confirm.jsp',
                resolve: route("worldHuntingSummonConfirmCtrl"),
                controller: "worldHuntingSummonConfirmCtrl"
            })
            .state('worldHuntingSummonCreate', {
                url: '/world/hunting/summon/create',
                templateUrl: 'views/world/hunting_summon_create.jsp',
                resolve: route("worldHuntingSummonCreateCtrl"),
                controller: "worldHuntingSummonCreateCtrl"
            })
            .state('worldHuntingNetConfirm', {
                url: '/world/hunting/net/confirm',
                templateUrl: 'views/world/hunting_net_confirm.jsp',
                resolve: route("worldHuntingNetConfirmCtrl"),
                controller: "worldHuntingNetConfirmCtrl"
            })
            .state('shopCenter', {
                url: '/shop/',
                templateUrl: 'views/shop/shop_center.jsp'
            })
            .state('shopDetail', {
                url: '/shop/detail/:type',
                templateUrl: 'views/shop/shop_detail.jsp',
                resolve: route("shopDetailCtrl"),
                controller: "shopDetailCtrl"
            })
            .state('townCenter', {
                url: '/town/',
                templateUrl: 'views/town/town_center.jsp'
            })
            .state('friendCenter', {
                url: '/friend/',
                templateUrl: 'views/friend/friend_center.jsp'
            })
            .state('friendList', {
                url: '/friend/list',
                templateUrl: 'views/friend/friend_list.jsp',
                resolve: route("friendListCtrl"),
                controller: "friendListCtrl"
            })
            .state('friendRequestList', {
                url: '/friend/requestlist',
                templateUrl: 'views/friend/friend_request_list.jsp',
                resolve: route("friendRequestListCtrl"),
                controller: "friendRequestListCtrl"
            })
            .state('friendFind', {
                url: '/friend/find',
                templateUrl: 'views/friend/friend_find.jsp',
                resolve: route("friendFindCtrl"),
                controller: "friendFindCtrl"
            })
            .state('friendFindResult', {
                url: '/friend/find/:searchText',
                templateUrl: 'views/friend/friend_find_result.jsp',
                resolve: route("friendFindResultCtrl"),
                controller: "friendFindResultCtrl"
            })
            .state('healiaRiver', {
                url: '/river/healia',
                templateUrl: 'views/river/healia_river.jsp',
                resolve: route("healiaRiverCtrl"),
                controller: "healiaRiverCtrl"
            })
            .state('bank', {
                url: '/bank',
                templateUrl: 'views/bank/bank.jsp',
                resolve: route("bankCtrl"),
                controller: "bankCtrl"
            })
            .state('bankConfig', {
                url: '/bank/config',
                templateUrl: 'views/bank/bank_config.jsp',
                resolve: route("bankConfigCtrl"),
                controller: "bankConfigCtrl"
            })
            .state('bankCenter', {
                url: '/bank/center',
                templateUrl: 'views/bank/bank_center.jsp'
            })
            .state('bankTrade', {
                url: '/bank/trade',
                templateUrl: 'views/bank/bank_trade.jsp',
                resolve: route("bankTradeCtrl"),
                controller: "bankTradeCtrl"
            })
            .state('hatch', {
                url: '/hatch',
                templateUrl: 'views/hatch/hatch_view.jsp'
            })
            .state('quest', {
                url: '/quest',
                templateUrl: 'views/quest/quest_view.jsp'
            })
            .state('card', {
                url: '/card',
                templateUrl: 'views/card/card.jsp',
                resolve: route("cardCtrl"),
                controller: "cardCtrl"
            })
            .state('smithCenter', {
                url: '/smith',
                templateUrl: 'views/smith/smith_center.jsp'
            })
            .state('smithUpdateCenter', {
                url: '/smith/update',
                templateUrl: 'views/smith/smith_update.jsp',
                resolve: route("smithUpdateCenterCtrl"),
                controller: "smithUpdateCenterCtrl"
            })
            .state('smithCreatorCenter', {
                url: '/smith/creator',
                templateUrl: 'views/smith/smith_creator.jsp',
                resolve: route("smithCreatorCenterCtrl"),
                controller: "smithCreatorCenterCtrl"
            })
            .state('questCenter', {
                url: '/quest',
                templateUrl: 'views/quest/quest_center.jsp',
                resolve: route("questCenterCtrl"),
                controller: "questCenterCtrl"
            })
            .state('questItemCenter', {
                url: '/quest/item',
                templateUrl: 'views/quest/quest_item_center.jsp',
                resolve: route("questItemCenterCtrl"),
                controller: "questItemCenterCtrl"
            })
            .state('questSummonCenter', {
                url: '/quest/summon',
                templateUrl: 'views/quest/quest_summon_center.jsp',
                resolve: route("questSummonCenterCtrl"),
                controller: "questSummonCenterCtrl"
            })
            .state('questHeroCenter', {
                url: '/quest/hero',
                templateUrl: 'views/quest/quest_hero_center.jsp',
                resolve: route("questHeroCenterCtrl"),
                controller: "questHeroCenterCtrl"
            })
            .state('topcharacter', {
                url: '/topcharacter/:topType',
                templateUrl: 'views/character/top_character.jsp',
                resolve: route("topCharacterCtrl"),
                controller: "topCharacterCtrl"
            });
    });

    return app; // Return for other define
});