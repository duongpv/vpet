// Don't change this file
var HOST = "";
var IMAGES_HOST = "http://thuao.com:8000/";

require(["jquery","twbsPagination"], function () {
    $("#contentdiv").bind("DOMSubtreeModified", function () {
        setTimeout(function () {
            var url = document.URL;
            if (url.indexOf("#/home") > -1) {
                $("#bannertop").removeClass("backgroundbannerbottom2");
                $("#bannertop").addClass("backgroundbannerbottom");
            } else {
                $("#bannertop").removeClass("backgroundbannerbottom");
                $("#bannertop").addClass("backgroundbannerbottom2");
            }
        }, 10);
    });
});

require(["app", "imgPreLoad", "ngRightClick"], function () {
    angular.bootstrap(document, ['app']);
});
