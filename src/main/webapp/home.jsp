<%@page import="org.springframework.security.core.GrantedAuthority"%>
<%@page import="java.util.Collection"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
    Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();

    request.setAttribute("hasLogin", false);
    boolean hasLogin = false;
    for (GrantedAuthority authority : authorities) {
        hasLogin = authority.getAuthority().equals("ROLE_USER");
        if (hasLogin) {
            request.setAttribute("hasLogin", true);
            break;
        }
    }
%>

<div id="contentdiv" class="contentdiv">
    <c:if test="${hasLogin == false}">
        <div class="rightmenu">
            <div class="registerrope"></div>
            <div>
                <a ui-sref="loginOption">
                    <img alt="Đăng nhập &amp; Đăng ký" src="images/menu/register_login_button.png">
                </a>
            </div>
            <div class="registerbottom"></div>
        </div>
    </c:if>

    <c:if test="${hasLogin == true}">
        <br>
        <br>
        <br>
        <br>
        <br>
    </c:if>

    <div style="margin-left: 40px; margin-right: 40px;">
        <div class="news-box-parent">
            <div>
                <img class="new-box-title" src="images/thong_bao.png"/>
            </div>

            <div class="news-box backgroundorange">
                <div ng-repeat="news in newss">
                    <div class="news-text">
                        <a target="blank" href="{{news.link}}">
                            <strong class="news-title"> {{news.title}}</strong> {{news.description}}
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <div>
            <img src="images/noi_bat.png"/>
        </div>

        <br>
        <div style="text-align: center;">
            <a>
                <img style="position: relative; z-index: 10" src="images/slide/thu_cung_slide.png"/>
            </a>
            <a class="slide-item slide-item-animation">
                <img style="z-index: 9" src="images/slide/nhan_vat_slide.png"/>
            </a>
            <a class="slide-item slide-item-animation">
                <img style="z-index: 8" src="images/slide/bang_hoi_slide.png"/>
            </a>
            <a class="slide-item slide-item-animation">
                <img style="z-index: 7" src="images/slide/thap_tu_vong_slide.png"/>
            </a>
            <a class="slide-item slide-item-animation">
                <img style="z-index: 6" src="images/slide/di_san_slide.png"/>
            </a>
            <a class="slide-item slide-item-animation">
                <img style="z-index: 5" src="images/slide/nhiem_vu_slide.png"/>
            </a>
        </div>

        <br>
        <div style="text-align: center;">
            <a target="blank" href="https://www.facebook.com/VuongQuocThuAo">
                <img src="images/button/fanpage_button.png"/>
            </a>
            <a target="blank" href="http://forum.thuao.com">
                <img src="images/button/forum_button.png"/>
            </a>
            <a>
                <img src="images/button/lien_lac_button.png"/>
            </a>
        </div>
    </div>

    <br>
    <br>
</div>
