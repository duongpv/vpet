<%@page import="org.springframework.security.core.GrantedAuthority"%>
<%@page import="java.util.Collection"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- for Google -->
        <meta name="description" content="Vương quốc thú ảo" />
        <meta name="keywords" content="" />
        <meta name="author" content="lthien" />
        <meta name="copyright" content="Vương quốc thú ảo" />
        <meta name="application-name" content="Vương quốc thú ảo" />

        <!-- for Facebook -->          
        <meta property="og:title" content="Vương quốc thú ảo" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="" />
        <meta property="og:url" content="http://thuao.com" />
        <meta property="og:description" content="Vương quốc thú ảo" />

        <title>Vương quốc thú ảo</title>

        <link rel="stylesheet" href="js/libs/twitter-bootstrap/css/bootstrap.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="js/libs/ngProgress/ngProgress.css"/>
        <link rel="stylesheet" href="js/libs/angular-loading-bar/loading-bar.css"/>

        <link rel="stylesheet" href="js/libs/toastr/toastr.min.css"/>

        <script type="text/javascript" src="//www.google.com/recaptcha/api.js?render=explicit&onload=vcRecaptchaApiLoaded" async defer></script>

        <script type="text/javascript" src="js/libs/jquery/jquery.min.js"></script>

        <script type="text/javascript" src="js/libs/toastr/toastr.min.js"></script>

        <script type="text/javascript" src="js/libs/requirejs/require.js"></script>

        <%
            request.setAttribute("hasRole", false);

            Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
            boolean hasRole = false;
            for (GrantedAuthority authority : authorities) {
                hasRole = authority.getAuthority().equals("ROLE_ADMIN");
                if (!hasRole) {
                    hasRole = authority.getAuthority().equals("ROLE_EDITOR");
                }
                if (hasRole) {
                    request.setAttribute("hasRole", true);
                    break;
                }
            }

            request.setAttribute("hasLogin", false);
            boolean hasLogin = false;
            for (GrantedAuthority authority : authorities) {
                hasLogin = authority.getAuthority().equals("ROLE_USER");
                if (hasLogin) {
                    request.setAttribute("hasLogin", true);
                    break;
                }
            }

        %>
        <c:if test="${hasRole == true}">
            <script type="text/javascript" src="js/admin/config.js"></script>
        </c:if>

        <c:if test="${hasRole == false}">
            <script type="text/javascript" src="js/config.js"></script>
        </c:if>

        <link rel="icon" type="image/png" href="images/favicon.ico">

    </head>
    <body>
        <div class="banner">
            <div class=" banner backgroundbanner" align="center" style="position: relative; z-index: 3;"></div>
            <div id="bannertop" class="banner backgroundbannerbottom2" align="center" style="position: relative; z-index: 1;">&nbsp;</div>
        </div>

        <div class="menu backgroundmenu">
            <div class="menurope"></div>
            <div>
                <a href="#/">
                    <img src="images/menu/menu_home_button.png"/>
                </a>
            </div>
            <div class="menurope"></div>
            <div>
                <a ui-sref="townCenter">
                    <img src="images/menu/menu_kinh_thanh_button.png"/>
                </a>
            </div>
            <div class="menurope"></div>
            <div ui-sref="world">
                <a href="#/">
                    <img src="images/menu/menu_the_gioi_button.png"/>
                </a>
            </div>
            <div class="menurope"></div>
            <div>
                <a ui-sref="panel">
                    <img src="images/menu/menu_bang_quan_ly_button.png"/>
                </a>
            </div>
            <div class="menurope"></div>
            <div>
                <a ui-sref="topcharacter">
                    <img src="images/menu/menu_bxh.png"/>
                </a>
            </div>
            <div class="menurope"></div>
            <div class="menubottom"></div>
        </div>
        <div id="contentdiv" class="contentdiv">
            <a class="egg-fly" title="Nạp card" ui-sref="card">
                <img src="images/egg_fly.gif"/>  
            </a>
            <c:if test="${hasLogin == true}">
                <div id="dropDownDetail" style="margin-left: 462px; margin-top: -180px; position: absolute">
                    <div style="position: relative">
                        <img src="images/drop_down_detail.png" onclick="toggleDropDown()"/>
                        <div class="dropdown-menu-text-wraper">
                            <div>ID: <span id="accountId"></span></div>
                            <div>Vàng: <span id="gold"></span></div>
                            <div>Kim Cương: <span id="crystal"></span></div>
                            <div>Tài phú: <span id="bcoin"></span></div>
                            <div style="text-align: center; font-size: 18px">
                                <a href="j_spring_security_logout" style="color: #000">Đăng xuất</a>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
            <div ui-view></div>
        </div>
        <div style="clear: both"></div>

        <div class="copyrightbottom">
            lthien
        </div>

        <script>
                    var isShow = false;
                    toggleDropDown = function () {
                    if (isShow) {
                    $("#dropDownDetail").animate({
                    'margin-top': '-180px'
                    }, 100, function () {
                    $("#dropDownDetail").css({
                    'position': 'absolute'
                    });
                    });
                            $("#bannertop").removeClass("backgroundbannerbottom");
                            $("#bannertop").addClass("backgroundbannerbottom2");
                    } else {
                    $("#dropDownDetail").css({
                    'position': 'inherit'
                    });
                            $("#dropDownDetail").animate({
                    'margin-top': '-3px'
                    }, 100, function () {
                    // Animation complete.
                    });
                            $("#bannertop").removeClass("backgroundbannerbottom2");
                            $("#bannertop").addClass("backgroundbannerbottom");
                            updateShortDetai();
                    }
                    isShow = !isShow;
                    };
                    function updateShortDetai() {
                    var request = $.ajax(HOST + "/api/character/shortdetail/");
                            request.done(function (msg) {
                            var shortDetail = msg.data;
                                    $("#accountId").text(shortDetail.name);
                                    $("#gold").text(shortDetail.gold);
                                    $("#crystal").text(shortDetail.crystal);
                                    $("#bcoin").text(shortDetail.bcoin);
                            });
                    }
        </script>

        <script>
            (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                    ga('create', 'UA-46947873-5', 'auto');
                    ga('send', 'pageview');</script>

        <script>
                    toastr.options = {
                    "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                    }
        </script>
        <script>
            $(document).ready(function () {
                animateDiv();
            });
                    function makeNewPosition($container) {

                    // Get viewport dimensions (remove the dimension of the div)
                    $container = ($container || $(window))
                            var h = window.screen.availHeight - 200;
                            var w = window.screen.availWidth - 100;
                            var nh = Math.floor(Math.random() * h);
                            var nw = Math.floor(Math.random() * w);
                            return [nh, nw];
                    }

            function animateDiv() {
            var $target = $('.egg-fly');
                    var newq = makeNewPosition($("body"));
                    var oldq = $target.offset();
                    var speed = calcSpeed([oldq.top, oldq.left], newq);
                    $('.egg-fly').animate({
            top: newq[0],
                    left: newq[1]
            }, speed, function () {
            animateDiv();
            });
            }

            function calcSpeed(prev, next) {

            var x = Math.abs(prev[1] - next[1]);
                    var y = Math.abs(prev[0] - next[0]);
                    var greatest = x > y ? x : y;
                    var speedModifier = 0.1;
                    var speed = Math.ceil(greatest / speedModifier);
                    return speed;
            }
        </script>
    </body>
</html>
