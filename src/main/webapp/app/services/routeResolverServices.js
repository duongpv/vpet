define(["angular"], function () {

    var routeResolver = function () {
        this.$get = function () {
            return this;
        };

        this.route = function (url) {
            var resolve = {
                route: function ($q) {
                    var deferred = $q.defer();
                    require([url], function () {
                        deferred.resolve('');
                    });

                    return deferred.promise;
                }
            };

            return resolve;
        };
    };

    var servicesApp = angular.module('routeResolverServices', []);   // Create route resolver service
    servicesApp.provider('routeResolver', routeResolver);  // Create routeResolverProvider
});