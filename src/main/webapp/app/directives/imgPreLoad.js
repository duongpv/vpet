define(["app"], function (app) {

    app.directive("staticImgPreLoad", function () {
        return {
            link: function (scope, element, attrs) {
                var img, loadImage;
                img = null;

                loadImage = function () {

                    element[0].src = "images/loading.gif";

                    img = new Image();

                    img.src = attrs.staticImgPreLoad;

                    img.onload = function () {
                        element[0].src = img.src;
                    };
                };

                scope.$watch((function () {
                    return attrs.staticImgPreLoad;
                }), function (newVal, oldVal) {
                    loadImage();
                });
            }
        };
    });

    app.directive("imgPreLoad", function () {
        return {
            link: function (scope, element, attrs) {
                var img, loadImage;
                img = null;

                loadImage = function () {

                    element[0].src = "images/loading.gif";

                    img = new Image();

                    img.src = IMAGES_HOST + attrs.imgPreLoad;

                    img.onload = function () {
                        element[0].src = img.src;
                    };
                };

                scope.$watch((function () {
                    return attrs.imgPreLoad;
                }), function (newVal, oldVal) {
                    loadImage();
                });
            }
        };
    });

});