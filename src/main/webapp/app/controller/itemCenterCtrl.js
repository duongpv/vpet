define(["app"], function (app) {
    app.registerCtrl('itemCenterCtrl', function ($scope, $http, $state) {
        $scope.items = [];

        $scope.otherItems = [];

        $http.get(HOST + '/api/character/weaponitem').
                success(function (data, status, headers, config) {
                    $scope.weaponItems = data.data;

                    $scope.setItemsData($scope.weaponItems);
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });


        $http.get(HOST + '/api/character/eggs').
                success(function (data, status, headers, config) {
                    var eggs = characterPetToEggs(data.data);
                    Array.prototype.push.apply($scope.otherItems, eggs);
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/character/souveniritem').
                success(function (data, status, headers, config) {
                    $scope.souvenirItems = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/character/fooditem').
                success(function (data, status, headers, config) {
                    $scope.foodItems = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/character/medicalitem').
                success(function (data, status, headers, config) {
                    $scope.medicalItems = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/character/otheritem').
                success(function (data, status, headers, config) {
                    Array.prototype.push.apply($scope.otherItems, data.data);
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });


        $scope.setItemsData = function (items) {
            $scope.items = items;
            if (items.length !== 0) {
                $scope.itemRows = getPageData(0, items);
                var totalPages = Math.ceil($scope.items.length / ($scope.itemRows.length * 3));
                initPage(totalPages);
            } else {
                $scope.itemRows = [];
            }
        }

        function initPage(totalPages) {
            $("#pagination-box").html("");
            $("#pagination-box").html('<ul id="pagination" class="pagination-sm"></ul>');
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                first: "Đầu",
                last: "Cuối",
                prev: "",
                next: "",
                visiblePages: 3,
                onPageClick: function (event, page) {
                    page = page - 1;
                    $scope.itemRows = getPageData(page, $scope.items);
                    $scope.$apply();
                }
            });
        }

        function getPageData(page, data) {
            var pageData = [];

            var maxLen = data.length;
            for (var i = page * 9; i < (page + 1) * 9 && i < maxLen; ) {
                var row = {
                    items: []
                };

                row.items.push(data[i]);

                if (i + 1 < maxLen)
                    row.items.push(data[i + 1]);

                if (i + 2 < maxLen)
                    row.items.push(data[i + 2]);

                pageData.push(row);

                i = i + 3;
            }

            return pageData;
        }

        function characterPetToEggs(characterPets) {
            var eggs = [];

            for (var i = 0; i < characterPets.length; i++) {
                var characterPet = characterPets[i];
                characterPet.description = characterPet.pet.description;
                characterPet.imageUrl = characterPet.pet.imageEggUrl;
                characterPet.name = characterPet.pet.name;

                var egg = {
                    id: characterPet.id,
                    item: characterPet,
                    egg: true
                };

                eggs.push(egg);
            }

            return eggs;
        }
    });
});