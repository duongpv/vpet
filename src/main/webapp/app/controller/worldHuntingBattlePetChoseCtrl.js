define(["app"], function (app) {
    app.registerCtrl('worldHuntingBattlePetChoseCtrl', function ($scope, $http, $state, $stateParams) {
        var id = $stateParams.id;
        $scope.battleInfo = {
            petId: -1,
            monsterId: id
        };

        $http.get(HOST + '/api/character/pets').
                success(function (data, status, headers, config) {
                    $scope.pets = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.createBattle = function () {
            if ($scope.battleInfo.petId === -1) {
                alert("Bạn chưa chọn thú để chiến đấu");
                return;
            }

            $http.post(HOST + '/api/huntingbattle/battle/create', $scope.battleInfo.petId).
                    success(function (data, status, headers, config) {
                        $state.go("areaBattle");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };


    });
});