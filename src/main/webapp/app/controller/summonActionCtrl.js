define(["app"], function (app) {
    app.registerCtrl('summonActionCtrl', function ($scope, $http, $state, $stateParams) {
        var characterSummonId = $stateParams.id;
        var processing = false;

        $http.get(HOST + '/api/character/summon/' + characterSummonId).
                success(function (data, status, headers, config) {
                    $scope.summon = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/character/pet/').
                success(function (data, status, headers, config) {
                    $scope.characterPets = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/character/friends/').
            success(function (data, status, headers, config) {
                $scope.characters = data.data;
            }).
            error(function (data, status, headers, config) {
                alert(data.msg);
            });

        $scope.action = function (actionType) {
            switch (actionType) {
                case "1":
                    if (processing) {
                        return;
                    }

                    processing = true
                    equipPetSummon($scope.selectedCharacterPetId, characterSummonId);
                    break;
                case "2":
                    releaseSummon(characterSummonId);
                    break;
                case "3":
                    sendSummon($scope.selectedCharacterId, characterSummonId);
                    break;
            }
        };

        function equipPetSummon(characterPetId, characterSummonId) {
            $http.post(HOST + '/api/character/equippetsummon/' + characterPetId + "/" + characterSummonId).
                    success(function (data, status, headers, config) {
                        processing = false;
                        alert("Trang bị thành công");
                        $state.go("summonCenter");
                    }).
                    error(function (data, status, headers, config) {
                        processing = false;
                        toastr.warning(data.msg);
                        $state.go("summonCenter");
                    });
        }

        function releaseSummon(characterSummonId) {
            $http.post(HOST + '/api/character/releasesummon/' + characterSummonId).
                    success(function (data, status, headers, config) {
                        $state.go("summonCenter");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.success(data.msg);
                        $state.go("summonCenter");
                    });
        }

        function sendSummon(characterId, characterSummonId) {
            $http.post(HOST + '/api/character/sendsummon/' + characterId + "/" + characterSummonId).
                success(function (data, status, headers, config) {
                    processing = false;
                    toastr.success("Gửi thành công");
                    $state.go("summonCenter");
                }).
                error(function (data, status, headers, config) {
                    processing = false;
                    toastr.warning(data.msg);
                    $state.go("summonCenter");
                });
        }
    });
});