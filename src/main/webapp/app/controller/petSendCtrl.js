define(["app"], function (app) {

    app.registerCtrl('petSendCtrl', function ($scope, $http, $state, $stateParams) {
        var characterPetId = $stateParams.id;

        $http.get(HOST + '/api/character/pet/' + characterPetId).
                success(function (data, status, headers, config) {
                    $scope.characterPet = data.data;
                }).
                error(function (data, status, headers, config) {
                    toastr.error(data.msg);
                    $state.go("petCenter");
                });

        $http.get(HOST + '/api/character/friends/').
                success(function (data, status, headers, config) {
                    $scope.characters = data.data;
                }).
                error(function (data, status, headers, config) {
                    toastr.error(data.msg);
                });

        $scope.send = function () {
            sendPet($scope.selectedCharacterId, characterPetId);
        };

        function sendPet(characterId, characterPetId) {
            $http.post(HOST + '/api/character/sendpet/' + characterId + "/" + characterPetId).
                    success(function (data, status, headers, config) {
                        toastr.success("Gửi thành công");
                        $state.go("petCenter");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.error(data.msg);
                        $state.go("petCenter");
                    });
        }
    });

});