define(["app"], function (app) {

    app.registerCtrl('itemBuyDialogCtrl', function ($scope, item, close) {
        $scope.display = true;

        $scope.item = item;

        $scope.close = function (value) {
            $scope.display = false;
            close(value);
        };
    });


    app.registerCtrl('shopDetailCtrl', function ($scope, $http, $state, $stateParams, ModalService) {
        var type = $stateParams.type;

        $scope.items = [];

        switch (type) {
            case "1":
                getWeapon();
                $scope.shopName = "vũ khí";
                break;
            case "2":
                getMedical();
                $scope.shopName = "Cửa hàng dược phẩm";
                break;
            case "3":
                getPremium();
                $scope.shopName = "Cửa hàng cao cấp";
                break;
            case "4":
                getGrocery();
                $scope.shopName = "Cửa hàng tạp hóa";
                break;
            case "5":
                getGolden();
                $scope.shopName = "Cửa hàng hoàng kim";
                break;
            case "6":
                getSpecial();
                $scope.shopName = "Cửa hàng đặc biệt";
                break;
            default :
                alert("Cửa hàng chưa mở");
                $state.go("shopCenter");
                break;
        }

        function getWeapon() {
            $http.get(HOST + '/api/shop/weapon/').
                    success(function (data, status, headers, config) {
                        $scope.itemRows = presentData(data.data);
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function getMedical() {
            $http.get(HOST + '/api/shop/medical/').
                    success(function (data, status, headers, config) {
                        $scope.itemRows = presentData(data.data);
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function getPremium() {
            $http.get(HOST + '/api/shop/premium/').
                    success(function (data, status, headers, config) {
                        $scope.itemRows = presentData(data.data);
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function getGrocery() {
            $http.get(HOST + '/api/shop/grocery/').
                    success(function (data, status, headers, config) {
                        $scope.itemRows = presentData(data.data);
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function getGolden() {
            $http.get(HOST + '/api/shop/golden/').
                    success(function (data, status, headers, config) {
                        $scope.itemRows = presentData(data.data);
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function getSpecial() {
            $http.get(HOST + '/api/shop/special/summon').
                    success(function (data, status, headers, config) {
                        var temp = data.data;
                        $http.get(HOST + '/api/shop/special/weapon').
                                success(function (data, status, headers, config) {
                                    Array.prototype.push.apply(temp, data.data);
                                    $scope.itemRows = presentData(temp);
                                }).
                                error(function (data, status, headers, config) {
                                    alert(data.msg);
                                });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        $scope.buy = function (item) {
            if (!item.itemClass) {
                showBuyDialog(item, function (result) {
                    if (result) {
                        $http.post(HOST + '/api/shop/buy/summon/' + item.id).
                                success(function (data, status, headers, config) {
                                    toastr.success("Mua thành công");
                                }).
                                error(function (data, status, headers, config) {
                                    alert(data.msg);
                                });
                    }
                });
            } else {
                showBuyDialog(item, function (result) {
                    if (result) {
                        $http.post(HOST + '/api/shop/buy/item/' + item.id).
                                success(function (data, status, headers, config) {
                                    toastr.success("Mua thành công");
                                }).
                                error(function (data, status, headers, config) {
                                    alert(data.msg);
                                });
                    }
                });
            }
        };

        function showBuyDialog(item, callBack) {
            ModalService.showModal({
                templateUrl: "views/dialog/buy_item_dialog.jsp",
                controller: "itemBuyDialogCtrl",
                inputs: {
                    item: item
                }
            }).then(function (modal) {
                modal.close.then(callBack);
            });
        }

        function presentData(items) {
            var data = [];

            for (var i = 0; i < items.length; i = i + 3) {
                var itemRow = [];

                for (var j = i; j < items.length && j < i + 3; j++) {
                    itemRow.push(items[j]);
                }

                data.push(itemRow);
            }

            return data;
        }
    });

});