define(["app"], function (app) {
    app.registerCtrl('adminMonsterRewardItemDetailCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        var id = $stateParams.id;

        $scope.imageSources = [];
        $scope.tempItem = 0;
        $scope.tempRate = 0;

        $http.get(HOST + '/api/monster/' + id).
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.monster = data.data;

                        for (var i = 0; i < $scope.monster.monsterRewardItems.length; i++) {
                            var imageSource = IMAGES_HOST + $scope.monster.monsterRewardItems[i].item.imageUrl;
                            $scope.imageSources.push(imageSource);
                        }
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/item/').
                success(function (data, status, headers, config) {
                    $scope.items = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            updateMonster($scope.monster);
        };

        $scope.add = function () {
            var tempItem = findInList($scope.tempItem.id);
            console.log(tempItem);

            //Create new item for ignore dupplicate item error
            var item = {
                id: tempItem.id,
                name: tempItem.name,
                imageUrl: tempItem.imageUrl
            };

            var rewardItem = {
                item: item,
                rate: $scope.tempRate
            };

            $scope.monster.monsterRewardItems.push(rewardItem);

            var imageSource = IMAGES_HOST + item.imageUrl;
            $scope.imageSources.push(imageSource);
        };

        $scope.delete = function (idx) {
            $scope.monster.monsterRewardItems.splice(idx, 1);
            $scope.imageSources.splice(idx, 1);
        };

        $scope.itemChange = function (itemId) {
            var item = findInList(itemId);
            var imageSource = IMAGES_HOST + item.imageUrl;

            $scope.imageTempSources = imageSource;
        }

        function updateMonster(monster) {
            console.log(monster);
            $http.put(HOST + '/api/monster/', monster).
                    success(function (data, status, headers, config) {
                        alert("Chỉnh sửa monster thành công");
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function findInList(id) {
            for (var i = 0; i < $scope.items.length; i++) {
                if ($scope.items[i].id == id) {
                    return $scope.items[i];
                }
            }
        }
    });
});