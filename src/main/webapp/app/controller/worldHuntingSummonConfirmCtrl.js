define(["app"], function (app) {
    app.registerCtrl('worldHuntingSummonConfirmCtrl', function ($scope, $http, $state) {

        updateInfo();

        $scope.escape = function () {
            $http.post(HOST + '/api/huntingbattle/summon/escape').
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.capture = function (typeNet) {
            $http.post(HOST + '/api/huntingbattle/summon/capture/' + typeNet).
                    success(function (data, status, headers, config) {
                        if (data.data.summon == null) {
                            toastr.info("Bắt thất bại. Linh thú đã chạy mất");
                            $state.go("worldHuntingArea");
                        } 
                        else if (data.data.type != 0) {
                            $state.go("worldHuntingSummonCreate");
                        } 
                        else {
                            if (typeNet == 1) {
                                $scope.battleContext.normalNet -= 1;
                            }
                            else if (typeNet == 2) {
                                $scope.battleContext.electricNet -= 1;
                            }
                            else if (typeNet == 3) {
                                $scope.battleContext.magicNet -= 1;
                            }
                            toastr.info("Bắt thất bại.");
                        }
                    }).
                    error(function (data, status, headers, config) {
                        toastr.info(data.msg);
                    });
        };

        function updateInfo() {
            $http.get(HOST + '/api/huntingbattle/current').
                    success(function (data, status, headers, config) {
                        $scope.battleContext = data.data;
                        $scope.summon = data.data.summon;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});