define(["app"], function (app) {
    app.registerCtrl('adminPetItemAddCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        $scope.item = {
            itemClass: {
                id: 11
            },
            characterClass: {
                id: -1
            },
            increasingAttribute: "Strength",
            goldCostSell: 0,
            goldCostBuy: 0,
            crystalCostSell: 0,
            crystalCostBuy: 0,
            endurance: 0,
            hp: 0,
            strength: 0,
            defense: 0,
            intelligent: 0,
            speed: 0,
            magic: 0,
            levelRequire: 0,
            sell: false
        };

        var itemUploader = $scope.itemUploader = new FileUploader({
            url: HOST + '/api/file/'
        });
        itemUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        itemUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.item.imageUrl = response;
            saveItem($scope.item);
        };
        itemUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $http.get(HOST + '/api/itemclass/pet').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.itemClasses = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            itemUploader.uploadAll();
        };

        function saveItem(item) {
            $http.post(HOST + '/api/item/', item).
                    success(function (data, status, headers, config) {
                        alert("Thêm item thành công");
                        $state.go("adminPetItemDetail", {id: data.data.id});
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});