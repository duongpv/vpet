define(["app"], function (app) {

    app.registerCtrl('unequipPetItemDialogCtrl', ['$scope', 'close', function ($scope, close) {
            $scope.display = true;

            $scope.close = function (value) {
                $scope.display = false;
                close(value);
            };
        }]);

    app.registerCtrl('releasePetDialogCtrl', ['$scope', 'close', function ($scope, close) {
            $scope.display = true;

            $scope.close = function (value) {
                $scope.display = false;
                close(value);
            };
        }]);

    app.registerCtrl('unequipPetSummonDialogCtrl', ['$scope', 'close', function ($scope, close) {
            $scope.display = true;

            $scope.close = function (value) {
                $scope.display = false;
                close(value);
            };
        }]);

    app.registerCtrl('petDetailCtrl', function ($scope, $http, $state, $stateParams, ModalService) {
        var id = $stateParams.id;
        $scope.characterPet = {};
        $scope.petEquipments = [];

        updateView();

        $scope.showUnusePetItemModal = function (petEquipment) {
            ModalService.showModal({
                templateUrl: "views/dialog/unequip_pet_item_dialog.jsp",
                controller: "unequipPetItemDialogCtrl"
            }).then(function (modal) {
                modal.close.then(function (result) {
                    if (result) {
                        unusePetItem(petEquipment);
                    }
                });
            });
        };

        $scope.showReleasePetModal = function (characterPet) {
            ModalService.showModal({
                templateUrl: "views/dialog/release_pet_dialog.jsp",
                controller: "releasePetDialogCtrl"
            }).then(function (modal) {
                modal.close.then(function (result) {
                    if (result) {
                        releasePet(characterPet);
                    }
                });
            });
        };

        $scope.showUnusePetSummonModal = function (petSummon) {
            ModalService.showModal({
                templateUrl: "views/dialog/unequip_pet_item_dialog.jsp",
                controller: "unequipPetSummonDialogCtrl"
            }).then(function (modal) {
                modal.close.then(function (result) {
                    if (result) {
                        unequipPetSummon(petSummon);
                    }
                });
            });
        };

        $scope.setActive = function (characterPet) {
            $http.post(HOST + '/api/character/activepet/' + characterPet.id).
                    success(function (data, status, headers, config) {
                        alert("Thành công");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        $scope.sendFriend = function (characterPet) {
            $state.go("petSend", {id: characterPet.id});
        }

        $scope.petConsign = function (characterPet) {
            $state.go("petConsign", {id: characterPet.id});
        }

        $scope.unConsign = function (characterPet) {
            unConsign(characterPet.id);
        }

        $scope.renderTooltipSummon = function (petSummon) {
            var summon = petSummon.summon;
            if (typeof summon != 'undefined') {

                var toolToop = "<strong>" + summon.name + "<strong>";
                toolToop += "<div>" + "Ma thuật: " + summon.magic + "</div>";
                toolToop += "<div>" + "Loại: " + summon.summonClass.name + "</div>";
                toolToop += "</div>";

                return toolToop;
            }
            return "";
        };

        $scope.renderTooltip = function (petEquipment) {
            var item = petEquipment.item;
            if (typeof item != 'undefined') {

                var toolToop = "<strong>" + item.name + "<strong>";
                toolToop += "<br><div align='left' >";
                toolToop += "<div>" + "Sức mạnh: " + item.strength + "</div>";
                toolToop += "<div>" + "Phòng thủ: " + item.defense + "</div>";
                toolToop += "<div>" + "Thông minh: " + item.intelligent + "</div>";
                toolToop += "<div>" + "Tốc độ: " + item.speed + "</div>";
                toolToop += "<div>" + "Ma thuật: " + item.magic + "</div>";

                if (item.endurance >= 0) {
                    toolToop += "<div>" + "Độ bền: " + petEquipment.endurance + "/" + item.endurance + "</div>";
                } else {
                    toolToop += "<div>" + "Độ bền: Vô hạn</div>";
                }

                toolToop += "<div>" + "Loại: " + item.itemClass.name + "</div>";
                toolToop += "</div>";

                return toolToop;
            }
            return "";
        };

        function releasePet(characterPet) {
            $http.post(HOST + '/api/character/releasepet/' + characterPet.id).
                    success(function (data, status, headers, config) {
                        alert("Thả thành công");
                        $state.go("petCenter");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function updateView() {
            $http.get(HOST + '/api/character/pet/' + id).
                    success(function (data, status, headers, config) {
                        $scope.characterPet = data.data;
                        $scope.characterPet.nextExperience = getNextExperience($scope.characterPet.level + 1);

                        $scope.petEquipments = $scope.characterPet.petEquipments;
                        $scope.petSummons = $scope.characterPet.petSummons;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                        $state.go("petCenter");
                    });
        }

        function unequipPetSummon(petSummon) {
            $http.post(HOST + '/api/character/unequippetsummon/' + id + '/' + petSummon.id).
                    success(function (data, status, headers, config) {
                        updateView();
                    }).
                    error(function (data, status, headers, config) {
                        toastr.warning(data.msg);
                    });

            console.log(petSummon)
        }

        function unusePetItem(petEquipment) {
            $http.post(HOST + '/api/character/unequippetitem/' + id + '/' + petEquipment.id).
                    success(function (data, status, headers, config) {
                        updateView();
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function getNextExperience(level) {
            if (level <= 1) {
                return 0;
            }

            var result = getNextExperience(level - 1) + level * level * level / 100 + 30 * level * level;

            return parseInt(result);
        }

        function unConsign(characterPetId) {
            $http.post(HOST + '/api/character/unconsign/' + characterPetId).
                success(function (data, status, headers, config) {
                    toastr.success("Hủy ủy thác thành công");
                    updateView();
                }).
                error(function (data, status, headers, config) {
                    toastr.error(data.msg);
                });
        }
    });
});