define(["app"], function (app) {
    app.registerCtrl('adminPetListCtrl', function ($scope, $http, $state) {
        $scope.petList = {};

        $http.get(HOST + '/api/pet/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.petList = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.delete = function (id, idx) {
            var r = confirm("Bạn có chắc chắn xóa không ?");
            if (r == true) {
                $http.delete(HOST + '/api/pet/' + id).
                        success(function (data, status, headers, config) {
                            $scope.petList.splice(idx, 1);
                        }).
                        error(function (data, status, headers, config) {
                            alert(data.msg);
                        });
            }
        };
    });
});