define(["app"], function (app) {
    app.registerCtrl('memberRegisterCtrl', function ($scope, vcRecaptchaService, $http, $state) {
        $scope.registerInfo = {};

        $scope.response = null;
        $scope.widgetId = null;
        $scope.model = {
            key: '6LcBOQYTAAAAANlWlmryaFpGos-pfh9_gQn0QwH8'
        };
        $scope.setResponse = function (response) {
            $scope.registerInfo.captcha = response;
        };
        $scope.setWidgetId = function (widgetId) {
            console.info('Created widget ID: %s', widgetId);
            $scope.widgetId = widgetId;
        };

        $scope.submit = function () {
            if (!$scope.registerMemberForm.$valid || $scope.registerInfo.captcha === null) {
                alert("Thông tin không hợp lệ");
                return;
            }
            $http.post(HOST + '/api/member/', $scope.registerInfo).
                    success(function (data, status, headers, config) {
                        alert("Đăng kí thành công");
                        $state.go("login");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                        $scope.submitted = false; // Remove submitted flag
                        $scope.registerInfo.captcha = null; // Remove captcha
                        vcRecaptchaService.reload($scope.widgetId);
                    });
        };
    });
});