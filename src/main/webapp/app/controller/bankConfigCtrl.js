define(["app"], function (app) {
    app.registerCtrl('bankConfigCtrl', function ($scope, $http, $state) {

        $scope.bankConfig = {
            pass: "",
            oldRootpass: "",
            rootpass: "",
            repeatRootpass: ""
        };

        $scope.setBankConfig = function (bankConfig) {

            if (bankConfig.rootpass != bankConfig.repeatRootpass) {
                toastr.error("Mật khẩu cấp 2 không trùng khớp");
                return;
            }

            $http.post(HOST + '/api/character/bankconfig', bankConfig).
                    success(function (data, status, headers, config) {
                        toastr.success("Cài đặt thành công");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.error(data.msg);
                    });
        };

    });
});