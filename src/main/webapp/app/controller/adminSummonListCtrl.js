define(["app"], function (app) {
    app.registerCtrl('adminSummonListCtrl', function ($scope, $http, $state) {
        $scope.summons = {};

        $http.get(HOST + '/api/summon/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.summons = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.delete = function (id, idx) {
            var r = confirm("Bạn có chắc chắn xóa không ?");
            if (r == true) {
                $http.delete(HOST + '/api/summon/' + id).
                        success(function (data, status, headers, config) {
                            $scope.summons.splice(idx, 1);
                        }).
                        error(function (data, status, headers, config) {
                            alert(data.msg);
                        });
            }
        };
    });
});