define(["app"], function (app) {
    app.registerCtrl('adminCharacterListCtrl', function ($scope, $http, $state) {
        $scope.characterList = {};

        $http.get(HOST + '/api/admincharacter/all').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.characterList = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
    });
});