define(["app"], function (app) {
    app.registerCtrl('avatarCenterCtrl', function ($scope, $http, $state) {
        $scope.character = {};

        $http.get(HOST + '/api/character/viewdetail').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                        $state.go("characterRegister");
                    } else {
                        $scope.character = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
    });
});