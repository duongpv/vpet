define(["app"], function (app) {
    app.registerCtrl('questCenterCtrl', function ($scope, $http, $state) {

        $scope.quest = null;
        $scope.showCenter = false;
        $scope.items = [];
        $scope.summons = [];

        $http.get(HOST + '/api/quest/current').
                success(function (data, status, headers, config) {
                    if (data.data != null) {
                        $scope.quest = data.data;

                        showData($scope.quest);
                    } else {

                        $scope.showCenter = true;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.remove = function () {
            $http.post(HOST + '/api/quest/remove').
                    success(function (data, status, headers, config) {
                        toastr.success("Hủy nhiệm vụ thành công");
                        $state.transitionTo($state.current, null, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.heroQuestComplete = function () {
            $http.post(HOST + '/api/quest/hero/complete').
                    success(function (data, status, headers, config) {
                        toastr.success("Hoàn thành nhiệm vụ");
                        $scope.complete = true;
                        $scope.items = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        toastr.info(data.msg);
                    });
        };

        $scope.summonQuestComplete = function () {
            $http.post(HOST + '/api/quest/summon/complete').
                    success(function (data, status, headers, config) {
                        toastr.success("Hoàn thành nhiệm vụ");
                        $scope.completeSummon = true;
                        $scope.summons = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        toastr.info(data.msg);
                    });
        };

        $scope.itemQuestComplete = function () {
            $http.post(HOST + '/api/quest/item/complete').
                    success(function (data, status, headers, config) {
                        toastr.success("Hoàn thành nhiệm vụ");
                        $scope.complete = true;
                        $scope.items = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        toastr.info(data.msg);
                    });
        };

        $scope.reload = function () {
            $state.transitionTo($state.current, null, {
                reload: true,
                inherit: false,
                notify: true
            });
        };

        function showData(quest) {
            switch (quest.questType) {
                case 1:
                    showSummonData(quest);
                    break;
                case 2:
                    showItemData(quest);
                    break;
                case 3:
                    showHeroData(quest);
                    break;
            }
        }

        function showItemData(quest) {
            for (var i = 0; i < quest.questRequires.length; i++) {
                fetchItemData(quest.questRequires[i].id);
            }
        }

        function showSummonData(quest) {
            var timeEnd = new Date(quest.timeEnd).getTime();
            var timeNow = new Date().getTime();
            if (timeEnd < timeNow) {
                $scope.notTimeOut = false;
            } else {
                initializeClock("countDown", quest.timeEnd);
                $scope.notTimeOut = true;
            }

            for (var i = 0; i < quest.questRequires.length; i++) {
                fetchSummonData(quest.questRequires[i].id);
            }
        }

        function getTimeRemaining(endtime) {
            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));
            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, endtime) {
            var timeinterval = setInterval(function () {
                var clock = document.getElementById(id);
                var t = getTimeRemaining(endtime);
                if (t.days > 1 || t.total <= 0) {
                    clearInterval(timeinterval);
                    clock.remove();
                }
                clock.innerHTML =
                        '' + t.hours + ' giờ ' +
                        '' + t.minutes + ' phút ' +
                        '' + t.seconds + ' giây ';
            }, 1000);
        }

        function fetchSummonData(id) {
            $http.get(HOST + '/api/summon/' + id).
                    success(function (data, status, headers, config) {
                        $scope.summons.push(data.data);
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function fetchItemData(id) {
            $http.get(HOST + '/api/item/item/' + id).
                    success(function (data, status, headers, config) {
                        $scope.items.push(data.data);
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function showHeroData(quest) {
            $http.get(HOST + '/api/monster/' + quest.questRequires[0].id).
                    success(function (data, status, headers, config) {
                        $scope.quest.monster = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});