define(["app"], function (app) {
    app.registerCtrl('adminCharacterLevelListCtrl', function ($scope, $http, $state) {

        $http.get(HOST + '/api/characterlevel/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.characterLevels = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });


    });
});