define(["app"], function (app) {
    app.registerCtrl('itemActionCtrl', function ($scope, $http, $state, $stateParams) {
        var characterItemId = $stateParams.id;
        var callbackView = $stateParams.callbackView;
        var processing = false;

        var actionList = [
        ];

        $http.get(HOST + '/api/character/item/' + characterItemId).
                success(function (data, status, headers, config) {
                    $scope.item = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/character/pet/').
                success(function (data, status, headers, config) {
                    $scope.characterPets = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/character/friends/').
                success(function (data, status, headers, config) {
                    $scope.characters = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.action = function (actionType) {
            switch (actionType) {
                case "1":
                    if (processing) {
                        return;
                    }

                    processing = true
                    equipPetItem($scope.selectedCharacterPetId, characterItemId);
                    break;
                case "2":
                    useMedicalItem($scope.selectedCharacterPetId, characterItemId);
                    break;
                case "3":
                    useEvolutionItem($scope.selectedCharacterPetId, characterItemId);
                    break;
                case "4":
                    usePointItem($scope.selectedCharacterPetId, characterItemId);
                    break;
                case "5":
                    sendItem($scope.selectedCharacterId, characterItemId);
                    break;
                case "6":
                    useItem(characterItemId);
                    break;
                case "9":
                    var item = {
                        id: characterItemId
                    };
                    sellItem(item);
                    break;
            }
        };

        function useMedicalItem(characterPetId, characterItemId) {
            $http.post(HOST + '/api/character/usemedicalitem/' + characterPetId + "/" + characterItemId).
                    success(function (data, status, headers, config) {
                        alert("Chữa trị thành công");
                        $state.go("itemCenter");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                        $state.go("itemCenter");
                    });
        }

        function sendItem(characterId, characterItemId) {
            $http.post(HOST + '/api/character/senditem/' + characterId + "/" + characterItemId).
                    success(function (data, status, headers, config) {
                        toastr.success("Gửi thành công");

                        if (callbackView !== null) {
                            $state.go(callbackView);
                        } else {
                            $state.go("itemCenter");
                        }
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                        $state.go("itemCenter");
                    });
        }


        function sellItem(item) {
            $http.post(HOST + '/api/character/characteritemsell', item).
                    success(function (data, status, headers, config) {
                        var canSell = data.data;

                        if (canSell) {
                            alert("Bán trang bị thành công");
                            $state.go("itemCenter");
                        }
                        else {
                            alert("Không thể bán trang bị");
                        }
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function equipPetItem(characterPetId, characterItemId) {
            $http.post(HOST + '/api/character/equippetitem/' + characterPetId + "/" + characterItemId).
                    success(function (data, status, headers, config) {
                        processing = false;
                        toastr.info("Trang bị thành công");
                        $state.go("itemCenter");
                    }).
                    error(function (data, status, headers, config) {
                        processing = false;
                        alert(data.msg);
                        $state.go("itemCenter");
                    });
        }

        function usePointItem(characterPetId, characterItemId) {
            $http.post(HOST + '/api/character/usepointitem/' + characterPetId + "/" + characterItemId).
                    success(function (data, status, headers, config) {
                        toastr.info("Sử dụng thành công");
                        $state.go("itemCenter");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                        $state.go("itemCenter");
                    });
        }

        function useEvolutionItem(characterPetId, characterItemId) {
            $http.post(HOST + '/api/character/useevolutionitem/' + characterPetId + "/" + characterItemId).
                    success(function (data, status, headers, config) {
                        toastr.info("Sử dụng thành công");
                        $state.go("itemCenter");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                        $state.go("itemCenter");
                    });
        }

        function useItem(characterItemId) {
            $http.post(HOST + '/api/character/useitem/' + characterItemId).
                    success(function (data, status, headers, config) {
                        alert(data.data);
                        $state.go("itemCenter");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                        $state.go("itemCenter");
                    });
        }
    });
});