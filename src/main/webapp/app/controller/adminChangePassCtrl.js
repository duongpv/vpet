define(["app"], function (app) {
    app.registerCtrl('adminChangePassCtrl', function ($scope, $http, $state, $stateParams) {
        $scope.pass = "";

        $scope.submit = function () {
            $http.post(HOST + '/api/admincharacter/changepass', $scope.pass).
                    success(function (data, status, headers, config) {
                        alert("Đổi pass thành công");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };
    });
});