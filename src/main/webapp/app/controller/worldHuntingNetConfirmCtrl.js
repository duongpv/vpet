define(["app"], function (app) {
    app.registerCtrl('worldHuntingNetConfirmCtrl', function ($scope, $http, $state) {

        $("#hunting_net_confirm_tbl").bind("keydown", onKeydown);

        $http.get(HOST + '/api/huntingbattle/current').
                success(function (data, status, headers, config) {

                    $scope.rewardItem = {
                        name: ""
                    };

                    switch (data.data.dropNet) {
                        case 0:
                            $scope.rewardItem.name = "Lưới thường";
                            break;
                        case 1:
                            $scope.rewardItem.name = "Lưới điện";
                            break;
                        case 2:
                            $scope.rewardItem.name = "Lưới phép";
                            break;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.ok = function () {
            $http.get(HOST + '/api/huntingbattle/current').
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});