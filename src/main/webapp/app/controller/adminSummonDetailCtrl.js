define(["app"], function (app) {
    app.registerCtrl('adminSummonDetailCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        var id = $stateParams.id;

        $scope.summon = {
            summonClass: {
                id: -1
            },
            hpRegen: 0,
            areas: [{
                    id: 1
                }],
            magic: 0
        };

        $scope.addMap = function () {
            var map = {
                id: 1
            };
            $scope.summon.areas.push(map);
        };

        $scope.removeMap = function (mapId) {
            var areas = $scope.summon.areas;

            for (var i = 0; i < areas.length; i++) {
                var area = areas[i];

                if (area.id == mapId) {
                    areas.splice(i, 1);
                    return
                }
            }
        };

        $http.get(HOST + '/api/area/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.areas = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/summon/' + id).
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.summon = data.data;
                        $scope.imageSource = IMAGES_HOST + $scope.summon.imageUrl;
                        $scope.summonClassId = $scope.summon.summonClass.id;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        var imageUploader = $scope.imageUploader = new FileUploader({
            url: HOST + '/api/file/'
        });

        imageUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        imageUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.summon.imageUrl = response;
            updateItem($scope.summon);
        };
        imageUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $http.get(HOST + '/api/summonclass/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.summonClasses = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            if (imageUploader.queue.length === 0) {
                updateItem($scope.summon);
            } else {
                imageUploader.uploadAll();
            }
        };

        function updateItem(summon) {
            $http.put(HOST + '/api/summon/', summon).
                    success(function (data, status, headers, config) {
                        alert("Chỉnh sửa linh thú thành công");
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        $scope.summonClassChange = function (summonClass) {
            $scope.summon.summonClass = summonClass;
        };
    });
});