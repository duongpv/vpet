define(["app"], function (app) {

    app.registerCtrl('itemSellDialogCtrl', ['$scope', 'close', function ($scope, close) {
            $scope.display = true;

            $scope.close = function (value) {
                $scope.display = false;
                close(value);
            };
        }]);

    app.registerCtrl('avatarViewCtrl', function ($scope, $http, $state, ModalService) {
        $scope.character = {};
        $scope.characterItems = [];
        $scope.isShowDetail = false;
        $scope.nowPage = 0;
        $scope.characterEquipmentsItem = [];

        var selectedItem = null;
        var isMenuShow = false;

        getCharacterInfo();

        $scope.showDetail = function () {
            $scope.isShowDetail = !$scope.isShowDetail;
        };

        $scope.showContextMenu = function (item) {
            var target = $("#item-" + item.id).position();
            var x = target.left - (target.left < 900 ? 80 : -85);
            var y = target.top - 15;
            var css = {
                top: y,
                left: x,
                position: 'absolute',
                display: "block"
            };

            selectedItem = item;

            $("#context-menu").css(css);
            $("#context-menu").addClass("open");
            isMenuShow = true;
        };

        $scope.equip = function () {
            equip(selectedItem);
        };

        $scope.itemEquip = function (selectedItem) {
            equip(selectedItem);
        };

        $scope.unequip = function (equipmentItem) {
            $http.post(HOST + '/api/character/characteritemunequip', equipmentItem).
                    success(function (data, status, headers, config) {
                        getCharacterInfo();
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.send = function () {
            $state.go("itemAction", {id: selectedItem.id, callbackView: "avatarView"});
        };
        $scope.sell = function () {
            $scope.showAModal();
        };

        $scope.okeClick = function () {
            alert("");
        };

        $scope.renderTooltip = function (equipmentItem) {
            var item = equipmentItem.item;

            if (typeof item != 'undefined') {
                var toolToop = ""
                if (equipmentItem.level > 0) {
                    toolToop = "<strong>" + item.name + " +" + equipmentItem.level + "<strong>";
                    toolToop += "<br><div align='left' >";
                    toolToop += "<div>" + "Sức mạnh: " + (item.strength * (1 + equipmentItem.level / 10)).toFixed(0) + "</div>";
                    toolToop += "<div>" + "Phòng thủ: " + (item.defense * (1 + equipmentItem.level / 10)).toFixed(0) + "</div>";
                    toolToop += "<div>" + "Thông minh: " + (item.intelligent * (1 + equipmentItem.level / 10)).toFixed(0) + "</div>";
                    toolToop += "<div>" + "Tốc độ: " + (item.speed * (1 + equipmentItem.level / 10)).toFixed(0) + "</div>";
                } else {
                    toolToop = "<strong>" + item.name + "<strong>";
                    toolToop += "<br><div align='left' >";
                    toolToop += "<div>" + "Sức mạnh: " + item.strength + "</div>";
                    toolToop += "<div>" + "Phòng thủ: " + item.defense + "</div>";
                    toolToop += "<div>" + "Thông minh: " + item.intelligent + "</div>";
                    toolToop += "<div>" + "Tốc độ: " + item.speed + "</div>";

                }
                toolToop += "<div>" + "Nhân vật: " + item.characterClass.name + "</div>";
                toolToop += "<div>" + "Giá bán: " + item.goldCostSell + "</div>";
                toolToop += "</div>";

                return toolToop;
            }
            
            return "";
        };

        $scope.showAModal = function () {
            ModalService.showModal({
                templateUrl: "views/dialog/sell_item_dialog.jsp",
                controller: "itemSellDialogCtrl"
            }).then(function (modal) {
                modal.close.then(function (result) {
                    if (result) {
                        sellItem(selectedItem);
                    }
                });
            });
        };

        $(document.body).on("click", function () {
            if (isMenuShow) {
                $("#context-menu").removeClass("open");
            }
        });

        function equip(characterItem) {
            $http.post(HOST + '/api/character/characteritemequip', characterItem).
                    success(function (data, status, headers, config) {
                        var canEquip = data.data;

                        if (canEquip)
                        {
                            getCharacterInfo();
                        }
                        else
                        {
                            alert("Không thể trang bị");
                        }
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function getCharacterInfo() {
            $http.get(HOST + '/api/character/viewdetail').
                    success(function (data, status, headers, config) {
                        $scope.character = data.data;

                        $http.get(HOST + '/api/character/characteritem').
                                success(function (data, status, headers, config) {
                                    $scope.characterItems = data.data;

                                    updateItemView($scope.characterItems, $scope.character);
                                }).
                                error(function (data, status, headers, config) {
                                    alert(data.msg);
                                });

                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });

            $http.get(HOST + '/api/character/characterpoint').
                    success(function (data, status, headers, config) {
                        $scope.characterInfo = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function updateItemView(items, character) {
            $scope.itemRows = getPageData(0, items);

            if ($scope.itemRows.length !== 0) {
                var totalPages = Math.ceil(items.length / ($scope.itemRows.length * 2));
                initPage(totalPages);
            }

            var hat = {id: -1};
            var cloak = {id: -1};
            var wing = {id: -1};
            var arm = {id: -1};
            var leg = {id: -1};
            for (var i = 0; i < character.characterEquipments.length; i++) {
                var equipmentItem = character.characterEquipments[i];
                var itemClass = equipmentItem.item.itemClass;
                switch (itemClass.id) {
                    case 1:
                        hat = equipmentItem;
                        break;
                    case 2:
                        cloak = equipmentItem;
                        break;
                    case 3:
                        wing = equipmentItem;
                        break;
                    case 4:
                        arm = equipmentItem;
                        break;
                    case 5:
                        leg = equipmentItem;
                        break;
                }
            }

            $scope.characterEquipmentsItem = [];
            $scope.characterEquipmentsItem.push(hat);
            $scope.characterEquipmentsItem.push(cloak);
            $scope.characterEquipmentsItem.push(wing);
            $scope.characterEquipmentsItem.push(arm);
            $scope.characterEquipmentsItem.push(leg);
        }

        function getPageData(page, data) {
            var pageData = [];

            var maxLen = data.length;
            for (var i = page * 8; i < (page + 1) * 8 && i < maxLen; ) {
                var row = {
                    items: []
                };

                row.items.push(data[i]);
                if (i + 1 < maxLen)
                    row.items.push(data[i + 1]);

                pageData.push(row);

                i = i + 2;
            }

            return pageData;
        }

        function sellItem(item) {
            $http.post(HOST + '/api/character/characteritemsell', item).
                    success(function (data, status, headers, config) {
                        var canEquip = data.data;

                        if (canEquip)
                        {
                            getCharacterInfo();
                        }
                        else
                        {
                            alert("Không thể bán trang bị");
                        }
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function initPage(totalPages) {
            $("#pagination-box").html("");
            $("#pagination-box").html('<ul id="pagination" class="pagination-sm"></ul>');
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                first: "Đầu",
                last: "Cuối",
                prev: "",
                next: "",
                visiblePages: 3,
                onPageClick: function (event, page) {
                    page = page - 1;
                    $scope.itemRows = getPageData(page, $scope.characterItems);
                    $scope.$apply();
                }
            });
        }

    });

});