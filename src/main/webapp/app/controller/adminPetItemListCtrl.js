define(["app"], function (app) {
    app.registerCtrl('adminPetItemListCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        $http.get(HOST + '/api/item/pet/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.itemes = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.delete = function (id, idx) {
            var r = confirm("Bạn có chắc chắn xóa không ?");
            if (r == true) {
                $http.delete(HOST + '/api/item/' + id).
                        success(function (data, status, headers, config) {
                            $scope.itemes.splice(idx, 1);
                        }).
                        error(function (data, status, headers, config) {
                            alert(data.msg);
                        });
            }
        };
    });
});