define(["app"], function (app) {
    app.registerCtrl('adminSkillAddCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        $scope.skill = {
            areas: [{
                    id: 1
                }],
            t: {
                id: 1
            },
            description: "",
            imageUrl: null,
            name: "",
            rate: 0,
            rateCritDame: 0,
            rateDameReflect: 0,
            rateDameTaken: 0,
            rateDefense: 0,
            rateIntelligent: 0,
            ratePoisonDame: 0,
            rateRegenHP: 0,
            rateSpeed: 0,
            rateStealDefToStr: 0,
            rateStealIntToSpeed: 0,
            rateStealSpeedToInt: 0,
            rateStealStrToDef: 0,
            rateStrength: 0,
            turn: 0,
            turnFreeze: 0
        };

        $http.get(HOST + '/api/skillclass/').
                success(function (data, status, headers, config) {
                    $scope.skillClasses = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/area/').
                success(function (data, status, headers, config) {

                    $scope.areas = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.addMap = function () {
            var map = {
                id: 1
            };
            $scope.skill.areas.push(map);
        };

        $scope.removeMap = function (mapId) {
            var areas = $scope.skill.areas;

            for (var i = 0; i < areas.length; i++) {
                var area = areas[i];

                if (area.id == mapId) {
                    areas.splice(i, 1);
                    return;
                }
            }
        };

        var imageUploader = $scope.imageUploader = new FileUploader({
            url: HOST + '/api/file/'
        });
        imageUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        imageUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.skill.imageUrl = response;

            save($scope.skill);
        };
        imageUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $scope.submit = function () {
            imageUploader.uploadAll();
        };


        function save(skill) {
            $http.post(HOST + '/api/skill/', skill).
                    success(function (data, status, headers, config) {
                        alert("Thêm skill thành công");
                        $state.go("adminSkillDetail", {id: data.data.id});
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});