define(["app"], function (app) {
    app.registerCtrl('cardCtrl', function ($scope, $http, $state, $stateParams) {
        $scope.cardInfo = {};

        $scope.send = function () {
            $http.post(HOST + '/api/character/card', $scope.cardInfo).
                    success(function (data, status, headers, config) {
                        toastr.success("Nạp thành công");
                    }).
                    error(function (data, status, headers, config) {
                        if (status == 403) {
                            toastr.error("Nhập sai quá nhiều. Bạn bị khóa tài khoản.");
                            window.location.reload();
                        } else {
                            toastr.error("Nạp thất bại. Nếu sai quá 3 lần trong vòng 30 phút bạn sẽ bị khóa tài khoản");
                            toastr.info(data.data);
                        }
                    });
        };
    });
});