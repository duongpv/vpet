define(["app"], function (app) {
    app.registerCtrl('areaViewCtrl', function ($scope, $http, $state) {
        $scope.monsters = {};

        $http.get(HOST + '/api/monsterarea/monsters').
                success(function (data, status, headers, config) {
                    $scope.monsters = data.data;
                    $scope.monstersArray = [];
                    for (var i = 0; i < $scope.monsters.length / 2; i++) {
                        $scope.monstersArray.push(i);
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
    });
});