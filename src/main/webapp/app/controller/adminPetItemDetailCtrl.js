define(["app"], function (app) {
    app.registerCtrl('adminPetItemDetailCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        var id = $stateParams.id;

        $scope.item = {
            itemClass: {
                id: 1
            },
            characterClass: {
                id: -1
            },
            increasingAttribute: "Strength",
            goldCostSell: 0,
            goldCostBuy: 0,
            crystalCostSell: 0,
            crystalCostBuy: 0,
            endurance: 0,
            hp: 0,
            strength: 0,
            defense: 0,
            intelligent: 0,
            speed: 0,
            magic: 0,
            levelRequire: 0,
            sell: false
        };

        $http.get(HOST + '/api/item/pet/' + id).
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.item = data.data;
                        $scope.imageSource = IMAGES_HOST + $scope.item.imageUrl;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        var itemUploader = $scope.itemUploader = new FileUploader({
            url: HOST + '/api/file/'
        });

        itemUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        itemUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.item.imageUrl = response;
            updateItem($scope.item);
        };
        itemUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $http.get(HOST + '/api/itemclass/pet').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.itemClasses = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            if (itemUploader.queue.length === 0) {
                updateItem($scope.item);
            } else {
                itemUploader.uploadAll();
            }
        };

        function updateItem(item) {
            $http.put(HOST + '/api/item/', item).
                    success(function (data, status, headers, config) {
                        alert("Chỉnh sửa item thành công");
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});