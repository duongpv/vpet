define(["app"], function (app) {
    app.registerCtrl('adminNewsListCtrl', function ($scope, $http, $state) {
        
        $http.get(HOST + '/api/news/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.newss = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.delete = function (id, idx) {
            var r = confirm("Bạn có chắc chắn xóa không ?");
            if (r == true) {
                $http.delete(HOST + '/api/news/' + id).
                        success(function (data, status, headers, config) {
                            $scope.newss.splice(idx, 1);
                        }).
                        error(function (data, status, headers, config) {
                            alert(data.msg);
                        });
            }
        };
        
    });
});