define(["app"], function (app) {
    app.registerCtrl('adminNewsDetailCtrl', function ($scope, $http, $state, $stateParams) {
        var id = $stateParams.id;

        $http.get(HOST + '/api/news/' + id).
                success(function (data, status, headers, config) {
                    $scope.news = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            updateItem($scope.news);
        };

        function updateItem(news) {
            $http.put(HOST + '/api/news/', news).
                    success(function (data, status, headers, config) {
                        alert("Chỉnh sửa thành công");
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});