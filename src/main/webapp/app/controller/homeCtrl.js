define(["app"], function (app) {
    app.registerCtrl('homeCtrl', function ($scope, $http, $state) {
        $scope.newss = [];

        $http.get(HOST + '/api/news/').
                success(function (data, status, headers, config) {
                    $scope.newss = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
    });
});