define(["app"], function (app) {
    app.registerCtrl('worldHuntingPetCreateCtrl', function ($scope, $http, $state) {
        $scope.name = "";

        $http.get(HOST + '/api/huntingbattle/current').
                success(function (data, status, headers, config) {
                    $scope.pet = data.data.pet;

                    if (data.data.type == 1) {
                        $scope.netType = "Lưới thường";
                    } else if (data.data.type == 2) {
                        $scope.netType = "Lưới điện";
                    } else {
                        $scope.netType = "Lưới ma thuật";
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            var info = {
                pet: {
                    name: $scope.name
                }
            };

            $http.post(HOST + '/api/huntingbattle/pet/create', info).
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };
    });
});
