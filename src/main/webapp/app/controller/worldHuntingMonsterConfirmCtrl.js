define(["app"], function (app) {
    app.registerCtrl('worldHuntingMonsterConfirmCtrl', function ($scope, $http, $state, $stateParams) {

        $http.get(HOST + '/api/huntingbattle/current').
                success(function (data, status, headers, config) {
                    $scope.monster = data.data.monster;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.escape = function () {
            $http.post(HOST + '/api/huntingbattle/battle/escape').
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.ok = function () {
            $state.go("worldHuntingBattlePetChose");
        };
    });
});