define(["app"], function (app) {
    app.registerCtrl('adminMonsterListCtrl', function ($scope, $http, $state) {
        $scope.monsters = {};

        $http.get(HOST + '/api/monster/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.monsters = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.delete = function (id, idx) {
            var r = confirm("Bạn có chắc chắn xóa không ?");
            if (r == true) {
                $http.delete(HOST + '/api/monster/' + id).
                        success(function (data, status, headers, config) {
                            $scope.monsters.splice(idx, 1);
                        }).
                        error(function (data, status, headers, config) {
                            alert(data.msg);
                        });
            }
        };
    });
});