define(["app"], function (app) {
    app.registerCtrl('smithUpdateCenterCtrl', function ($scope, $http, $state, $stateParams) {

        $http.get(HOST + '/api/character/characteritem/').
                success(function (data, status, headers, config) {
                    $scope.characterItems = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.selectedChange = function () {
            var characterItems = $scope.characterItems;
            var selectedCharacterItem = findItem($scope.selectedUpdateId, $scope.characterItems);

            $http.get(HOST + '/api/character/itemupdategoldcost/' + selectedCharacterItem.id).
                    success(function (data, status, headers, config) {
                        $scope.goldCost = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
            $scope.needItems = [];

            for (var i = 0; i < characterItems.length; i++) {
                var characterItem = characterItems[i];
                if (characterItem.id == selectedCharacterItem.id) {
                    continue;
                }
                if (selectedCharacterItem.item.id == characterItem.item.id) {
                    $scope.needItems.push(characterItem);
                }
            }
        };

        $scope.updateItem = function () {
            $http.post(HOST + '/api/character/updateitem/' + $scope.selectedUpdateId + "/" + $scope.selectedNeedlId).
                    success(function (data, status, headers, config) {
                        if (data.data == true) {
                            toastr.success("Update thành công");
                        } else {
                            toastr.error("Update thất bại");
                        }
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        $scope.calculateGoldCost = function (level) {

        }

        function findItem(id, characterItems) {
            for (var i = 0; i < characterItems.length; i++) {
                var characterItem = characterItems[i];
                if (characterItem.id == id) {
                    return  characterItem;
                }
            }

            return null;
        }
    });

});