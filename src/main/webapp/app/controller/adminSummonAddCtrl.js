define(["app"], function (app) {
    app.registerCtrl('adminSummonAddCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        $scope.summon = {
            summonClass: {
                id: 1
            },
            hpRegen: 0,
            areas: [{
                    id: 1
                }],
            magic: 0
        };
        $scope.summonClassId = 1;

        var imageUploader = $scope.imageUploader = new FileUploader({
            url: HOST + '/api/file/'
        });

        imageUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        imageUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.summon.imageUrl = response;
            updateSummon($scope.summon);
        };
        imageUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $http.get(HOST + '/api/area/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.areas = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/summonclass/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.summonClasses = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            imageUploader.uploadAll();
        };

        $scope.addMap = function () {
            var map = {
                id: 1
            };
            $scope.summon.areas.push(map);
        };

        $scope.removeMap = function (mapId) {
            var areas = $scope.summon.areas;

            for (var i = 0; i < areas.length; i++) {
                var area = areas[i];

                if (area.id == mapId) {
                    areas.splice(i, 1);
                    return
                }
            }
        };

        function updateSummon(summon) {
            $http.post(HOST + '/api/summon/', summon).
                    success(function (data, status, headers, config) {
                        alert("Thêm linh thú thành công");
                        $state.go("adminSummonDetail", {id: data.data.id});
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        $scope.summonClassChange = function (summonClass) {
            $scope.summon.summonClass = summonClass;
        };
    });
});