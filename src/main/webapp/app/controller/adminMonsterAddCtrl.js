define(["app"], function (app) {
    app.registerCtrl('adminMonsterAddCtrl', function ($scope, $http, $state, FileUploader) {
        $scope.monster = {
            level: 0,
            hp: 0,
            mp: 0,
            speed: 0,
            defense: 0,
            intelligent: 0,
            strength: 0,
            imagePath: "",
            hatchTime: 0,
            appearRate: 0,
            powerRate: 0,
            areas: [{
                    id: 1
                }]
        };

        $http.get(HOST + '/api/area/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.areas = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        var imageUploader = $scope.imageUploader = new FileUploader({
            url: HOST + '/api/file/'
        });
        imageUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        imageUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.monster.imageUrl = response;

            saveMonster($scope.monster);
        };
        imageUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $scope.submit = function () {
            imageUploader.uploadAll();
        };

        $scope.addMap = function () {
            var map = {
                id: 1
            };
            $scope.monster.areas.push(map);
        };

        $scope.removeMap = function (mapId) {
            var areas = $scope.monster.areas;

            for (var i = 0; i < areas.length; i++) {
                var area = areas[i];

                if (area.id == mapId) {
                    areas.splice(i, 1);
                    return
                }
            }
        };

        function saveMonster(monster) {
            $http.post(HOST + '/api/monster/', monster).
                    success(function (data, status, headers, config) {
                        alert("Thêm monster thành công");
                        $state.go("adminMonsterDetail", {id: data.data.id});
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});