define(["app"], function (app) {
    app.registerCtrl('adminAvatarItemAddCtrl', function ($scope, $http, $state, FileUploader) {
        $scope.item = {
            itemClass: {
                id: 1
            },
            characterClass: {
                id: 1
            },
            goldCostSell: 0,
            goldCostBuy: 0,
            crystalCostSell: 0,
            crystalCostBuy: 0,
            endurance: 0,
            hp: 0,
            strength: 0,
            defense: 0,
            intelligent: 0,
            speed: 0,
            magic: 0,
            levelRequire: 0,
            sell: false
        };

        var itemUploader = $scope.itemUploader = new FileUploader({
            url: HOST + '/api/file/'
        });
        itemUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        itemUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.item.imageUrl = response;
            saveItem($scope.item);
        };
        itemUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $http.get(HOST + '/api/itemclass/avatar').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.itemClasses = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/item/avatar').
                success(function (data, status, headers, config) {
                    $scope.items = data.data;
                    $scope.items.unshift({id: -1, name: "Bỏ qua"});
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.selectedItemNeedChange = function () {
            if ($scope.item.id == $scope.selectedItemNeed) {
                toastr.error("Không thể trùng với item gốc");
                $scope.selectedItemNeed = -1;
            }

            if ($scope.selectedItemNeed == -1) {
                $scope.item.itemRecipes = [];
                return;
            }

            $scope.item.itemRecipes = [{
                    itemNeed: {id: $scope.selectedItemNeed}
                }];
        };

        $http.get(HOST + '/api/characterclass').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.characterClasses = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            itemUploader.uploadAll();
        };

        function saveItem(item) {
            $http.post(HOST + '/api/item/', item).
                    success(function (data, status, headers, config) {
                        alert("Thêm item thành công");
                        $state.go("adminAvatarItemDetail", {id: data.data.id});
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});