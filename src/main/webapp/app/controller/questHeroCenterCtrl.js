define(["app"], function (app) {

    app.registerCtrl('questHeroCenterCtrl', function ($scope, $http, $state, $stateParams) {
        $scope.accept = function (questMode) {
            $http.post(HOST + '/api/quest/hero/' + questMode).
                    success(function (data, status, headers, config) {
                        $state.go("questCenter");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.error(data.msg);
                    });
        };
    });
});