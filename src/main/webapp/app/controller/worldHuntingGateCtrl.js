define(["app"], function (app) {
    app.registerCtrl('worldHuntingGateCtrl', function ($scope, $http, $state, $stateParams) {
        var id = $stateParams.id;

        $http.get(HOST + '/api/huntingbattle/current').
                success(function (data, status, headers, config) {
                    if (data.data == null) {
                        $scope.isHunting = false;
                    } else {
                        $scope.isHunting = true;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/area/' + id).
                success(function (data, status, headers, config) {
                    $scope.area = data.data;
                    
                    console.log($scope.area);
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.ok = function (id, feeType) {
            $http.post(HOST + '/api/huntingbattle/init/' + id, feeType).
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.escape = function () {
            $http.post(HOST + '/api/huntingbattle/area/escape').
                    success(function (data, status, headers, config) {
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

    });
});