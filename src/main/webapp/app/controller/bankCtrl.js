define(["app"], function (app) {
    app.registerCtrl('bankCtrl', function ($scope, $http, $state) {

        $scope.pass = "";

        $scope.goBank = function (pass) {
            $http.post(HOST + '/api/character/bankaccess', pass).
                    success(function (data, status, headers, config) {
                        if (data.data == true) {
                            $state.go("bankCenter");
                        } else {
                            toastr.error("Mật khẩu cấp 2 không đúng");
                        }
                    }).
                    error(function (data, status, headers, config) {
                        toastr.error(data.msg);
                    });
        };
    });
});