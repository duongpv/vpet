define(["app"], function (app) {
    app.registerCtrl('areaBattleCtrl', function ($scope, $http, $state, $stateParams) {
        $scope.battleContext = null;
        $scope.monster = null;
        $scope.characterPet = null;
        $scope.rewardItems = null;
        var attacking = false;

        var sock = null;
        initsock();

        $http.get(HOST + '/api/areabattle/current').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                        alert("Trận đấu không tồn tại");
                        $state.go("areaView");
                        return;
                    }

                    refreshView(data.data);
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.normalAttack = function () {
            if (attacking) {
                return;
            }
            attacking = true;
            sock.send('useitem:-1');
        };

        $scope.magicAttack = function () {
            if (attacking) {
                return;
            }
            attacking = true;
            sock.send('useitem:-2');
        };

        $scope.defend = function (itemId) {
            if (attacking) {
                return;
            }
            attacking = true;
            sock.send('useitem:-3');
        };

        $scope.itemUse = function (petEquipment) {
            if (attacking) {
                return;
            }
            attacking = true;
            sock.send('useitem:' + petEquipment.id);
        };

        $scope.huntingBattleEscape = function () {
            $http.post(HOST + '/api/huntingbattle/battle/escape').
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.areaBattleEscape = function () {
            $state.go("areaView");
        };

        $scope.createBattle = function () {
            var battleInfo = {
                petId: $scope.characterPet.id,
                monsterId: $scope.monster.id
            };

            $http.post(HOST + '/api/areabattle/init', battleInfo).
                    success(function (data, status, headers, config) {
                        initsock();
                        refreshView(data.data)
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.floatToInt = function (value) {
            return Math.floor(value);
        };

        $scope.renderTooltip = function (petEquipment) {
            var item = petEquipment.item;
            if (typeof item !== 'undefined') {
                var toolToop = "<strong >" + item.name + "<strong>";
                toolToop += "<br><div align='left' >";
                toolToop += "<div>" + "Loại: " + item.itemClass.name + "</div>";
                toolToop += "<div>" + "Sức mạnh: " + item.strength + "</div>";
                toolToop += "<div>" + "Phòng thủ: " + item.defense + "</div>";
                toolToop += "<div>" + "Thông minh: " + item.intelligent + "</div>";
                toolToop += "<div>" + "Tốc độ: " + item.speed + "</div>";
                toolToop += "<div>" + "Ma thuật: " + item.magic + "</div>";

                if (item.endurance >= 0) {
                    toolToop += "<div>" + "Độ bền: " + petEquipment.endurance + "/" + item.endurance + "</div>";
                } else {
                    toolToop += "<div>" + "Độ bền: Vô hạn</div>";
                }
                toolToop += "</div>";

                return toolToop;
            }
            return "";
        };

        $scope.renderTooltipSummon = function (petSummon) {
            var summon = petSummon.summon;
            if (typeof summon != 'undefined') {
                switch (summon.summonClass.id) {
                    case 1:
                        return renderPointSummon(summon);
                    case 2:
                        return renderRegenSummon(summon);
                    case 3:
                        return renderExpSummon(summon);
                    case 4:
                        return renderLuckSummon(summon);
                }
            }

            return "";
        };

        function renderPointSummon(summon) {
            var toolToop = renderBeginSummon(summon);

            if (summon.increasingDefense) {
                toolToop += "<div>" + "Tăng phòng thủ: " + summon.magic + "</div>";
            }
            if (summon.increasingHp) {
                toolToop += "<div>" + "Tăng phòng máu: " + summon.magic + "</div>";
            }
            if (summon.increasingIntelligent) {
                toolToop += "<div>" + "Tăng thông minh: " + summon.magic + "</div>";
            }
            if (summon.increasingSpeed) {
                toolToop += "<div>" + "Tăng tốc độ: " + summon.magic + "</div>";
            }
            if (summon.increasingStrength) {
                toolToop += "<div>" + "Tăng sức mạnh: " + summon.magic + "</div>";
            }

            return toolToop + renderEndSummon(summon);
        }

        function renderRegenSummon(summon) {
            var toolToop = renderBeginSummon(summon);

            toolToop += "<div>" + "Tỉ lệ xuất hiện: " + summon.magic + "%</div>";
            toolToop += "<div>" + "Máu hồi: " + summon.hpRegen + "%</div>";

            return toolToop + renderEndSummon(summon);
        }

        function renderExpSummon(summon) {
            var toolToop = renderBeginSummon(summon);

            toolToop += "<div>" + "Tăng : " + summon.magic + "% exp nhận được</div>";

            return toolToop + renderEndSummon(summon);
        }

        function renderLuckSummon(summon) {
            var toolToop = renderBeginSummon(summon);

            toolToop += "<div>" + "Tăng may mắn: " + summon.magic + "</div>";

            return toolToop + renderEndSummon(summon);
        }

        function renderBeginSummon(summon) {
            var toolToop = "<strong>" + summon.name + "<strong>";
            toolToop += "<div>" + "Loại: " + summon.summonClass.name + "</div>";

            return toolToop;
        }

        function renderEndSummon(summon) {
            return "</div>";
        }

        function initsock() {
            sock = new SockJS(HOST + '/ws');
            sock.onmessage = function(e) {
                data = angular.fromJson(e.data);
                refreshView(data.data);
                attacking = false;
            };

            sock.onclose = function(e) {
                initsock();
                console.log('reconnect..')
            };
        }

        function refreshView(data) {
            $scope.battleContext = data;
            $scope.monster = $scope.battleContext.monster;
            $scope.characterPet = $scope.battleContext.characterPet;
            $scope.rewardItems = $scope.battleContext.rewardItems;

            $scope.$apply();
            // console.log(data);
        }
    });
});