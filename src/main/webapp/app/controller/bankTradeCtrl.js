define(["app"], function (app) {
    app.registerCtrl('bankTradeCtrl', function ($scope, $http, $state) {

        $scope.goldSend = 0;
        $scope.goldFriendId = 0;

        $scope.crystalSend = 0;
        $scope.crystalFriendId = 0;

        $scope.friends = [];

        $http.get(HOST + '/api/character/friends').
                success(function (data, status, headers, config) {
                    $scope.friends = data.data;
                }).
                error(function (data, status, headers, config) {
                    toastr.error(data.msg);
                });

        $scope.sendGold = function (gold, friendId) {
            var traceInfo = {
                quality: gold,
                reveiver: friendId
            };

            $http.post(HOST + '/api/character/sendgold', traceInfo).
                    success(function (data, status, headers, config) {
                        toastr.success("Gửi thành công");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.error(data.msg);
                    });
        };

        $scope.sendCrystal = function (crystal, friendId) {
            var traceInfo = {
                quality: crystal,
                reveiver: friendId
            };

            $http.post(HOST + '/api/character/sendcrystal', traceInfo).
                    success(function (data, status, headers, config) {
                        toastr.success("Gửi thành công");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.error(data.msg);
                    });
        };
    });
});