define(["app"], function (app) {
    app.registerCtrl('friendRequestListCtrl', function ($scope, $http, $state) {

        updateFriendRequestList();

        $scope.accept = function (id) {
            $http.post(HOST + '/api/character/friendaccept/' + id).
                    success(function (data, status, headers, config) {
                        toastr.success("Kết bạn thành công");

                        updateFriendRequestList();
                    }).
                    error(function (data, status, headers, config) {
                        if (status == 500) {
                            toastr.error("Có lỗi từ server");
                        } else {
                            toastr.error(data.msg);
                        }
                    });
        };

        $scope.deny = function (id) {
            $http.post(HOST + '/api/character/frienddeny/' + id).
                    success(function (data, status, headers, config) {
                        toastr.success("Xóa thành công");

                        updateFriendRequestList();
                    }).
                    error(function (data, status, headers, config) {
                        if (status == 500) {
                            toastr.error("Có lỗi từ server");
                        } else {
                            toastr.error(data.msg);
                        }
                    });
        };

        function updateFriendRequestList() {
            $http.get(HOST + '/api/character/friendrequests').
                    success(function (data, status, headers, config) {
                        $scope.friends = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});