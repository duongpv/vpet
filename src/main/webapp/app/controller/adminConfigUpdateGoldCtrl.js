define(["app"], function (app) {
    app.registerCtrl('adminConfigUpdateGoldCtrl', function ($scope, $http, $state, $stateParams) {

        $http.get(HOST + '/api/config/itemupdategoldcost/').
                success(function (data, status, headers, config) {
                    $scope.config = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            $http.post(HOST + '/api/config/itemupdategoldcost/', $scope.config).
                    success(function (data, status, headers, config) {
                        toastr.success("Cập nhật thành công");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };
    });
});