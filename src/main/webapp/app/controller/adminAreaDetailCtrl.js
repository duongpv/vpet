define(["app"], function (app) {
    app.registerCtrl('adminAreaDetailCtrl', function ($scope, $http, $state, $stateParams) {
        var id = $stateParams.id;

        $scope.mapElements = [];
        $scope.area = {};
        $scope.rowIdx = $scope.colIdx = 0;

        $http.get(HOST + '/api/area/' + id).
                success(function (data, status, headers, config) {
                    $scope.area = data.data;

                    $scope.mapElements = JSON.parse($scope.area.mapElements);
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.addRow = function () {
            var elements = [];

            if ($scope.mapElements.length !== 0) {
                for (var i = 0; i < $scope.mapElements[0].length; i++) {
                    var element = {
                        id: makeID(),
                        barrier: false
                    };
                    elements.push(element);
                }
            } else {
                elements = [{
                        id: makeID(),
                        barrier: false
                    }];
            }

            $scope.mapElements.splice($scope.rowIdx, 0, elements);

            console.log($scope.mapElements);
        };

        $scope.addCol = function () {
            for (var i = 0; i < $scope.mapElements.length; i++) {
                var element = {
                    id: makeID(),
                    barrier: false
                };

                $scope.mapElements[i].splice($scope.colIdx, 0, element);
            }

            console.log($scope.mapElements);
        };

        $scope.deleteCol = function () {
            for (var i = 0; i < $scope.mapElements.length; i++) {
                $scope.mapElements[i].splice($scope.colIdx, 1);
            }
            console.log($scope.mapElements);
        };

        $scope.deleteRow = function () {
            $scope.mapElements.splice($scope.rowIdx, 1);

            console.log($scope.mapElements);
        };

        $scope.submit = function () {
            $scope.area.mapElements = JSON.stringify($scope.mapElements);

            $http.put(HOST + '/api/area/', $scope.area).
                    success(function (data, status, headers, config) {
                        alert("Cập nhật bản đồ thành công");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function makeID()
        {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }

    });
});