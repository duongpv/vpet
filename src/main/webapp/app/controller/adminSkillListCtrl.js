define(["app"], function (app) {
    app.registerCtrl('adminSkillListCtrl', function ($scope, $http, $state) {
        $scope.skillList = [];

        $http.get(HOST + '/api/skill/').
                success(function (data, status, headers, config) {
                    $scope.skillList = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

    });
});