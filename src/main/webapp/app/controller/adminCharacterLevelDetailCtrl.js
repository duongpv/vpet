define(["app"], function (app) {
    app.registerCtrl('adminCharacterLevelDetailCtrl', function ($scope, $http, $state, $stateParams) {
        var id = $stateParams.id;


        $http.get(HOST + '/api/characterlevel/' + id).
                success(function (data, status, headers, config) {
                    $scope.characterLevel = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });




        $scope.submit = function () {
            $http.put(HOST + '/api/characterlevel/', $scope.characterLevel).
                    success(function (data, status, headers, config) {
                        alert("Cập nhật thành công");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }


    });
});