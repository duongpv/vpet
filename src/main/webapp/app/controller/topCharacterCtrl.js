define(["app"], function (app) {
    app.registerCtrl('topCharacterCtrl', function ($scope, $http, $state, $stateParams) {
        $scope.characterList = {};

        $scope.topType = "char";
        var type = $stateParams.topType;
        if (type === null || type == "") {
            topGold();
        }

        switch (type) {
            case "gold":
                $scope.topType = "char";
                topGold();
                break;
            case "crystal":
                $scope.topType = "char";
                topCrystal();
                break;
            case "exp":
                topExp();
                $scope.topType = "char";
                break;
            case "pet":
                $scope.topType = "pet";
                topPet();
                break;
        }

        function topGold() {
            $http.get(HOST + '/api/character/topgold').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.characterList = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
        }

        function topCrystal() {
            $http.get(HOST + '/api/character/topcrystal').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.characterList = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
        }

        function topExp() {
            $http.get(HOST + '/api/character/topexp').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.characterList = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
        }

        function topPet() {
            $http.get(HOST + '/api/character/pet/top').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.characterPets = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
        }
    });
});