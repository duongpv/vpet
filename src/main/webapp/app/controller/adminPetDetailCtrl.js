define(["app"], function (app) {
    app.registerCtrl('adminPetDetailCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        var id = $stateParams.id;

        $scope.petClasses = {};
        $scope.pet = {
            defaultLevel: 1,
            requireLevel: 1,
            hp: 0,
            mp: 0,
            speed: 0,
            defense: 0,
            intelligent: 0,
            strength: 0,
            imagePath: "",
            catched: true,
            evolved: true,
            hatchTime: 0,
            appearRate: 0,
            catchedRate: 0,
            areas: [{
                    id: 1
                }],
            mutantAttribute: "Strength",
        };
        $scope.petClassId = 1;

        $http.get(HOST + '/api/petclass/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.petClasses = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/area/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.areas = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/pet/' + id).
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.pet = data.data;
                        $scope.petClassId = $scope.pet.petClass.id;
                        $scope.imageAvatarSource = IMAGES_HOST + $scope.pet.imageAvatarUrl;
                        $scope.imageEggSource = IMAGES_HOST + $scope.pet.imageEggUrl;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        var petAvatarUploader = $scope.petAvatarUploader = new FileUploader({
            url: HOST + '/api/file/'
        });
        petAvatarUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageAvatarSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        petAvatarUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.pet.imageAvatarUrl = response;

            if (petEggUploader.queue.length === 0) {
                updatePet($scope.pet);
            } else {
                petEggUploader.uploadAll();
            }
        };
        petAvatarUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        var petEggUploader = $scope.petEggUploader = new FileUploader({
            url: HOST + '/api/file/'
        });
        petEggUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageEggSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        petEggUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.pet.imageEggUrl = response;
            updatePet($scope.pet);
        };
        petEggUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $scope.submit = function () {
            if (petAvatarUploader.queue.length === 0) {
                if (petEggUploader.queue.length === 0) {
                    updatePet($scope.pet);
                } else {
                    if (petEggUploader.queue.length === 0) {
                        updatePet($scope.pet);
                    } else {
                        petEggUploader.uploadAll();
                    }
                }
            } else {
                petAvatarUploader.uploadAll();
            }
        };

        $scope.petClassChange = function (petClass) {
            $scope.pet.petClass = petClass;
        };

        $scope.addMap = function () {
            var map = {
                id: 1
            };
            $scope.pet.areas.push(map);
        };

        $scope.removeMap = function (mapId) {
            var areas = $scope.pet.areas;

            for (var i = 0; i < areas.length; i++) {
                var area = areas[i];

                if (area.id == mapId) {
                    areas.splice(i, 1);
                    return
                }
            }
        };

        function updatePet(pet) {
            $http.put(HOST + '/api/pet/', pet).
                    success(function (data, status, headers, config) {
                        alert("Chỉnh sửa pet thành công");
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});