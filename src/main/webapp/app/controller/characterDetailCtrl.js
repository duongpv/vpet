define(["app"], function (app) {
    app.registerCtrl('characterDetailCtrl', function ($scope, $http, $state) {
        $scope.character = {};

        $http.get(HOST + '/api/character/info').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.character = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
    });
});