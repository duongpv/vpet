define(["app"], function (app) {
    app.registerCtrl('friendFindCtrl', function ($scope, $http, $state) {

        $scope.searchText = "";

        $scope.search = function (searchText) {
            $state.go("friendFindResult", {searchText: searchText});
        };
    });
});