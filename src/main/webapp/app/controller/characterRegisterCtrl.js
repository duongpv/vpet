define(["app"], function (app) {
    var characterClassIndex = 0;
    var petIndex = 0;
    app.registerCtrl('characterRegisterCtrl', function ($scope, $http, $state) {
        $scope.characterClass = [];
        $scope.petList = [];
        $scope.character = {
            sex: 'Male'
        };
        $scope.pet = {
        };

        $http.get(HOST + '/api/characterclass').
                success(function (data, status, headers, config) {
                    $scope.characterClass = data.data;
                    refeshAvatarInfo();
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/pet').
                success(function (data, status, headers, config) {
                    var pets = [];
                    for (var i = 0; i < data.data.length; i++)
                    {
                        if (data.data[i].petClass.id === 4) {
                            pets.push(data.data[i]);
                        }
                    }
                    $scope.petList = pets;
                    refeshPetInfo();
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.setSex = function (sex) {
            $scope.character.sex = sex;
        };

        $scope.prevAvatar = function () {
            if (characterClassIndex > 0) {
                characterClassIndex--;
                refeshAvatarInfo();
            }
        };

        $scope.nextAvatar = function () {
            if (characterClassIndex < $scope.characterClass.length - 1) {
                characterClassIndex++;
                refeshAvatarInfo();
            }
        };

        function refeshAvatarInfo() {
            $scope.imageAvatarSource = $scope.characterClass[characterClassIndex].imageUrl;
            $scope.avatarDescription = $scope.characterClass[characterClassIndex].name + ": " + $scope.characterClass[characterClassIndex].description;
        }

        $scope.prevPet = function () {
            if (petIndex > 0) {
                petIndex--;
                refeshPetInfo();
            }
        };

        $scope.nextPet = function () {
            if (petIndex < $scope.petList.length - 1) {
                petIndex++;
                refeshPetInfo();
            }
        };

        function refeshPetInfo() {
            $scope.imagePetSource = IMAGES_HOST + $scope.petList[petIndex].imageAvatarUrl;
            $scope.petDescription = $scope.petList[petIndex].description;
        }

        $scope.doCreate = function () {
            $scope.character.characterClass = {
                id: $scope.characterClass[characterClassIndex].id
            };

            $scope.pet.id = $scope.petList[petIndex].id;

            var characterRegisterInfo = {
                characters: $scope.character,
                pet: $scope.pet
            };

            console.log(characterRegisterInfo);

            registerCharacter(characterRegisterInfo);
        };

        function registerCharacter(characterRegisterInfo) {
            $http.post(HOST + '/api/character', characterRegisterInfo).
                    success(function (data, status, headers, config) {
                        alert("Tạo nhân vật thành công");
                        $state.go("panel");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});