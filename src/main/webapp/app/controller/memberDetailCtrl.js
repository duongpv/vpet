define(["app"], function (app) {

    app.registerCtrl('memberDetailCtrl', function ($scope, $http, $state, FileUploader) {
        $scope.character = {};

        getCharacterInfo();

        var imageUploader = $scope.imageUploader = new FileUploader({
            url: HOST + '/api/file/'
        });

        $scope.uploadeProfile = function () {
            imageUploader.clearQueue();

            $("#imageUploader").click();
        };
        imageUploader.onErrorItem = function (fileItem, response, status, headers) {
            toastr.error(response);
        };
        imageUploader.onAfterAddingFile = function (fileItem) {
            imageUploader.uploadAll();
        };
        imageUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $http.post(HOST + '/api/character/profileimage', response).
                    success(function (data, status, headers, config) {
                        $scope.character = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.upgrade = function () {
            $http.post(HOST + '/api/character/avatarupgrade').
                    success(function (data, status, headers, config) {
                        toastr.success("Nâng cấp thành công");

                        $state.transitionTo($state.current, null, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        toastr.warning(data.msg);
                    });
        };

        function getCharacterInfo() {
            $http.get(HOST + '/api/character/viewdetail').
                    success(function (data, status, headers, config) {
                        $scope.character = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

    });
});