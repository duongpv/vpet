define(["app"], function (app) {
    app.registerCtrl('adminPetEvolutionDetailCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        var error = false;
        var count = 0;
        var total = 0;

        var id = $stateParams.id;

        $scope.imageUploaders = [];
        $scope.imageSources = [];

        $http.get(HOST + '/api/pet/' + id).
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.pet = data.data;

                        for (var i = 0; i < $scope.pet.evolutions.length; i++) {
                            var uploader = createUploader(i);
                            $scope.imageUploaders.push(uploader);

                            var imageSource = IMAGES_HOST + $scope.pet.evolutions[i].imageUrl;
                            $scope.imageSources.push(imageSource);
                        }
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/item/evolution').
                success(function (data, status, headers, config) {
                    $scope.items = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            error = false;
            count = 0;
            total = 0;

            for (var i = 0; i < $scope.pet.evolutions.length; i++) {
                if ($scope.imageUploaders[i].queue.length !== 0) {
                    total++;
                }
            }

            if (total === 0) {
                updatePet($scope.pet);
                return;
            }

            for (var i = 0; i < $scope.pet.evolutions.length; i++) {
                $scope.imageUploaders[i].uploadAll();
            }
        };

        function updatePet(pet) {
            $http.put(HOST + '/api/pet/', pet).
                    success(function (data, status, headers, config) {
                        alert("Chỉnh sửa pet thành công");
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
            console.log(pet);
        }

        $scope.add = function () {
            var evolution = {
                item: {
                    id: $scope.items[0].id
                },
                requireLevel: 0,
                description: "",
                imageUrl: ""
            };

            $scope.pet.evolutions.push(evolution);

            var uploader = createUploader($scope.pet.evolutions.length - 1);
            $scope.imageUploaders.push(uploader);
        };

        $scope.delete = function (idx) {
            $scope.pet.evolutions.splice(idx, 1);
            $scope.imageUploaders.splice(idx, 1);
            $scope.imageSources.splice(idx, 1);
        };

        function createUploader(idx) {
            var itemUploader = new FileUploader({
                url: HOST + '/api/file/'
            });

            itemUploader.onAfterAddingFile = function (fileItem) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    $scope.imageSources[idx] = event.target.result;
                    $scope.$apply();
                };
                reader.readAsDataURL(fileItem._file);
            };
            itemUploader.onSuccessItem = function (fileItem, response, status, headers) {
                $scope.pet.evolutions[idx].imageUrl = response;
                count++;

                if (count >= total) {
                    if (error) {
                        alert("Can't upload image");
                    } else {
                        updatePet($scope.pet);
                    }
                }
            };
            itemUploader.onErrorItem = function (fileItem, response, status, headers) {
                error = true;
                count++;
            };

            return itemUploader;
        }
    });
});