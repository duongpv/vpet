define(["app"], function (app) {

    app.registerCtrl('questSummonCenterCtrl', function ($scope, $http, $state, $stateParams) {
        $scope.accept = function (questMode) {
            $http.post(HOST + '/api/quest/summon/' + questMode).
                    success(function (data, status, headers, config) {
                        $state.go("questCenter");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.error(data.msg);
                    });
        };
    });
});