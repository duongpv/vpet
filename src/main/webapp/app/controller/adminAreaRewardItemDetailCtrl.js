define(["app"], function (app) {
    app.registerCtrl('adminAreaRewardItemDetailCtrl', function ($scope, $http, $state, $stateParams) {
        var id = $stateParams.id;

        $scope.area = {};
        $scope.imageSources = [];
        $scope.tempItem = 0;
        $scope.tempRate = 0;

        $http.get(HOST + '/api/area/' + id).
                success(function (data, status, headers, config) {
                    $scope.area = data.data;

                    for (var i = 0; i < $scope.area.areaRewardItems.length; i++) {
                        var imageSource = IMAGES_HOST + $scope.area.areaRewardItems[i].item.imageUrl;
                        $scope.imageSources.push(imageSource);
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/item/').
                success(function (data, status, headers, config) {
                    $scope.items = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            $http.put(HOST + '/api/area/', $scope.area).
                    success(function (data, status, headers, config) {
                        alert("Chỉnh sửa bản đồ thành công");
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.add = function () {
            var tempItem = findInList($scope.tempItem.id);
            console.log(tempItem);

            //Create new item for ignore duplicate item error
            var item = {
                id: tempItem.id,
                name: tempItem.name,
                imageUrl: tempItem.imageUrl
            };

            var rewardItem = {
                item: item,
                rate: $scope.tempRate
            };

            $scope.area.areaRewardItems.push(rewardItem);

            var imageSource = IMAGES_HOST + item.imageUrl;
            $scope.imageSources.push(imageSource);
        };


        $scope.delete = function (idx) {
            $scope.area.areaRewardItems.splice(idx, 1);
            $scope.imageSources.splice(idx, 1);
        };

        $scope.itemChange = function (itemId) {
            var item = findInList(itemId);
            var imageSource = IMAGES_HOST + item.imageUrl;

            $scope.imageTempSources = imageSource;
        };

        function findInList(id) {
            for (var i = 0; i < $scope.items.length; i++) {
                if ($scope.items[i].id == id) {
                    return $scope.items[i];
                }
            }
        }

    });
});