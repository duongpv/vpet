define(["app"], function (app) {
    app.registerCtrl('worldHuntingSummonCreateCtrl', function ($scope, $http, $state) {
        $scope.name = "";

        $http.get(HOST + '/api/huntingbattle/current').
                success(function (data, status, headers, config) {
                    $scope.summon = data.data.summon;

                    if (data.data.type == 1) {
                        $scope.netType = "Lưới thường";
                    } else if (data.data.type == 2) {
                        $scope.netType = "Lưới điện";
                    } else {
                        $scope.netType = "Lưới ma thuật";
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            var info = {
                summon: {
                    name: $scope.name
                }
            };

            $http.post(HOST + '/api/huntingbattle/summon/create', info).
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };
    });
});
