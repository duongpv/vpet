define(["app"], function (app) {
    app.registerCtrl('worldHuntingPetBattleConfirmCtrl', function ($scope, $http, $state) {

        $http.get(HOST + '/api/huntingbattle/current').
                success(function (data, status, headers, config) {
                    $scope.pet = data.data.pet;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.escape = function () {
            $http.post(HOST + '/api/huntingbattle/battle/escape').
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.ok = function () {
            $http.post(HOST + '/api/huntingbattle/battle/petcreate').
                    success(function (data, status, headers, config) {
                        $state.go("areaBattle");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

    });
});