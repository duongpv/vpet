define(["app"], function (app) {

    app.registerCtrl('petConsignCtrl', function ($scope, $http, $state, $stateParams) {
        var characterPetId = $stateParams.id;

        $http.get(HOST + '/api/pet/' + characterPetId).
                success(function (data, status, headers, config) {
                    $scope.characterPet = data.data;
                }).
                error(function (data, status, headers, config) {
                    toastr.error(data.msg);
                    $state.go("petCenter");
                });

        $http.get(HOST + '/api/monster/').
                success(function (data, status, headers, config) {
                    $scope.monsters = data.data;
                }).
                error(function (data, status, headers, config) {
                    toastr.error(data.msg);
                });

        $scope.consign = function () {
            petConsign(characterPetId, $scope.selectedMonsterId);
        };

        function petConsign(characterPetId, monsterId) {
            $http.post(HOST + '/api/character/consign/' + characterPetId + "/" + monsterId).
                    success(function (data, status, headers, config) {
                        toastr.success("Ủy thác thành công");
                        $state.go("petCenter");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.warning(data.msg);
                    });
        }
    });

});