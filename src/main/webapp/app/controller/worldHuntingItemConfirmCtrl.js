define(["app"], function (app) {
    app.registerCtrl('worldHuntingItemConfirmCtrl', function ($scope, $http, $state) {

        $("#hunting_item_confirm_tbl").bind("keydown", onKeydown);
        
        $http.get(HOST + '/api/huntingbattle/current').
                success(function (data, status, headers, config) {
                    $scope.rewardItem = data.data.rewardItems[0];
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.ok = function () {
            $http.post(HOST + '/api/huntingbattle/battle/item').
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function onKeydown(e) {
            if (e.keyCode != 13) {
                e.preventDefault();
                return;
            }

            $http.post(HOST + '/api/huntingbattle/battle/item').
                    success(function (data, status, headers, config) {
                        $state.go("worldHuntingArea");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});