define(["app"], function (app) {
    app.registerCtrl('friendFindResultCtrl', function ($scope, $http, $state, $stateParams) {

        var searchText = $stateParams.searchText;

        if (searchText === null || searchText == "") {
            $state.go("friendFind");
            return;
        }

        friendfind();

        $scope.addFriend = function (friendId) {
            $http.post(HOST + '/api/character/addfriend/' + friendId).
                    success(function (data, status, headers, config) {
                        toastr.success("Kết bạn thành công");

                        friendfind();
                    }).
                    error(function (data, status, headers, config) {
                        toastr.error(data.msg);
                    });
        };

        function friendfind() {
            $http.get(HOST + '/api/character/friendfind/' + searchText).
                    success(function (data, status, headers, config) {
                        $scope.friends = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

    });
});