define(["app"], function (app) {
    app.registerCtrl('summonCenterCtrl', function ($scope, $http, $state) {
        $scope.summons = [];

        $http.get(HOST + '/api/character/summon').
                success(function (data, status, headers, config) {
                    $scope.characterSummons = data.data;

                    $scope.setItemsData($scope.characterSummons)
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.setItemsData = function (summons) {
            $scope.summons = summons;
            if (summons.length !== 0) {
                $scope.summonRows = getPageData(0, summons);
                var totalPages = Math.ceil($scope.summons.length / ($scope.summonRows.length * 3));
                initPage(totalPages);
            } else {
                $scope.summonRows = [];
            }
            
            console.log($scope.summonRows)
        }

        function initPage(totalPages) {
            $("#pagination-box").html("");
            $("#pagination-box").html('<ul id="pagination" class="pagination-sm"></ul>');
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                first: "Đầu",
                last: "Cuối",
                prev: "",
                next: "",
                visiblePages: 3,
                onPageClick: function (event, page) {
                    page = page - 1;
                    $scope.summonRows = getPageData(page, $scope.summons);
                    $scope.$apply();
                }
            });
        }

        function getPageData(page, data) {
            var pageData = [];

            var maxLen = data.length;
            for (var i = page * 9; i < (page + 1) * 9 && i < maxLen; ) {
                var row = {
                    summons: []
                };

                row.summons.push(data[i]);

                if (i + 1 < maxLen)
                    row.summons.push(data[i + 1]);

                if (i + 2 < maxLen)
                    row.summons.push(data[i + 2]);

                pageData.push(row);

                i = i + 3;
            }

            return pageData;
        }


    });
});