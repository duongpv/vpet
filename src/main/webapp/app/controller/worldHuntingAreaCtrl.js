define(["app"], function (app) {
    app.registerCtrl('worldHuntingAreaCtrl', function ($scope, $http, $state, $stateParams) {
        var isSend = false;
        $scope.mapContext = {}
        $scope.mapElements = null;

//        for (var i = 0; i < 7; i++) {
//            $scope.mapElements.push([]);
//            for (var j = 0; j < 7; j++) {
//                var mapElement = {
//                    move: true
//                };
//                $scope.mapElements[i].push(mapElement);
//            }
//        }

        $("body").unbind("keydown");
        $("body").bind("keydown", onKeydown);

        updateMap();

        $scope.moveUp = function () {
            if ($scope.mapContext.step <= 0) {
                toastr.info("Bạn đã đi hết 100 bước");
                return;
            }

            var pos = $scope.mapContext.currentPos.row - 1;
            if (pos >= 0) {
                if (!validStep(pos, $scope.mapContext.currentPos.col)) {
                    return;
                }

                $scope.mapContext.currentPos.row--;
                sendMoveToServer("up");
            }
        };

        $scope.moveDown = function () {
            if ($scope.mapContext.step <= 0) {
                toastr.info("Bạn đã đi hết 100 bước");
                return;
            }

            var pos = $scope.mapContext.currentPos.row + 1;
            if (pos < $scope.mapElements.length) {
                if (!validStep(pos, $scope.mapContext.currentPos.col)) {
                    return;
                }

                $scope.mapContext.currentPos.row++;
                sendMoveToServer("down");
            }
        };

        $scope.moveRight = function () {
            if ($scope.mapContext.step <= 0) {
                toastr.info("Bạn đã đi hết 100 bước");
                return;
            }

            var pos = $scope.mapContext.currentPos.col + 1;
            if (pos < $scope.mapElements[$scope.mapContext.currentPos.row].length) {
                if (!validStep($scope.mapContext.currentPos.row, pos)) {
                    return;
                }

                $scope.mapContext.currentPos.col++;
                sendMoveToServer("right");
            }
        };

        $scope.moveLeft = function () {
            if ($scope.mapContext.step <= 0) {
                toastr.info("Bạn đã đi hết 100 bước");
                return;
            }

            var pos = $scope.mapContext.currentPos.col - 1;
            if (pos >= 0) {
                if (!validStep($scope.mapContext.currentPos.row, pos)) {
                    return;
                }

                $scope.mapContext.currentPos.col--;
                sendMoveToServer("left");
            }
        };

        function updatePosition(newRow, newCol) {
            $(".hunting-box").find("div").removeClass("user-position");

            id = "r" + newRow + "c" + newCol;
            $("#" + id).addClass("user-position");
        }

        function validStep(row, col) {
            return !$scope.mapElements[row][col].barrier;
        }

        function sendMoveToServer(direction) {
            isSend = true;
            $http.post(HOST + '/api/huntingbattle/direction/', direction).
                    success(function (data, status, headers, config) {
                        if (data.data === null) {
                            $state.go("world");
                            return;
                        }
        
                        $scope.mapContext = data.data;
        
                        if ($scope.mapElements == null) {
                            getMap($scope.mapContext.mapID);
                        }
        
                        var currentPos = $scope.mapContext.currentPos;
                        updatePosition(currentPos.row, currentPos.col);

                        if ($scope.mapContext.step <= 0) {
                            if ($scope.mapContext.exp > 0) {
                                toastr.info("Đi săn kết thúc. Bạn nhận được " + $scope.mapContext.exp + " điểm kinh nghiệm.");
                            }
                            else {
                                toastr.info("Đi săn kết thúc.");
                            }    
                            
                            $scope.escape();
                        }
        
                        if ($scope.mapContext.monster !== null) {
                            $state.go("worldHuntingMonsterConfirm");
                        }
                        else if ($scope.mapContext.pet !== null) {
                            if ($scope.mapContext.legend == true) {
                                $state.go("worldHuntingPetBattleConfirm");
                            } else {
                                $state.go("worldHuntingPetConfirm");
                            }
                        }
                        else if ($scope.mapContext.summon !== null) {
                            $state.go("worldHuntingSummonConfirm");
                        }
                        else if ($scope.mapContext.rewardItems.length > 0) {
                            $state.go("worldHuntingItemConfirm");
                        }
                        else if ($scope.mapContext.dropNet >= 0) {
                            var name = "";
                            switch ($scope.mapContext.dropNet) {
                                case 0:
                                    name = "Lưới thường";
                                    break;
                                case 1:
                                    name = "Lưới điện";
                                    break;
                                case 2:
                                    name = "Lưới phép";
                                    break;
                            }
                            toastr.info("Bạn nhặt được " + name + " trong lúc săn");
                        }

                        isSend = false;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                        isSend = false;
                    });
        }

        function updateMap() {
            $http.get(HOST + '/api/huntingbattle/current').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                        $state.go("world");
                        return;
                    }

                    $scope.mapContext = data.data;

                    if ($scope.mapElements == null) {
                        getMap($scope.mapContext.mapID);
                    }

                    var currentPos = $scope.mapContext.currentPos;
                    updatePosition(currentPos.row, currentPos.col);

                    if ($scope.mapContext.step <= 0) {
                            toastr.info("Hết bước đi");
                            $scope.escape();
                    }

                    if ($scope.mapContext.monster !== null) {
                        $state.go("worldHuntingMonsterConfirm");
                    }
                    else if ($scope.mapContext.pet !== null) {
                        if ($scope.mapContext.legend == true) {
                            $state.go("worldHuntingPetBattleConfirm");
                        } else {
                            $state.go("worldHuntingPetConfirm");
                        }
                    }
                    else if ($scope.mapContext.summon !== null) {
                        $state.go("worldHuntingSummonConfirm");
                    }
                    else if ($scope.mapContext.rewardItems.length > 0) {
                        $state.go("worldHuntingItemConfirm");
                    }
                    else if ($scope.mapContext.dropNet >= 0) {
                        $state.go("worldHuntingNetConfirm");
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
        }

        function getMap(mapId) {
            $http.get(HOST + '/api/area/' + mapId).
                success(function (data, status, headers, config) {
                    $scope.mapElements = JSON.parse(data.data.mapElements);
                    $(".hunting-able").css("background-image", "url('../" + data.data.imageMapUrl + "')");
                    setTimeout(function () {
                        var currentPos = $scope.mapContext.currentPos;
                        updatePosition(currentPos.row, currentPos.col);
                    }, 500);
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
        }

        $scope.escape = function () {
            $http.post(HOST + '/api/huntingbattle/area/escape').
                success(function (data, status, headers, config) {
                    $state.go("world");
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
        };

        $scope.createBattle = function () {
            $http.post(HOST + '/api/huntingbattle/battle/create').
                success(function (data, status, headers, config) {
                    $state.go("areaBattle");
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
        };

        function onKeydown(e) {
            if (location.href.indexOf("#/world/hunting/area/") < 0) {
                return;
            }
            if (isSend) {
                e.preventDefault();
                return;
            }

            if (e.keyCode === 37) {
                $scope.moveLeft();
            }
            else if (e.keyCode === 39) {
                $scope.moveRight();
            } 
            else if (e.keyCode === 38) {
                $scope.moveUp();
            }
            else if (e.keyCode === 40) {
                $scope.moveDown();
            }

            if (e.keyCode > 36 && e.keyCode < 41) {
                e.preventDefault();
            }
        }
    });
});