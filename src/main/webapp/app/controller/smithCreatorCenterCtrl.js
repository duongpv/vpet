define(["app"], function (app) {
    app.registerCtrl('smithCreatorCenterCtrl', function ($scope, $http, $state) {

        $http.get(HOST + '/api/item/repices').
                success(function (data, status, headers, config) {
                    $scope.items = data.data;

                    var totalPages = Math.ceil($scope.items.length / 3);

                    $scope.rows = getPageData(0, $scope.items);

                    initPage(totalPages);
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.create = function (itemId) {
            $http.post(HOST + '/api/character/createitem/' + itemId).
                    success(function (data, status, headers, config) {
                        toastr.success("Chế tạo thành công");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.info(data.msg);
                    });
        };

        function getPageData(page, data) {
            var pageData = [];

            var maxLen = data.length;
            for (var i = page * 3; i < page * 3 + 3 && i < maxLen; i++) {
                pageData.push(data[i]);
            }

            return pageData;
        }

        function initPage(totalPages) {
            $("#pagination-box").html("");
            $("#pagination-box").html('<ul id="pagination" class="pagination-sm"></ul>');
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                first: "Đầu",
                last: "Cuối",
                prev: "",
                next: "",
                visiblePages: 3,
                onPageClick: function (event, page) {
                    page = page - 1;
                    $scope.nowPage = page;
                    $scope.rows = getPageData(page, $scope.items);
                    $scope.$apply();
                }
            });
        }
    });
});