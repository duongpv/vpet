define(["app"], function (app) {
    app.registerCtrl('adminPetEvolutionListCtrl', function ($scope, $http, $state) {
        $scope.petList = {};

        $http.get(HOST + '/api/pet/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.petList = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
    });
});