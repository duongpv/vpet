define(["app"], function (app) {
    app.registerCtrl('adminMonsterRewardItemListCtrl', function ($scope, $http, $state) {
        $scope.monsters = {};

        $http.get(HOST + '/api/monster/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.monsters = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });
    });
});