define(["app"], function (app) {
    app.registerCtrl('adminSkillDetailCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        var id = $stateParams.id;

        $http.get(HOST + '/api/skillclass/').
                success(function (data, status, headers, config) {
                    $scope.skillClasses = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/area/').
                success(function (data, status, headers, config) {
                    $scope.areas = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/skill/' + id).
                success(function (data, status, headers, config) {
                    $scope.skill = data.data;

                    $scope.imageSource = IMAGES_HOST + $scope.skill.imageUrl;

                    /**
                     * fix not select skill class item
                     */
                    $scope.skillClass = {
                        id: $scope.skill.skillClass.id
                    };
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.addMap = function () {
            var map = {
                id: 1
            };
            $scope.skill.areas.push(map);
        };

        $scope.removeMap = function (mapId) {
            var areas = $scope.skill.areas;

            for (var i = 0; i < areas.length; i++) {
                var area = areas[i];

                if (area.id == mapId) {
                    areas.splice(i, 1);
                    return;
                }
            }
        };

        var imageUploader = $scope.imageUploader = new FileUploader({
            url: HOST + '/api/file/'
        });
        imageUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        imageUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.skill.imageUrl = response;

            save($scope.skill);
        };
        imageUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $scope.submit = function () {
            if (imageUploader.queue.length != 0) {
                imageUploader.uploadAll();
            } else {
                save($scope.skill);
            }
        };

        function save(skill) {
            $http.put(HOST + '/api/skill/', skill).
                    success(function (data, status, headers, config) {
                        alert("Update skill thành công");
                        $state.go("adminSkillDetail", {id: data.data.id});
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

    });
});