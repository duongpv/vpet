define(["app"], function (app) {
    app.registerCtrl('healiaRiverCtrl', function ($scope, $http, $state) {
        $scope.goldCost = 0;

        $http.get(HOST + '/api/character/recoverpet').
                success(function (data, status, headers, config) {
                    $scope.goldCost = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.revoverPet = function () {
            $http.post(HOST + '/api/character/recoverpet').
                    success(function (data, status, headers, config) {
                        toastr.success("Đã hồi đầy máu cho tất cả thú cưng");
                    }).
                    error(function (data, status, headers, config) {
                        toastr.info(data.msg);
                    });
        };
    });
});