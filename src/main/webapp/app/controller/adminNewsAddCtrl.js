define(["app"], function (app) {
    app.registerCtrl('adminNewsAddCtrl', function ($scope, $http, $state) {
        $scope.news = {};

        $scope.submit = function () {
            addItem($scope.news);
        };

        function addItem(news) {
            $http.post(HOST + '/api/news/', news).
                    success(function (data, status, headers, config) {
                        alert("Thêm thành công");
                        $state.go("adminNewsDetail", {id: data.data.id});
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});