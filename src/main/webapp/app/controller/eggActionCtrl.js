define(["app"], function (app) {
    app.registerCtrl('eggActionCtrl', function ($scope, $http, $state, $stateParams) {
        var characterPetId = $stateParams.id;

        $http.get(HOST + '/api/character/pet/' + characterPetId).
                success(function (data, status, headers, config) {
                    $scope.characterPet = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.action = function (actionType) {
            switch (actionType) {
                case "1":
                    toastr.info("Chức năng chưa mở");
                    break;
                case "2":
                    sellEgg(characterPetId);
                    break;
            }
        };

        function sellEgg(characterPetId) {
            $http.post(HOST + '/api/character/charactereggsell', characterPetId).
                    success(function (data, status, headers, config) {
                        var canSell = data.data;

                        if (canSell)
                        {
                            toastr.success("Bán trứng thành công");
                            $state.go("itemCenter");
                        }
                        else
                        {
                            toastr.info("Không thể bán trứng");
                        }
                    }).
                    error(function (data, status, headers, config) {
                        toastr.error(data.msg);
                    });
        }

    });
});