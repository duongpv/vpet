define(["app"], function (app) {
    app.registerCtrl('adminCharacterDetailCtrl', function ($scope, $http, $state, $stateParams) {
        var id = $stateParams.id;

        $scope.itemImageSources = [];
        $scope.petImageSources = [];
        $scope.summonImageSources = [];
        $scope.petName = "";

        $scope.gold = 0;
        $scope.crystal = 0;

        $scope.character = {};
        $scope.items = {};

        $http.get(HOST + '/api/admincharacter/detail/' + id).
                success(function (data, status, headers, config) {
                    $scope.character = data.data;
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/item/').
                success(function (data, status, headers, config) {
                    $scope.items = data.data;

                    for (var i = 0; i < $scope.items.length; i++) {
                        var imageSource = IMAGES_HOST + $scope.items[i].imageUrl;
                        $scope.itemImageSources.push(imageSource);
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/pet/').
                success(function (data, status, headers, config) {
                    $scope.pets = data.data;
                    for (var i = 0; i < $scope.pets.length; i++) {
                        var imageSource = IMAGES_HOST + $scope.pets[i].imageAvatarUrl;
                        $scope.petImageSources.push(imageSource);
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/summon/').
                success(function (data, status, headers, config) {
                    $scope.summons = data.data;
                    for (var i = 0; i < $scope.summons.length; i++) {
                        var imageSource = IMAGES_HOST + $scope.summons[i].imageAvatarUrl;
                        $scope.summonImageSources.push(imageSource);
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });


        $scope.petChange = function (petId) {
            var pet = findInPetList(petId);
            var imageSource = IMAGES_HOST + pet.imageAvatarUrl;

            $scope.petTempSources = imageSource;
        };

        $scope.itemChange = function (itemId) {
            var item = findInItemList(itemId);
            var imageSource = IMAGES_HOST + item.imageUrl;

            $scope.imageTempSources = imageSource;
        };

        $scope.summonChange = function (summonId) {
            var summon = findInSummonList(summonId);
            var imageSource = IMAGES_HOST + summon.imageUrl;

            $scope.summonTempSources = imageSource;
        };

        $scope.addGold = function (gold) {
            $http.post(HOST + '/api/admincharacter/addgold/' + id, gold).
                    success(function (data, status, headers, config) {
                        alert("Thêm thành công");
                        $scope.character = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.addCrystal = function (crystal) {
            $http.post(HOST + '/api/admincharacter/addcrystal/' + id, crystal).
                    success(function (data, status, headers, config) {
                        alert("Thêm thành công");
                        $scope.character = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.addBcoin = function (bcoin) {
            $http.post(HOST + '/api/admincharacter/addbcoin/' + id, bcoin).
                    success(function (data, status, headers, config) {
                        alert("Thêm thành công");
                        $scope.character = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.addItem = function (itemId) {
            $http.post(HOST + '/api/admincharacter/additem/' + id, itemId).
                    success(function (data, status, headers, config) {
                        alert("Thêm thành công");
                        $scope.character = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.addPet = function (petId, petName) {
            var characterRegisterInfo = {
                pet: {
                    id: petId,
                    name: petName
                }
            };

            $http.post(HOST + '/api/admincharacter/addpet/' + id, characterRegisterInfo).
                    success(function (data, status, headers, config) {
                        alert("Thêm thành công");
                        $scope.character = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.addSummon = function (summonId, summonName) {
            var characterRegisterInfo = {
                summon: {
                    id: summonId,
                    name: summonName
                }
            };

            $http.post(HOST + '/api/admincharacter/addsummon/' + id, characterRegisterInfo).
                    success(function (data, status, headers, config) {
                        alert("Thêm thành công");
                        $scope.character = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.block = function (id) {
            $http.post(HOST + '/api/admincharacter/block/' + id).
                    success(function (data, status, headers, config) {
                        alert("Block thành công");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        $scope.unblock = function (id) {
            $http.post(HOST + '/api/admincharacter/unblock/' + id).
                    success(function (data, status, headers, config) {
                        alert("Unblock thành công");
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        };

        function findInItemList(id) {
            for (var i = 0; i < $scope.items.length; i++) {
                if ($scope.items[i].id == id) {
                    return $scope.items[i];
                }
            }
        }

        function findInPetList(id) {
            for (var i = 0; i < $scope.pets.length; i++) {
                if ($scope.pets[i].id == id) {
                    return $scope.pets[i];
                }
            }
        }

        function findInSummonList(id) {
            for (var i = 0; i < $scope.summons.length; i++) {
                if ($scope.summons[i].id == id) {
                    return $scope.summons[i];
                }
            }
        }
    });
});