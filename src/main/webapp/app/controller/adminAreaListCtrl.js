define(["app"], function (app) {
    app.registerCtrl('adminAreaListCtrl', function ($scope, $http, $state) {

        $http.get(HOST + '/api/area/').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.areas = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });


    });
});