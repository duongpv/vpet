define(["app"], function (app) {
    app.registerCtrl('adminAvatarItemDetailCtrl', function ($scope, $http, $state, $stateParams, FileUploader) {
        var id = $stateParams.id;

        $scope.item = {
            itemClass: {
                id: 1
            },
            characterClass: {
                id: 1
            },
            goldCostSell: 0,
            goldCostBuy: 0,
            crystalCostSell: 0,
            crystalCostBuy: 0,
            endurance: 0,
            hp: 0,
            strength: 0,
            defense: 0,
            intelligent: 0,
            speed: 0,
            magic: 0,
            levelRequire: 0,
            sell: false
        };

        $http.get(HOST + '/api/item/avatar/' + id).
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.item = data.data;
                        $scope.imageSource = IMAGES_HOST + $scope.item.imageUrl;

                        if ($scope.item.itemRecipes.length > 0) {
                            $scope.selectedItemNeed = $scope.item.itemRecipes[0].itemNeed.name;
                        }
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/item/avatar').
                success(function (data, status, headers, config) {
                    $scope.items = data.data;
                    $scope.items.unshift({id: -1, name: "Bỏ qua"});
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.add = function () {
            if ($scope.item.id == $scope.selectedItemNeed) {
                toastr.error("Không thể trùng với item gốc");
                $scope.selectedItemNeed = -1;
            }

            if ($scope.selectedItemNeed == -1) {
                $scope.item.itemRecipes = [];
                return;
            }

            var item = getItem($scope.selectedItemNeed, $scope.items);
            $scope.item.itemRecipes.push({
                itemNeed: {
                    id: item.id,
                    name: item.name
                },
                itemLevel: $scope.selectedItemLevel
            });
        };

        $scope.deleteItemRecipe = function (itemId) {
            var itemRecipes = $scope.item.itemRecipes;
            for (var i = 0; i < itemRecipes.length; i++) {
                var itemRecipe = itemRecipes[i];
                if (itemRecipe.itemNeed.id == itemId) {
                    itemRecipes.splice(i, 1);
                }
            }
        };

        var itemUploader = $scope.itemUploader = new FileUploader({
            url: HOST + '/api/file/'
        });

        itemUploader.onAfterAddingFile = function (fileItem) {
            var reader = new FileReader();
            reader.onload = function (event) {
                $scope.imageSource = event.target.result;
                $scope.$apply();
            };
            reader.readAsDataURL(fileItem._file);
        };
        itemUploader.onSuccessItem = function (fileItem, response, status, headers) {
            $scope.item.imageUrl = response;
            updateItem($scope.item);
        };
        itemUploader.onErrorItem = function (fileItem, response, status, headers) {
            alert("Can't upload image");
        };

        $http.get(HOST + '/api/itemclass/avatar').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.itemClasses = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $http.get(HOST + '/api/characterclass').
                success(function (data, status, headers, config) {
                    if (data.data === null) {
                    } else {
                        $scope.characterClasses = data.data;
                    }
                }).
                error(function (data, status, headers, config) {
                    alert(data.msg);
                });

        $scope.submit = function () {
            if (itemUploader.queue.length === 0) {
                updateItem($scope.item);
            } else {
                itemUploader.uploadAll();
            }
        };

        function updateItem(item) {
            $http.put(HOST + '/api/item/', item).
                    success(function (data, status, headers, config) {
                        alert("Chỉnh sửa item thành công");
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }

        function getItem(itemId, items) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].id == itemId) {
                    return items[i];
                }
            }
            return null;
        }
    });
});