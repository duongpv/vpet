define(["app"], function (app) {

    app.registerCtrl('petCenterCtrl', function ($scope, $http, $state) {
        $scope.characterPets = [];
        $scope.rows = [];
        $scope.nowPage = 0;

        $http.get(HOST + '/api/character/pet').
                success(function (data, status, headers, config) {
                    $scope.characterPets = data.data;

                    updateItemView($scope.characterPets)
                }).
                error(function (data, status, headers, config) {
                    alert(data);
                });

        $scope.dataSort = function (sortBy, orderBy) {
            $scope.characterPets = dataSort($scope.characterPets, sortBy, orderBy);

            $scope.rows = getPageData($scope.nowPage, $scope.characterPets);
        };

        function dataSort(data, sortBy, orderBy) {
            var comparer = null;
            switch (sortBy) {
                case "auto":
                    comparer = createIdComparer(orderBy);
                    break;
                case "name":
                    comparer = createNameComparer(orderBy);
                    break;
                case "level":
                    comparer = createLevelComparer(orderBy);
                    break;
            }

            return data.sort(comparer);
        }

        function createIdComparer(orderBy) {
            if (orderBy === "asc") {
                return  function (a, b) {
                    return a.id - b.id;
                };
            } else {
                return  function (a, b) {
                    return b.id - a.id;
                };
            }
        }

        function createNameComparer(orderBy) {
            if (orderBy === "asc") {
                return  function (a, b) {
                    return a.name.localeCompare(b.name);
                };
            } else {
                return  function (a, b) {
                    return b.name.localeCompare(a.name);
                };
            }
        }

        function createLevelComparer(orderBy) {
            if (orderBy === "asc") {
                return  function (a, b) {
                    return a.level - b.level;
                };
            } else {
                return  function (a, b) {
                    return b.level - a.level;
                };
            }
        }

        function updateItemView(pets) {
            $scope.rows = getPageData(0, pets);

            if ($scope.rows.length !== 0) {
                var totalPages = Math.ceil(pets.length / ($scope.rows.length * 2));
                initPage(totalPages);
            }
        }

        function getPageData(page, data) {
            var pageData = [];

            var maxLen = data.length;
            for (var i = page * 10; i < (page + 1) * 10 && i < maxLen; ) {
                var row = {
                    pets: []
                };

                row.pets.push(data[i]);
                if (i + 1 < maxLen)
                    row.pets.push(data[i + 1]);

                pageData.push(row);

                i = i + 2;
            }

            return pageData;
        }

        function initPage(totalPages) {
            $("#pagination-box").html("");
            $("#pagination-box").html('<ul id="pagination" class="pagination-sm"></ul>');
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                first: "Đầu",
                last: "Cuối",
                prev: "",
                next: "",
                visiblePages: 3,
                onPageClick: function (event, page) {
                    page = page - 1;
                    $scope.nowPage = page;
                    $scope.rows = getPageData(page, $scope.characterPets);
                    $scope.$apply();
                }
            });
        }
    });

});