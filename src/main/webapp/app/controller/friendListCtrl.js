define(["app"], function (app) {
    app.registerCtrl('friendListCtrl', function ($scope, $http, $state) {

        updateFriendList();

        $scope.unfriend = function (id) {
            $http.post(HOST + '/api/character/unfriend/' + id).
                    success(function (data, status, headers, config) {
                        toastr.success("Hủy kết bạn thành công");

                        updateFriendList();
                    }).
                    error(function (data, status, headers, config) {
                        if (status == 500) {
                            toastr.error("Có lỗi từ server");
                        } else {
                            toastr.error(data.msg);
                        }
                    });
        };

        function updateFriendList() {
            $http.get(HOST + '/api/character/friends').
                    success(function (data, status, headers, config) {
                        $scope.friends = data.data;
                    }).
                    error(function (data, status, headers, config) {
                        alert(data.msg);
                    });
        }
    });
});